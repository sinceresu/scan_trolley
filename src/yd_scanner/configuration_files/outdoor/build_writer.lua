-- Copyright 2016 The Cartographer Authors
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

VOXEL_SIZE =5e-2
include "transform.lua"

options = {
    tracking_frame = "base_link",
    pipeline = {
      {
        action = "min_max_range_filter",
        min_range = 0.5,
        max_range = 60.0,
      },
      -- {
      --   action = "min_max_intensity_filter",
      --   min_intensity = 5,
      --   max_intensity = 100.0,
      -- },
      {
        action = "horizontal_range_filter",
        max_range = 100.0,
      },
      {
        action = "fixed_ratio_sampler",
        sampling_ratio = 0.1
      },
      -- {
      --   action = "dump_num_points",
      -- },
      {
        action = "frame_id_filter",
        keep_frames = {"velodyne_h"},
      },
      -- {
      --   action = "voxel_filter_and_remove_moving_objects",
      --   voxel_size = VOXEL_SIZE,
      --   miss_per_hit_limit = 1,
      -- },
      {
        action = "vertical_range_filter",
        min_z = -2.0,
        max_z = 10.0,
      },
      -- {
      --   action = "write_pcd",
      --   filename = ".pcd",
      -- },
      {
        action = "write_ply",
        filename = ".ply",
      },
      -- Gray X-Rays. These only use geometry to color pixels.
      -- {
      --   action = "write_xray_image",
      --   voxel_size = VOXEL_SIZE,
      --   filename = "xray_yz_all",
      --   transform = YZ_TRANSFORM,
      -- },
      {
        action = "write_xray_image",
        voxel_size = VOXEL_SIZE,
        filename = "",
        transform = XY_TRANSFORM,
      },
      -- {
      --   action = "write_ros_map",
      --   range_data_inserter = {
      --     insert_free_space = true,
      --     hit_probability = 0.55,
      --     miss_probability = 0.49,
      --   },
      --   filestem = "_map",
      --   resolution = VOXEL_SIZE,
      -- }
      -- {
      --   action = "write_xray_image",
      --   voxel_size = VOXEL_SIZE,
      --   filename = "xray_xz_all",
      --   transform = XZ_TRANSFORM,
      -- },

      -- Now we recolor our points by frame and write another batch of X-Rays. It
      -- is visible in them what was seen by the horizontal and the vertical
      -- laser.
      -- {
      --   action = "color_points",
      --   frame_id = "velodyne_h",
      --   color = { 255., 0., 0. },
      -- },
      -- {
      --   action = "color_points",
      --   frame_id = "velodyne_v",
      --   color = { 0., 255., 0. },
      -- },

      -- {
      --   action = "write_xray_image",
      --   voxel_size = VOXEL_SIZE,
      --   filename = "xray_yz_all_color",
      --   transform = YZ_TRANSFORM,
      -- },
      -- {
      --   action = "write_xray_image",
      --   voxel_size = VOXEL_SIZE,
      --   filename = "xray_xy_all_color",
      --   transform = XY_TRANSFORM,
      -- },
      -- {
      --   action = "write_xray_image",
      --   voxel_size = VOXEL_SIZE,
      --   filename = "xray_xz_all_color",
      --   transform = XZ_TRANSFORM,
      -- },
    }
}

return options
