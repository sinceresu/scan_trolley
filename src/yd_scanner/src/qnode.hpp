#ifndef yd_scanner_ui_QNODE_HPP_
#define yd_scanner_ui_QNODE_HPP_

// To workaround boost/qt4 problems that won't be bugfixed. Refer to https://bugreports.qt.io/browse/QTBUG-22829
#ifndef Q_MOC_RUN
    #include <ros/ros.h>
    // Msg
    #include <std_msgs/String.h>
    #include <diagnostic_msgs/DiagnosticArray.h>
    #include <nav_msgs/Odometry.h>
    #include <geometry_msgs/TransformStamped.h>

    #include <sensor_msgs/Image.h>
    #include <sensor_msgs/CompressedImage.h>
    #include <sensor_msgs/Imu.h>
    #include <sensor_msgs/NavSatFix.h>
    #include <sensor_msgs/LaserScan.h>
    #include <sensor_msgs/PointCloud2.h>

    //#include <yidamsg/Image.h>
#endif

#include <string>
#include <vector>

//Qt5
#include <QtCore/QThread>
#include <QtCore/QStringListModel>

using namespace std;

namespace yd_scanner_ui {
    class QNode : public QThread {
        Q_OBJECT
            public:
                // Constructor & Destructor
                QNode(int argc, char** argv);
                virtual ~QNode();

                // Fun
                bool init();
                bool init(const std::string &master_url, const std::string &host_url);
                void launchInit();
                void initMessages(ros::NodeHandle& n);

                void run();

                /// 设备连接状态变量
                static bool hasAlarm;
                static string alarmInfo;
                // 激光
                static bool localization_vlp16_ip_pingable;
                static bool scan_vlp16_ip_pingable;
                // GPS & IMU
                static bool gps_port_opened;
                static bool imu_port_opened;
                // Cameras
                static bool left_camera_port_opened;
                static bool right_camera_port_opened;

                /// 设备通信状态变量
                static bool localization_vlp16_have_messages; static bool localization_vlp16_messages_normal;
                static bool scan_vlp16_have_messages;         static bool scan_vlp16_messages_normal;

                static bool gps_have_messages; static bool gps_messages_normal;
                static bool imu_have_messages; static bool imu_messages_normal;

                static bool left_camera_have_messages; static bool left_camera_messages_normal;
                static bool right_camera_have_messages; static bool right_camera_messages_normal;

                /// 消息回调
                // 激光
                void ns1VelodyneCallback(const sensor_msgs::PointCloud2ConstPtr& ptr) {
                    localization_vlp16_have_messages = true;

                    if(ptr->data.size() > 0) localization_vlp16_messages_normal = true;
                    else localization_vlp16_messages_normal = false;
                }
                void ns2VelodyneCallback(const sensor_msgs::PointCloud2ConstPtr& ptr) {
                    scan_vlp16_have_messages = true;

                    if(ptr->data.size() > 0) scan_vlp16_messages_normal = true;
                    else scan_vlp16_messages_normal = false;
                }
                // IMU & GPS
                void gpsDataCallback(const sensor_msgs::NavSatFixConstPtr& ptr) {
                    gps_have_messages = true;

                    if(ptr->header.frame_id.length() > 0) gps_messages_normal = true;
                    else gps_messages_normal = false;
                }
                void imuDataCallback(const sensor_msgs::ImuConstPtr& ptr) {
                    imu_have_messages = true;

                    if(ptr->header.frame_id.length() > 0) imu_messages_normal = true;
                    else imu_messages_normal = false;
                }
                // Cams
                void leftCameraCallback(const sensor_msgs::CompressedImageConstPtr& ptr) {
                    left_camera_have_messages = true;

                    if(ptr->data.size() > 0) left_camera_messages_normal = true;
                    else left_camera_messages_normal = false;
                }
                void rightCameraCallback(const sensor_msgs::CompressedImageConstPtr& ptr) {
                    right_camera_have_messages = true;

                    if(ptr->data.size() > 0) right_camera_messages_normal = true;
                    else right_camera_messages_normal = false;
                }

            private:
                void resetDevicesStatusFlag();
                void checkDevicesStatusFlag();

                string updateDevicesStatueData();

            Q_SIGNALS:
                // 设备状态 回调消息 更新ui 信号
                void uiUpdate(QString qstr);
                void rosShutdown();

            protected:
                int init_argc;
                char** init_argv;

                ros::Subscriber _velodynePoints[2], _camCompressedImage[2], _imu, _gps;
    };
}
#endif
