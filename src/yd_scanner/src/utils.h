#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <vector>

#include <jsoncpp/json/json.h>
#include "boost/date_time/posix_time/posix_time.hpp"

#ifndef Q_MOC_RUN
    #include <sensor_msgs/PointCloud2.h>
    #include <sensor_msgs/Image.h>
    #include <sensor_msgs/CompressedImage.h>
    #include <sensor_msgs/Imu.h>
    #include <sensor_msgs/NavSatFix.h>
#endif

using namespace std;

// 按格式返回系统当前时间戳string
string get_system_cur_time();
// 以某字符分割字符串
vector<string> split(string str, string pattern);
// 以指定字符串替换原字符串中的字符串
string replace(string &str, const string &old_value, const string &new_value);

// base64解、编码
string base64_encode(unsigned char const *, unsigned int len);
string base64_decode(string const &s);

// 解析json
Json::Value readJsonInfo(string path);

// 根据文件路径获取文件名(包含属性.type)
string getFileNameFromPath(string path);

// Vector元素 去重
vector<string> vector_unique_element(vector<string> v);

// 根据消息，拼接log字符串
string getLogInfo(sensor_msgs::PointCloud2 vlpMsg1,
                  sensor_msgs::PointCloud2 vlpMsg2,

                  sensor_msgs::Image flCamMsg,
                  sensor_msgs::Image rlCamMsg,
                  sensor_msgs::Image blCamMsg,
                  sensor_msgs::Image brCamMsg,
                  //sensor_msgs::CompressedImage lCamMsg,
                  //sensor_msgs::CompressedImage rCamMsg,

                  sensor_msgs::Imu imuMsg,
                  sensor_msgs::NavSatFix gpsMsg);
bool copyDir(
    const std::string& src_dir,
    const std::string & dst_dir
);
#endif
