#ifndef NOTIFICATION_MONITOR_HPP
#define NOTIFICATION_MONITOR_HPP

#ifndef WITHOUT_ROS
    #include "qnode.hpp"
#endif

#include <QtCore>
#include <QObject>

#include <iostream>
#include <string>

using namespace std;

namespace yd_scanner_ui {
    class NotificationMonitor: public QObject {
        Q_OBJECT
            public:
                NotificationMonitor();
                ~NotificationMonitor();

                bool hasAlarm = false;

                void setWindowClosedStatus();

            private:
                bool isWindowClosed = false;

            public Q_SLOTS:
                void notify();

            Q_SIGNALS:
                void finished();
    };
}

#endif
