#include "rvizplugin.hpp"

//Qt4
//#include <QtGui/QVBoxLayout>
//Qt5
#include <QtWidgets/QVBoxLayout>
#ifndef Q_MOC_RUN
//rviz
#include <rviz/visualization_manager.h>
#include <rviz/render_panel.h>
#include <rviz/display.h>
#include <rviz/tool_manager.h>
#include <rviz/visualization_manager.h>
#include <rviz/render_panel.h>
#include <rviz/display.h>
#include <rviz/tool.h>
#include <rviz/default_plugin/view_controllers/orbit_view_controller.h>
#include <rviz/view_manager.h>
#endif


using namespace yd_scanner_ui;

 void RvizPlugin::init(rviz::RenderPanel* render_panel_, std::string frame_id, std::string topic_name){
        //创建rviz的容器，并将该容器作为组建加入到ui内，其中关键class为VisualizationManager，是个管理类，起到控制创建rviz图层和设置坐标系的作用。
        // render_panel_= new rviz::RenderPanel();
        // render_panel_->setMinimumSize( QSize(450, 450));

        // ui->addWidget(render_panel_);
        manager_=new rviz::VisualizationManager(render_panel_);
        render_panel_->initialize(manager_->getSceneManager(),manager_);
        manager_->initialize();
        // manager_->startUpdate();
        manager_->setFixedFrame(QString::fromStdString(frame_id));
    //设置整个地图的展示方式，如视角、距离等
        viewManager = manager_->getViewManager();
        viewManager->setRenderPanel(render_panel_);
        //创建grid
        rviz::Display *grid_=manager_->createDisplay( "rviz/Grid", "adjustable grid", true );
        // Configure the GridDisplay the way we like it.
        QColor color=QColor(255, 255, 255);
        grid_->subProp( "Line Style" )->setValue("Billboards");
        grid_->subProp( "Color" )->setValue(color);
        grid_->subProp( "Reference Frame" )->setValue("map");
        grid_->subProp("Plane Cell Count")->setValue("25");

        //创建一个类型为rviz/PointCloud2的图层，用于接收topic为points_raw的点云数据，就是雷达实时扫描数据的展示
        rviz::Display *match_points=manager_->createDisplay("rviz/PointCloud2","pointCloud2",true);
        match_points->subProp("Topic")->setValue(QString::fromStdString(topic_name));
        //match_points->subProp("Use Rainbow")->setValue(true);
        match_points->subProp("Size (m)")->setValue("0.05");
        match_points->subProp("Style")->setValue("Squares");
        //match_points->subProp("Autocompute Intensity Bounds")->setValue(true);
        //match_points->subProp("Color Transformer")->setValue("Intensity");
        match_points->subProp("Color Transformer")->setValue("AxisColor");
        //map_->subProp("Decay Time")->setValue("0");
        match_points->subProp("Queue Size")->setValue("20");
        match_points->subProp("Alpha")->setValue("1");
        //match_points->subProp("Channel Name")->setValue("Intensity");

        //创建一个类型为rviz/Trajectories的图层，用于接收名为/trajectory_node_list的轨迹
        rviz::Display *road_points=manager_->createDisplay("rviz/MarkerArray","trajectories",true);
        road_points->subProp("Marker Topic")->setValue("/trajectory_node_list");
        road_points->subProp("Queue Size")->setValue("100") ;
        
        //创建一个类型为rviz/Trajectories的图层，用于接收名为/trajectory_node_list的轨迹
        // rviz::Display *occupancy_grid_map=manager_->createDisplay("rviz/Map","map",true);
        // occupancy_grid_map->subProp("Topic")->setValue("/map");
        // occupancy_grid_map->subProp("Alpha")->setValue("1");
        // occupancy_grid_map->subProp("Queue Size")->setValue("100");

        //创建一个名为cartographer_rviz/Submaps的图层，用于接收submap_list的地图数据
        /*rviz::Display *submap = manager_->createDisplay("Submaps", "submaps", true);
        submap->subProp("Topic")->setValue("/submap_list");
        submap->subProp("Unreliable")->setValue("false");
        submap->subProp("Submap query service")->setValue("/submap_query");
        submap->subProp("Tracking frame")->setValue("base_link");
        submap->subProp("High Resolution")->setValue("true");
        submap->subProp("Low Resolution")->setValue("true");*/
}
void RvizPlugin::deinit()
{
    if (nullptr !=manager_ ) {

        stopDisplay();
        manager_->removeAllDisplays();

        delete manager_;
        manager_ = nullptr;
    }
}
void RvizPlugin::stopDisplay()
{
    manager_->stopUpdate();
}

void RvizPlugin::startDisplay()
{
    //sleep(10);
    viewManager->setCurrentViewControllerType("rviz/ThirdPersonFollower");
    viewManager->getCurrent()->subProp("Target Frame")->setValue("/map");
    viewManager->getCurrent()->subProp("Near Clip Distance")->setValue("0.01");
    viewManager->getCurrent()->subProp("Focal Point")->setValue("1.90735e-06;-7.62939e-06;0");
    viewManager->getCurrent()->subProp("Focal Shape Size")->setValue("0.05");
    viewManager->getCurrent()->subProp("Focal Shape Fixed Size")->setValue("true");
    viewManager->getCurrent()->subProp("Distance")->setValue("25");
    viewManager->getCurrent()->subProp("Yaw")->setValue("1.7004");
    viewManager->getCurrent()->subProp("Pitch")->setValue("0.770398");

    manager_->startUpdate();

}
