#include "workspace_config_rw.hpp"
#include <fstream>
#include <jsoncpp/json/json.h>
#include "utils.h"

using namespace std;

namespace yd_scanner_ui {
const static  std::string kWorkspaceConfigRWFileName = "workspace.json";
WorkspaceConfigRW::WorkspaceConfigRW(std::string wp_dir) :
wp_dir_(wp_dir)
{

}
bool WorkspaceConfigRW::ReadConfiguration(Configuration& config){

  Json::Value root = readJsonInfo(wp_dir_ + "/" + kWorkspaceConfigRWFileName);
  if (root.empty())
    return false;
  Json::Value projects = root["projects"];
  for (const auto& project :projects) {
    string project_name = project["name"].asString();
    string parent_directory = project["parent_directory"].asString();
    config.projects.push_back(ProjectItem{project_name, parent_directory});
  }
  
  return true;
  
}

bool WorkspaceConfigRW::WriteConfiguration(const Configuration& config) {

  Json::Value root;
  Json::Value projects;
  for (int i = 0; i < config.projects.size(); i++) {
    Json::Value project;
    project["name"]   = Json::Value(config.projects[i].name);
    project["parent_directory"]   = Json::Value(config.projects[i].parent_directory);

    projects.append(project);
  }
  root["projects"]   = projects;

  ofstream os;
  Json::StyledWriter sw;
  string jsnPath = wp_dir_+ "/" + kWorkspaceConfigRWFileName;

  os.open(jsnPath.c_str());
  os << sw.write(root);
  os.close();
  return true;

}

}