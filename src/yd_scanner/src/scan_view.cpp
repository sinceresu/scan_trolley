#include "scan_view.hpp"

#include "utils.h"
#include "ui_scan_view.h"

#include "scan_controller.hpp"
#include "project_config_rw.hpp"
#include "device_monitor_view.hpp"

using namespace std::chrono;

namespace yd_scanner_ui {
    constexpr int kStatisticPeriodSeconds = 10;

    ScanView::ScanView(QWidget *parent, string scene_id, string project_path, bool is_supplement)
        : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint),
            ui_(new Ui::ScanView), scene_id_(scene_id), project_path_(project_path),
            it_(nh_),
            left_image_sub_(nh_, "/camera/image_left", 1),
            right_image_sub_(nh_, "/camera/image_right", 1),
            sync(MySyncPolicy(10), left_image_sub_, right_image_sub_) {

        camCaptureStatePub = nh_.advertise<std_msgs::Bool>("/cam_capture_state", 1);
        camCaptureStateSub = nh_.subscribe<std_msgs::Bool>("/cam_capture_state", 1, boost::bind(&ScanView::camCaptureStateCallback, this, _1));

        ui_->setupUi(this);
        this->setFocusPolicy(Qt::StrongFocus);

        //this->installEventFilter(this);

        ui_->render_panel_->setFocusPolicy(Qt::StrongFocus);
        ui_->map_2d->setFocusPolicy(Qt::StrongFocus);
        ui_->statisticText->setFocusPolicy(Qt::StrongFocus);
        ui_->left_image_viewer->setFocusPolicy(Qt::StrongFocus);
        ui_->right_image_viewer->setFocusPolicy(Qt::StrongFocus);

        ProjectConfigRW project_config_rw(project_path);
        ProjectConfigRW::Configuration project_config;

        if(!project_config_rw.ReadConfiguration(project_config)) return;

        //rviz_play_w.init(rviz_play_layout, "velodyne_h", "/ns1/velodyne_points");
        rviz_play_w.init(ui_->render_panel_, "map", "/scan_matched_points2");

        //ui_->map_2d->installEventFilter(this);

        connect(ui_->start_scan_btn, SIGNAL(clicked()), SLOT(startScan()));
        connect(ui_->complete_scan_btn, SIGNAL(clicked()), SLOT(completeScan()));
        connect(ui_->snap_shot_btn, SIGNAL(clicked()), SLOT(doSnapShot()));

        ui_->complete_scan_btn->setEnabled(false);

        scan_controller_ = std::unique_ptr<ScanController>(new ScanController(scene_id_, project_path_, is_supplement));
        scan_controller_->setMapUpdataCallback(boost::bind(&ScanView::handleMapUpdate, this));

        sync.registerCallback(boost::bind(&ScanView::callback, this, _1, _2));

        statistic_timer_ = new QTimer(this);
    }
    ScanView::~ScanView() { }

    // 开始、完成 Start-Complete Scan
    // 开始
    void ScanView::startScan() {
        ROS_INFO("ScanView -> startScan()");

        if(0 != scan_controller_->start()) {
             QMessageBox::information(this, "打开设备错误", "设备未准备好，请检查设备是否正常。");
             return;
        }

        ui_->start_scan_btn->setEnabled(false);
        ui_->complete_scan_btn->setEnabled(true);
        // TODO: test
        //rviz_play_w.startDisplay();

        connect(statistic_timer_, SIGNAL(timeout()), this, SLOT(updateStatistic()));
        statistic_timer_->start(kStatisticPeriodSeconds * 1000);
    }
    // 完成
    void ScanView::completeScan() {
        ROS_INFO("ScanView -> completeScan()");

        statistic_timer_->stop();
        // TODO: test
        //rviz_play_w.stopDisplay();

        scan_controller_->stop();

        ui_->start_scan_btn->setEnabled(true);
        ui_->complete_scan_btn->setEnabled(false);
    }

    // 实时更新扫描
    void ScanView::drawImage() {
        ROS_INFO("ScanView -> drawImage()");

        string bmp_path = scan_controller_->getImageFile();
        QImage qimg(bmp_path.c_str());

        QPixmap pixmap = QPixmap::fromImage(qimg);
        ui_->map_2d->setAlignment(Qt::AlignCenter);
        ui_->map_2d->setPixmap(pixmap.scaled(ui_->map_2d->width(),
                                             ui_->map_2d->height(), Qt::KeepAspectRatio));
    }
    void ScanView::displayImage(QLabel* image_viewer, const sensor_msgs::ImageConstPtr& image_msg) {
        cv_bridge::CvImagePtr cv_ptr;
        cv_ptr = cv_bridge::toCvCopy(image_msg, "rgb8");

        cv::Mat rgb = cv_ptr->image;
        QImage img;
        {
            //cvt Mat BGR 2 QImage RGB
            img = QImage((const unsigned char*)(rgb.data),
                         rgb.cols,rgb.rows, rgb.cols*rgb.channels(), QImage::Format_RGB888);
        }
        QPixmap pixmap = QPixmap::fromImage(img);
        image_viewer->setPixmap(pixmap.scaled(image_viewer->width(),
                                              image_viewer->height(), Qt::KeepAspectRatio));
    }

    void ScanView::callback(const sensor_msgs::ImageConstPtr& left_msg,
                            const sensor_msgs::ImageConstPtr& right_msg) {
        ROS_INFO("ScanView::callback()");
        displayImage(ui_->left_image_viewer, left_msg);
        displayImage(ui_->right_image_viewer, right_msg);
    }
    void ScanView::handleMapUpdate() {
        ROS_INFO("handleMapUpdate()");
        drawImage();
    }

    // Prepare Snap Shot
    void ScanView::prepareSnapShot() {
        //ui_->snap_shot_btn->setStyleSheet("background-color: red;");
        ui_->snap_shot_btn->setEnabled(false);

        std_msgs::Bool msg;
        msg.data = true;
        camCaptureStatePub.publish(msg);

        ROS_INFO("do snap shot...");
    }
    // Do Snap Shot
    void ScanView::doSnapShot() {
        ROS_INFO("!!!!!!!!!!!!!!!!!!!!ScanView::doSnapShot()");

        if(!isCaptureFinished) {
            prepareSnapShot();

            if(scan_controller_->snapshot()) {
                ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera Capture Success!");
            } else {
                ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera Capture Failed!");
            }
        } else {
            ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera is Capturing...");
        }
    }
    // Mouse Middel Button Event
    void ScanView::mousePressEvent(QMouseEvent *event) {
        if(event->buttons() == Qt::MiddleButton) {
            ROS_INFO("Mouse middle button pressed!");

            if(!isCaptureFinished) {
                prepareSnapShot();

                if(scan_controller_->snapshot()) {
                    ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera Capture Success!");
                } else {
                    ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera Capture Failed!");
                }
            } else {
                ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera is Capturing...");
            }
        }
    }
    // Enter-Key Event
    void ScanView::keyPressEvent(QKeyEvent *event) {
        if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
            ROS_INFO("Enter/Return key pressed!");

            if(!isCaptureFinished) {
                prepareSnapShot();

                if(scan_controller_->snapshot()) {
                    ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera Capture Success!");
                } else {
                    ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera Capture Failed!");
                }
            } else {
                ROS_INFO("!!!!!!!!!!!!!!!!!!!!Camera is Capturing...");
            }
        }
    }
    // Camera Capture State Callback
    void ScanView::camCaptureStateCallback(const std_msgs::BoolConstPtr& msg) {
        isCaptureFinished = msg->data;

        if(!isCaptureFinished) {
            ui_->snap_shot_btn->setEnabled(true);
            //ui_->snap_shot_btn->setStyleSheet("background-color: white;");
        }
    }

    // Event Filter
    bool ScanView::eventFilter(QObject *target, QEvent *e) {
        // todo.
        return false;
    }

    // 关闭窗口
    void ScanView::closeEvent(QCloseEvent *e) {
        ROS_INFO("ScanView -> close()");
        rviz_play_w.deinit();
        QDialog::closeEvent(e);
    }

    // 进度完成更新数据
    void ScanView::updateStatistic() {
        std::string statistic_info = scan_controller_->getStatisticInfo();
        ui_->statisticText->setPlainText(ui_->statisticText->toPlainText() + QString::fromStdString( statistic_info));
        ui_->statisticText->verticalScrollBar()->setValue(ui_->statisticText->verticalScrollBar()->maximum());
    }
}
