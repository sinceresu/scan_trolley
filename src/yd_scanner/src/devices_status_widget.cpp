#include "devices_status_widget.hpp"

#include "utils.h"

#include <ostream>

using namespace Qt;
using namespace std;
using namespace yd_scanner_ui;

// 注册
Q_DECLARE_METATYPE(QString);

DevicesStatusWidget::DevicesStatusWidget() : QWidget()/*, qnode()*/ {
    // 获取PC资源信息 Get Computer Resources Info
    getComputerResourcesInfo();

    QVBoxLayout *layout = new QVBoxLayout();

    /// VLP16
    QGroupBox *VLP16Box = new QGroupBox(tr("VLP16"));
    QHBoxLayout *vlp16h = new QHBoxLayout();
    VLP16Box->setStyleSheet("border-width: 2px; border-style: solid; border-radius: 10px; border-color: gray; margin-top: 0.5ex;");
    // 1(ip = "192.168.1.201", topic = "/ns1/velodybe_points")
    QGroupBox *vlp16box1 = new QGroupBox(tr("定位激光VLP"));
    QHBoxLayout *vlp16h1 = new QHBoxLayout();
    // 第1列
    QWidget *vlp16h1_infoblock1 = new QWidget();
    QVBoxLayout *vlp161_c1      = new QVBoxLayout();

    QHBoxLayout *h1 = new QHBoxLayout(); QHBoxLayout *h2 = new QHBoxLayout(); QHBoxLayout *h3 = new QHBoxLayout();

    QLabel *l1_t = new QLabel("设备链接："); l1_v = new QLabel("");
    QLabel *l2_t = new QLabel("消息通信："); l2_v = new QLabel("");
    QLabel *l3_t = new QLabel("数据状态："); l3_v = new QLabel("");
    l1_v->setStyleSheet("color: green"); l2_v->setStyleSheet("color: green"); l3_v->setStyleSheet("color: green");

    h1->addWidget(l1_t); h1->addWidget(l1_v);
    h2->addWidget(l2_t); h2->addWidget(l2_v);
    h3->addWidget(l3_t); h3->addWidget(l3_v);

    vlp161_c1->addLayout(h1); vlp161_c1->addLayout(h2); vlp161_c1->addLayout(h3);

    vlp16h1_infoblock1->setLayout(vlp161_c1);
    vlp16h1->addWidget(vlp16h1_infoblock1);

    // 第2列
    QWidget *vlp16h1_infoblock2 = new QWidget();
    QVBoxLayout *vlp161_c2      = new QVBoxLayout();

    QHBoxLayout *h4 = new QHBoxLayout(); QHBoxLayout *h5 = new QHBoxLayout(); QHBoxLayout *h6 = new QHBoxLayout();

    QLabel *l4_t = new QLabel("数据4："); QLabel *l4_v = new QLabel("data4");
    QLabel *l5_t = new QLabel("数据5："); QLabel *l5_v = new QLabel("data5");
    QLabel *l6_t = new QLabel("数据6："); QLabel *l6_v = new QLabel("data6");
    l4_v->setStyleSheet("color: green"); l5_v->setStyleSheet("color: green"); l6_v->setStyleSheet("color: green");

    h4->addWidget(l4_t); h4->addWidget(l4_v);
    h5->addWidget(l5_t); h5->addWidget(l5_v);
    h6->addWidget(l6_t); h6->addWidget(l6_v);

    vlp161_c2->addLayout(h4); vlp161_c2->addLayout(h5); vlp161_c2->addLayout(h6);

    vlp16h1_infoblock2->setLayout(vlp161_c2);
    vlp16h1->addWidget(vlp16h1_infoblock2);

    // 第3列
    QWidget *vlp16h1_infoblock3 = new QWidget();
    QVBoxLayout *vlp161_c3      = new QVBoxLayout();

    QHBoxLayout *h7 = new QHBoxLayout(); QHBoxLayout *h8 = new QHBoxLayout(); QHBoxLayout *h9 = new QHBoxLayout();

    QLabel *l7_t = new QLabel("数据7："); QLabel *l7_v = new QLabel("data7");
    QLabel *l8_t = new QLabel("数据8："); QLabel *l8_v = new QLabel("data8");
    QLabel *l9_t = new QLabel("数据9："); QLabel *l9_v = new QLabel("data9");
    l7_v->setStyleSheet("color: green"); l8_v->setStyleSheet("color: green"); l9_v->setStyleSheet("color: green");

    h7->addWidget(l7_t); h7->addWidget(l7_v);
    h8->addWidget(l8_t); h8->addWidget(l8_v);
    h9->addWidget(l9_t); h9->addWidget(l9_v);

    vlp161_c3->addLayout(h7); vlp161_c3->addLayout(h8); vlp161_c3->addLayout(h9);

    vlp16h1_infoblock3->setLayout(vlp161_c3);
    vlp16h1->addWidget(vlp16h1_infoblock3);
    vlp16box1->setLayout(vlp16h1);

    // 2(ip = "192.168.1.202", topic = "/ns1/velodybe_points")
    QGroupBox *vlp16box2 = new QGroupBox(tr("采集激光VLP"));
    QHBoxLayout *vlp16h2 = new QHBoxLayout();

    QWidget *vlp16h2_infoblock1 = new QWidget();
    QVBoxLayout *vlp162_c1      = new QVBoxLayout();

    QHBoxLayout *h1_ = new QHBoxLayout(); QHBoxLayout *h2_ = new QHBoxLayout(); QHBoxLayout *h3_ = new QHBoxLayout();

    QLabel *l1__t = new QLabel("设备链接："); l1__v = new QLabel("");
    QLabel *l2__t = new QLabel("消息通信："); l2__v = new QLabel("");
    QLabel *l3__t = new QLabel("数据状态："); l3__v = new QLabel("");
    l1__v->setStyleSheet("color: green"); l2__v->setStyleSheet("color: green"); l3__v->setStyleSheet("color: green");

    h1_->addWidget(l1__t); h1_->addWidget(l1__v);
    h2_->addWidget(l2__t); h2_->addWidget(l2__v);
    h3_->addWidget(l3__t); h3_->addWidget(l3__v);

    vlp162_c1->addLayout(h1_); vlp162_c1->addLayout(h2_); vlp162_c1->addLayout(h3_);

    vlp16h2_infoblock1->setLayout(vlp162_c1);
    vlp16h2->addWidget(vlp16h2_infoblock1);

    // 第2列
    QWidget *vlp16h2_infoblock2 = new QWidget();
    QVBoxLayout *vlp162_c2      = new QVBoxLayout();

    QHBoxLayout *h4_ = new QHBoxLayout(); QHBoxLayout *h5_ = new QHBoxLayout(); QHBoxLayout *h6_ = new QHBoxLayout();

    QLabel *l4__t = new QLabel("数据4："); QLabel *l4__v = new QLabel("data4");
    QLabel *l5__t = new QLabel("数据5："); QLabel *l5__v = new QLabel("data5");
    QLabel *l6__t = new QLabel("数据6："); QLabel *l6__v = new QLabel("data6");
    l4__v->setStyleSheet("color: green"); l5__v->setStyleSheet("color: green"); l6__v->setStyleSheet("color: green");

    h4_->addWidget(l4__t); h4_->addWidget(l4__v);
    h5_->addWidget(l5__t); h5_->addWidget(l5__v);
    h6_->addWidget(l6__t); h6_->addWidget(l6__v);

    vlp162_c2->addLayout(h4_); vlp162_c2->addLayout(h5_); vlp162_c2->addLayout(h6_);

    vlp16h2_infoblock2->setLayout(vlp162_c2);
    vlp16h2->addWidget(vlp16h2_infoblock2);

    // 第3列
    QWidget *vlp16h2_infoblock3 = new QWidget();
    QVBoxLayout *vlp162_c3      = new QVBoxLayout();

    QHBoxLayout *h7_ = new QHBoxLayout(); QHBoxLayout *h8_ = new QHBoxLayout(); QHBoxLayout *h9_ = new QHBoxLayout();

    QLabel *l7__t = new QLabel("数据7："); QLabel *l7__v = new QLabel("data7");
    QLabel *l8__t = new QLabel("数据8："); QLabel *l8__v = new QLabel("data8");
    QLabel *l9__t = new QLabel("数据9："); QLabel *l9__v = new QLabel("data9");
    l7__v->setStyleSheet("color: green"); l8__v->setStyleSheet("color: green"); l9__v->setStyleSheet("color: green");

    h7_->addWidget(l7__t); h7_->addWidget(l7__v);
    h8_->addWidget(l8__t); h8_->addWidget(l8__v);
    h9_->addWidget(l9__t); h9_->addWidget(l9__v);

    vlp162_c3->addLayout(h7_); vlp162_c3->addLayout(h8_); vlp162_c3->addLayout(h9_);

    vlp16h2_infoblock3->setLayout(vlp162_c3);
    vlp16h2->addWidget(vlp16h2_infoblock3);
    vlp16box2->setLayout(vlp16h2);

    vlp16h->addWidget(vlp16box1);
    vlp16h->addWidget(vlp16box2);
    VLP16Box->setLayout(vlp16h);
    VLP16Box->setGeometry(0, 0, 1600, 100);

    /// IMU & GPS
    QHBoxLayout *imu_gps_layout = new QHBoxLayout();
    // IMU(port = "/dev/ttyUSB0", topic = "/imu/data")
    QGroupBox *IMUBox = new QGroupBox(tr("IMU"));
    QHBoxLayout *imuh = new QHBoxLayout();
    IMUBox->setStyleSheet("border-width: 2px; border-style: solid; border-radius: 10px; border-color: gray; margin-top: 0.5ex;");

    QWidget *imu_infoblock = new QWidget();
    QVBoxLayout *imuc      = new QVBoxLayout();

    QHBoxLayout *h10 = new QHBoxLayout(); QHBoxLayout *h11 = new QHBoxLayout(); QHBoxLayout *h12 = new QHBoxLayout();

    QLabel *l10_t = new QLabel("设备连接："); l10_v = new QLabel("");
    QLabel *l11_t = new QLabel("消息通信："); l11_v = new QLabel("");
    QLabel *l12_t = new QLabel("数据状态："); l12_v = new QLabel("");
    l10_v->setStyleSheet("color: green"); l11_v->setStyleSheet("color: green"); l12_v->setStyleSheet("color: green");

    h10->addWidget(l10_t); h10->addWidget(l10_v);
    h11->addWidget(l11_t); h11->addWidget(l11_v);
    h12->addWidget(l12_t); h12->addWidget(l12_v);

    imuc->addLayout(h10); imuc->addLayout(h11); imuc->addLayout(h12);

    imu_infoblock->setLayout(imuc);
    imuh->addWidget(imu_infoblock);

    IMUBox->setLayout(imuh);
    IMUBox->setGeometry(0, 0, 800, 100);

    /// GPS(port = "/dev/ttyUSB0", topic = "/gps")
    QGroupBox *GPSBox = new QGroupBox(tr("GPS"));
    QHBoxLayout *gpsh = new QHBoxLayout();
    GPSBox->setStyleSheet("border-width: 2px; border-style: solid; border-radius: 10px; border-color: gray; margin-top: 0.5ex;");

    QWidget *gps_infoblock = new QWidget();
    QVBoxLayout *gpsc      = new QVBoxLayout();

    QHBoxLayout *h13 = new QHBoxLayout(); QHBoxLayout *h14 = new QHBoxLayout(); QHBoxLayout *h15 = new QHBoxLayout();

    QLabel *l13_t = new QLabel("设备连接："); l13_v = new QLabel("");
    QLabel *l14_t = new QLabel("消息通信："); l14_v = new QLabel("");
    QLabel *l15_t = new QLabel("数据状态："); l15_v = new QLabel("");
    l13_v->setStyleSheet("color: green"); l14_v->setStyleSheet("color: green"); l15_v->setStyleSheet("color: green");

    h13->addWidget(l13_t); h13->addWidget(l13_v);
    h14->addWidget(l14_t); h14->addWidget(l14_v);
    h15->addWidget(l15_t); h15->addWidget(l15_v);

    gpsc->addLayout(h13); gpsc->addLayout(h14); gpsc->addLayout(h15);

    gps_infoblock->setLayout(gpsc);
    gpsh->addWidget(gps_infoblock);

    GPSBox->setLayout(gpsh);
    GPSBox->setGeometry(0, 0, 800, 100);

    imu_gps_layout->addWidget(IMUBox);
    imu_gps_layout->addWidget(GPSBox);

    /// CAMERA(port : ip = "", topic = "/camera")
    QHBoxLayout *cameras_layout = new QHBoxLayout();
    // Left Camera
    QGroupBox *l_cameraBox = new QGroupBox(tr("Left"));
    QHBoxLayout *l_camerah = new QHBoxLayout();
    l_cameraBox->setStyleSheet("border-width: 2px; border-style: solid; border-radius: 10px; border-color: gray; margin-top: 0.5ex;");

    QWidget *l_camera_infoblock = new QWidget();
    QVBoxLayout *l_camerac      = new QVBoxLayout();

    QHBoxLayout *h16 = new QHBoxLayout(); QHBoxLayout *h17 = new QHBoxLayout(); QHBoxLayout *h18 = new QHBoxLayout();

    QLabel *l16_t = new QLabel("设备连接："); l16_v = new QLabel("");
    QLabel *l17_t = new QLabel("消息通信："); l17_v = new QLabel("");
    QLabel *l18_t = new QLabel("数据状态："); l18_v = new QLabel("");
    l16_v->setStyleSheet("color: green"); l17_v->setStyleSheet("color: green"); l18_v->setStyleSheet("color: green");

    h16->addWidget(l16_t); h16->addWidget(l16_v);
    h17->addWidget(l17_t); h17->addWidget(l17_v);
    h18->addWidget(l18_t); h18->addWidget(l18_v);

    l_camerac->addLayout(h16); l_camerac->addLayout(h17); l_camerac->addLayout(h18);

    l_camera_infoblock->setLayout(l_camerac);
    l_camerah->addWidget(l_camera_infoblock);

    l_cameraBox->setLayout(l_camerah);
    l_cameraBox->setGeometry(0, 0, 1600, 100);
    // Right Camera
    QGroupBox *r_cameraBox = new QGroupBox(tr("Right"));
    QHBoxLayout *r_camerah = new QHBoxLayout();
    r_cameraBox->setStyleSheet("border-width: 2px; border-style: solid; border-radius: 10px; border-color: gray; margin-top: 0.5ex;");

    QWidget *r_camera_infoblock = new QWidget();
    QVBoxLayout *r_camerac      = new QVBoxLayout();

    QHBoxLayout *h19 = new QHBoxLayout(); QHBoxLayout *h20 = new QHBoxLayout(); QHBoxLayout *h21 = new QHBoxLayout();

    QLabel *l19_t = new QLabel("设备连接："); l19_v = new QLabel("");
    QLabel *l20_t = new QLabel("消息通信："); l20_v = new QLabel("");
    QLabel *l21_t = new QLabel("数据状态："); l21_v = new QLabel("");
    l19_v->setStyleSheet("color: green"); l20_v->setStyleSheet("color: green"); l21_v->setStyleSheet("color: green");

    h19->addWidget(l19_t); h19->addWidget(l19_v);
    h20->addWidget(l20_t); h20->addWidget(l20_v);
    h21->addWidget(l21_t); h21->addWidget(l21_v);

    r_camerac->addLayout(h19); r_camerac->addLayout(h20); r_camerac->addLayout(h21);

    r_camera_infoblock->setLayout(r_camerac);
    r_camerah->addWidget(r_camera_infoblock);

    r_cameraBox->setLayout(r_camerah);
    r_cameraBox->setGeometry(0, 0, 1600, 100);

    cameras_layout->addWidget(l_cameraBox);
    cameras_layout->addWidget(r_cameraBox);

    /// System Info GroupBox
    QGroupBox *systemBox = new QGroupBox(tr("SYSTEM"));
    systemBox->setStyleSheet("border-width: 2px; border-style: solid; border-radius: 10px; border-color: gray; margin-top: 0.5ex;");

    QHBoxLayout *systemh = new QHBoxLayout();

    QWidget *system_infoblock = new QWidget();
    QVBoxLayout *systemc      = new QVBoxLayout();

    QHBoxLayout *h_mem = new QHBoxLayout(); QHBoxLayout *h_disk = new QHBoxLayout(); QHBoxLayout *h_cpu = new QHBoxLayout();
    // Mem
    QLabel *l_mem_t = new QLabel("内存"); QLabel *l_mem_v = new QLabel();
    l_mem_v->setText(tr("全部：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(MEM_TOTAL) +
                      tr("        ") +
                      tr("可用：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(MEM_USE));
    l_mem_v->setTextFormat(Qt::TextFormat::RichText);
    // Disk
    QLabel *l_disk_t = new QLabel("磁盘"); QLabel *l_disk_v = new QLabel();
    l_disk_v->setText(tr("已用：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(DISK_USE) +
                      tr("    ") +
                      tr("可用：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(DISK_FREE));
    l_disk_v->setTextFormat(Qt::TextFormat::AutoText);
    // CPU
    QLabel *l_cpu_t  = new QLabel("CPU"); QLabel *l_cpu_v  = new QLabel();
    l_cpu_v->setText(tr("已用：<font style = 'font-size: 14px; color: green;'>%1</font>%").arg(CPU_RATE));
    l_cpu_v->setTextFormat(Qt::TextFormat::AutoText);

    h_mem->addWidget(l_mem_t);   h_mem->addWidget(l_mem_v);
    h_disk->addWidget(l_disk_t); h_disk->addWidget(l_disk_v);
    h_cpu->addWidget(l_cpu_t);   h_cpu->addWidget(l_cpu_v);

    systemc->addLayout(h_mem); systemc->addLayout(h_disk); systemc->addLayout(h_cpu);

    system_infoblock->setLayout(systemc);
    systemh->addWidget(system_infoblock);

    systemBox->setLayout(systemh);
    systemBox->setGeometry(0, 0, 1600, 100);

    /// Action Button
    QPushButton *okBtn = new QPushButton(tr("确定"));
    okBtn->setGeometry(0, 0, 1600, 10);
    connect(okBtn, SIGNAL(clicked()), SLOT(closeDevicesStatusDialog()));

    layout->addWidget(VLP16Box);
    layout->addLayout(imu_gps_layout);
    layout->addLayout(cameras_layout);
    layout->addWidget(systemBox);
    layout->addWidget(okBtn);

    setLayout(layout);

    // 隐藏关闭按钮 Hide Close Btn
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);
    this->setGeometry(QApplication::desktop()->width() / 2 - 500,
                      QApplication::desktop()->height() / 2 - 300, 1000, 300);

    /* 注册并启动 设备状态窗口UI更新线程 Register & Start Devices Status UI-Update Thread
    ROS_INFO("Register & Start Devices Status UI Update Thread!");
    int register_id = qRegisterMetaType<QString>("QString");
    ROS_INFO("register_id: %d", register_id);
    QObject::connect(&qnode, SIGNAL(uiUpdate(QString)), this, SLOT(devicesStatusUiUpdate(QString)));
    */
}
DevicesStatusWidget::~DevicesStatusWidget() {}

// 获取PC信息
int DevicesStatusWidget::run_time = 0;;
void DevicesStatusWidget::getComputerResourcesInfo() {
    ROS_INFO("DevicesStatusWidget -> getComputerResourcesInfo()");

    p = new QProcess(this);
    p->start("bash");
    p->waitForStarted();

    connect(p, SIGNAL(readyReadStandardOutput()), this, SLOT(outputInfo()));
    connect(p, SIGNAL(readyReadStandardError()), this, SLOT(errorInfo()));

    p->write("free -m\n");         // 内存状态 Memory State
    p->write("df -k\n");           // 磁盘状态 Disk State
    p->write("cat /proc/stat\n");  // CPU
}
// PC信息终端输出
double DevicesStatusWidget::MEM_TOTAL;
double DevicesStatusWidget::MEM_USE;
double DevicesStatusWidget::DISK_USE;
double DevicesStatusWidget::DISK_FREE;
double DevicesStatusWidget::CPU_RATE;
void DevicesStatusWidget::outputInfo() {
    run_time++;

    QString qstr = p->readAllStandardOutput().data();
    qstr.replace("\n", "||");
    qstr.replace(QRegExp("( ){1,}"), " ");
    vector<string> info = split(qstr.toStdString(), "||");

    // TODO: 不同机器可能需要不同的判断条件
    switch(run_time) {
        case 1: {
            ROS_INFO("**********PC INFO**********");
            vector<string> mem_data = split(info[1], " ");
            MEM_TOTAL = atof(mem_data[1].c_str()) / 1024.0;
            MEM_USE   = atof(mem_data[2].c_str()) / 1024.0;
            ROS_INFO("内存：-全部%.0lfG -已用%.0lfG", MEM_TOTAL, MEM_USE);
            break;
        }
        case 2: {
            vector<string> dis_data = split(info[3], " ");
            DISK_USE  = atof(dis_data[2].c_str()) / (1024.0 * 1024.0);
            DISK_FREE = atof(dis_data[3].c_str()) / (1024.0 * 1024.0);
            ROS_INFO("磁盘：-已用%.0lfG -可用%.0lfG", DISK_USE, DISK_FREE);
            break;
        }
        case 3: {
            /* TODO: test待重写
            vector<string> cpu_data = split(info[2], " ");
            CPU_RATE = 100.0 - atof(cpu_data[14].c_str());
            ROS_INFO("CPU：-%f%", CPU_RATE);
            */
            ROS_INFO("**********PC INFO**********");
            break;
        }
    }
}
void DevicesStatusWidget::errorInfo() {
    ROS_INFO("DevicesStatusWidget -> QProcess errorInfo()");
    QMessageBox::information(0, "Error", p->readAllStandardError().data());
}

/* 更新设备状态窗口UI Update Devices Status UI
void DevicesStatusWidget::devicesStatusUiUpdate(QString qstr) {
    std::string data = qstr.toStdString();
    std::vector<std::string> data_vec = split(data, " ");

    /// 激光
    // 定位激光
    l1_v->setText(QString::fromStdString(data_vec[0]));
    l2_v->setText(QString::fromStdString(data_vec[1]));
    l3_v->setText(QString::fromStdString(data_vec[2]));
    // 扫描激光
    l1__v->setText(QString::fromStdString(data_vec[3]));
    l2__v->setText(QString::fromStdString(data_vec[4]));
    l3__v->setText(QString::fromStdString(data_vec[5]));
    /// IMU & GPS
    // IMU
    l10_v->setText(QString::fromStdString(data_vec[6]));
    l11_v->setText(QString::fromStdString(data_vec[7]));
    l12_v->setText(QString::fromStdString(data_vec[8]));
    // GPS
    l13_v->setText(QString::fromStdString(data_vec[9]));
    l14_v->setText(QString::fromStdString(data_vec[10]));
    l15_v->setText(QString::fromStdString(data_vec[11]));
    /// Cameras
    // Left
    l16_v->setText(QString::fromStdString(data_vec[12]));
    l17_v->setText(QString::fromStdString(data_vec[13]));
    l18_v->setText(QString::fromStdString(data_vec[14]));
    // Right
    l19_v->setText(QString::fromStdString(data_vec[15]));
    l20_v->setText(QString::fromStdString(data_vec[16]));
    l21_v->setText(QString::fromStdString(data_vec[17]));
}
*/

// 关闭窗口
void DevicesStatusWidget::closeDevicesStatusDialog() {
    ROS_INFO("DevicesStatusWidget -> closeDevicesStatusDialog()");
    this->close();
}
void DevicesStatusWidget::closeEvent(QCloseEvent *event) {
    ROS_INFO("DevicesStatusWidget -> close()");
    QWidget::closeEvent(event);
}
