#ifndef PROJECT_CONTROLLER_HPP
#define PROJECT_CONTROLLER_HPP

#include "check_connexity.hpp"

#include <QDir>
#include <QList>

// LIBMAP_SCANNER
#include "map_common/global.h"
#include "map_common/err_code.h"
#include "map_build/libmap_build.h"
#include "map_build/map_build_interface.h"
#include "map_registrate/libmap_registrate.h"
#include "map_registrate/map_registrate_interface.h"
#include "map_colorize/libmap_colorize.h"
#include "map_colorize/map_colorize_interface.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <boost/thread.hpp>
#include <jsoncpp/json/json.h>

#include <map>
#include <functional>

using namespace std;
using namespace yida_mapping;

using namespace map_registrate;
using namespace map_build;
using namespace map_colorize;

namespace yd_scanner_ui {
    class ProjectController {
        public:
            enum class ProcessStatus {
                kBuildMap = 0,
                kWriteMap = 1,
                kMergeMap = 2,
                kColorizeMap = 3,
                kPostProcess = 4,
                kRegistrate = 5,
                kComplete = 6,
            };
            using ProgressCallback = std::function<void(ProcessStatus status,
                                                        float percentage)>;
            // Constructor & Destructor
            ProjectController(string project_name, string project_path);
            ~ProjectController();

            void setProgressCallback(ProjectController::ProgressCallback call_back) {
                progress_call_back_ = call_back;
            };

            bool startBuild(const std::string& scene_id);
            void stopBuild();
            bool startRegistrate(const std::string& scene_id);
            void cancelMerge();

            //bool startMerge(const std::string& scene_id);
            bool startMerge(const std::string& l_scene_id, const std::string& r_scene_id);
            bool startSelfMerge(const std::string& scene_id);

            void stopMerge();

            bool startSupplement(const std::string& scene_id);

            bool IsSceneScanned(std::string scene_id);
            bool IsSceneRegistrated(std::string scene_id);

            bool IsReadyToRegistrate(std::string scene_id);
            bool IsReadyToMerge(std::string scene_id);
            bool IsStationMapReady();

            bool IsReadyForRegistrate(std::string scene_id);
            bool IsSceneBuilt(std::string scene_id);
            bool IsFirstScene(std::string scene_id);

            std::set<std::string> GetScannedScenes(int scene_number);
            std::set<std::string> GetMergedScenes(int scene_number);

            std::string getStatusInfo();

            bool checkConnexity(string l_scene_id, string r_scene_id, int scene_rows_, int scene_cols_);

            std::string GetScenePcl(std::string scene_id) {
                return GetSceneMapDirectory() + scene_id + ".ply";
            }
            std::string GetScene2dMap(std::string scene_id) {
                return GetSceneMapDirectory() + scene_id + ".png";
            }
            std::string GetRegistratedMap(std::string scene_id) {
                return GetRegistratedMapDirectory() + scene_id + ".ply";
            }
            std::string GetSceneStateFile(std::string scene_id) {
                return GetSceneMapDirectory() + scene_id + ".pbstream";
            }
            std::string GetRegistratedStateFile(std::string scene_id) {
                return GetRegistratedMapDirectory() + scene_id + ".pbstream";
            }
            std::string GetStationMapFilePath() {
                return project_path_ + "/" + project_name_ + ".ply";
            }

            std::vector<int> GetRegistratedSceneIds();

        private:
            std::string GetBagDirectory() { return project_path_ + "/bags/"; };
            std::string GetSceneMapDirectory() { return project_path_ + "/scene_maps/"; };
            std::string GetRegistratedMapDirectory() { return project_path_ + "/registrated_maps/"; };
            std::string GetMergedMapDirectory() { return project_path_ + "/merged_maps/"; };
            std::string GetColoredMapDirectory() { return project_path_ + "/colored_maps/"; };

            void InitializeBuildMap();
            void InitializeRegistrateMap();
            void InitializeMergeMap();
            void InitializeColorizeMap();

            void BuildProgressCallback(MapBuildInterface::ProcessType process_type, float percentage);
            void ColorizeProgressCallback(float percentage);

            void doBuild(string scene_id);
            void doRegistrate(string scene_id);

            //void doMerge(string scene_id);
            void doMerge(string l_scene_id, string r_scene_id);
            void doSelfMerge(string scene_id);

            void doSupplement(string scene_id);

            //int mergeMaps(string scene_id);
            int mergeMaps(string l_scene_id, string r_scene_id);
            int selfMergeMaps(string scene_id);
            //void colorizeMap(string scene_id);
            void colorizeMap(string l_scene_id, string r_scene_id);
            void selfColorizeMap(string scene_id);

            string project_name_;
            std::vector<std::string> getSceneBags(string scene_id);

            std::shared_ptr<MapBuildInterface> map_builder_;
            std::shared_ptr<MapRegistrateInterface> map_registrater_;
            std::shared_ptr<MapColorizeInterface> map_colorizer_;

            boost::shared_ptr<CheckConnexity> check_connexity_ptr;
            boost::shared_ptr<CheckConnexity> merge_check_connexity_ptr;
            std::vector<int> registrateSceneIdIntersection;

            string project_path_;

            boost::thread work_thread_;
            ProgressCallback progress_call_back_ = nullptr;

            int scene_cols, scene_rows;
            map<int, int> sceneIdIndexMap;

            std::string status_info_;

            float merge_ratio_    = 1.0f;
            float colorize_ratio_ = 1.0f;
            float progress_       = 0;
    };
}
#endif
