﻿#include "lua_config_view.hpp"
#include "ui_lua_config_view.h"

#include "lua_config_controller.hpp"

// Qt5
#include <QtGui/QList>
#include <QtCore/QMetaType>

#include <iostream>
#include <fstream>
#include <thread>

#include <pthread.h>

#include "utils.h"
#include "gflags/gflags.h"

DECLARE_string(luas);

using namespace std;
using namespace yd_scanner_ui;

LuaConfigView::LuaConfigView(string project_config_path, QWidget *parent) :
    QDialog(parent),
    ui_(new Ui::LuaConfigView),
    config_dir_path(project_config_path + "/") {

    printf("LuaConfigView -> constructor\n");
    // 窗口关闭后，强制执行析构函数
    setAttribute(Qt::WA_DeleteOnClose);

    this->setFixedSize(867, 762);
    ui_->setupUi(this);

    lua_controller = unique_ptr<LuaConfigController>(new LuaConfigController());

    // 需要配置的luas
    vector<string> paths = split(FLAGS_luas, ",");
    for(string p : paths) {
        p += ".lua";
        lua_name_vec.push_back(p);
    }

    paramsInit(); // 获取默认参数
    dialogInit(); // 初始化
}
LuaConfigView::~LuaConfigView() {
    printf("LuaConfigView -> destructor\n");

    // clear
    vector<string>().swap(mapBuildNewValues);
    vector<string>().swap(buildWriterNewValues);
    vector<string>().swap(mapRegistrateNewValues);
    vector<string>().swap(mergeWriterNewValues);
    vector<string>().swap(mapColorizeNewValues);
    vector<string>().swap(colorizeWriterNewValues);
}

// Params Init
void LuaConfigView::paramsInit() {
    printf("LuaConfigView -> paramsInit()\n");

    // TODO.暂时不解析 online.lua, two_vlp_outdoor.lua
    // map_build
    vector<string> map_build_vs = lua_controller->readMapBuild(config_dir_path, lua_name_vec[2]);
    if(map_build_vs.size() > 0) {
        istringstream(map_build_vs[0]) >> old_buildFilterWaveY;
        //istringstream(map_build_vs[1]) >> old_landmarkCheckY;

        ui_->point_cloud_build_filter_wave_yes_rbtn->setChecked(old_buildFilterWaveY);
        ui_->point_cloud_build_filter_wave_no_rbtn->setChecked(!old_buildFilterWaveY);

        //ui_->landmark_check_yes_rbtn->setChecked(old_landmarkCheckY);
        //ui_->landmark_check_no_rbtn->setChecked(!old_landmarkCheckY);
    }
    // build_writer
    vector<string> build_writer_vs = lua_controller->readBuildWriter(config_dir_path, lua_name_vec[3]);
    if(build_writer_vs.size() > 0) {
        old_buildMinDis = build_writer_vs[0];
        old_buildMaxDis = build_writer_vs[1];

        old_buildMaxHorDis = build_writer_vs[2];

        old_buildMinHeight = build_writer_vs[3];
        old_buildMaxHeight = build_writer_vs[4];

        ui_->point_cloud_build_mindis_lineEdit->setText(QString::fromStdString(old_buildMinDis));
        ui_->point_cloud_build_maxdis_lineEdit->setText(QString::fromStdString(old_buildMaxDis));

        ui_->point_cloud_build_max_horDis_lineEdit->setText(QString::fromStdString(old_buildMaxHorDis));

        ui_->point_cloud_build_minHeight_lineEdit->setText(QString::fromStdString(old_buildMinHeight));
        ui_->point_cloud_build_maxHeight_lineEdit->setText(QString::fromStdString(old_buildMaxHeight));
    }
    // map_registrate
    vector<string> map_registrate_vs = lua_controller->readMapRegistrate(config_dir_path, lua_name_vec[4]);
    if(map_registrate_vs.size() > 0) {
        old_registrateLeafSize   = map_registrate_vs[0];
        old_registrateIteatorNum = map_registrate_vs[1];

        ui_->point_cloud_registrate_leafSize_lineEdit->setText(QString::fromStdString(old_registrateLeafSize));
        ui_->point_cloud_registrate_iteatorNum_lineEdit->setText(QString::fromStdString(old_registrateIteatorNum));
    }
    // merge_writer
    vector<string> merge_writer_vs = lua_controller->readMergeWriter(config_dir_path, lua_name_vec[5]);
    if(merge_writer_vs.size() > 0) {
        old_mergeMinDis = merge_writer_vs[0];
        old_mergeMaxDis = merge_writer_vs[1];

        old_mergeMaxHorDis = merge_writer_vs[2];
        old_mergeSamplingRatio = merge_writer_vs[3];

        old_mergeMinHeight = merge_writer_vs[4];
        old_mergeMaxHeight = merge_writer_vs[5];

        ui_->point_cloud_merge_mindis_lineEdit->setText(QString::fromStdString(old_mergeMinDis));
        ui_->point_cloud_merge_maxdis_lineEdit->setText(QString::fromStdString(old_mergeMaxDis));

        ui_->point_cloud_merge_max_horDis_lineEdit->setText(QString::fromStdString(old_mergeMaxHorDis));
        ui_->point_cloud_frame_sampling_ratio_lineEdit->setText(QString::fromStdString(old_mergeSamplingRatio));

        ui_->point_cloud_merge_minHeight_lineEdit->setText(QString::fromStdString(old_mergeMinHeight));
        ui_->point_cloud_merge_maxHeight_lineEdit->setText(QString::fromStdString(old_mergeMaxHeight));
    }
    // map_colorize
    vector<string> map_colorize_vs = lua_controller->readMapColorize(config_dir_path, lua_name_vec[6]);
    if(map_colorize_vs.size() > 0) {
        istringstream(map_colorize_vs[0]) >> old_colorizeFilterWaveY;

        old_colorizeSkipTime = map_colorize_vs[1];

        old_colorizeMinDis = map_colorize_vs[2];
        old_colorizeMaxDis = map_colorize_vs[3];

        ui_->point_cloud_colorize_filter_wave_yes_rbtn->setChecked(old_colorizeFilterWaveY);
        ui_->point_cloud_colorize_filter_wave_no_rbtn->setChecked(!old_colorizeFilterWaveY);

        ui_->point_cloud_colorize_skip_time_lineEdit->setText(QString::fromStdString(old_colorizeSkipTime));

        ui_->point_cloud_colorize_mindis_lineEdit->setText(QString::fromStdString(old_colorizeMinDis));
        ui_->point_cloud_colorize_maxdis_lineEdit->setText(QString::fromStdString(old_colorizeMaxDis));
    }
    // colorize_writer
    vector<string> colorize_writer_vs = lua_controller->readColorizeWriter(config_dir_path, lua_name_vec[7]);
    if(colorize_writer_vs.size() > 0) {
        old_colorizeMinRangeDis = colorize_writer_vs[0];
        old_colorizeMaxRangeDis = colorize_writer_vs[1];

        old_colorizeMaxHorDis = colorize_writer_vs[2];
        old_colorizeRemoveMovingObjFilterLevel = colorize_writer_vs[3];

        old_colorizeMinHeight = colorize_writer_vs[4];
        old_colorizeMaxHeight = colorize_writer_vs[5];

        ui_->point_cloud_colorize_min_rangeDis_lineEdit->setText(QString::fromStdString(old_colorizeMinRangeDis));
        ui_->point_cloud_colorize_max_rangeDis_lineEdit->setText(QString::fromStdString(old_colorizeMaxRangeDis));

        ui_->point_cloud_colorize_max_horDis_lineEdit->setText(QString::fromStdString(old_colorizeMaxHorDis));
        ui_->point_cloud_colorize_remove_moving_obj_filterLevel_lineEdit->setText(QString::fromStdString(old_colorizeRemoveMovingObjFilterLevel));

        ui_->point_cloud_colorize_minHeight_lineEdit->setText(QString::fromStdString(old_colorizeMinHeight));
        ui_->point_cloud_colorize_maxHeight_lineEdit->setText(QString::fromStdString(old_colorizeMaxHeight));
    }
}

// Dialog Init
void LuaConfigView::dialogInit() {
    printf("LuaConfigView -> dialogInit()\n");

    setWindowTitle("扫描参数配置");
    setGeometry((QApplication::desktop()->width() - 867) / 2, (QApplication::desktop()->height() - 758) / 2, 867, 758);
}

/// 窗口关闭事件
// map_build
bool LuaConfigView::isMapBuildChanged() {
    if(old_buildFilterWaveY != ui_->point_cloud_build_filter_wave_yes_rbtn->isChecked() /*||
       old_landmarkCheckY != ui->landmark_check_yes_rbtn->isChecked()*/) {

        string o_bstr = old_buildFilterWaveY ? "true" : "false";
        string n_bstr = ui_->point_cloud_build_filter_wave_yes_rbtn->isChecked() ? "true" : "false";

        mapBuildNewValues.push_back("filter_pcl = " + o_bstr + "|filter_pcl = " + n_bstr);

        // string o_bstr2 = old_landmarkCheckY ? "true" : "false";
        // string n_bstr2 = ui_->landmark_check_yes_rbtn->isChecked() ? "true" : "false";
        //mapBuildNewValues.push_back("do_landmark = " + o_bstr2 + "|do_landmark = " + n_bstr2);
        return true;
    }
    return false;
}
// build_writer
bool LuaConfigView::isBuildWriterChanged() {
    new_buildMinDis = ui_->point_cloud_build_mindis_lineEdit->text().toStdString();
    new_buildMaxDis = ui_->point_cloud_build_maxdis_lineEdit->text().toStdString();

    new_buildMaxHorDis = ui_->point_cloud_build_max_horDis_lineEdit->text().toStdString();

    new_buildMinHeight = ui_->point_cloud_build_minHeight_lineEdit->text().toStdString();
    new_buildMaxHeight = ui_->point_cloud_build_maxHeight_lineEdit->text().toStdString();

    if(strcmp(old_buildMinDis.c_str(), new_buildMinDis.c_str()) != 0 || strcmp(old_buildMaxDis.c_str(), new_buildMaxDis.c_str()) != 0 ||
       strcmp(old_buildMaxHorDis.c_str(), new_buildMaxHorDis.c_str()) != 0 ||
       strcmp(old_buildMinHeight.c_str(), new_buildMinHeight.c_str()) != 0 || strcmp(old_buildMaxHeight.c_str(), new_buildMaxHeight.c_str()) != 0) {

        if(strcmp(old_buildMinDis.c_str(), new_buildMinDis.c_str()) != 0 && strcmp(old_buildMaxDis.c_str(), new_buildMaxDis.c_str()) != 0) {
            buildWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->build_minMaxRangeFilterTableDepth));
            buildWriterNewValues.push_back("min_range = " + old_buildMinDis + "|min_range = " + new_buildMinDis);
            buildWriterNewValues.push_back("max_range = " + old_buildMaxDis + "|max_range = " + new_buildMaxDis);
        } else if(strcmp(old_buildMinDis.c_str(), new_buildMinDis.c_str()) != 0) {
            buildWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->build_minMaxRangeFilterTableDepth));
            buildWriterNewValues.push_back("min_range = " + old_buildMinDis + "|min_range = " + new_buildMinDis);
        } else if(strcmp(old_buildMaxDis.c_str(), new_buildMaxDis.c_str()) != 0) {
            buildWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->build_minMaxRangeFilterTableDepth));
            buildWriterNewValues.push_back("max_range = " + old_buildMaxDis + "|max_range = " + new_buildMaxDis);
        }

        if(strcmp(old_buildMaxHorDis.c_str(), new_buildMaxHorDis.c_str()) != 0) {
            buildWriterNewValues.push_back("action|horizontal_range_filter*" + to_string(lua_controller->build_horizontalRangeFilterTableDepth));
            buildWriterNewValues.push_back("max_range = " + old_buildMaxHorDis + "|max_range = " + new_buildMaxHorDis);
        }

        if(strcmp(old_buildMinHeight.c_str(), new_buildMinHeight.c_str()) != 0) { buildWriterNewValues.push_back("min_z = " + old_buildMinHeight + "|min_z = " + new_buildMinHeight); }
        if(strcmp(old_buildMaxHeight.c_str(), new_buildMaxHeight.c_str()) != 0) { buildWriterNewValues.push_back("max_z = " + old_buildMaxHeight + "|max_z = " + new_buildMaxHeight); }

        return true;
    }
    return false;
}
// map_registrate
bool LuaConfigView::isMapRegistrateChanged() {
    new_registrateLeafSize   = ui_->point_cloud_registrate_leafSize_lineEdit->text().toStdString();
    new_registrateIteatorNum = ui_->point_cloud_registrate_iteatorNum_lineEdit->text().toStdString();

    if(strcmp(old_registrateLeafSize.c_str(), new_registrateLeafSize.c_str()) != 0 || strcmp(old_registrateIteatorNum.c_str(), new_registrateIteatorNum.c_str()) != 0) {
        if(strcmp(old_registrateLeafSize.c_str(), new_registrateLeafSize.c_str()) != 0) { mapRegistrateNewValues.push_back("leaf_size = " + old_registrateLeafSize + "|leaf_size = " + new_registrateLeafSize); }
        if(strcmp(old_registrateIteatorNum.c_str(), new_registrateIteatorNum.c_str()) != 0) { mapRegistrateNewValues.push_back("icp_iterations = " + old_registrateIteatorNum + "|icp_iterations = " + new_registrateIteatorNum); }

        return true;
    }
    return false;
}
// merge_writer
bool LuaConfigView::isMergeWriterChanged() {
    new_mergeMinDis = ui_->point_cloud_merge_mindis_lineEdit->text().toStdString();
    new_mergeMaxDis = ui_->point_cloud_merge_maxdis_lineEdit->text().toStdString();

    new_mergeMaxHorDis     = ui_->point_cloud_merge_max_horDis_lineEdit->text().toStdString();
    new_mergeSamplingRatio = ui_->point_cloud_frame_sampling_ratio_lineEdit->text().toStdString();

    new_mergeMinHeight = ui_->point_cloud_merge_minHeight_lineEdit->text().toStdString();
    new_mergeMaxHeight = ui_->point_cloud_merge_maxHeight_lineEdit->text().toStdString();

    if(strcmp(old_mergeMinDis.c_str(), new_mergeMinDis.c_str()) != 0 || strcmp(old_mergeMaxDis.c_str(), new_mergeMaxDis.c_str()) != 0 ||
       strcmp(old_mergeMaxHorDis.c_str(), new_mergeMaxHorDis.c_str()) != 0 ||
       strcmp(old_mergeSamplingRatio.c_str(), new_mergeSamplingRatio.c_str()) != 0 ||
       strcmp(old_mergeMinHeight.c_str(), new_mergeMinHeight.c_str()) != 0 || strcmp(old_mergeMaxHeight.c_str(), new_mergeMaxHeight.c_str()) != 0) {

        if(strcmp(old_mergeMinDis.c_str(), new_mergeMinDis.c_str()) != 0 && strcmp(old_mergeMaxDis.c_str(), new_mergeMaxDis.c_str()) != 0) {
            mergeWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->merge_minMaxRangeFilterTableDepth));
            mergeWriterNewValues.push_back("min_range = " + old_mergeMinDis + "|min_range = " + new_mergeMinDis);
            mergeWriterNewValues.push_back("max_range = " + old_mergeMaxDis + "|max_range = " + new_mergeMaxDis);
        } else if(strcmp(old_mergeMinDis.c_str(), new_mergeMinDis.c_str()) != 0) {
            mergeWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->merge_minMaxRangeFilterTableDepth));
            mergeWriterNewValues.push_back("min_range = " + old_mergeMinDis + "|min_range = " + new_mergeMinDis);
        } else if(strcmp(old_mergeMaxDis.c_str(), new_mergeMaxDis.c_str()) != 0) {
            mergeWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->merge_minMaxRangeFilterTableDepth));
            mergeWriterNewValues.push_back("max_range = " + old_mergeMaxDis + "|max_range = " + new_mergeMaxDis);
        }

        if(strcmp(old_mergeMaxHorDis.c_str(), new_mergeMaxHorDis.c_str()) != 0) {
            mergeWriterNewValues.push_back("action|horizontal_range_filter*" + to_string(lua_controller->merge_horizontalRangeFilterTableDepth));
            mergeWriterNewValues.push_back("max_range = " + old_mergeMaxHorDis + "|max_range = " + new_mergeMaxHorDis);
        }
        if(strcmp(old_mergeSamplingRatio.c_str(), new_mergeSamplingRatio.c_str()) != 0) { mergeWriterNewValues.push_back("sampling_ratio = " + old_mergeSamplingRatio + "|sampling_ratio = " + new_mergeSamplingRatio); }

        if(strcmp(old_mergeMinHeight.c_str(), new_mergeMinHeight.c_str()) != 0) { mergeWriterNewValues.push_back("min_z = " + old_mergeMinHeight + "|min_z = " + new_mergeMinHeight); }
        if(strcmp(old_mergeMaxHeight.c_str(), new_mergeMaxHeight.c_str()) != 0) { mergeWriterNewValues.push_back("max_z = " + old_mergeMaxHeight + "|max_z = " + new_mergeMaxHeight); }

        return true;
    }
    return false;
}
// map_colorize
bool LuaConfigView::isMapColorizeChanged() {
    new_colorizeSkipTime = ui_->point_cloud_colorize_skip_time_lineEdit->text().toStdString();

    new_colorizeMinDis = ui_->point_cloud_colorize_mindis_lineEdit->text().toStdString();
    new_colorizeMaxDis = ui_->point_cloud_colorize_maxdis_lineEdit->text().toStdString();

    if(old_colorizeFilterWaveY != ui_->point_cloud_colorize_filter_wave_yes_rbtn->isChecked() ||
       strcmp(old_colorizeSkipTime.c_str(), new_colorizeSkipTime.c_str()) != 0 ||
       strcmp(old_colorizeMinDis.c_str(), new_colorizeMinDis.c_str()) != 0 || strcmp(old_colorizeMaxDis.c_str(), new_colorizeMaxDis.c_str()) != 0) {

        if(old_colorizeFilterWaveY != ui_->point_cloud_colorize_filter_wave_yes_rbtn->isChecked()) {
            string o_bstr = old_colorizeFilterWaveY ? "true" : "false";
            string n_bstr = ui_->point_cloud_colorize_filter_wave_yes_rbtn->isChecked() ? "true" : "false";
            mapColorizeNewValues.push_back("filter_pcl = " + o_bstr + "|filter_pcl = " + n_bstr);
        }

        if(strcmp(old_colorizeSkipTime.c_str(), new_colorizeSkipTime.c_str()) != 0) { mapColorizeNewValues.push_back("skip_seconds = " + old_colorizeSkipTime + "|skip_seconds = " + new_colorizeSkipTime); }

        if(strcmp(old_colorizeMinDis.c_str(), new_colorizeMinDis.c_str()) != 0 && strcmp(old_colorizeMaxDis.c_str(), new_colorizeMaxDis.c_str()) != 0) {
            mapColorizeNewValues.push_back("plane_distances = {" + old_colorizeMinDis + ", " + old_colorizeMaxDis + "}" + "|plane_distances = {" + new_colorizeMinDis + ", " + new_colorizeMaxDis + "}");
        } else if(strcmp(old_colorizeMinDis.c_str(), new_colorizeMinDis.c_str()) != 0) {
            mapColorizeNewValues.push_back("plane_distances = {" + old_colorizeMinDis + ", " + old_colorizeMaxDis + "}" + "|plane_distances = {" + new_colorizeMinDis + ", " + old_colorizeMaxDis + "}");
        } else if(strcmp(old_colorizeMaxDis.c_str(), new_colorizeMaxDis.c_str()) != 0) {
            mapColorizeNewValues.push_back("plane_distances = {" + old_colorizeMinDis + ", " + old_colorizeMaxDis + "}" + "|plane_distances = {" + old_colorizeMinDis + ", " + new_colorizeMaxDis + "}");
        }

        return true;
    }

    return false;
}
// colorize_writer
bool LuaConfigView::isColorizeWriterChanged() {
    new_colorizeMinRangeDis = ui_->point_cloud_colorize_min_rangeDis_lineEdit->text().toStdString();
    new_colorizeMaxRangeDis = ui_->point_cloud_colorize_max_rangeDis_lineEdit->text().toStdString();

    new_colorizeMaxHorDis = ui_->point_cloud_colorize_max_horDis_lineEdit->text().toStdString();
    new_colorizeRemoveMovingObjFilterLevel = ui_->point_cloud_colorize_remove_moving_obj_filterLevel_lineEdit->text().toStdString();

    new_colorizeMinHeight = ui_->point_cloud_colorize_minHeight_lineEdit->text().toStdString();
    new_colorizeMaxHeight = ui_->point_cloud_colorize_maxHeight_lineEdit->text().toStdString();

    if(strcmp(old_colorizeMinRangeDis.c_str(), new_colorizeMinRangeDis.c_str()) != 0 || strcmp(old_colorizeMaxRangeDis.c_str(), new_colorizeMaxRangeDis.c_str()) != 0 ||
       strcmp(old_colorizeMaxHorDis.c_str(), new_colorizeMaxHorDis.c_str()) != 0 ||
       strcmp(old_colorizeRemoveMovingObjFilterLevel.c_str(), new_colorizeRemoveMovingObjFilterLevel.c_str()) != 0 ||
       strcmp(old_colorizeMinHeight.c_str(), new_colorizeMinHeight.c_str()) != 0 || strcmp(old_colorizeMaxHeight.c_str(), new_colorizeMaxHeight.c_str()) != 0) {

        if(strcmp(old_colorizeMinRangeDis.c_str(), new_colorizeMinRangeDis.c_str()) != 0 && strcmp(old_colorizeMaxRangeDis.c_str(), new_colorizeMaxRangeDis.c_str()) != 0) {
            colorizeWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->colorize_minMaxRangeFilterTableDepth));
            colorizeWriterNewValues.push_back("min_range = " + old_colorizeMinRangeDis + "|min_range = " + new_colorizeMinRangeDis);
            colorizeWriterNewValues.push_back("max_range = " + old_colorizeMaxRangeDis + "|max_range = " + new_colorizeMaxRangeDis);
        } else if(strcmp(old_colorizeMinRangeDis.c_str(), new_colorizeMinRangeDis.c_str()) != 0) {
            colorizeWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->colorize_minMaxRangeFilterTableDepth));
            colorizeWriterNewValues.push_back("min_range = " + old_colorizeMinRangeDis + "|min_range = " + new_colorizeMinRangeDis);
        } else if(strcmp(old_colorizeMaxRangeDis.c_str(), new_colorizeMaxRangeDis.c_str()) != 0) {
            colorizeWriterNewValues.push_back("action|min_max_range_filter*" + to_string(lua_controller->colorize_minMaxRangeFilterTableDepth));
            colorizeWriterNewValues.push_back("max_range = " + old_colorizeMaxRangeDis + "|max_range = " + new_colorizeMaxRangeDis);
        }

        if(strcmp(old_colorizeMaxHorDis.c_str(), new_colorizeMaxHorDis.c_str()) != 0) {
            colorizeWriterNewValues.push_back("action|horizontal_range_filter*" + to_string(lua_controller->colorize_horizontalRangeFilterTableDepth));
            colorizeWriterNewValues.push_back("max_range = " + old_colorizeMaxHorDis + "|max_range = " + new_colorizeMaxHorDis);
        }
        if(strcmp(old_colorizeRemoveMovingObjFilterLevel.c_str(), new_colorizeRemoveMovingObjFilterLevel.c_str()) != 0) { colorizeWriterNewValues.push_back("miss_per_hit_limit = " + old_colorizeRemoveMovingObjFilterLevel + "|miss_per_hit_limit = " + new_colorizeRemoveMovingObjFilterLevel); }

        if(strcmp(old_colorizeMinHeight.c_str(), new_colorizeMinHeight.c_str()) != 0) { colorizeWriterNewValues.push_back("min_z = " + old_colorizeMinHeight + "|min_z = " + new_colorizeMinHeight); }
        if(strcmp(old_colorizeMaxHeight.c_str(), new_colorizeMaxHeight.c_str()) != 0) { colorizeWriterNewValues.push_back("max_z = " + old_colorizeMaxHeight + "|max_z = " + new_colorizeMaxHeight); }

        return true;
    }
    return false;
}
// 保存配置 do save
void LuaConfigView::doSave(isLuaChangedParasStru stru) {
    printf("LuaConfigView -> doSave()\n");

    if(stru.is_map_build_changed) {
        printf(">>> save map_build.lua\n");
        if(lua_controller->saveToFile(stru.map_build_path, stru.map_build_values)) {
            printf(">>> map_build.lua saved!\n");
            pdw->setProgress(16);
            sleep(1);
        }
    }
    if(stru.is_build_writer_changed) {
        printf(">>> save build_writer.lua\n");
        if(lua_controller->saveToFile(stru.build_writer_path, stru.build_writer_values)) {
            printf(">>> build_writer.lua saved!\n");
            pdw->setProgress(32);
            sleep(1);
        }
    }
    if(stru.is_map_registrate_changed) {
        printf(">>> save map_registrate.lua\n");
        if(lua_controller->saveToFile(stru.map_registrate_path, stru.map_registrate_values)) {
            printf(">>> map_registrate.lua saved!\n");
            pdw->setProgress(48);
            sleep(1);
        }
    }
    if(stru.is_merge_writer_changed) {
        printf(">>> save merge_writer.lua\n");
        if(lua_controller->saveToFile(stru.merge_writer_path, stru.merge_writer_values)) {
            printf(">>> merge_writer.lua saved!\n");
            pdw->setProgress(64);
            sleep(1);
        }
    }
    if(stru.is_map_colorize_changed) {
        printf(">>> save map_colorize.lua\n");
        if(lua_controller->saveToFile(stru.map_colorize_path, stru.map_colorize_values)) {
            printf(">>> map_colorize.lua saved!\n");
            pdw->setProgress(80);
            sleep(1);
        }
    }
    if(stru.is_colorize_writer_changed) {
        printf(">>> save colorize_writer.lua\n");
        if(lua_controller->saveToFile(stru.colorize_writer_path, stru.colorize_writer_values)) {
            printf(">>> colorize_writer.lua saved!\n");
            pdw->setProgress(96);
            sleep(1);
        }
    }

    pdw->setProgress(100);
    pdw->close();
}
void LuaConfigView::closeEvent(QCloseEvent *event) {
    printf("LuaConfigView -> close()\n");

    // 判断是否有更改
    bool map_build_state    = isMapBuildChanged();
    bool build_writer_state = isBuildWriterChanged();

    bool map_registrate_state = isMapRegistrateChanged();
    bool merge_writer_state   = isMergeWriterChanged();

    bool map_colorize_state    = isMapColorizeChanged();
    bool colorize_writer_state = isColorizeWriterChanged();

    if(map_build_state || build_writer_state ||
       map_registrate_state || merge_writer_state ||
       map_colorize_state || colorize_writer_state) {
        // 保存
        pdw = new ProgressDialogWidget(this, "扫描参数", "正在保存配置，请稍候...");
        pdw->setAttribute(Qt::WA_DeleteOnClose);

        struct isLuaChangedParasStru stru;
        // map_build
        stru.is_map_build_changed = map_build_state;
        stru.map_build_path       = config_dir_path + "/" + lua_name_vec[2];
        stru.map_build_values     = mapBuildNewValues;
        // build_writer
        stru.is_build_writer_changed = build_writer_state;
        stru.build_writer_path       = config_dir_path + "/" + lua_name_vec[3];
        stru.build_writer_values     = buildWriterNewValues;
        // map_registrate
        stru.is_map_registrate_changed = map_registrate_state;
        stru.map_registrate_path       = config_dir_path + "/" + lua_name_vec[4];
        stru.map_registrate_values     = mapRegistrateNewValues;
        // merge_writer
        stru.is_merge_writer_changed = merge_writer_state;
        stru.merge_writer_path       = config_dir_path + "/" + lua_name_vec[5];
        stru.merge_writer_values     = mergeWriterNewValues;
        // map_colorize
        stru.is_map_colorize_changed = map_colorize_state;
        stru.map_colorize_path       = config_dir_path + "/" + lua_name_vec[6];
        stru.map_colorize_values     = mapColorizeNewValues;
        // colorize_writer
        stru.is_colorize_writer_changed = colorize_writer_state;
        stru.colorize_writer_path       = config_dir_path + "/" + lua_name_vec[7];
        stru.colorize_writer_values     = colorizeWriterNewValues;

        save_thread = boost::thread(boost::bind(&LuaConfigView::doSave, this, stru));
        pdw->exec();
        save_thread.join();
    }

    QWidget::closeEvent(event);
}
