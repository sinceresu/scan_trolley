﻿#include "convert_select_view.hpp"
#include "convert_select_controller.hpp"
#include "toast_tool.hpp"

#include "ui_convert_select_view.h"

#include <QtGui/QList>
#include <QtCore/QMetaType>

#include <iostream>
#include <fstream>
#include <thread>

#include <stdlib.h>
#include <pthread.h>

#include "utils.h"

using namespace std;
using namespace yd_scanner_ui;

ConvertSelectView::ConvertSelectView(string project_path, QWidget *parent) : QDialog(parent),
    ui_(new Ui::ConvertSelectView),
    path(project_path),
    isConvertSuccess(false) {

    printf("ConvertSelectView -> constructor\n");
    // 窗口关闭后，强制执行析构函数
    setAttribute(Qt::WA_DeleteOnClose);

    this->setFixedSize(311, 241);
    ui_->setupUi(this);

    convert_select_controller = std::unique_ptr<ConvertSelectController>(new ConvertSelectController());
    convert_select_controller->setConvertProgressCallback(boost::bind(&ConvertSelectView::convertProgressCallback, this, boost::placeholders::_1));

    dialogInit(); // 初始化
}
ConvertSelectView::~ConvertSelectView() {
    printf("ConvertSelectView -> destructor\n");
    // to do sth.
}

// 转换进度回调
void ConvertSelectView::convertProgressCallback(float percent) {
    pdw->setProgress(percent);
}

// Dialog Init
void ConvertSelectView::dialogInit() {
    printf("ConvertSelectView -> dialogInit()\n");

    // 文件类型
    QStringList fileTypes;
    fileTypes << "建图文件" << "拼接文件" << "合并文件" << "着色文件";
    ui_->file_type_comboBox->addItems(fileTypes);

    // 默认为“建图文件”
    fileTypeChanged(0);
    connect(ui_->file_type_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(fileTypeChanged(int)));

    // 点云文件
    connect(ui_->ply_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(plyFileChanged(int)));

    // 点云格式
    QStringList types;
    types << "las" << "pcd"/* << "e57"*/;
    ui_->convert_type_comboBox->addItems(types);

    setGeometry((QApplication::desktop()->width() - 311) / 2,
                (QApplication::desktop()->height() - 241) / 2, 311, 241);

    // Point Intensity
    ui_->point_size_slider->setMinimum(0);
    ui_->point_size_slider->setMaximum(4);
    connect(ui_->point_size_slider, SIGNAL(valueChanged(int)), this, SLOT(setConvertPointIntensity(int)));

    // 确定、取消
    connect(ui_->ok_btn, SIGNAL(clicked()), this, SLOT(startConvert()));
    connect(ui_->cancel_btn, SIGNAL(clicked()), this, SLOT(quitConvert()));
}

// 文件类型 改变监听
void ConvertSelectView::fileTypeChanged(int idx) {
    printf("ConvertSelectView -> fileTypeChanged()\n");

    string tmp_path;
    switch(idx) {
        case 0: tmp_path = path + "/scene_maps/";       break;
        case 1: tmp_path = path + "/registrated_maps/"; break;
        case 2: tmp_path = path + "/merged_maps/";      break;
        case 3: tmp_path = path + "/colored_maps/";     break;

        default:
            tmp_path = path + "/scene_maps/";
            break;
    }

    file_path = tmp_path;
    ui_->ply_comboBox->clear();

    QStringList plys = convert_select_controller->getPlys(tmp_path);

    if(plys.size() != 0) { ui_->ply_comboBox->addItems(plys); }
    else { QMessageBox::information(this, "文件类型", "该文件类型下暂无点云文件！"); }
}
// ply文件 选择监听
void ConvertSelectView::plyFileChanged(int idx) {
    printf("ConvertSelectView -> plyFileChanged()\n");
}
// Point Intensity 改变监听
void ConvertSelectView::setConvertPointIntensity(int v) {
    //printf("ConvertSelectView -> setConvertPointIntensity()\n");

    string pointSize;
    switch(v) {
        case 0: pointSize = "20"; break;
        case 1: pointSize = "25"; break;
        case 2: pointSize = "30"; break;
        case 3: pointSize = "35"; break;
        case 4: pointSize = "40"; break;
    }
    ui_->point_size_value->setText(QString::fromStdString(pointSize));
}
/// 开始、放弃转换
// 开始转换
void ConvertSelectView::startConvert() {
    printf("ConvertSelectView -> startConvert()\n");

    string name = ui_->ply_comboBox->currentText().toStdString();
    if(name == "") {
        QMessageBox::information(this, "格式转换", "请选择需要转换的点云文件！");
        return;
    }

    // 进度提示
    pdw = new ProgressDialogWidget(this, "点云格式转换", "正在转换格式，请稍候...");
    pdw->setAttribute(Qt::WA_DeleteOnClose);

    // 转换线程
    progress_thread = boost::thread(boost::bind(
          &ConvertSelectView::doConvert,
          this,
          file_path + name,
          ui_->convert_type_comboBox->currentText().toStdString(),
          ui_->point_size_value->text().toStdString()));

    pdw->exec();
    progress_thread.join();

    if(isConvertSuccess) {
        isConvertSuccess = false;
        QMessageBox::information(this, "格式转换", "格式转换完成！");
    } else {
        QMessageBox::information(this, "格式转换", "格式转换失败！");
    }
}
// 放弃转换
void ConvertSelectView::quitConvert() {
    printf("ConvertSelectView -> quitConvert()\n");
    this->close();
}

// 转换
void ConvertSelectView::doConvert(string file_path, string type, string pointSize) {
    printf("ConvertSelectView -> doConvert()\n");

    if(convert_select_controller->convert(file_path, type, pointSize)) {
        isConvertSuccess = true;
    }
    pdw->setProgress(100.0);
    pdw->close();
}

/// 窗口关闭事件
void ConvertSelectView::closeEvent(QCloseEvent *e) {
    printf("ConvertSelectView -> close()\n");
    QWidget::closeEvent(e);
}
