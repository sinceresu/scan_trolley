#ifndef CONVERT_SELECT_VIEW_HPP
#define CONVERT_SELECT_VIEW_HPP

#include "progress_dialog_widget.hpp"

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>

#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>

#include <vector>
#include <string>

#include "string.h"

using namespace Qt;
using namespace std;

namespace Ui { class ConvertSelectView; }

namespace yd_scanner_ui {
    class ConvertSelectController;

    class ConvertSelectView : public QDialog {
        Q_OBJECT
            public:
                ConvertSelectView(string project_path, QWidget *parent = 0);
                ~ConvertSelectView();

            private:
                /// Variables
                Ui::ConvertSelectView *ui_;
                ProgressDialogWidget *pdw;

                // ConvertSelectController
                boost::shared_ptr<ConvertSelectController> convert_select_controller;

                bool isConvertSuccess;
                boost::thread progress_thread;

                string path;
                string file_path;

                /// Fun
                void convertProgressCallback(float percent);
                void doConvert(string file_path, string type, string pointSize);

                // 窗口初始化
                void dialogInit();

                /// Event
                void closeEvent(QCloseEvent *event);

            Q_SIGNALS:
                void currentIndexChanged(int);
                void valueChanged(int);

            private Q_SLOTS:
                void fileTypeChanged(int);
                void plyFileChanged(int);
                void setConvertPointIntensity(int);

                void startConvert();
                void quitConvert();
    };
}
#endif
