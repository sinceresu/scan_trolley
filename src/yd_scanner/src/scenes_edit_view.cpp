﻿#include "scenes_edit_view.hpp"
#include "ui_scenes_edit_view.h"

#include "scenes_edit_controller.hpp"

// Qt5
#include <QtGui/QList>
#include <QtCore/QMetaType>

#include <iostream>
#include <fstream>
#include <thread>

#include <pthread.h>

#include "utils.h"

using namespace std;
using namespace yd_scanner_ui;

/// Constructor & Destructor
// Constructor
ScenesEditView::ScenesEditView(int *rs, int *cs, string *sceneIdIndexMapInfo, QWidget *parent) :
    QDialog(parent),
    ui_(new Ui::ScenesEditView),
    sceneROWs(rs),
    sceneCOLs(cs),
    sceneIdIndexMapInfoStr(sceneIdIndexMapInfo) {

    printf("ScenesEditView -> constructor\n");

    // 窗口关闭后，强制执行析构函数
    setAttribute(Qt::WA_DeleteOnClose);

    this->setFixedSize(471, 570);

    ui_->setupUi(this);

    scenes_edit_controller = unique_ptr<ScenesEditController>(new ScenesEditController());
    scenes_edit_controller->setSaveProgressCallback(boost::bind(&ScenesEditView::saveProgressCallback, this, boost::placeholders::_1));

    dialogInit(); // 初始化
}
ScenesEditView::ScenesEditView(int rs, int cs, vector<string> idVec, QWidget *parent) :
    QDialog(parent), ui_(new Ui::ScenesEditView) {

    printf("ScenesEditView -> constructor()\n");

    // 窗口关闭后，强制执行析构函数
    setAttribute(Qt::WA_DeleteOnClose);

    this->setFixedSize(471, 270);
    ui_->setupUi(this);
    ui_->tool_widget->setVisible(false);

    int count = 1;
    int k     = 0;
    int m     = 1;

    for(int i = 0; i < rs; i++) {
        for(int j = 0; j < cs; j++) {
            QPushButton *btn = new QPushButton();

            btn->setWhatsThis(QString::fromStdString(to_string(count)));
            btn->setFixedSize(449 / cs, 449 / rs);
            btn->setFlat(true);

            if(to_string(count) == idVec[k]) {
                btn->setStyleSheet("background-color: rgb(244, 150, 111); border: 1px solid red");
                btn->setText(QString::fromStdString(to_string(m)));
                k++;
                m++;
            } else btn->setStyleSheet("background-color: gray; border: 1px solid red");

            ui_->scenes_gridLayout->addWidget(btn, i, j);
            count++;
        }
    }
}
// Destructor
ScenesEditView::~ScenesEditView() {
    printf("ScenesEditView -> destructor\n");
    // clear
}

/// Funs
// Save Level Progress Callback
void ScenesEditView::saveProgressCallback(float percent) {
    pdw->setProgress(percent);
}
// Init UI
void ScenesEditView::uiInit() {
    printf("ScenesEditView -> uiInit()\n");

    string p_ss = "border: 1px solid #808080; border-radius: 1px;";
    string c_ss = "border: 0.5px solid #808080; border-radius: 3px;";

    /// tool
    ui_->tool_widget->setFixedSize(450, 100);
    ui_->tool_widget->setStyleSheet(QString::fromStdString("QWidget#tool_widget{" + p_ss + "}"));
    // level
    ui_->level_widget->setFixedSize(450, 50);
    ui_->level_widget->setStyleSheet(QString::fromStdString("QWidget#level_widget{" + c_ss + "}"));
    // btn
    ui_->btn_widget->setFixedSize(450, 50);
    ui_->btn_widget->setStyleSheet(QString::fromStdString("QWidget#btn_widget{" + c_ss + "}"));

    /// grid
    ui_->grid_widget->setFixedSize(450, 450);
    ui_->grid_widget->setStyleSheet(QString::fromStdString("QWidget#grid_widget{" + p_ss + "}"));

    /// level
    QStringList levelLst;
    levelLst << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8";
    ui_->row_comboBox->addItems(levelLst);
    ui_->col_comboBox->addItems(levelLst);

    ui_->row_comboBox->setCurrentIndex(0);
    ui_->col_comboBox->setCurrentIndex(0);

    /// slot
    connect(ui_->set_btn, SIGNAL(clicked()), SLOT(setLevels()));
    connect(ui_->reset_btn, SIGNAL(clicked()), SLOT(reset()));
    connect(ui_->ok_btn, SIGNAL(clicked()), SLOT(completed()));

    ui_->reset_btn->setEnabled(false);
    ui_->ok_btn->setEnabled(false);

    setGeometry((QApplication::desktop()->width() - 471) / 2, (QApplication::desktop()->height() - 570) / 2, 471, 570);
}
// Init Dialog
void ScenesEditView::dialogInit() {
    printf("ScenesEditView -> dialogInit()\n");
    uiInit();
}
// Init SceneIdCoordinate
void ScenesEditView::sceneIdCoordinateInit(int rs, int cs) {
    printf("ScenesEditView -> sceneIdCoordinateInit()\n");

    int val = 1;
    for(int i = 1; i <= rs; i++) {
        for(int j = 1; j <= cs; j++) {
            map<int, int> tmp_m;

            tmp_m[i] = j;
            sceneIdCoordinateMap[tmp_m] = val;
            val++;
        }
    }

    /* Test
    map<map<int, int>, int>::iterator m_it;
    map<int, int>::iterator it;

    int value;
    for(m_it = sceneIdCoordinateMap.begin(); m_it != sceneIdCoordinateMap.end(); m_it++) {
        map<int, int> t_m = m_it->first;

        // (3,4)
        for(it = t_m.begin(); it != t_m.end(); it++) {
            cout << "(" << it->first << ", " << it->second << ")";

            if(it->first == 3 && it->second == 4) {
                value = sceneIdCoordinateMap[t_m];
            }
        }
        cout << "->" << m_it->second << endl;
    }

    cout << "**********" << endl;
    cout << "(3, 4)->" << value << endl;
    */
}

/// SLOTS
// SetLevels
void ScenesEditView::setLevels() {
    printf("ScenesEditView -> setLevels()\n");

    Rs = ui_->row_comboBox->currentIndex() + 1;
    Cs = ui_->col_comboBox->currentIndex() + 1;

    int count = 1;
    for(int i = 0; i < Rs; i++) {
        for(int j = 0; j < Cs; j++) {
            QPushButton *btn = new QPushButton();

            btn->setWhatsThis(QString::fromStdString(to_string(count)));
            btn->setFixedSize(449 / Cs, 449 / Rs);
            btn->setFlat(true);
            btn->setStyleSheet("background-color: gray; border: 1px solid red");

            connect(btn, SIGNAL(clicked()), SLOT(sceneChecked()));
            scenesButtonLst.push_back(btn);

            ui_->scenes_gridLayout->addWidget(btn, i, j);
            count++;
        }
    }

    // Init SceneIdCoordinate
    sceneIdCoordinateInit(Rs, Cs);

    ui_->set_btn->setEnabled(false);
    ui_->reset_btn->setEnabled(true);
    ui_->ok_btn->setEnabled(true);
}
// Reset
void ScenesEditView::reset() {
    printf("ScenesEditView -> reset()\n");

    // Clear GridLayout
    QLayoutItem *child;
    while((child = ui_->scenes_gridLayout->takeAt(0)) != 0) {
        ui_->scenes_gridLayout->removeWidget(child->widget());
        child->widget()->setParent(0);
        delete child;
    }
    ui_->scenes_gridLayout->update();

    // Clear ScenesButtonLst
    foreach(QPushButton *btn, scenesButtonLst) {
        if(btn) {
            scenesButtonLst.removeOne(btn);
            delete btn;
            btn = nullptr;
        }
    }
    // Clear checkedScenes & sceneIdIndexMap
    checkedScenes.clear();
    sceneIdIndexMap.clear();

    ui_->set_btn->setEnabled(true);
    ui_->reset_btn->setEnabled(false);
    ui_->ok_btn->setEnabled(false);
}
// Completed
void ScenesEditView::completed() {
    printf("ScenesEditView -> completed()\n");

    // TODO: Single Scene

    pdw = new ProgressDialogWidget(this, "场景规划", "正在保存，请稍候...");
    pdw->setAttribute(Qt::WA_DeleteOnClose);
    save_thread = boost::thread(boost::bind(&ScenesEditView::doSave, this));

    pdw->exec();
    save_thread.join();

    if(isSaved) {
        isSaved = false;
        QMessageBox::StandardButton result = QMessageBox::information(this, "场景规划", "保存规划成功！");

        if(result == QMessageBox::Ok) this->close();
    } else {
        QMessageBox::information(this, "场景规划", "保存规划失败，请检查场景是否存在不相连！");
        // SetText("");
        for(int i = 0; i < ui_->scenes_gridLayout->count(); i++) {
            QPushButton *btn = (QPushButton *)ui_->scenes_gridLayout->itemAt(i)->widget();
            if(!btn->isFlat()) btn->setText("");
        }
    }
}
// SceneChecked
void ScenesEditView::sceneChecked() {
    QPushButton *btn          = (QPushButton*)sender();
    int currentCheckedScendId = atoi(btn->whatsThis().toStdString().c_str());
    //cout << "scene_default_id: " << currentCheckedScendId << endl;

    if(scenesButtonLst[currentCheckedScendId - 1]->isFlat()) {
        scenesButtonLst[currentCheckedScendId - 1]->setFlat(false);
        btn->setStyleSheet("background-color: rgb(244, 150, 111); border: 1px solid red");

        //cout << "add: " << currentCheckedScendId << endl;
        checkedScenes.push_back(currentCheckedScendId);
    } else {
        scenesButtonLst[currentCheckedScendId - 1]->setFlat(true);
        btn->setStyleSheet("background-color: gray; border: 1px solid red");

        //cout << "remove: " << checkedScenes.indexOf(currentCheckedScendId) << endl;
        checkedScenes.removeAt(checkedScenes.indexOf(currentCheckedScendId));
    }
}
// Do Save
void ScenesEditView::doSave() {
    printf("ScenesEditView -> doSave()\n");

    // Make Index
    int index = 1;
    vector<int> indexVec;

    for(int i = 0; i < ui_->scenes_gridLayout->count(); i++) {
        QPushButton *btn = (QPushButton *)ui_->scenes_gridLayout->itemAt(i)->widget();

        if(!btn->isFlat()) {
            indexVec.push_back(index);
            btn->setText(QString::fromStdString(to_string(index)));
            index++;
        }
    }
    pdw->setProgress(15.0);

    // Init SceneIdIndexMap
    qSort(checkedScenes.begin(), checkedScenes.end());
    for(int i = 0; i < checkedScenes.size(); i++) sceneIdIndexMap[indexVec[i]] = checkedScenes[i];

    map<int, int>::iterator it;
    it = sceneIdIndexMap.begin();
    while(it != sceneIdIndexMap.end()) {
        cout << "[index: " << it->first << ", id: " << it->second << "]" << endl;
        it++;
    }
    pdw->setProgress(30.0);

    // Check Connexity
    if(scenes_edit_controller->checkConnexity(sceneIdIndexMap, Rs, Cs)) {
        isSaved = true;
        pdw->setProgress(60.0);

        map<int, int>::iterator it = sceneIdIndexMap.begin();
        vector<string> sceneIdIndexMapStrInfo;
        while(it != sceneIdIndexMap.end()) {
            sceneIdIndexMapStrInfo.push_back(to_string(it->first) + "-" + to_string(it->second));
            it++;
        }
        pdw->setProgress(90.0);

        // Save Levels
        *sceneROWs = Rs;
        *sceneCOLs = Cs;

        string scene_id_index_map_info_str = "";
        for(string s : sceneIdIndexMapStrInfo)
            scene_id_index_map_info_str += s + " ";
        boost::algorithm::trim(scene_id_index_map_info_str);

        *sceneIdIndexMapInfoStr = scene_id_index_map_info_str;
    }

    sleep(1);
    pdw->setProgress(100.0);
    pdw->close();
}

/// Dialog Close Event
void ScenesEditView::closeEvent(QCloseEvent *event) {
    printf("ScenesEditView -> close()\n");
    QWidget::closeEvent(event);
}
