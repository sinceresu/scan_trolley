#ifndef MAIN_VIEW_HPP
#define MAIN_VIEW_HPP

#include "toast_tool.hpp"
#include "notification_monitor.hpp"

//Qt5
#include <QTimer>
#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QtOpenGL/QtOpenGL>

#include <QThread>
//#include <QtMultimedia>
//#include <QtMultimedia/QMediaPlayer>
//#include <QUrl>

#ifndef WITHOUT_ROS
    #include "qnode.hpp"
#endif

#include <string>
#include <vector>
#include <map>

#include <thread>
#include <jsoncpp/json/json.h>

using namespace Qt;
using namespace std;

namespace Ui { class MainView; }
namespace yd_scanner_ui {
    class FlowLayout;
    class DeviceMonitorView;
    class LuaConfigView;
    class ScenesEditView;

    class MainView : public QDialog {
        private:
            enum class Scenario { OUTDOOR = 0, INDOOR  = 1, };

        Q_OBJECT
            public:
                // Constructor & Destructor
                MainView(int argc, char** argv, string wp, QWidget *parent = 0);
                ~MainView();

            private Q_SLOTS:
                void showProjectData();
                void showProjectFolderDialog();
                void startProjectWindow();

                void showProjectImportDialog();
                void showProjectOutputDialog();
                void showProjectRemoveDialog();

                void exportProject();
                void closeExportDialog();
                void cancelExportProject();

                void luaConfigDialogInit();    // 扫描参数配置
                void scenesConfigDialogInit(); // 扫描场景规划配置

                void trigerDevicesLabelMenu();

                //void showNotifications();

            private:
                //NotificationMonitor *nm;
                //QMediaPlayer *player;

                QToolButton *createImageButton(const QPixmap &icon, const string& text, const string& id);

                // Fun
                void devicesStatusDialogInit();
                // Event
                bool eventFilter(QObject *target, QEvent *e);
                void closeEvent(QCloseEvent *event);
                bool setupProjectDirectories(const string & project_dir, Scenario scenario = Scenario::INDOOR);
                void saveProjectsToConfig(map<string, string> projects) ;
                void displaySelectedProject(string scene_id) ;
                void removeSelectedProject(string scene_id) ;

                map<string, string> getProjectsFromConfig() ;

                Ui::MainView *ui_;

                /// Variables
                string work_space_path;
                map<string, string> projects_;
                string current_project_id;

                bool isShowProjectData = false;
                string sceneIdIndexMapInfo = "";

                /// UI
                FlowLayout *flow;

                // LuaConfigView
                LuaConfigView *luaConfigView;

                // ScenesEditView
                ScenesEditView *scenesEditView;

                // DevicesStatus Dialog
                DeviceMonitorView *devicesStatusDialog;
                // Project Export
                QDialog *outputDialog;
                QProgressDialog *exportProjectProgressDialog;

                #ifndef WITHOUT_ROS
                    QNode qnode;
                #endif
    };
}
#endif
