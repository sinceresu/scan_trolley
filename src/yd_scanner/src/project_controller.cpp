﻿#include "project_controller.hpp"
#include "scan_controller.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <thread>

#include <stdlib.h>
#include <pthread.h>
#include <sys/stat.h>

#include <boost/filesystem.hpp>

#include "time.h"
#include "utils.h"

// PCL
#include<pcl/io/io.h>
#include<pcl/io/pcd_io.h>
#include<pcl/point_types.h>
#include<pcl/common/common.h>

#include <ros/ros.h>

using namespace boost::filesystem;

namespace fs = boost::filesystem;
namespace yd_scanner_ui {
    namespace { }

    // Constructor & Destructor
    ProjectController::ProjectController(string project_name, string project_path) :
        project_name_(project_name), project_path_(project_path) {

        ROS_INFO("ProjectController -> constructor");

        Json::Value root = readJsonInfo(project_path + "/project.json");

        string idIndexInfo = root["sceneIdIndexMapInfo"].asString();
        scene_rows = root["scene_rows"].asInt();
        scene_cols = root["scene_cols"].asInt();

        vector<string> tmp_vec = split(idIndexInfo, " ");
        for(string v : tmp_vec) {
            vector<string> vec = split(v, "-");
            sceneIdIndexMap[atoi(vec[0].c_str())] = atoi(vec[1].c_str());
        }

        check_connexity_ptr = unique_ptr<CheckConnexity>(new CheckConnexity(sceneIdIndexMap, scene_rows, scene_cols));
    }
    ProjectController::~ProjectController() {
        ROS_INFO("ProjectController -> destructor");
    }

    // Strart-Stop Build
    bool ProjectController::startBuild(const std::string& scene_id) {
        ROS_INFO("ProjectController -> startBuild(%s)", scene_id.c_str());

        InitializeBuildMap();
        work_thread_ = boost::thread(boost::bind(&ProjectController::doBuild, this, scene_id));

        return true;
    }
    void ProjectController::stopBuild() {
        ROS_INFO("ProjectController -> stopBuild");
        map_builder_->Stop();
    }

    // Start Registrate
    bool ProjectController::startRegistrate(const std::string& scene_id) {
        ROS_INFO("ProjectController -> startRegistrate(%s)", scene_id.c_str());
        work_thread_ = boost::thread(boost::bind(&ProjectController::doRegistrate, this, scene_id));
        return true;
    }

    // Start Merge
    //bool ProjectController::startMerge(const std::string& scene_id) {
    bool ProjectController::startMerge(const std::string& l_scene_id, const std::string& r_scene_id) {
        InitializeMergeMap();
        InitializeColorizeMap();

        merge_ratio_    = 0.5f;
        colorize_ratio_ = 0.5f;

        //work_thread_ = boost::thread(boost::bind(&ProjectController::doMerge, this, scene_id));
        work_thread_ = boost::thread(boost::bind(&ProjectController::doMerge, this, l_scene_id, r_scene_id));

        return true;

        /*
        if(!IsSceneScanned(scene_id)) {
            status_info_ = "scene has not been scanned!";
            return false;
        }
        if(!IsReadyToMerge(scene_id)) {
            status_info_ = "scene is not ready for merge!";
            return false;
        }

        ROS_INFO("merge scene files!");

        InitializeMergeMap();
        InitializeColorizeMap();

        merge_ratio_    = 0.5f;
        colorize_ratio_ = 0.5f;

        work_thread_ = boost::thread(boost::bind(&ProjectController::doMerge, this, scene_id));

        return true;
        */
    }
    bool ProjectController::startSelfMerge(const std::string& scene_id) {
        InitializeMergeMap();
        InitializeColorizeMap();

        merge_ratio_    = 0.5f;
        colorize_ratio_ = 0.5f;

        work_thread_ = boost::thread(boost::bind(&ProjectController::doSelfMerge, this, scene_id));
        return true;
    }
    void ProjectController::stopMerge() {
        ROS_INFO("ProjectController -> stopBuild");
        map_builder_->Stop();
    }

    // Start Supplement
    bool ProjectController::startSupplement(const std::string& scene_id) {
        ROS_INFO("ProjectController -> scanExtendBtnClickEvent()");

        // supplement if the scene is built.
        if(!IsSceneBuilt(scene_id)) {
            //rebuild scene map
            InitializeBuildMap();
            work_thread_ = boost::thread(boost::bind(&ProjectController::doSupplement, this, scene_id));
        }
        return true;
    }

    std::string ProjectController::getStatusInfo() { return status_info_; }

    bool ProjectController::IsSceneScanned(std::string scene_id) {
        return exists(GetBagDirectory() + scene_id + ".bag");
    }
    bool ProjectController::IsSceneRegistrated(std::string scene_id) {
        std::string scene_map = GetRegistratedMapDirectory() + scene_id + ".ply";
        return exists(scene_map);
    }

    bool ProjectController::IsStationMapReady() {
        return exists(project_path_  + "/" + project_name_ + ".ply") ;
    }
    bool ProjectController::IsReadyToRegistrate(std::string scene_id) {
        // Clear
        vector<int>().swap(registrateSceneIdIntersection);

        if(!IsFirstScene(scene_id)) {
            vector<int> adjacentNumsVec     = check_connexity_ptr->getAdjacentConnexityNums(sceneIdIndexMap[atoi(scene_id.c_str())]);
            vector<int> registratedSceneIds = GetRegistratedSceneIds();

            // Only One Scene
            if(adjacentNumsVec.size() == 0) return true;

            int count = 0;
            vector<int> adjacentIdsVec;

            for(auto it = sceneIdIndexMap.begin(); it != sceneIdIndexMap.end(); it++) {
                cout << it->second << " " << adjacentNumsVec[count] << endl;

                if(it->second == adjacentNumsVec[count]) {
                    adjacentIdsVec.push_back(it->first);
                    count++;
                }
            }
            cout << "adjacentIds: ";
            for(int id : adjacentIdsVec) cout << id << " ";
            cout << endl;

            set_intersection(adjacentIdsVec.begin(), adjacentIdsVec.end(),
                             registratedSceneIds.begin(), registratedSceneIds.end(),
                             back_inserter(registrateSceneIdIntersection));

            if(registrateSceneIdIntersection.size() == 0) return false;
        }

        /*
        int scene_index = atoi(scene_id.c_str());

        for(size_t i = 0; i < scene_index; i++) {
            if(!IsSceneScanned(to_string(i + 1))) return false;
        }
        */

        return true;
    }
    bool ProjectController::IsReadyToMerge(std::string scene_id) {
        int scene_index = atoi(scene_id.c_str());
        for(size_t i = 0; i < scene_index; i++) {
            if(!IsSceneRegistrated(to_string(i + 1))) return false;
        }
        return true;
    }
    bool ProjectController::IsReadyForRegistrate(std::string scene_id) {
        ROS_INFO("ProjectController -> IsReadyForRegistrate()");

        if(registrateSceneIdIntersection.size() == 0) return true;
        else {
            return exists(GetRegistratedMapDirectory() + to_string(registrateSceneIdIntersection[0]) + ".ply")
                   &&
                   exists(GetRegistratedMapDirectory() + to_string(registrateSceneIdIntersection[0]) + ".pbstream");
        }

        /*
        int scene_num = atoi(scene_id.c_str());
        if(scene_num == 1 ) { return true; }

        return exists(GetRegistratedMapDirectory() + to_string(scene_num - 1) + ".ply")
               &&
               exists(GetRegistratedMapDirectory() + to_string(scene_num - 1) + ".pbstream");
        */
    }

    bool ProjectController::IsSceneBuilt(std::string scene_id) {
        return exists(GetSceneMapDirectory() + scene_id+ ".ply")
               &&
               exists(GetSceneMapDirectory() + scene_id + ".pbstream");
    }
    bool ProjectController::IsFirstScene(std::string scene_id) {
        QDir dir(QString::fromStdString(GetRegistratedMapDirectory()));
        QStringList nameFilters;
        nameFilters << "*.ply";

        dir.setNameFilters(nameFilters);
        QList<QFileInfo> *files = new QList<QFileInfo>(dir.entryInfoList(nameFilters));

        if(files->count() != 0) return false;

        return true;

        //return scene_id == "1";
    }

    // Merge Check Connexity
    bool ProjectController::checkConnexity(string l_scene_id, string r_scene_id, int scene_rows_, int scene_cols_) {
        vector<int> scene_id_lst;

        if(l_scene_id.find("-") != string::npos) {
            vector<string> tmp = split(l_scene_id, "-");
            for(string id : tmp) scene_id_lst.push_back(atoi(id.c_str()));
        }
        scene_id_lst.push_back(atoi(l_scene_id.c_str()));

        if(r_scene_id.find("-") != string::npos) {
            vector<string> tmp = split(r_scene_id, "-");
            for(string id : tmp) scene_id_lst.push_back(atoi(id.c_str()));
        }
        scene_id_lst.push_back(atoi(r_scene_id.c_str()));

        qSort(scene_id_lst.begin(), scene_id_lst.end());

        // Get New idIndexMap
        int count = 0;
        map<int, int> idIndexMap;

        for(auto it = sceneIdIndexMap.begin(); it != sceneIdIndexMap.end(); it++) {
            if(it->first == scene_id_lst[count]) idIndexMap[it->first] = it->second;
            count++;
        }

        merge_check_connexity_ptr = unique_ptr<CheckConnexity>(new CheckConnexity(idIndexMap, scene_rows_, scene_cols_));

        return merge_check_connexity_ptr->check();
    }

    // Get Registrated Scene Ids
    std::vector<int> ProjectController::GetRegistratedSceneIds() {
        ROS_INFO("ProjectController -> GetRegistratedSceneIds()");

        QDir dir(QString::fromStdString(GetRegistratedMapDirectory()));
        QStringList nameFilters;
        nameFilters << "*.ply";

        dir.setNameFilters(nameFilters);
        QList<QFileInfo> *files = new QList<QFileInfo>(dir.entryInfoList(nameFilters));

        vector<int> registratedSceneIds;
        for(int i = 0; i < files->count(); i++) {
            string fileName = files->at(i).fileName().toStdString();
            vector<string> fileNameVec = split(fileName, ".");

            registratedSceneIds.push_back(atoi(fileNameVec[0].c_str()));
        }
        qSort(registratedSceneIds.begin(), registratedSceneIds.end());

        return registratedSceneIds;
    }

    void ProjectController::InitializeBuildMap() {
        ROS_INFO("ProjectController -> InitializeBuildMap()");

        MapBuildInterface::BuildParam param;

        param.urdf_filename                = project_path_ + "/urdf/two_vlp_rig.urdf";
        param.configuration_directory      = project_path_ + "/configuration_files";
        param.map_configuration_basename   = "two_vlp_outdoor.lua";
        param.build_configuration_basename = "map_build.lua";
        param.write_configuration_basename = "build_writer.lua";

        ROS_INFO("create Map Build interface!");

        map_builder_ = CreateMapBuilder();
        map_builder_->SetParam(param);
        map_builder_->SetProgressCallback(std::bind(&ProjectController::BuildProgressCallback, this, std::placeholders::_1, std::placeholders::_2));
    }
    void ProjectController::InitializeRegistrateMap() {
        ROS_INFO("create Map Build interface!");

        map_registrater_ = CreateMapRegistrater();

        MapRegistrateInterface::RegistrateParam param;
        param.configuration_directory           = project_path_ + "/configuration_files";
        param.registrate_configuration_basename = "map_registrate.lua";
        map_registrater_->SetParam(param);
    }
    void ProjectController::InitializeMergeMap() {
        ROS_INFO("ProjectController -> InitializeMergeMap()");

        MapBuildInterface::BuildParam param;

        param.urdf_filename                = project_path_ + "/urdf/two_vlp_rig.urdf";
        param.configuration_directory      = project_path_ + "/configuration_files";
        param.map_configuration_basename   = "two_vlp_outdoor.lua";
        param.build_configuration_basename = "map_build.lua";
        param.write_configuration_basename = "merge_writer.lua";

        ROS_INFO("create Map Build interface!");

        map_builder_ = CreateMapBuilder();
        map_builder_->SetParam(param);
        map_builder_->SetProgressCallback(std::bind(&ProjectController::BuildProgressCallback, this, std::placeholders::_1, std::placeholders::_2));
    }
    void ProjectController::InitializeColorizeMap() {
        ROS_INFO("ProjectController -> InitializeRegistrateMap()");

        MapColorizeInterface::ColorizeParam param;

        param.urdf_filename                   = project_path_ + "/urdf/two_vlp_rig.urdf";
        param.configuration_directory         = project_path_ + "/configuration_files";
        param.colorize_configuration_basename = "map_colorize.lua";
        param.write_configuration_basename    = "colorize_writer.lua";

        ROS_INFO("create Map Colorize interface!");

        map_colorizer_ = CreateMapColorizer();
        map_colorizer_->SetParam(param);
        map_colorizer_->SetProgressCallback(std::bind(&ProjectController::ColorizeProgressCallback, this, std::placeholders::_1));
    }

    void ProjectController::BuildProgressCallback(MapBuildInterface::ProcessType process_type,
                                                  float percentage) {
        ROS_INFO("BuildProgressCallback progress %f", percentage);

        if(progress_call_back_) {
            switch (process_type) {
                case MapBuildInterface::ProcessType::kBuildMap :
                    {
                        progress_call_back_(ProcessStatus::kBuildMap, percentage);
                        break;
                    }
                case MapBuildInterface::ProcessType::kWriteMap :
                    {
                        progress_call_back_(ProcessStatus::kWriteMap, percentage);
                        break;
                    }
                case MapBuildInterface::ProcessType::kMergeMap :
                    {
                        progress_call_back_(ProcessStatus::kMergeMap, percentage);
                        break;
                    }
                case MapBuildInterface::ProcessType::kPostProcess :
                    {
                        progress_call_back_(ProcessStatus::kPostProcess, percentage);
                        break;
                    }
                default : break;
            }
        }
    }
    void ProjectController::ColorizeProgressCallback(float percentage) {
        if(progress_call_back_) {
            ROS_INFO("ProjectController::ColorizeProgressCallback: %f", percentage);
            progress_call_back_(ProcessStatus::kColorizeMap, percentage);
        }
    }

    void ProjectController::doBuild(string scene_id) {
        vector<string> scene_bags  = getSceneBags(scene_id);
        std::string state_filepath = "";

        int result = map_builder_->BuildMaps(state_filepath, scene_bags,
                                             GetSceneMapDirectory(), scene_id);
        ROS_INFO("Build_RESULT: %d", result);

        if(progress_call_back_) progress_call_back_(ProcessStatus::kComplete, 100.f);
    }
    // Do Registrate
    void ProjectController::doRegistrate(string scene_id) {
        if(IsFirstScene(scene_id)) {
            // first build map of
            string src_file = GetSceneMapDirectory()  + scene_id + ".ply";
            string dst_file = GetRegistratedMapDirectory()  + scene_id + ".ply";

            if(!exists(src_file)) status_info_ = "scene has not been built!";

            fs::copy_file(fs::path(src_file), fs::path(dst_file), fs::copy_option::overwrite_if_exists);

            src_file = GetSceneMapDirectory()  + scene_id + ".pbstream";
            dst_file = GetRegistratedMapDirectory()  + scene_id + ".pbstream";

            fs::copy_file(fs::path(src_file), fs::path(dst_file), fs::copy_option::overwrite_if_exists);
        } else {
            ROS_INFO("Registrate Scene Files!");
            if(progress_call_back_) progress_call_back_(ProcessStatus::kRegistrate, 0.f);

            InitializeRegistrateMap();

            vector<string> pcd_files;
            //int last_scene_id = atoi(scene_id.c_str()) - 1;
            //pcd_files.push_back(GetRegistratedMapDirectory() + to_string(last_scene_id) + ".ply");

            pcd_files.push_back(GetRegistratedMapDirectory() + to_string(registrateSceneIdIntersection[0]) + ".ply");
            pcd_files.push_back(GetSceneMapDirectory() + scene_id + ".ply");

            vector<string> pbstream_files;
            //pbstream_files.push_back(GetRegistratedMapDirectory() + to_string(last_scene_id) + ".pbstream");
            pbstream_files.push_back(GetRegistratedMapDirectory() + to_string(registrateSceneIdIntersection[0]) + ".pbstream");
            pbstream_files.push_back(GetSceneMapDirectory() + scene_id + ".pbstream");

            if(progress_call_back_) progress_call_back_(ProcessStatus::kRegistrate, 10.f);

            // TODO: test
            int result = map_registrater_->RegistrateMaps(pbstream_files, pcd_files, GetRegistratedMapDirectory());

            if(progress_call_back_) progress_call_back_(ProcessStatus::kRegistrate, 100.f);
        }

        if(progress_call_back_) progress_call_back_(ProcessStatus::kComplete, 100.f);

        // Clear Registrate Scene Intersection Vec
        vector<int>().swap(registrateSceneIdIntersection);
    }
    // Do Merge
    //void ProjectController::doMerge(string scene_id) {
    void ProjectController::doMerge(string l_scene_id, string r_scene_id) {
        int result = mergeMaps(l_scene_id, r_scene_id);
        if(progress_call_back_) progress_call_back_(ProcessStatus::kColorizeMap, 0.0f);

        colorizeMap(l_scene_id, r_scene_id);
        if(progress_call_back_) progress_call_back_(ProcessStatus::kComplete, 100.0f);

        /*
        int result = mergeMaps(scene_id);

        if(progress_call_back_) progress_call_back_(ProcessStatus::kColorizeMap, 0.0f);
        colorizeMap(scene_id);
        if(progress_call_back_) progress_call_back_(ProcessStatus::kComplete, 100.0f);
        */
    }
    void ProjectController::doSelfMerge(string scene_id) {
        int result = selfMergeMaps(scene_id);
        if(progress_call_back_) progress_call_back_(ProcessStatus::kColorizeMap, 0.0f);

        selfColorizeMap(scene_id);
        if(progress_call_back_) progress_call_back_(ProcessStatus::kComplete, 100.0f);
    }
    // Merge Maps
    //int ProjectController::mergeMaps(string scene_id) {
    int ProjectController::mergeMaps(string l_scene_id, string r_scene_id) {
        vector<string> bags_files;
        vector<string> pbstream_files;

        std::string merge_file_name;

        if(l_scene_id.find("-") != string::npos) {
            vector<string> tmp = split(l_scene_id, "-");

            for(string id : tmp) {
                bags_files.push_back(GetBagDirectory() + id + ".bag");
                pbstream_files.push_back(GetRegistratedMapDirectory() + id + ".pbstream");
                /*
                vector<string> scene_bags = getSceneBags(id);
                bags_files.insert(bags_files.end(), scene_bags.begin(), scene_bags.end());

                std::stringstream pbstream_filepath;
                pbstream_filepath << GetRegistratedMapDirectory()  << id <<  ".pbstream";
                pbstream_files.push_back(pbstream_filepath.str());
                */
            }
        } else {
            bags_files.push_back(GetBagDirectory() + l_scene_id + ".bag");
            pbstream_files.push_back(GetRegistratedMapDirectory() + l_scene_id + ".pbstream");
        }

        if(r_scene_id.find("-") != string::npos) {
            vector<string> tmp = split(r_scene_id, "-");

            for(string id : tmp) {
                bags_files.push_back(GetBagDirectory() + id + ".bag");
                pbstream_files.push_back(GetRegistratedMapDirectory() + id + ".pbstream");
            }
        } else {
            bags_files.push_back(GetBagDirectory() + r_scene_id + ".bag");
            pbstream_files.push_back(GetRegistratedMapDirectory() + r_scene_id + ".pbstream");
        }

        merge_file_name = l_scene_id + "-" + r_scene_id;

        int result = map_builder_->MergeMaps(pbstream_files, bags_files, GetMergedMapDirectory()  + merge_file_name);
        return result;
    }
    int ProjectController::selfMergeMaps(string scene_id) {
        vector<string> bags_files;
        vector<string> pbstream_files;

        std::string merge_file_name;

        bags_files.push_back(GetBagDirectory() + scene_id + ".bag");
        pbstream_files.push_back(GetRegistratedMapDirectory() + scene_id + ".pbstream");

        /*
        vector<string> scene_bags = getSceneBags(scene_id);
        bags_files.insert(bags_files.end(), scene_bags.begin(), scene_bags.end());

        std::stringstream pbstream_filepath;
        pbstream_filepath << GetRegistratedMapDirectory()  << scene_id <<  ".pbstream";
        pbstream_files.push_back(pbstream_filepath.str());
        */

        merge_file_name = scene_id + "-" + scene_id;

        int result = map_builder_->MergeMaps(pbstream_files, bags_files, GetMergedMapDirectory()  + merge_file_name);
        return result;
    }
    // Colorize Map
    //void ProjectController::colorizeMap(string scene_id) {
    void ProjectController::colorizeMap(string l_scene_id, string r_scene_id) {
        vector<string> bags_files ;
        vector<string> pbstream_files;

        std::string merge_file_name;

        if(l_scene_id.find("-") != string::npos) {
            vector<string> tmp = split(l_scene_id, "-");

            for(string id : tmp) {
                bags_files.push_back(GetBagDirectory() + id + ".bag");
                pbstream_files.push_back(GetRegistratedMapDirectory() + id + ".pbstream");

                /*
                vector<string> scene_bags = getSceneBags(id);
                bags_files.insert(bags_files.end(), scene_bags.begin(), scene_bags.end());

                std::stringstream pbstream_filepath;
                pbstream_filepath << GetRegistratedMapDirectory() << id << ".pbstream";
                pbstream_files.push_back(pbstream_filepath.str());
                */
            }
        } else {
            bags_files.push_back(GetBagDirectory() + l_scene_id + ".bag");
            pbstream_files.push_back(GetRegistratedMapDirectory() + l_scene_id + ".pbstream");
        }

        if(r_scene_id.find("-") != string::npos) {
            vector<string> tmp = split(r_scene_id, "-");

            for(string id : tmp) {
                bags_files.push_back(GetBagDirectory() + id + ".bag");
                pbstream_files.push_back(GetRegistratedMapDirectory() + id + ".pbstream");
            }
        } else {
            bags_files.push_back(GetBagDirectory() + r_scene_id + ".bag");
            pbstream_files.push_back(GetRegistratedMapDirectory() + r_scene_id + ".pbstream");
        }

        merge_file_name = l_scene_id + "-" + r_scene_id;

        std::string merged_pcl_file = GetMergedMapDirectory()  + "/" + merge_file_name + ".ply";
        int result = map_colorizer_->ColorizeMaps(pbstream_files,
                                                  bags_files,
                                                  merged_pcl_file,
                                                  GetColoredMapDirectory(),
                                                  merge_file_name);

        std::string colored_pcl_file = GetColoredMapDirectory()  + "/" + merge_file_name + ".ply";
        fs::copy_file(fs::path(colored_pcl_file),
                               fs::path(project_path_ + "/" + project_name_ + ".ply"),
                               fs::copy_option::overwrite_if_exists);

        ROS_INFO("Colorize Result: %d", result);
    }
    void ProjectController::selfColorizeMap(string scene_id) {
        vector<string> bags_files ;
        vector<string> pbstream_files;

        std::string merge_file_name;

        vector<string> scene_bags = getSceneBags(scene_id);
        bags_files.insert(bags_files.end(), scene_bags.begin(), scene_bags.end());

        std::stringstream pbstream_filepath;
        pbstream_filepath << GetRegistratedMapDirectory() << scene_id << ".pbstream";
        pbstream_files.push_back(pbstream_filepath.str());

        merge_file_name = scene_id + "-" + scene_id;

        std::string merged_pcl_file = GetMergedMapDirectory()  + "/" + merge_file_name + ".ply";
        int result = map_colorizer_->ColorizeMaps(pbstream_files,
                                                  bags_files,
                                                  merged_pcl_file,
                                                  GetColoredMapDirectory(),
                                                  merge_file_name);

        std::string colored_pcl_file = GetColoredMapDirectory()  + "/" + merge_file_name + ".ply";
        fs::copy_file(fs::path(colored_pcl_file),
                               fs::path(project_path_ + "/" + project_name_ + ".ply"),
                               fs::copy_option::overwrite_if_exists);

        ROS_INFO("Colorize Result: %d", result);
    }

    // Do Supplement
    void ProjectController::doSupplement(string scene_id) {
        if(!IsSceneRegistrated(scene_id)) return;

        vector<string> scene_bags  = getSceneBags(scene_id);
        std::string state_filepath = "";
        state_filepath = GetSceneMapDirectory() + scene_id + ".pbstream";

        if(!exists(state_filepath)) return;

        int result = map_builder_->BuildMaps(state_filepath, scene_bags,
                                             GetSceneMapDirectory(), scene_id);
        ROS_INFO("Build_RESULT: %d", result);

        if(progress_call_back_) progress_call_back_(ProcessStatus::kComplete, 100.f);
    }

    // TODO: Stop Invoke
    std::vector<std::string> ProjectController::getSceneBags(string scene_id) {
        vector<string> bags_files = { {GetBagDirectory() + scene_id + ".bag"}};
        auto supplement_files = ScanController::getSupplementBags(GetBagDirectory(), scene_id);
        bags_files.insert(bags_files.end(), supplement_files.begin(), supplement_files.end());

        return bags_files;
    }

    std::set<std::string> ProjectController::GetScannedScenes(int scene_number){
        std::set<std::string> scanned_scenes;

        for(int i = 0; i < scene_number; i++) {
            const std::string scene_id = to_string(i + 1);
            if(IsSceneScanned(scene_id)) scanned_scenes.insert(scene_id);
        }
        return scanned_scenes;
    }
    std::set<std::string> ProjectController::GetMergedScenes(int scene_number) {
        std::set<std::string> merged_scenes;

        QDir dir(QString::fromStdString(GetMergedMapDirectory()));
        QStringList nameFilters;
        nameFilters << "*.ply";

        dir.setNameFilters(nameFilters);
        QList<QFileInfo> *files = new QList<QFileInfo>(dir.entryInfoList(nameFilters));

        for(int i = 0; i < files->count(); i++) {
            string fileName = files->at(i).fileName().toStdString();
            fileName = fileName.substr(0, fileName.length() - 4);
            merged_scenes.insert(fileName);
        }

        return merged_scenes;
    }
}
