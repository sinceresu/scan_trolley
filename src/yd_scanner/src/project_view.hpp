#ifndef PROJECT_VIEW_HPP
#define PROJECT_VIEW_HPP

#ifndef Q_MOC_RUN
    // LIBMAP_SCANNER
    #include "map_common/global.h"
    #include "map_common/err_code.h"

    #include "map_build/libmap_build.h"
    #include "map_build/map_build_interface.h"
    #include "map_registrate/libmap_registrate.h"
    #include "map_registrate/map_registrate_interface.h"
    #include "map_colorize/libmap_colorize.h"
    #include "map_colorize/map_colorize_interface.h"
#endif

//Qt5
#include "vtkRenderWindow.h"

#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QDialog>
#include <QtOpenGL/QtOpenGL>
#include <QVTKWidget.h>
#include <QProgressDialog>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <jsoncpp/json/json.h>

#include "common.hpp"
//#include "viewcontrol.hpp"
#include "project_controller.hpp"
#include "progress_dialog_widget.hpp"

#include <algorithm>

using namespace Qt;
using namespace std;
using namespace yida_mapping;
using namespace map_registrate;
using namespace map_build;
using namespace map_colorize;

namespace Ui { class ProjectView; }
namespace yd_scanner_ui {
    class FlowLayout;
    class DeviceMonitorView;
    class ConvertSelectView;

    class ProjectView : public QDialog {
        Q_OBJECT
            public:
                // Constructor & Destructor
                ProjectView(string project_name, string project_path);
                ~ProjectView();

                // Fun
                QToolButton *createImageButton(const QPixmap &icon, const string idx);

                void showPcl(string scene_id);
                bool showScenePcl(string scene_id);
                bool showScene2dMap(string scene_id);

                void clearPcl();
                void clearScenePcl();
                void clearScene2dMap();

                void initConfigurations();

                void initFlowLayout();
                void initStationScenesNoteBlock();

                void CleanPage();
                void UpdatePage();
                void UpdateStationBmp();

                // Event
                void closeEvent(QCloseEvent *event);

            public Q_SLOTS:
                void sceneListClicked();

                // 补扫、扫描、优化、合并、电站预览 点击事件监听
                void scanExtendBtnClickEvent();
                void scanBtnClickEvent();

                void previewBtnClickEvent();

                void registrateBtnClickEvent();
                void mergeBtnClickEvent();
                void convertBtnClickEvent();

                // 合并 开始、取消
                void startRegistrate(const std::string& scene_name);
                //void startMerge(std::string scene_id);
                void startMerge(std::string l_scene_id, std::string r_scene_id);
                void startSelfMerge(std::string scene_id);

                void resizeEvent(QResizeEvent *);

            private:
                Ui::ProjectView *ui_;

                // ConvertSelect View
                ConvertSelectView *convertSelectView;

                std::string GetBagDirectory() { return project_path_ + "/bags/"; };
                std::string GetRegistratedMapDirectory() { return project_path_ + "/registrated_maps/"; };
                std::string GetMergedMapDirectory() { return project_path_ + "/merged_maps/"; };

                void UpdateMappingHistory();

                void controllerProgressCallback(ProjectController::ProcessStatus info, float percent);

                bool eventFilter(QObject *target, QEvent *e);

                Json::Value root;
                string project_name_;

                int toolButtonIndex;

                bool isLeftFocused = true;

                vector<string> index_vec;
                vector<string> id_vec;
                vector<string> current_scenes_vec;

                set<string> scanned_scene_lst;
                set<string> registrated_scene_lst;

                std::shared_ptr<ProjectController> project_controller_;

                string scene_id_;
                int scene_cols_, scene_rows_;
                SceneLayout scene_layout_;

                string project_path_;

                // UI
                ProgressDialogWidget *pdw;
                QProgressDialog * progress_dlg;
                FlowLayout *flow;

                pcl::visualization::PCLVisualizer::Ptr scene_pcl_viewer_l;
                pcl::visualization::PCLVisualizer::Ptr scene_pcl_viewer_r;

                // Timer
                QLabel *count_l;
                static int COUNT_NUM;
                QTimer *count_timer;
                QPixmap map2d;
    };
}
#endif
