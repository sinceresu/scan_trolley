#ifndef PROGRESS_DIALOG_WIDGET_HPP
#define PROGRESS_DIALOG_WIDGET_HPP

#ifndef Q_MOC_RUN
    #include "ros/ros.h"
#endif

//Qt5
#include <QtCore/QtCore>
#include <QtCore/QTimer>
#include <QtGui/QtGui>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>

using namespace Qt;
using namespace std;

namespace Ui { class ProgressView; }
namespace yd_scanner_ui {
    class ProgressDialogWidget : public QDialog {
        Q_OBJECT
        public:
            ProgressDialogWidget(QWidget *parent, QString title, QString fNote, bool can_cancel = false);
            ~ProgressDialogWidget();
            bool isCanceled() { return is_canceled_;};
            // Fun
            void setNote(QString str);
            void setProgress(int v);

        private:
            // Event
            void closeEvent(QCloseEvent *e);

            // UI
            Ui::ProgressView *ui_;

            // Variables
            int progressValue;
            bool isForceClose;
            bool is_canceled_;

        private Q_SLOTS:
            void onCancelClicked();


    };
}
#endif
