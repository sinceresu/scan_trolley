#ifndef DEVICE_ERROR_HPP
#define DEVICE_ERROR_HPP

#include "qnode.hpp"

#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>

#include <string>
#include <map>

using namespace std;

namespace yd_scanner_ui {
    class DeviceError {
        public:
            DeviceError();
            ~DeviceError();

            void parseErrorCode(int code);

        private:
            map<int, string> error_map;
    };
}
#endif
