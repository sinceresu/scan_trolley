#ifndef __RVIZ_PLUGIN_HPP__
#define __RVIZ_PLUGIN_HPP__ 

//Qt4
//#include <QtGui/QVBoxLayout>
//Qt5
#include <QtWidgets/QVBoxLayout>

#ifndef Q_MOC_RUN
//rviz
#include <rviz/visualization_manager.h>
#include <rviz/render_panel.h>
#include <rviz/display.h>
#include <rviz/tool_manager.h>
#include <rviz/visualization_manager.h>
#include <rviz/render_panel.h>
#include <rviz/display.h>
#include <rviz/tool.h>
#include <rviz/default_plugin/view_controllers/orbit_view_controller.h>
#include <rviz/view_manager.h>
#endif

namespace yd_scanner_ui {

    class RvizPlugin: public QObject{
        Q_OBJECT
    public:
        RvizPlugin() 
        : manager_(nullptr)
        , viewManager(nullptr){
        }

        ~RvizPlugin() {
            deinit();
        }

        void init(rviz::RenderPanel* render_panel_, std::string frame_id, std::string topic_name);
        void deinit();
        void startDisplay();
        void stopDisplay();

    private:
        rviz::VisualizationManager *manager_;
        rviz::ViewManager* viewManager;
        //rviz工具
        rviz::Tool *current_tool;
        rviz::ToolManager *tool_manager_;
    };
}

#endif