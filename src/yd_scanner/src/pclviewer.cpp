#include "pclviewer.hpp"
#include "ui_pclviewer.h"

#include<pcl/io/ply_io.h>
#include<pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>

#include <boost/thread.hpp>

PCLViewer::PCLViewer(QWidget *parent) : QMainWindow (parent), ui_(new Ui::PCLViewer) {
    ui_->setupUi(this);
    this->setWindowTitle ("全站预览");

    // Set up the QVTK window
    viewer.reset(new pcl::visualization::PCLVisualizer ("viewer", false));
    ui_->qvtkWidget->SetRenderWindow(viewer->getRenderWindow());
    viewer->setupInteractor(ui_->qvtkWidget->GetInteractor(), ui_->qvtkWidget->GetRenderWindow());
    ui_->qvtkWidget->update();

    // Connect "random" button and the function
    // Connect R,G,B sliders and their functions
    // Connect point size slider
    connect(ui_->comboBox_color, SIGNAL (currentTextChanged (QString)), this, SLOT (cCombocurrentTextChanged (int)));

    // Connect point size slider
    connect(ui_->horizontalSlider_p, SIGNAL (valueChanged (int)), this, SLOT (pSliderValueChanged (int)));
    cloud_.reset(new PointCloudT);
}

void PCLViewer::pSliderValueChanged (int value) {
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, value, "cloud");
    ui_->qvtkWidget->update ();
}

void PCLViewer::cCombocurrentTextChanged (QString value) { }

void PCLViewer::displayPcl (std::string pcl_filepath) {
    pdw = new yd_scanner_ui::ProgressDialogWidget(this, "加载点云", "正在加载，请稍候...");
    boost::thread(boost::bind(&PCLViewer::loadPcl, this, pcl_filepath));
    pdw->exec();

    delete pdw;
    pdw = nullptr;
}

void PCLViewer::loadPcl (std::string pcl_filepath) {
    PointCloudT::Ptr cloud_input = PointCloudT::Ptr(new PointCloudT());

    // std::string filepath = "/home/sujin/ScannerWorkspace/a/a.pcd";
    pcl::io::loadPLYFile(pcl_filepath, *cloud_input);
    if(cloud_input->empty()) {
      pdw->setProgress(100);
      sleep(1);
      pdw->close();

      return;
    }

    pdw->setProgress(40);

    PointCloudT::Ptr cloud_subsampled= PointCloudT::Ptr(new PointCloudT());

    pcl::VoxelGrid<PointT> grid;
    double leaf_size = 0.02;
    grid.setLeafSize(leaf_size, leaf_size, leaf_size);
    grid.setInputCloud(cloud_input);
    grid.filter(*cloud_);

    pdw->setProgress(70);

    viewer->removeAllPointClouds ();
    viewer->addPointCloud(cloud_,  "cloud");
    viewer->resetCamera();
    ui_->qvtkWidget->update();

    pdw->setProgress(100);
    pdw->close();
}

PCLViewer::~PCLViewer() { delete ui_; }
