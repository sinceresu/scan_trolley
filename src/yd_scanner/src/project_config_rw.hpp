#pragma once
#include <string>
#include <vector>

#include "common.hpp"

namespace yd_scanner_ui {
    class ProjectConfigRW {
        public:
            struct Configuration {
                Configuration() : title(""), folder(""), scene_rows(0), scene_cols(0) { }

                std::string title;
                std::string folder;
                std::string create_time;
                std::string modify_time;
                std::string description;
                std::string scene_id_index_map_info;

                int scene_rows;
                int scene_cols;
                int scenario;

                SceneLayout scene_layout;
            };

        public:
            // Constructor & Destructor
            ProjectConfigRW(std::string project_dir);
            ~ProjectConfigRW() {};

            bool ReadConfiguration(Configuration & config);
            bool WriteConfiguration(const Configuration& config);

      private:
            std::string project_dir_;
            std::string project_config_file_;
    };
}
