#pragma once

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>

#include "std_msgs/Bool.h"

#include <image_transport/image_transport.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/subscriber.h>

#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QDialog>
#include <QLabel>
#include <QPushButton>

#ifndef WITHOUT_ROS
    #include "rvizplugin.hpp"
#endif

using namespace Qt;
using namespace std;

namespace Ui { class ScanView; }
namespace yd_scanner_ui {
    class ScanController;
    class DeviceMonitorView;

    class ScanView : public QDialog {
        Q_OBJECT
            public:
                ScanView(QWidget *parent, string scene_index, string project_path, bool is_supplement = false);
                ~ScanView();

                bool IsScanned() {return scanned_;};

                bool isCaptureFinished = false;
                void camCaptureStateCallback(const std_msgs::BoolConstPtr& msg);

            protected:
                virtual void keyPressEvent(QKeyEvent *event) override;
                virtual void closeEvent(QCloseEvent *event) override;
                virtual void mousePressEvent(QMouseEvent *event) override;

            private:
                void drawImage();
                void callback(const sensor_msgs::ImageConstPtr& left_msg,
                              const sensor_msgs::ImageConstPtr& right_msg);
                void displayImage(QLabel* image_viewer, const sensor_msgs::ImageConstPtr& image_msg);

                void handleMapUpdate();
                void prepareSnapShot();

                // Event
                bool eventFilter(QObject *target, QEvent *e);

                // Variables
                Ui::ScanView *ui_;
                RvizPlugin rviz_play_w;

                string scene_id_;
                string project_path_;

                std::shared_ptr<ScanController> scan_controller_;
                bool scanned_ = false;

                ros::NodeHandle nh_;

                ros::Publisher camCaptureStatePub;
                ros::Subscriber camCaptureStateSub;

                image_transport::ImageTransport it_;
                message_filters::Subscriber< sensor_msgs::Image > left_image_sub_;
                message_filters::Subscriber< sensor_msgs::Image > right_image_sub_;

                typedef message_filters::sync_policies::ApproximateTime<
                    sensor_msgs::Image, sensor_msgs::Image> MySyncPolicy;

                message_filters::Synchronizer<MySyncPolicy> sync;
                QTimer * statistic_timer_;

            private Q_SLOTS:
                void startScan();
                void completeScan();

                void doSnapShot();
                void updateStatistic();
    };
}
