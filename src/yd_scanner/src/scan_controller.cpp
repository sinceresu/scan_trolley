#include "scan_controller.hpp"

#include "utils.h"
#include "bag_recorder.h"
#include "glog/logging.h"
#include "absl/strings/str_split.h"
#include "map_ros_msgs/CamCaptureCmd.h"
#include "cam_capture/capture_const.h"

#include <ostream>
#include <fstream>
#include <thread>

#include <time.h>
#include <sys/stat.h>
#include <png.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <boost/filesystem.hpp>

DECLARE_string(topics);

namespace filesys = boost::filesystem;
namespace yd_scanner_ui {
    ScanController::ScanController(string scene_id, string project_path, bool is_supplement)
        : client_(node_handle_.serviceClient<::map_ros_msgs::CamCaptureCmd>(
            yida_mapping::cam_capture::kCamCaptureServiceName)),
                scan_state_(ScanState::kStopped), map_updata_callback_(nullptr) {

        is_supplement_ = is_supplement;
        scene_id_      = scene_id;
        project_path_  = project_path;
    }
    ScanController::~ScanController() { stop(); }

    void ScanController::OnUpdateOccupancyImage(::yida_mapping::Image img) {
        // 保存bmp
        uint32_t *image = img.pixels.data();
        int w = img.width;
        int h = img.height;

        FILE *f;
        int filesize = 54 + (3 * w + 3) / 4 * 4 * h;

        unsigned char bmpfileheader[14] = {'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0};
        unsigned char bmpinfoheader[40] = {40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0};
        unsigned char bmppad[3]         = {0, 0, 0};

        bmpfileheader[2] = (unsigned char)(filesize);
        bmpfileheader[3] = (unsigned char)(filesize>>8);
        bmpfileheader[4] = (unsigned char)(filesize>>16);
        bmpfileheader[5] = (unsigned char)(filesize>>24);

        bmpinfoheader[4]  = (unsigned char)(w);
        bmpinfoheader[5]  = (unsigned char)(w>>8);
        bmpinfoheader[6]  = (unsigned char)(w>>16);
        bmpinfoheader[7]  = (unsigned char)(w>>24);
        bmpinfoheader[8]  = (unsigned char)(h);
        bmpinfoheader[9]  = (unsigned char)(h>>8);
        bmpinfoheader[10] = (unsigned char)(h>>16);
        bmpinfoheader[11] = (unsigned char)(h>>24);

        string bmp_path;
        bmp_path = getImageFile();

        f = fopen(bmp_path.c_str(), "wb");
        fwrite(bmpfileheader, 1, 14, f);
        fwrite(bmpinfoheader, 1, 40, f);

        uint32_t *line = image + w * (h - 1 );
        for(int i = 0; i < h; i++) {
            for(int j = 0; j < w ; j += 1) {
                uint32_t pixel_value = line[j];
                uint8_t  rgb[] = {(unsigned char)(pixel_value>>16),
                                  (unsigned char)(pixel_value>>8),
                                  (unsigned char)(pixel_value)};
                fwrite(rgb, 3 , 1, f);
            }

            line -= w;
            fwrite(bmppad, 1, (4 - (w * 3) % 4) % 4, f);
        }

        fclose(f);

        if(map_updata_callback_) { map_updata_callback_(); }
    }

    bool ScanController::startCamCapture() {
        ROS_INFO("ScanController -> startCamCapture()");

        ::map_ros_msgs::CamCaptureCmd srv;
        srv.request.command_id = (uint32_t)yida_mapping::cam_capture::CamCaptureID::OPEN_DEVICE;

        if(!client_.call(srv)) {
            ROS_INFO("call OPEN_DEVICE Failed");
            return false;
        }

        ROS_INFO("call OPEN_DEVICE OK");
        return true;
    }

    void ScanController::stopCamCapture() {
        /*
        ::map_ros_msgs::CamCaptureCmd srv;
        srv.request.command_id = (uint32_t)yida_mapping::cam_capture::CamCaptureID::CLOSE_DEVICE;
        if(!client_.call(srv)) { return; }

        return;
        */
    }

    bool ScanController::snapshot() {
        ROS_INFO("!!!!!!!!!!!!!!!!!!!!ScanController -> snapshot()");

        if(scan_state_ != ScanState::kScanning) return false;

        ::map_ros_msgs::CamCaptureCmd srv;
        srv.request.command_id = (uint32_t)yida_mapping::cam_capture::CamCaptureID::SNAPSHOT;

        if(!client_.call(srv)) { return false; }

        return true;
    }

    void ScanController::startRecord() {
        ROS_INFO("ScanController -> startRecord()");

        std::string bag_prefix;
        if(!is_supplement_) { bag_prefix = GetBagDirectory() + scene_id_; }
        else { bag_prefix = getSupplementPrefix(); }

        ROS_INFO("topics: %s", FLAGS_topics.c_str());

        BagRecorderOptions record_options;
        //record_options.record_all  = false;
        record_options.topics      = absl::StrSplit(FLAGS_topics, ",", absl::SkipEmpty());
        record_options.prefix      = bag_prefix;
        record_options.append_date = false;
        record_options.verbose     = false;
        record_options.transport_hints.tcpNoDelay();

        bag_recorder_ = std::unique_ptr<BagRecorder>(new BagRecorder(record_options));
        bag_recorder_->run();
    }

    void ScanController::startScanMapping() {
        ROS_INFO("ScanController -> recordBag()");

        MapScannerInterface::ScanParam param;
        param.configuration_directory      = project_path_ + "/configuration_files";
        param.configuration_basename       = "online.lua";
        param.write_configuration_basename = "scanner_writer.lua";
        param.urdf_filename                = project_path_ + "/urdf/two_vlp_rig.urdf";
        param.collect_metrics              = false;

        ms_interface = CreateMapScanner();
        ms_interface->SetParam(param);
        ms_interface->SubscribeOccupancyImageCallback(std::bind(&ScanController::OnUpdateOccupancyImage, this,  std::placeholders::_1));
        ms_interface->StartScan(GetBagDirectory(), GetMapName());
    }

    std::string ScanController::getImageFile() {
        return GetBagDirectory() + scene_id_ + ".bmp";
    }
    std::string ScanController::getStatisticInfo()  {
        if(scan_state_ == ScanState::kScanning) { return bag_recorder_->getStatisticInfo(); }
        return std::string();
    }

    // 开始、完成 Start-Complete Scan
    // 开始
    int ScanController::start() {
        LOG(INFO) << "ScanController -> start()";

        if(!startCamCapture()) {
            LOG(INFO) << "Failed to startCamCapture";
            return -1;
        }

        startRecord();
        startScanMapping();

        scan_state_ = ScanState::kScanning;
        return 0;
    }
    // 完成
    void ScanController::stop() {
        ROS_INFO("ScanController -> stop()");
        stopCamCapture();

        if(scan_state_ == ScanState::kScanning) {
            ms_interface->SubscribeOccupancyImageCallback(nullptr) ;
            bag_recorder_->stop();
            // 停止扫描
            if(ms_interface->StopScan() != ERRCODE_OK) { ROS_WARN("Failed to StopScan!"); }
        }

        scan_state_ = ScanState::kStopped;
    }

    std::vector<std::string> ScanController::getSupplementBags(const string& dirPath,
                                                               const string& scene_name) {
        std::vector<std::string> listOfFiles;
        // Check if given path exists and points to a directory
        if(filesys::exists(dirPath) && filesys::is_directory(dirPath)) {
            // Create a Recursive Directory Iterator object and points to the starting of directory
            filesys::recursive_directory_iterator iter(dirPath);
            // Create a Recursive Directory Iterator object pointing to end.
            filesys::recursive_directory_iterator end;

            // Iterate till end
            while(iter != end) {
                // Check if current entry is a directory and if exists in skip list
                if(filesys::is_directory(iter->path())) {
                    // Skip the iteration of current directory pointed by iterator
                    #ifdef USING_BOOST
                        // Boost Fileystsem  API to skip current directory iteration
                        iter.no_push();
                    #else
                        // c++17 Filesystem API to skip current directory iteration
                        iter.disable_recursion_pending();
                    #endif
                } else {
                    // Add the name in vector
                    if(iter->path().extension().string() == ".bag" &&
                       iter->path().string().find(scene_name + "-") != std::string::npos) {
                        listOfFiles.push_back(iter->path().string());
                    }
                }

                boost::system::error_code ec;
                // Increment the iterator to point to next entry in recursive iteration
                iter.increment(ec);

                if(ec) {
                    std::cerr << "Error While Accessing : "
                              << iter->path().string()
                              << " :: " << ec.message() << '\n';
                }
            }
        }

        std::sort(listOfFiles.begin(), listOfFiles.end());
        return listOfFiles;
    }

    int ScanController::getSupplementIndexToScan(const std::vector<std::string>& file_list) {
        if(file_list.empty()) { return 1; }

        std::string latest_supplement_file      = file_list[file_list.size() - 1];
        std::string file_base_name              = filesys::basename(filesys::path(latest_supplement_file));
        std::vector<string> file_splitted_names = absl::StrSplit(file_base_name, "-", absl::SkipEmpty());

        int latest_scan_index = atoi(file_splitted_names[1].c_str());
        return latest_scan_index + 1;
    }

    std::string ScanController::getSupplementPrefix() {
        std::vector<std::string> supplement_files = getSupplementBags(GetBagDirectory(), scene_id_);
        int supplement_index = getSupplementIndexToScan(supplement_files);

        std::stringstream prefix;
        prefix << GetBagDirectory() + scene_id_ + "-" <<  std::setw(2) << setfill('0') << supplement_index;
        return  prefix.str();
    }
}
