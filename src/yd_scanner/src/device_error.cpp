#include "device_error.hpp"

#include "utils.h"

#include <ostream>

using namespace std;
using namespace yd_scanner_ui;

DeviceError::DeviceError() {
    error_map[10] = "network error, localization-vlp not pingable";
    error_map[11] = "network error, scan-vlp not pingable";

    error_map[20] = "port error, can't open imu port";
    error_map[21] = "port error, can't open gps port";
    error_map[22] = "port error, can't open left camera port";
    error_map[23] = "port error, can't open right camera port";

    error_map[30] = "ros message error, localization-vlp no message";
    error_map[31] = "ros message error, scan-vlp no message";
    error_map[32] = "ros message error, left camera no message";
    error_map[33] = "ros message error, right camera no message";

    error_map[40] = "ros message error, localization-vlp message error";
    error_map[41] = "ros message error, scan-vlp message error";
    error_map[42] = "ros message error, gps message error";
    error_map[43] = "ros message error, imu message error";
    error_map[44] = "ros message error, left camera message error";
    error_map[45] = "ros message error, right camera message error";
}
DeviceError::~DeviceError() {}

// Parse Error Code
void DeviceError::parseErrorCode(int code) {
    map<int, string>::iterator it = error_map.find(code);

    if(it != error_map.end()) {
        QNode::hasAlarm  = true;
        QNode::alarmInfo = it->second + "(ErrCode: " + to_string(it->first) + ")";
    } else {
        QNode::hasAlarm  = true;
        QNode::alarmInfo = "unknown error";
    }
}
