#ifndef SCANNER_CONTROLLER_HPP
#define SCANNER_CONTROLLER_HPP

#ifndef Q_MOC_RUN
    #include <memory>
    #include <thread>

    // ROS
    #include <rosbag/bag.h>
    // ROS MESSAGE
    #include <sensor_msgs/PointCloud2.h>
    #include <sensor_msgs/Image.h>
    #include <sensor_msgs/Imu.h>
    #include <sensor_msgs/NavSatFix.h>

    // LIBMAP_SCANNER
    #include "map_common/global.h"
    #include "map_common/err_code.h"
    #include "map_scanner/libmap_scanner.h"
    #include "map_scanner/map_scanner_interface.h"
#endif

#include "cam_capture/capture_const.h"

#ifndef WITHOUT_ROS
    #include "qnode.hpp"
    #include "rvizplugin.hpp"
#endif

#include <jsoncpp/json/json.h>

class Scanner;

using namespace std;
using namespace yida_mapping;
using namespace map_scanner;

class BagRecorder;

namespace yd_scanner_ui {
    class ScanController {
        enum class ScanState {
            kStopped = 0,
            kScanning = 1,
            kPaused = 1,
        };

        using MapUpdataCallback = std::function<void()>;

        public:
            ScanController(string scene_index, string project_path, bool is_supplement = false);
            ~ScanController();

            int start();
            void stop();

            bool snapshot();

            void setMapUpdataCallback(MapUpdataCallback callback) {map_updata_callback_ = callback;};
            std::string getImageFile() ;
            std::string getStatisticInfo() ;

            static std::vector<std::string> getSupplementBags(const string & directory, const string & scene_name);

        private:
            void OnUpdateOccupancyImage(::yida_mapping::Image img) ;
            std::string GetSceneMapDirectory() { return project_path_ + "/build/"; };
            std::string GetMapPrefix(bool supplement = false) { return GetSceneMapDirectory() + scene_id_ + ( supplement ? "-extend" : ""); };
            std::string GetMapName(bool supplement = false) { return scene_id_ +  ( supplement ? "-extend" : ""); };

            std::string GetBagDirectory() { return project_path_ + "/bags/"; };
            std::string GetBagFilePath(bool supplement = false) { return GetBagDirectory() + scene_id_ + (supplement ? "-extended" :"") + ".bag"; };
            int getSupplementIndexToScan(const std::vector<std::string>& file_list);
            std::string getSupplementPrefix();
            void loadScanOptions(const std::string & config_filepath);
            bool startCamCapture();

            void startRecord();
            void startScanMapping();

            void stopCamCapture();

            void handleTimeOut();

            void updateScanner();      // 更新扫描渲染
            void updateLoggingView();  // log update

            ros::NodeHandle node_handle_;

            ::ros::ServiceClient client_;

            // Variables
            shared_ptr<MapScannerInterface> ms_interface;

            yida_mapping::Image *image;

            string scene_id_;
            string project_path_;

            ScanState scan_state_;
            bool is_supplement_;

            string map_lua_config_path;
            string write_lua_config_path;

            rosbag::Bag bag;

            std::unique_ptr<BagRecorder> bag_recorder_;
            MapUpdataCallback map_updata_callback_;

            // std::shared_ptr<cam_capture::CameraInsta360> camera_;
    };
}
#endif
