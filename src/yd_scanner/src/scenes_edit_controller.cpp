#include "scenes_edit_controller.hpp"

#include "stdio.h"
#include "utils.h"

using namespace std;
using namespace yd_scanner_ui;

/// Constructor & Destructor
// Constructor
ScenesEditController::ScenesEditController() {
    printf("ScenesEditController -> constructor\n");
    // to do sth.
}
// Destructor
ScenesEditController::~ScenesEditController() {
    printf("ScenesEditController -> destructor\n");
    // to do sth.
}

/// Funs
// check connexity
bool ScenesEditController::checkConnexity(map<int, int> sceneIdIndexMap, int Rs, int Cs) {
    printf("ScenesEditController -> checkConnexity()\n");

    check_connexity_ptr = unique_ptr<CheckConnexity>(new CheckConnexity(sceneIdIndexMap, Rs, Cs));
    return check_connexity_ptr->check();
}
