#include "project_config_rw.hpp"

#include <fstream>
#include <jsoncpp/json/json.h>

#include "utils.h"

namespace yd_scanner_ui {
    Json::Value generateLayout(int rows, int cols, const SceneLayout& scene_layout) {
        std::vector<std::vector<std::string>> scene_blocks(rows, std::vector<std::string>(cols, ""));

        for(const auto scene : scene_layout) scene_blocks[scene.row][scene.col] = scene.scene_id;

        Json::Value layout_value;
        for(int i = 0; i < rows; i++) {
            Json::Value row;

            for(int j = 0; j < cols; j++) row.append(Json::Value(scene_blocks[i][j]));
            layout_value.append(row);
        }
        return layout_value;
    }

    const static  std::string kProjectConfigRWFile = std::string("project.json") ;
    ProjectConfigRW::ProjectConfigRW(std::string project_dir) :
        project_dir_(project_dir),
        project_config_file_(kProjectConfigRWFile) { }

    bool ProjectConfigRW::ReadConfiguration(Configuration & config) {
        Json::Value root = readJsonInfo(project_dir_ + "/" + project_config_file_);

        if(root.empty()) return false;

        config.create_time = root["create_time"].asString();
        config.modify_time = root["modify_time"].asString();
        config.scene_rows  = root["scene_rows"].asInt();
        config.scene_cols  = root["scene_cols"].asInt();
        config.scenario    = root["scenario"].asInt();
        config.description = root["description"].asString();

        Json::Value scene_layoutvalue = root["scene_layout"];
        std::vector<std::vector<std::string>> scene_blocks(config.scene_rows,
                                                           std::vector<std::string>(config.scene_cols));

        for(int i = 0; i < config.scene_rows; i++) {
            Json::Value row_value  = scene_layoutvalue[i];

            for(int j = 0; j < config.scene_cols; j++) {
                scene_blocks[i][j] = row_value[j].asString();
            }
        }
        config.scene_layout.clear();

        // snake layout
        for(int i = 0; i < config.scene_rows / 2; i++) {
            for(int j = 0; j < config.scene_cols; j++) {
                if(scene_blocks[i * 2][j] != "") {
                    SceneLayoutItem layout_item = {scene_blocks[i * 2][j] , i * 2, j};
                    config.scene_layout.push_back(layout_item);
                }
            }

            for(int j = config.scene_cols - 1; j >= 0; j--) {
                if(scene_blocks[i * 2 + 1][j]  != "") {
                    SceneLayoutItem layout_item = {scene_blocks[i * 2 + 1][j] , i * 2 + 1, j};
                    config.scene_layout.push_back(layout_item);
                }
            }
        }

        if((config.scene_rows % 2) == 1) {
            for(int j = 0; j < config.scene_cols; j++) {
                if(scene_blocks[config.scene_rows - 1][j]  != "") {
                    config.scene_layout.push_back(SceneLayoutItem{scene_blocks[config.scene_rows - 1][j] , config.scene_rows - 1, j} );
                }
            }
        }

        return true;
    }

    bool ProjectConfigRW::WriteConfiguration(const Configuration& config) {
        Json::Value root;
        root["title"]       = Json::Value(config.title);
        root["folder"]      = Json::Value(config.folder);
        root["create_time"] = Json::Value(config.create_time);
        root["modify_time"] = Json::Value(config.modify_time);
        root["scene_rows"]  = Json::Value(config.scene_rows);
        root["scene_cols"]  = Json::Value(config.scene_cols);
        root["scenario"]    = Json::Value(config.scenario);

        root["scene_layout"]        = generateLayout(config.scene_rows, config.scene_cols, config.scene_layout);
        root["description"]         = Json::Value(config.description);
        root["sceneIdIndexMapInfo"] = Json::Value(config.scene_id_index_map_info);

        ofstream os;
        Json::StyledWriter sw;
        string jsnPath = project_dir_+ "/" + project_config_file_;

        os.open(jsnPath.c_str());
        os << sw.write(root);
        os.close();

        return true;
    }
}
