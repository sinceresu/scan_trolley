//#ifndef TOAST_TOOL_HPP
//#define TOAST_TOOL_HPP

#pragma once

#include <QtWidgets/QWidget>
#include "ui_toast_tool.h"

class Toast : public QWidget {
    Q_OBJECT
				public:
						Toast(QWidget *parent = Q_NULLPTR);
						~Toast();

						/// Fun
    				void setText(const QString& text);
    				void showAnimation(int timeout = 2000); // 默认2秒后消失

    				static void showTip(const QString& text, QWidget* parent = nullptr);

				private:
						Ui::Toast ui;

				protected:
    				virtual void paintEvent(QPaintEvent *event);
};
