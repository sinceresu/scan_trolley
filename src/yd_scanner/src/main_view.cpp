﻿#include "main_view.hpp"
#include "ui_main_view.h"

#include <iostream>
#include <fstream>
#include <thread>

//Qt5
#include <QtGui/QList>
#include <QtCore/QMetaType>

#include <dirent.h>

#include "fcntl.h"
#include "sys/stat.h"
#include "sys/types.h"
#include "sys/statfs.h"

#include <boost/filesystem.hpp>

#include "utils.h"
#include "flowlayout.h"

#include "project_view.hpp"
#include "device_monitor_view.hpp"

#include "lua_config_view.hpp"
#include "scenes_edit_view.hpp"

#include "layout_editor.hpp"
#include "workspace_config_rw.hpp"
#include "project_config_rw.hpp"

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <exception>

using namespace yd_scanner_ui;

namespace fs = boost::filesystem;
namespace {
    SceneLayout initializeLayout(int rows, int cols) {
        std::vector<std::vector<std::string>> scene_blocks(
            rows, std::vector<std::string>(cols, " "));

        int id = 1;
        // snake layout
        for(int i = 0; i < rows / 2; i++) {
            for(int j = 0; j < cols; j++, id++) {
                scene_blocks[i * 2][j] = to_string(id);
            }
            for(int j = cols - 1; j >= 0; j--, id++) {
                scene_blocks[i * 2 + 1][j] = to_string(id);
            }
        }
        if((rows % 2) == 1) {
            for(int j = 0; j < cols; j++, id++) {
                scene_blocks[rows - 1][j] = to_string(id);
            }
        }

        SceneLayout scene_layout;
        // snake layout
        for(int i = 0; i < rows / 2; i++) {
            for(int j = 0; j < cols; j++) {
                SceneLayoutItem layout_item = {scene_blocks[i * 2][j] , i * 2, j};
                scene_layout.push_back(layout_item);
            }
            for(int j = cols - 1; j >= 0; j--) {
                SceneLayoutItem layout_item = {scene_blocks[i * 2 + 1][j] , i * 2 + 1, j};
                scene_layout.push_back(layout_item);
            }
        }
        if((rows %2) == 1) {
            for(int j = 0; j < cols; j++) {
                scene_layout.push_back(SceneLayoutItem {
                    scene_blocks[rows - 1][j] , rows - 1, j});
            }
        }
        return scene_layout;
    }

    Json::Value generateLayout(int rows, int cols, const SceneLayout& scene_layout) {
        std::vector<std::vector<std::string>> scene_blocks(rows, std::vector<std::string>(cols, ""));

        for(const auto scene : scene_layout) { scene_blocks[scene.row][scene.col] = scene.scene_id; }

        Json::Value layout_value;
        for(int i = 0; i < rows; i++) {
            Json::Value row;

            for(int j = 0; j < cols; j++) { row.append(Json::Value(scene_blocks[i][j])); }
            layout_value.append(row);
        }

        return layout_value;
    }
}

// 设备状态信息 注册 signal 消息类型
Q_DECLARE_METATYPE(QString);

MainView::MainView(int argc, char** argv, string wp, QWidget *parent) : QDialog(parent),
    ui_(new Ui::MainView),
    #ifndef WITHOUT_ROS
        qnode(argc, argv),
    #endif
    work_space_path(wp) {

    ui_->setupUi(this);
    //this->installEventFilter(this);

    {
        // 新建工程
        flow = new FlowLayout();
        flow->addWidget(createImageButton(QPixmap(":/images/add.png"), std::string("新建工程"), ""));

        // 读取ScannerWorkspace目录下工程文件个数
        projects_ = getProjectsFromConfig();
        for(auto project : projects_) {
            string station_map_bmp = project.second + "/" + project.first + "/station.bmp";
            string station_bmp = fs::exists(station_map_bmp) ? station_map_bmp : ":/images/empty.bmp";

            flow->addWidget(createImageButton(QPixmap(station_bmp.c_str()), project.first, project.first));
        }

        ui_->list_area->setMinimumWidth(232 * 5);  // 每排最多5个
        ui_->list_area->setLayout(flow);
    }

    {
        connect(ui_->browseFolder, SIGNAL(clicked()), SLOT(showProjectFolderDialog()));

        connect(ui_->start, SIGNAL(clicked()), SLOT(startProjectWindow()));
        connect(ui_->import, SIGNAL(clicked()), SLOT(showProjectImportDialog()));
        connect(ui_->output, SIGNAL(clicked()), SLOT(showProjectOutputDialog()));
        connect(ui_->remove, SIGNAL(clicked()), SLOT(showProjectRemoveDialog()));

        // 场景类型
        QStringList locationTypes;
        locationTypes << "室外" << "室内";
        ui_->location_comboBox->addItems(locationTypes);

        // 工程场景规划等级
        ui_->scenes_config_btn->setText("r * c");
        connect(ui_->scenes_config_btn, SIGNAL(clicked()), SLOT(scenesConfigDialogInit()));
    }

    // 扫描参数配置
    connect(ui_->location_config_btn, SIGNAL(clicked()), SLOT(luaConfigDialogInit()));

    // 设备状态
    devicesStatusDialogInit();
    connect(ui_->devicesBtn, SIGNAL(clicked()), SLOT(trigerDevicesLabelMenu()));

    #ifndef WITHOUT_ROS
        QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

        if(!qnode.init()) {
            QMessageBox msgBox;
            msgBox.setText("Couldn't find the ros master.");
            msgBox.exec();
            close();
        } else {
            // 注册并启动 设备状态窗口UI更新线程 Register & Start Devices Status UI-Update Thread
            ROS_INFO("Register & Start Devices Status UI Update Thread!");
            int register_id = qRegisterMetaType<QString>("QString");
            ROS_INFO("register_id: %d", register_id);
            QObject::connect(&qnode, SIGNAL(uiUpdate(QString)), devicesStatusDialog, SLOT(devicesStatusUiUpdate(QString)));
        }
    #endif

    devicesStatusDialog->exec();

    // TODO: Alarm Notification
    /* Notification Monitor
    QThread *t = new QThread();
    nm = new NotificationMonitor();
    nm->moveToThread(t);

    connect(t, SIGNAL(started()), nm, SLOT(notify()));
    connect(nm, SIGNAL(finished()), this, SLOT(showNotifications()));
    connect(nm, SIGNAL(finished()), t, SLOT(quit()));
    connect(nm, SIGNAL(finished()), nm, SLOT(deleteLater()));
    connect(t, SIGNAL(finished()), t, SLOT(deleteLater()));
    t->start();
    */
}
MainView::~MainView() {}

/*
void MainView::showNotifications() {
    ROS_INFO("MainView -> showNotifications()");

    if(nm->hasAlarm) {
        Toast::showTip(QString::fromStdString(QNode::alarmInfo), nullptr);

        player = new QMediaPlayer();
        player->setMedia(QUrl("qrc:/sounds/alarm.wav"));
        player->setVolume(40);
        player->play();

        QMessageBox::information(this, "设备状态告警", QString::fromStdString(QNode::alarmInfo));
    }
}
*/

// Devices Status Dialog Init
void MainView::devicesStatusDialogInit() {
    ROS_INFO("MainView -> devicesStatusDialogInit()");

    // 设备状态对话框 Devices Status Dialog
    devicesStatusDialog = new DeviceMonitorView(this);

    devicesStatusDialog->setWindowModality(Qt::ApplicationModal);
    devicesStatusDialog->setGeometry((QApplication::desktop()->width() - 800) / 2,
                                     (QApplication::desktop()->height() - 400) / 2, 800, 40);
}

// 打开、关闭 设备状态窗口 Open or Close Devices Status Dialog
void MainView::trigerDevicesLabelMenu() {
    ROS_INFO("MainView -> trigerDevicesLabelMenu()");
    ROS_INFO("Devices Status Dialog Exec Result: %d", MainView::devicesStatusDialog->exec());
}

// LuaConfig Dialog Init
void MainView::luaConfigDialogInit() {
    ROS_INFO("MainView -> luaConfigDialogInit()");

    if(!current_project_id.empty()) {
        string project_config_path = projects_[current_project_id] + "/" + current_project_id + "/configuration_files";

        luaConfigView = new LuaConfigView(project_config_path, this);
        luaConfigView->setWindowModality(Qt::ApplicationModal);
        luaConfigView->exec();
    } else { QMessageBox::information(this, "提示", "请选择需要进行配置的工程项目"); }
}
// ScenesConfig Dialog Init
void MainView::scenesConfigDialogInit() {
    ROS_INFO("MainView -> scenesConfigDialogInit()");

    if(ui_->scenes_config_btn->text().toStdString() == "r * c") {
        if(isShowProjectData) {
            int rs = 0;
            int cs = 0;

            scenesEditView = new ScenesEditView(&rs, &cs, &sceneIdIndexMapInfo, this);
            scenesEditView->setWindowModality(Qt::ApplicationModal);
            scenesEditView->exec();

            if(rs != 0) {
                ui_->scenes_config_btn->setText(QString::fromStdString(to_string(rs) + " * " + to_string(cs)));
            }
        } else QMessageBox::information(this, "提示", "请选择需要进行配置的工程项目");
    } else {
        // Show Levels Graph From Json
        if(!current_project_id.empty()) {
            string json_path = projects_[current_project_id] + "/" + current_project_id + "/project.json";
            Json::Value root = readJsonInfo(json_path);

            string idIndexInfo = root["sceneIdIndexMapInfo"].asString();
            int scene_rows     = root["scene_rows"].asInt();
            int scene_cols     = root["scene_cols"].asInt();

            vector<string> id_vec;
            if(!idIndexInfo.empty()) {
                vector<string> info_vec = split(idIndexInfo, " ");

                for(string v : info_vec) {
                    vector<string> tmp_vec = split(v, "-");
                    id_vec.push_back(tmp_vec[1]);
                }

                scenesEditView = new ScenesEditView(scene_rows, scene_cols, id_vec, this);
                scenesEditView->setWindowModality(Qt::ApplicationModal);
                scenesEditView->exec();
            }
        }
        // Show Levels Graph From Data
        else {
            vector<string> id_vec;
            vector<string> info_vec = split(sceneIdIndexMapInfo, " ");

            for(string v : info_vec) {
                vector<string> tmp_vec = split(v, "-");
                id_vec.push_back(tmp_vec[1]);
            }

            string scene_levels_title = ui_->scenes_config_btn->text().toStdString();
            scene_levels_title = replace(scene_levels_title, " ", "");
            vector<string> tmp_vec = split(scene_levels_title, "*");

            scenesEditView = new ScenesEditView(atoi(tmp_vec[0].c_str()),
                                                atoi(tmp_vec[1].c_str()), id_vec, this);
            scenesEditView->setWindowModality(Qt::ApplicationModal);
            scenesEditView->exec();
        }
    }
}

// Create Image Button
QToolButton* MainView::createImageButton(const QPixmap &icon, const string& text, const string& id) {
    QToolButton *button = new QToolButton();

    button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    button->setStyleSheet("background-color: white;");
    button->setIconSize(QSize(180, 180));
    button->setIcon(QIcon(icon.scaled(180, 180, Qt::KeepAspectRatio)));
    button->setText(QString::fromStdString(text));
    button->setWhatsThis(QString::fromStdString(id));

    connect(button, SIGNAL(clicked()), SLOT(showProjectData()));

    return button;
}

void MainView::displaySelectedProject(string scene_id) {
    for(int i = 0; i < flow->count(); ++i) {
        auto current_scene_icon = flow->itemAt(i)->widget();

        if(scene_id != current_scene_icon->whatsThis().toStdString()) {
            current_scene_icon->setStyleSheet("background-color: white;");
        } else {
            current_scene_icon->setStyleSheet("background-color: rgb(244, 150, 111);");
        }
    }
}
void MainView::removeSelectedProject(string scene_id) {
    for(int i = 0; i < flow->count(); ++i) {
        auto current_scene_icon =  flow->itemAt(i)->widget();
        if(scene_id == current_scene_icon->whatsThis().toStdString()) {
           flow->removeWidget(current_scene_icon);
        }
    }
    flow->update();
}
void MainView::showProjectData() {
    ROS_INFO("MainView -> showProjectData()");

    isShowProjectData = true;

    QToolButton *button = (QToolButton*)sender();
    current_project_id  = button->whatsThis().toStdString();

    displaySelectedProject(current_project_id);

    if(!current_project_id.empty()) {
        // Get Project Info
        ui_->title->setText(button->text());
        string parent_folder = projects_[current_project_id];
        ui_->folder->setText(parent_folder.c_str());

        string project_path = parent_folder + "/" + current_project_id;
        ProjectConfigRW project_config_rw(project_path);
        ProjectConfigRW::Configuration project_config;

        if(!project_config_rw.ReadConfiguration(project_config)) return;

        if(project_config.scene_rows != 0) {
            ui_->scenes_config_btn->setText(QString::fromStdString(
                to_string(project_config.scene_rows) + " * " +
                to_string(project_config.scene_cols)));
        }

        ui_->createdDate->setDateTime(QDateTime::fromString(project_config.create_time.c_str(), "yyyy-MM-dd HH:mm:ss"));
        ui_->modifiedDate->setDateTime(QDateTime::fromString(project_config.modify_time.c_str(), "yyyy-MM-dd HH:mm:ss"));
        ui_->location_comboBox->setCurrentIndex(project_config.scenario);
        ui_->description->setText(QString::fromStdString(project_config.description));
    }
    // Open New Project
    else {
        ui_->title->setText("");
        ui_->folder->setText("");
        ui_->description->setText("");
        ui_->scenes_config_btn->setText("r * c");

        ui_->createdDate->setDateTime(QDateTime::currentDateTime());
        ui_->modifiedDate->setDateTime(QDateTime::currentDateTime());
    }
}

void MainView::showProjectFolderDialog() {
    ROS_INFO("showProjectFolderDialog()");

    QString dir = QFileDialog::getExistingDirectory(this, tr("选择工程文件夹"), QString::fromStdString(work_space_path + "/projects"), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!dir.isEmpty()) ui_->folder->setText(dir);
}

bool MainView::setupProjectDirectories(const string & project_dir, Scenario scenario) {
    if(fs::exists(project_dir)) return false;

    if(!fs::create_directory(project_dir)) return false;
    ROS_INFO("工程文件夹创建成功");

    std::string dirPath = project_dir + "/bags";
    if(!fs::create_directory(dirPath)) return false;
    ROS_INFO("bags创建成功");

    dirPath = project_dir + "/scene_maps";
    if(!fs::create_directory(dirPath)) return false;
    ROS_INFO("scene maps dir 创建成功");

    dirPath = project_dir + "/registrated_maps";
    if(!fs::create_directory(dirPath)) return false;
    ROS_INFO("registrated map dir 创建成功");

    dirPath = project_dir+ "/merged_maps";
    if(!fs::create_directory(dirPath)) return false;
    ROS_INFO("merged map dir 创建成功");

    dirPath = project_dir+ "/colored_maps";
    if(!fs::create_directory(dirPath)) return false;
    ROS_INFO("colored map dir 创建成功");

    string config_path = work_space_path + "/configs/";
    switch(scenario) {
        case Scenario::INDOOR: {
            if(!copyDir(config_path + "/" + "configuration_files/indoor", project_dir + "/configuration_files"))
                return false;
            break;
        }
        case Scenario::OUTDOOR: {
            if(!copyDir(config_path + "/" + "configuration_files/outdoor", project_dir + "/configuration_files"))
                return false;
            break;
        }
        default: break;
    }

    if(!copyDir(config_path + "/urdf", project_dir + "/urdf"))     return false;
    if(!copyDir(config_path + "/styles", project_dir + "/styles")) return false;

    return true;
}
void MainView::startProjectWindow() {
    ROS_INFO("MainView -> startProjectWindow()");

    // 新建工程 相关信息保存
    if(current_project_id == "") {
        // 必填项判断
        if(ui_->title->text().toStdString().length() > 0 &&
           ui_->folder->text().toStdString().length() > 0 &&
           ui_->scenes_config_btn->text().toStdString() != "r * c") {

            string project_dir = ui_->folder->text().toStdString() + "/" + ui_->title->text().toStdString();
            Scenario sceario = static_cast<Scenario>(ui_->location_comboBox->currentIndex());
            if(!setupProjectDirectories(project_dir, sceario)) {
                QMessageBox::information(this, "提示", "Failed to create project directory");
                return;
            }

            vector<string> level_info_vec = split(ui_->scenes_config_btn->text().toStdString(), " ");
            int rs = atoi(level_info_vec[0].c_str());
            int cs = atoi(level_info_vec[2].c_str());
            auto scene_layout = initializeLayout(rs, cs);

            if(rs > 1 && cs > 1) {
                LayoutEditor *layout_editor = new LayoutEditor(rs, cs, scene_layout);
                //layout_editor->setWindowTitle("layout editor");
                //layout_editor->exec();

                scene_layout = layout_editor->getLayout();
                delete layout_editor;
            }

            string parent_folder  = ui_->folder->text().toStdString();
            string project_title  = ui_->title->text().toStdString();
            string project_folder = parent_folder + "/" + project_title;

            // create project configuration file
            ProjectConfigRW project_config_rw(project_dir);

            ProjectConfigRW::Configuration project_config;
            project_config.title        = project_title;
            project_config.folder       = project_folder;
            project_config.create_time  = ui_->createdDate->text().toStdString();
            project_config.modify_time  = ui_->createdDate->text().toStdString();
            project_config.scene_rows   = rs;
            project_config.scene_cols   = cs;
            project_config.scenario     = ui_->location_comboBox->currentIndex();
            project_config.scene_layout = scene_layout;
            project_config.description  = ui_->description->toPlainText().toStdString();

            project_config.scene_id_index_map_info = sceneIdIndexMapInfo;

            if(project_config_rw.WriteConfiguration(project_config)) {
                projects_[project_title] = parent_folder;
                saveProjectsToConfig(projects_);

                auto project_button = createImageButton(QPixmap(":/images/empty.bmp"), project_title, project_title);
                flow->addWidget(project_button);

                displaySelectedProject(project_title);
                this->showMinimized();

                // 打开新工程
                ProjectView *projectWindow = new ProjectView(project_title, project_folder);
                projectWindow->setGeometry((QApplication::desktop()->width() - 1200) / 2,
                                            (QApplication::desktop()->height() - 600) / 2,
                                            1200, 600);
                projectWindow->show();

                current_project_id = project_title;

                sceneIdIndexMapInfo = "";
            }
        } else { QMessageBox::information(this, "提示", "请填写工程信息"); }
    }
    // 打开现有工程
    else {
        if(projects_.count(current_project_id)) {
            this->showMinimized();

            string project_folder = projects_[current_project_id] + "/" + current_project_id;
            ProjectView *projectWindow = new ProjectView(current_project_id, project_folder);
            projectWindow->setGeometry((QApplication::desktop()->width() - 1200) / 2,
                                       (QApplication::desktop()->height() - 600) / 2,
                                       1200, 600);
            projectWindow->show();
        } else { QMessageBox::information(0, "提示", "请选择一个工程！"); }
    }
}

/// 工程导入-导出
// 以规定的格式导入工程文件
void MainView::showProjectImportDialog() {
    ROS_INFO("MainView -> showProjectImportDialog()");

    vector<string> vec = split(QDir::currentPath().toStdString(), "/");
    string path        = "/" + vec[0] + vec[1] + "/" +vec[2] + "/";

    QString file = QFileDialog::getExistingDirectory(this, tr("请选择需要导入的工程文件夹"), QString::fromStdString(path), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!file.isEmpty()) {
        QDir bags_d(file + "/bags/");
        QFile json_f(file + "/project.json");

        if(bags_d.exists() && json_f.exists()) {
            std::string parent_dir = fs::path(file.toStdString()).parent_path().string();
            std::string project_name = fs::path(file.toStdString()).stem().string();
            if (projects_.count(project_name) != 0) {
                QMessageBox::information(this, "导入工程", "有重名工程，无法导入！");
                return;
            }
            projects_[project_name] = parent_dir;
            saveProjectsToConfig(projects_);
            string station_map_bmp = file.toStdString() + "/station.bmp";
            string station_bmp = fs::exists(station_map_bmp) ? station_map_bmp : ":/images/empty.bmp";
            auto project_button = createImageButton(QPixmap(QString::fromStdString(station_bmp)), project_name, project_name);
            flow->addWidget(project_button);

        } else { QMessageBox::information(this, "导入工程", "工程文件异常，无法导入！"); }
    }
}
// 以规定格式导出工程文件
void MainView::showProjectOutputDialog() {
    ROS_INFO("MainView -> showProjectOutputDialog()");

    MainView::outputDialog = new QDialog();
    outputDialog->setWindowTitle("工程导出");

    QVBoxLayout *layout = new QVBoxLayout();

    // TODO: 确定导出的工程是哪一个?
    QLabel *formatLabel = new QLabel(tr("请选择xx工程的导出格式："));

    QHBoxLayout *btnLayout = new QHBoxLayout();
    {
        QPushButton *okBtn = new QPushButton(tr("确定"));
        connect(okBtn, SIGNAL(clicked()), SLOT(exportProject()));
        QPushButton *cancelBtn = new QPushButton(tr("取消"));
        connect(cancelBtn, SIGNAL(clicked()), SLOT(closeExportDialog()));

        btnLayout->addWidget(okBtn);
        btnLayout->addWidget(cancelBtn);
    }
    QHBoxLayout *checkLayout = new QHBoxLayout();
    {
        QCheckBox *pts = new QCheckBox(tr("pts"));
        QCheckBox *las = new QCheckBox(tr("las"));
        QCheckBox *e57 = new QCheckBox(tr("e57"));

        pts->setChecked(true);

        checkLayout->addWidget(pts);
        checkLayout->addWidget(las);
        checkLayout->addWidget(e57);
    }
    layout->addWidget(formatLabel);
    layout->addLayout(checkLayout);
    layout->addLayout(btnLayout);

    outputDialog->setLayout(layout);
    outputDialog->setGeometry(
          QApplication::desktop()->width() / 2 - 250,
          QApplication::desktop()->height() / 2 - 75, 500, 150);

    outputDialog->exec();
}
// 导出工程
void MainView::exportProject() {
    ROS_INFO("MainView -> exportProject()");
    MainView::outputDialog->close();

    MainView::exportProjectProgressDialog = new QProgressDialog(this);
    exportProjectProgressDialog->setWindowTitle("导出工程");
    exportProjectProgressDialog->setOrientation(Qt::Horizontal);
    exportProjectProgressDialog->setRange(0, 100);
    // TODO: 可以通过 value 来判断是否完成导出
    exportProjectProgressDialog->setValue(37);

    QLabel *text = new QLabel("正在导出工程，请稍候...");
    exportProjectProgressDialog->setLabel(text);

    QPushButton *cancelBtn = new QPushButton("取消");
    connect(cancelBtn, SIGNAL(clicked()), SLOT(cancelExportProject()));

    exportProjectProgressDialog->setCancelButtonText("取消");
    exportProjectProgressDialog->resize(400, 80);
    exportProjectProgressDialog->move((QApplication::desktop()->width() - exportProjectProgressDialog->width()) / 2,
                                      (QApplication::desktop()->height() - exportProjectProgressDialog->height()) / 2);
    exportProjectProgressDialog->setAutoReset(false);

    exportProjectProgressDialog->exec();
}
// 关闭导出对话框
void MainView::closeExportDialog() {
    ROS_INFO("MainView -> closeExportDialog()");
    MainView::outputDialog->close();
}
// 取消导出
void MainView::cancelExportProject() {
    ROS_INFO("MainView -> cancelExportProject()");
    MainView::exportProjectProgressDialog->close();
    // TODO. 清理已导出的临时文件
}

// 删除工程文件
void MainView::showProjectRemoveDialog() {
    ROS_INFO("MainView -> showProjectRemoveDialog()");
    if (current_project_id.empty())
        return;
    if (projects_.count(current_project_id) == 0) {
        current_project_id = "";
        return;
    }
    removeSelectedProject(current_project_id);
    projects_.erase(current_project_id);
    saveProjectsToConfig(projects_);
}

map<string, string> MainView::getProjectsFromConfig() {
    ROS_INFO("MainView -> getProjectsNum()");

    map<string, string> lst;
    WorkspaceConfigRW workspace_config(work_space_path);
    WorkspaceConfigRW::Configuration config;

    if(!workspace_config.ReadConfiguration(config)) return lst;

    for(const auto & project_item : config.projects) {
        if(fs::exists(fs::path(project_item.parent_directory) / fs::path(project_item.name)))
            lst[project_item.name] = project_item.parent_directory;
    }

    return lst;
}
void MainView::saveProjectsToConfig(map<string, string> projects)  {
    WorkspaceConfigRW workspace_config_rw(work_space_path);
    WorkspaceConfigRW::Configuration workspace_config;

    for(const auto & project: projects) {
        workspace_config.projects.push_back(WorkspaceConfigRW::ProjectItem{ project.first, project.second });
    }

    workspace_config_rw.WriteConfiguration(workspace_config);
}

// Event Filter
bool MainView::eventFilter(QObject *target, QEvent *e) {
    /* TODO: test
    if(e->type() == QEvent::KeyPress) {
        QKeyEvent *event = static_cast<QKeyEvent*>(e);
        if(event->key() == Qt::Key_Shift) {
            ROS_INFO("Shift key pressed!");
        }
    }
    */
}

// 窗口关闭事件
void MainView::closeEvent(QCloseEvent *event) {
    ROS_INFO("MainView -> close()");
    //nm->setWindowClosedStatus();
    QWidget::closeEvent(event);
}
