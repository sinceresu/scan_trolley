#ifndef LUA_CONFIG_VIEW_HPP
#define LUA_CONFIG_VIEW_HPP

#include "progress_dialog_widget.hpp"

extern "C" {
    #include <lua5.2/lua.h>
    #include <lua5.2/lualib.h>
    #include <lua5.2/lauxlib.h>
}

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>

// Qt5
#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>

#include <vector>
#include <string>

#include "string.h"

using namespace Qt;
using namespace std;

namespace Ui { class LuaConfigView; }

namespace yd_scanner_ui {
    class LuaConfigController;

    class LuaConfigView : public QDialog {
        Q_OBJECT
            public:
                LuaConfigView(string project_config_path, QWidget *parent = 0);
                ~LuaConfigView();

            private:
                /// Variables
                Ui::LuaConfigView *ui_;
                ProgressDialogWidget *pdw;

                string config_dir_path;

                struct isLuaChangedParasStru {
                    bool is_map_build_changed;
                    string map_build_path;
                    vector<string> map_build_values;

                    bool is_build_writer_changed;
                    string build_writer_path;
                    vector<string> build_writer_values;

                    bool is_map_registrate_changed;
                    string map_registrate_path;
                    vector<string> map_registrate_values;

                    bool is_merge_writer_changed;
                    string merge_writer_path;
                    vector<string> merge_writer_values;

                    bool is_map_colorize_changed;
                    string map_colorize_path;
                    vector<string> map_colorize_values;

                    bool is_colorize_writer_changed;
                    string colorize_writer_path;
                    vector<string> colorize_writer_values;
                };
                boost::thread save_thread;

                vector<string> lua_name_vec;

                vector<string> mapBuildNewValues;      vector<string> buildWriterNewValues;
                vector<string> mapRegistrateNewValues; vector<string> mergeWriterNewValues;
                vector<string> mapColorizeNewValues;   vector<string> colorizeWriterNewValues;

                /// 建图
                // two_vlp_outdoor

                // map_build
                bool old_buildFilterWaveY, old_landmarkCheckY;
                // build_writer
                string old_buildMinDis, old_buildMaxDis, old_buildMaxHorDis, old_buildMinHeight, old_buildMaxHeight,
                       new_buildMinDis, new_buildMaxDis, new_buildMaxHorDis, new_buildMinHeight, new_buildMaxHeight;

                /// 拼接
                // map_registrate
                string old_registrateLeafSize, old_registrateIteatorNum,
                       new_registrateLeafSize, new_registrateIteatorNum;

                /// 合并
                // merge_writer
                string old_mergeMinDis, old_mergeMaxDis,
                       old_mergeMaxHorDis,
                       old_mergeSamplingRatio,
                       old_mergeMinHeight, old_mergeMaxHeight,

                       new_mergeMinDis, new_mergeMaxDis,
                       new_mergeMaxHorDis,
                       new_mergeSamplingRatio,
                       new_mergeMinHeight, new_mergeMaxHeight;

                /// 着色
                // map_colorize
                bool old_colorizeFilterWaveY;
                string old_colorizeSkipTime,
                       old_colorizeMinDis, old_colorizeMaxDis,

                       new_colorizeSkipTime,
                       new_colorizeMinDis, new_colorizeMaxDis;
                // colorize_writer
                string old_colorizeMinRangeDis, old_colorizeMaxRangeDis,
                       old_colorizeMaxHorDis,
                       old_colorizeRemoveMovingObjFilterLevel,
                       old_colorizeMinHeight, old_colorizeMaxHeight,

                       new_colorizeMinRangeDis, new_colorizeMaxRangeDis,
                       new_colorizeMaxHorDis,
                       new_colorizeRemoveMovingObjFilterLevel,
                       new_colorizeMinHeight, new_colorizeMaxHeight;

                /// Fun
                void dialogInit();
                void paramsInit();

                bool isMapBuildChanged();      bool isBuildWriterChanged();
                bool isMapRegistrateChanged(); bool isMergeWriterChanged();
                bool isMapColorizeChanged();   bool isColorizeWriterChanged();

                void doSave(isLuaChangedParasStru stru);

                /// Event
                // 关闭窗口
                void closeEvent(QCloseEvent *event);

                // LuaConfigController
                boost::shared_ptr<LuaConfigController> lua_controller;

            private Q_SLOTS:
                // to do.
    };
}
#endif
