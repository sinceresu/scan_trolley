﻿#include "device_monitor_view.hpp"
#include "ui_device_monitor_view.h"

#include <iostream>
#include <fstream>
#include <thread>

//Qt5
#include <QtGui/QList>
#include <QtCore/QMetaType>

#include <dirent.h>

#include "fcntl.h"
#include "sys/stat.h"
#include "sys/types.h"
#include "sys/statfs.h"

#include "utils.h"
#include "flowlayout.h"

#include "project_view.hpp"
#include "layout_editor.hpp"

using namespace yd_scanner_ui;

DeviceMonitorView::DeviceMonitorView(QWidget *parent) : QDialog(parent),
    ui_(new Ui::DeviceMonitorView) {

    ui_->setupUi(this);

    // 初始化 设备状态对话框 Initial Devices Status Dialog
    devicesStatusDialogInit();
    getComputerResourcesInfo();
}
DeviceMonitorView::~DeviceMonitorView() {}

// GetComputerResourcesInfo
void DeviceMonitorView::outputInfo() {
    run_time++;

    QString qstr = p->readAllStandardOutput().data();
    qstr.replace("\n", "||");
    qstr.replace(QRegExp("( ){1,}"), " ");
    vector<string> info = split(qstr.toStdString(), "||");

    // TODO: 不同机器可能需要不同的判断条件
    switch(run_time) {
        case 1: {
            ROS_INFO("**********PC INFO**********");
            vector<string> mem_data = split(info[1], " ");
            MEM_TOTAL = atof(mem_data[1].c_str()) / 1024.0;
            MEM_USE   = atof(mem_data[2].c_str()) / 1024.0;
            ROS_INFO("内存：-全部%.0lfG -已用%.0lfG", MEM_TOTAL, MEM_USE);

            ui_->l_mem_v->setText(tr("全部：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(MEM_TOTAL) +
                                tr("        ") +
                                tr("可用：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(MEM_USE));
            break;
        }
        case 2: {
            vector<string> dis_data = split(info[3], " ");
            DISK_USE  = atof(dis_data[2].c_str()) / (1024.0 * 1024.0);
            DISK_FREE = atof(dis_data[3].c_str()) / (1024.0 * 1024.0);
            ROS_INFO("磁盘：-已用%.0lfG -可用%.0lfG", DISK_USE, DISK_FREE);

            // Disk
            ui_->l_disk_v->setText(tr("已用：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(DISK_USE) +
                                tr("    ") +
                                tr("可用：<font style = 'font-size: 14px; color: green;'>%1</font>G").arg(DISK_FREE));
            break;
        }
        case 3: {
            //cout << "info[2]: " << info[2] << endl;

            /* TODO: test待重写
            cout << info[2] << endl;

            vector<string> cpu_data = split(info[2], " ");
            for(string s : cpu_data) cout << s << endl;

            CPU_RATE = 100.0 - atof(cpu_data[14].c_str());

            ROS_INFO("**********PC INFO**********");

            // CPU
            ui_->l_cpu_v->setText(tr("已用：<font style = 'font-size: 14px; color: green;'>%1</font>%").arg(CPU_RATE));
            */

            break;
        }
    }
}
void DeviceMonitorView::errorInfo() {
    ROS_INFO("DeviceMonitorView -> QProcess errorInfo()");
    QMessageBox::information(0, "Error", p->readAllStandardError().data());
}
void DeviceMonitorView::getComputerResourcesInfo() {
    ROS_INFO("DeviceMonitorView -> getComputerResourcesInfo()");

    run_time = 0;

    p = new QProcess(this);
     connect(p, SIGNAL(readyReadStandardOutput()), this, SLOT(outputInfo()));
    connect(p, SIGNAL(readyReadStandardError()), this, SLOT(errorInfo()));
    p->start("free -m");
    p->waitForFinished();

    // p->write("free -m\n");         // 内存状态 Memory State
    p->start("df -k");           // 磁盘状态 Disk State
    p->waitForFinished();

    p->start("cat /proc/stat");  // CPU
    p->waitForFinished();

    delete p;
}

// Devices Status Dialog Init
void DeviceMonitorView::devicesStatusDialogInit() {
    ROS_INFO("DeviceMonitorView -> devicesStatusDialogInit()");

    setWindowTitle("设备状态信息");
    connect(ui_->okBtn, SIGNAL(clicked()), SLOT(close()));

    // 隐藏关闭按钮 Hide Close Btn
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::Dialog);
}

// 更新设备状态窗口UI Update Devices Status UI
void DeviceMonitorView::devicesStatusUiUpdate(QString qstr) {
    std::string data = qstr.toStdString();
    std::vector<std::string> data_vec = split(data, " ");

    /// 激光
    // 定位激光
    ui_->l1_v->setText(QString::fromStdString(data_vec[0]));
    ui_->l2_v->setText(QString::fromStdString(data_vec[1]));
    ui_->l3_v->setText(QString::fromStdString(data_vec[2]));
    // 扫描激光
    ui_->l4_v->setText(QString::fromStdString(data_vec[3]));
    ui_->l5_v->setText(QString::fromStdString(data_vec[4]));
    ui_->l6_v->setText(QString::fromStdString(data_vec[5]));
    /// IMU & GPS
    // IMU
    ui_->l7_v->setText(QString::fromStdString(data_vec[6]));
    ui_->l8_v->setText(QString::fromStdString(data_vec[7]));
    ui_->l9_v->setText(QString::fromStdString(data_vec[8]));
    // GPS
    ui_->l10_v->setText(QString::fromStdString(data_vec[9]));
    ui_->l11_v->setText(QString::fromStdString(data_vec[10]));
    ui_->l12_v->setText(QString::fromStdString(data_vec[11]));
    /// Cameras
    // Front Left
    ui_->lfl1_v->setText(QString::fromStdString(data_vec[12]));
    ui_->lfl2_v->setText(QString::fromStdString(data_vec[13]));
    ui_->lfl3_v->setText(QString::fromStdString(data_vec[14]));

    // Front Right
    ui_->rfl1_v->setText(QString::fromStdString(data_vec[15]));
    ui_->rfl2_v->setText(QString::fromStdString(data_vec[16]));
    ui_->rfl3_v->setText(QString::fromStdString(data_vec[17]));
}

// 窗口关闭事件
void DeviceMonitorView::closeEvent(QCloseEvent *event) {
    ROS_INFO("DeviceMonitorView -> close()");
    QWidget::closeEvent(event);
}
