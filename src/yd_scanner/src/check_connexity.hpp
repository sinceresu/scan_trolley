#ifndef CHECK_CONNEXITY_HPP
#define CHECK_CONNEXITY_HPP

#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>

#include <algorithm>

#include <map>
#include <set>
#include <vector>
#include <string>

#include <iostream>

#include "string.h"

using namespace Qt;
using namespace std;

namespace yd_scanner_ui {
    class CheckConnexity {
        public:
            CheckConnexity(map<int, int> sceneIdIndexMap, int rs, int cs);
            ~CheckConnexity();

            /// Variables

            /// Fun
            bool check();
            vector<int> getAdjacentConnexityNums(int scene_id);

        private:
            /// Variables
            map<int, int> sceneIdIndexMap;
            map<map<int, int>, int> sceneIdCoordinateMap;

            set<int> idSet;
            vector<int> scene_id_lst;

            int Rs, Cs;
            int COUNT = 0;

            // Funs
            int getValueBySceneIdCoordinate(int row, int col);
            vector<int> getSceneIdCoordinate(int scene_id);

            void clear();
            void search(int scene_id);

            bool is_in_id_lst(int num);
            bool is_in_id_set(int num);

            int getTop(int row, int col);
            int getRight(int row, int col, int Cs);
            int getDown(int row, int col, int Rs);
            int getLeft(int row, int col);
    };
}
#endif
