﻿#include "project_view.hpp"
#include "ui_project_view.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <thread>

#include <stdlib.h>
#include <pthread.h>
#include <sys/stat.h>

#include <boost/filesystem.hpp>

#include "time.h"
#include "utils.h"

// PCL
#include<pcl/io/io.h>
#include<pcl/io/ply_io.h>
#include<pcl/point_types.h>
#include<pcl/common/common.h>

#include "flowlayout.h"

#include "project_controller.hpp"
#include "device_monitor_view.hpp"
#include "convert_select_view.hpp"

#include "scan_view.hpp"
#include "pclviewer.hpp"
#include "layout_editor.hpp"

using namespace boost::filesystem;

namespace yd_scanner_ui {
    ProjectView::ProjectView(string project_name, string project_path) :
        QDialog(),
        ui_(new Ui::ProjectView),
        project_name_(project_name), project_path_(project_path),
        scene_cols_(0), scene_rows_(0), scene_id_("") {

            ui_->setupUi(this);
            setWindowTitle(QString::fromStdString(project_name));

            // 获取工程信息
            initConfigurations();

            project_controller_ = std::make_shared<ProjectController>(project_name_, project_path_);
            project_controller_->setProgressCallback(boost::bind(&ProjectView::controllerProgressCallback, this, boost::placeholders::_1, boost::placeholders::_2));

            // 列表
            UpdateMappingHistory();

            // 初始化Flow
            flow = new FlowLayout();
            initFlowLayout();
            ui_->list_area->setLayout(flow);

            scene_pcl_viewer_l.reset(new pcl::visualization::PCLVisualizer("scene_pcl_viewer", false));
            scene_pcl_viewer_r.reset(new pcl::visualization::PCLVisualizer("scene_pcl_viewer", false));

            // Left
            ui_->scene_pcl_container_left->SetRenderWindow(scene_pcl_viewer_l->getRenderWindow());
            scene_pcl_viewer_l->setupInteractor(ui_->scene_pcl_container_left->GetInteractor(),
                                                ui_->scene_pcl_container_left->GetRenderWindow());
            ui_->scene_pcl_container_left->update();
            // Right
            ui_->scene_pcl_container_right->SetRenderWindow(scene_pcl_viewer_r->getRenderWindow());
            scene_pcl_viewer_r->setupInteractor(ui_->scene_pcl_container_right->GetInteractor(),
                                                ui_->scene_pcl_container_right->GetRenderWindow());
            ui_->scene_pcl_container_right->update();

            // 初始化 场景提示块
            initStationScenesNoteBlock();

            // 扫描、优化、拼接、着色、格式转换、电站预览
            connect(ui_->scan_btn, SIGNAL(clicked()), SLOT(scanBtnClickEvent()));
            connect(ui_->registrate_btn, SIGNAL(clicked()), SLOT(registrateBtnClickEvent()));
            connect(ui_->scan_extend_btn, SIGNAL(clicked()), SLOT(scanExtendBtnClickEvent()));
            connect(ui_->merge_btn, SIGNAL(clicked()), SLOT(mergeBtnClickEvent()));
            connect(ui_->preview_btn, SIGNAL(clicked()), SLOT(previewBtnClickEvent()));
            connect(ui_->convert_btn, SIGNAL(clicked()), SLOT(convertBtnClickEvent()));

            // default focus
            ui_->scene_pcl_title_left->setStyleSheet("background-color: rgb(244, 150, 111);");

            ui_->scene_pcl_title_left->installEventFilter(this);
            ui_->scene_pcl_title_right->installEventFilter(this);
    }
    ProjectView::~ProjectView() {}

    // 初始化工程信息
    void ProjectView::initConfigurations() {
        ROS_INFO("ProjectView -> initConfigurations()");

        root = readJsonInfo(project_path_ + "/project.json");

        scene_rows_        = root["scene_rows"].asInt();
        scene_cols_        = root["scene_cols"].asInt();
        string idIndexInfo = root["sceneIdIndexMapInfo"].asString();

        if(!idIndexInfo.empty()) {
            vector<string> info_vec = split(idIndexInfo, " ");

            for(string v : info_vec) {
                vector<string> tmp_vec = split(v, "-");

                index_vec.push_back(tmp_vec[0]);
                id_vec.push_back(tmp_vec[1]);
            }
        }

        /*
        scene_rows_ = root["scene_rows"].asInt();
        scene_cols_ = root["scene_cols"].asInt();

        Json::Value scene_layout_value = root["scene_layout"];
        std::vector<std::vector<std::string>> scene_blocks(scene_rows_, std::vector<std::string>(scene_cols_));

        for(int i = 0; i < scene_rows_; i++) {
            Json::Value row_value = scene_layout_value[i];
            for(int j = 0; j < scene_cols_; j++) {
                scene_blocks[i][j] = row_value[j].asString();
            }
        }
        scene_layout_.clear();

        // snake layout
        for(int i = 0; i < scene_rows_ / 2; i++) {
            for(int j = 0; j < scene_cols_; j++) {
                if(scene_blocks[i * 2][j] != "") {
                    SceneLayoutItem layout_item = {scene_blocks[i * 2][j] , i * 2, j};
                    scene_layout_.push_back(layout_item);
                }
            }
            for(int j = scene_cols_ - 1; j >= 0; j--) {
                if(scene_blocks[i * 2 + 1][j]  != "") {
                    SceneLayoutItem layout_item = {scene_blocks[i * 2 + 1][j] , i * 2 + 1, j};
                    scene_layout_.push_back(layout_item);
                }
            }
        }
        if((scene_rows_ % 2) == 1){
            for(int j = 0; j < scene_cols_; j++) {
                if(scene_blocks[scene_rows_ - 1][j]  != "") {
                    scene_layout_.push_back(SceneLayoutItem{scene_blocks[scene_rows_ - 1][j] , scene_rows_ - 1, j} );
                }
            }
        }
        */
    }

    // 初始化FlowLayout(场景列表)
    void ProjectView::initFlowLayout() {
        ROS_INFO("ProjectView -> initFlowLayout()");

        vector<string> tmp;
        vector<string> tmp_index_vec = index_vec;

        for(string v1 : registrated_scene_lst) {
            tmp.push_back(v1);
            vector<string> tmp_vec = split(v1, "-");

            for(string v2 : tmp_vec) {
                for(vector<string>::iterator it = tmp_index_vec.begin(); it!= tmp_index_vec.end(); ) {
                    if(*it == v2) it = tmp_index_vec.erase(it);
                    else it++;
                }
            }
        }

        for(string v : tmp_index_vec) tmp.push_back(v);
        qSort(tmp.begin(), tmp.end());
        current_scenes_vec = tmp;

        for(string v : tmp) {
            if(v.find("-") != string::npos) {
                const string merge_png_filepath = project_path_ + "/merged_maps/" + v + ".png";
                flow->addWidget(createImageButton(QPixmap(merge_png_filepath.c_str()), v));
            } else if(scanned_scene_lst.count(v) != 0) {
                const string scan_bmp_filepath = GetBagDirectory() + v + ".bmp";
                flow->addWidget(createImageButton(QPixmap(scan_bmp_filepath.c_str()), v));
            } else {
                flow->addWidget(createImageButton(QPixmap(""), v));
            }
        }

        /*
        for(int i = 0; i < scene_layout_.size(); i++) {
            const std::string scene_id = to_string(i + 1);

            if(scanned_scene_lst.count(scene_id) != 0) {
                const std::string scan_bmp_filepath = GetBagDirectory() + scene_id + ".bmp";
                flow->addWidget(createImageButton(QPixmap(scan_bmp_filepath.c_str()), scene_id));
            } else {
                flow->addWidget(createImageButton(QPixmap(""), scene_id));
            }
        }
        */
    }
    // 初始化 场景提示块
    void ProjectView::initStationScenesNoteBlock() {
        ROS_INFO("ProjectView -> initStationScenesNoteBlock()");

        int index = 1;
        int m = 0, n = 0;

        QFont f; f.setPointSize(18);

        // Scanned
        for(int i = 0; i < scene_rows_; ++i) {
            for(int j = 0; j < scene_cols_; ++j) {
                QLabel *l = new QLabel();

                l->setFont(f);
                l->setFixedSize(28, 28);
                l->setAlignment(Qt::AlignCenter);
                l->setStyleSheet("background-color: gray; color: black;");

                if(to_string(index) == id_vec[m]) {
                    l->setText(QString::fromStdString(to_string(m + 1)));
                    l->setStyleSheet("background-color: rgb(244, 150, 111); color: black;");

                    if(scanned_scene_lst.count(to_string(m + 1)) != 0) {
                        l->setStyleSheet("background-color: green; color: black;");
                    }
                    m++;
                }

                ui_->station_scenes_note_block->addWidget(l, i, j);
                index++;
            }
        }
        // Registrated
        vector<int> registratedSceneIds = project_controller_->GetRegistratedSceneIds();
        if(registratedSceneIds.size() != 0) {
            int count = 0;
            vector<int> registratedSceneIdIndexs;

            for(int i = 0; i < index_vec.size(); i++) {
                if(atoi(index_vec[i].c_str()) == registratedSceneIds[count]) {
                    registratedSceneIdIndexs.push_back(atoi(id_vec[i].c_str()));
                    count++;

                    if(count == registratedSceneIds.size()) break;
                }
            }

            int k = 0;
            for(int i = 0; i < ui_->station_scenes_note_block->count(); i++) {
                if((i + 1) == registratedSceneIdIndexs[k]) {
                    ui_->station_scenes_note_block->itemAt(i)->widget()->setStyleSheet("background-color: blue; color: black;");
                    k++;
                }
            }
        }
        // Merged
        set<string> tmp_vec;
        for(string v1 : current_scenes_vec) {
            if(v1.find("-") != string::npos) {
                vector<string> vec = split(v1, "-");
                for(string v2 : vec) {
                    tmp_vec.insert(id_vec[atoi(v2.c_str()) - 1]);
                }
            }
        }
        if(tmp_vec.size() != 0) {
            set<string>::iterator it;
            it = tmp_vec.begin();

            for(int i = 0; i < ui_->station_scenes_note_block->count(); i++) {
                if(to_string(i + 1) == *it) {
                    ui_->station_scenes_note_block->itemAt(i)->widget()->setStyleSheet("background-color: red; color: black;");
                    it++;
                }
            }
        }

        /*
        int index = 1;
        int m = 0, n = 0;
        vector<vector<string>> scene_blocks(scene_rows_, vector<string>(scene_cols_, ""));

        for(const auto scene : scene_layout_) {
            scene_blocks[scene.row][scene.col] = scene.scene_id;
        }

        for(int i = 0; i < scene_rows_; ++i) {
            for(int j = 0; j < scene_cols_; ++j) {
                std::string scene_id = scene_blocks[i][j];
                QLabel *l = new QLabel(QString::fromStdString(scene_blocks[i][j]));

                QFont f ;
                f.setPointSize(18);

                l->setFont(f); l->setFixedSize(28, 28); l->setAlignment(Qt::AlignCenter);
                l->setStyleSheet("background-color: gray; color: black;");

                if(scanned_scene_lst.count(scene_id) != 0) {
                    l->setStyleSheet("background-color: green; color: black;");
                }
                if(registrated_scene_lst.count(scene_id) != 0) {
                    l->setStyleSheet("background-color: red; color: black;");
                }

                ui_->station_scenes_note_block->addWidget(l, i, j);
                index++;
            }
        }
        */
    }

    // 创建场景列表
    QToolButton* ProjectView::createImageButton(const QPixmap &icon, const std::string idx) {
        QToolButton *button = new QToolButton();

        button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        button->setStyleSheet("background-color: white;");
        button->setIconSize(QSize(70, 70));
        button->setIcon(QIcon(icon));
        button->setText(QString::fromStdString(idx));
        button->setWhatsThis(QString::fromStdString(idx));
        button->setToolTip(QString::fromStdString(idx));
        button->setFixedSize(120, 120);

        connect(button, SIGNAL(clicked()), SLOT(sceneListClicked()));
        toolButtonIndex++;

        return button;
    }

    // 显示点云
    void ProjectView::sceneListClicked() {
        ROS_INFO("ProjectView -> sceneListClicked()");

        QToolButton *button = (QToolButton*)sender();
        scene_id_ = button->whatsThis().toStdString();
        button->setStyleSheet("background-color: rgb(244, 150, 111);");

        int count = 0;
        for(string v : current_scenes_vec) {
            if(v == scene_id_) break;
            count++;
        }

        for(int i = 0; i < flow->count(); ++i) {
            if(i != count) {
                flow->itemAt(i)->widget()->setStyleSheet("background-color: white;");
            }
        }

        clearPcl();
        showPcl(scene_id_);
    }

    void ProjectView::showPcl(string scene_id) {
        ROS_INFO("ProjectView -> showPcl()");
        showScenePcl(scene_id);
        //showScene2dMap(scene_id);
    }
    bool ProjectView::showScenePcl(string scene_id) {
        ROS_INFO("ProjectView -> showScenePcl()");

        if(isLeftFocused) {
            ui_->scene_pcl_title_left->setText(QString::fromStdString("场景:" + scene_id));
        } else {
            ui_->scene_pcl_title_right->setText(QString::fromStdString("场景:" + scene_id));
        }

        string ply_path;
        bool is_scene_merged = false;

        if(scene_id.find("-") != string::npos) {
            ROS_INFO("merged scene!");

            is_scene_merged = true;
            ply_path = project_path_ + "/colored_maps/" + scene_id + ".ply";

            struct stat buffer;
            // No Colorized
            if(stat(ply_path.c_str(), &buffer) != 0) {
                ROS_INFO("%s scene no colorized!", scene_id.c_str());
                ply_path = project_path_ + "/merged_maps/" + scene_id + ".ply";
            }
        } else {
            if(!project_controller_->IsSceneBuilt(scene_id)) {
                ROS_INFO("%s scene no built!", scene_id.c_str());
                return false;
            }
        }

        string pcl_path = project_controller_->GetScenePcl(scene_id);

        QProgressDialog progress("正在加载点云", "", 0, 100, this);
        progress.setWindowTitle("加载点云");
        progress.setCancelButton(nullptr);
        progress.setWindowModality(Qt::WindowModal);

        pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPLYFile(is_scene_merged ? ply_path : pcl_path, *point_cloud);

        progress.setValue(90);

        if(isLeftFocused) {
            scene_pcl_viewer_l->addPointCloud(point_cloud, "cloud");
            scene_pcl_viewer_l->resetCamera();
            ui_->scene_pcl_container_left->update();
        } else {
            scene_pcl_viewer_r->addPointCloud(point_cloud, "cloud");
            scene_pcl_viewer_r->resetCamera();
            ui_->scene_pcl_container_right->update();
        }
        progress.setValue(100);

        return true;
    }

    bool ProjectView::showScene2dMap(string scene_id) {
        ROS_INFO("ProjectView -> showScene2dMap()");

        /*
        string map2d_path = project_controller_->GetScene2dMap(scene_id);
        map2d = QPixmap(map2d_path.c_str());

        ui_->scene_map_container->setPixmap(
            map2d.scaled(ui_->scene_map_container->width(),
                         ui_->scene_map_container->height(),
                         Qt::KeepAspectRatio));
        //ui_->scene_map_container->resize(map2d.size());
        */
        return true;
    }

    void ProjectView::resizeEvent(QResizeEvent * e) {
        //ROS_INFO("ProjectView -> resizeEvent()");
        /*
        if(!map2d.isNull()) {
            ui_->scene_map_container->setPixmap(
                map2d.scaled(ui_->scene_map_container->width(),
                             ui_->scene_map_container->height(),
                             Qt::KeepAspectRatio));
        }
        */
    }

    void ProjectView::clearPcl() {
        clearScenePcl();
        //clearScene2dMap();
    }
    void ProjectView::clearScenePcl() {
        if(isLeftFocused) {
            scene_pcl_viewer_l->removeAllPointClouds();
            ui_->scene_pcl_container_left->update ();
        } else {
            scene_pcl_viewer_r->removeAllPointClouds();
            ui_->scene_pcl_container_right->update ();
        }
    }
    void ProjectView::clearScene2dMap() {
        /*
        map2d = QPixmap();
        ui_->scene_map_container->clear();
        //ui_->merged_pcl_container->update();
        */
    }

    // 清空
    void ProjectView::CleanPage() {
        ROS_INFO("ProjectView -> CleanPage()");
    }

    // 更新当前页面
    void ProjectView::UpdatePage() {
        ROS_INFO("ProjectView -> UpdatePage()");

        UpdateMappingHistory();

        // 清空flow并重新加载
        QLayoutItem *f_child;
        while((f_child = flow->takeAt(0)) != 0) {
            flow->removeWidget(f_child->widget());
            f_child->widget()->setParent(0);
            delete f_child;
        }
        flow->update();

        initFlowLayout();

        // 更新 场景提示块
        ROS_INFO("update sences block display");

        QLayoutItem *s_child;
        while((s_child = ui_->station_scenes_note_block->takeAt(0)) != 0) {
            ui_->station_scenes_note_block->removeWidget(s_child->widget());
            s_child->widget()->setParent(0);
            delete s_child;
        }
        ui_->station_scenes_note_block->update();

        initStationScenesNoteBlock();
        ui_->station_scenes_note_block->update();
    }
    // 更新 station.bmp
    void ProjectView::UpdateStationBmp() {
        ROS_INFO("ProjectView -> UpdateStationBmp()");

        string in_f_p  = project_path_ + "/bags/" + scene_id_ + ".bmp";
        string out_f_p = project_path_ + "/station.bmp";

        std::ifstream in(in_f_p, ios::binary);
        std::ofstream out(out_f_p, ios::binary);

        if(!in) { ROS_INFO("open %s error", in_f_p.c_str()); return; }
        if(!out) { ROS_INFO("open %s error", out_f_p.c_str()); return; }

        out << in.rdbuf();

        in.close();
        out.close();
    }

    // Scan
    void ProjectView::scanBtnClickEvent() {
        ROS_INFO("ProjectView -> scanBtnClickEvent()");

        if(scene_id_ != "") {
            if(scene_id_.find("-") != string::npos) {
                QMessageBox::information(this, "提示信息！", "合并场景无需扫图！");
                return;
            }

            ScanView *scannerWindow = new ScanView(this, scene_id_, project_path_);
            scannerWindow->setWindowTitle(QString::fromStdString("场景-" + scene_id_ + "扫描"));
            scannerWindow->setObjectName("ScannerWindow-New");
            scannerWindow->installEventFilter(this);
            //scannerWindow->setGeometry((QApplication::desktop()->width() - 1200) / 2,
            //                            (QApplication::desktop()->height() - 800) / 2,
            //                             1200, 600);
            scannerWindow->showMaximized();

            scannerWindow->exec();
            ROS_INFO("Scanner Finished");

            UpdatePage();       // Update flow layout
            UpdateStationBmp(); // Copy scene_id.bmp to project-root-directory as station.bmp
        } else { QMessageBox::information(this, "点云扫描", "请选择需要扫描的点云场景！"); }
    }
    // Scan-Extend
    void ProjectView::scanExtendBtnClickEvent() {
        ROS_INFO("ProjectView -> scanExtendBtnClickEvent()");

        if(scene_id_ != "") {
            if(scene_id_.find("-") != string::npos) {
                QMessageBox::information(this, "提示信息！", "合并场景无需补扫！");
                return;
            }

            if(project_controller_->IsSceneScanned(scene_id_)) {
                ScanView *scannerWindow = new ScanView(this, scene_id_, project_path_, true);
                scannerWindow->setWindowTitle(QString::fromStdString("场景-" + scene_id_ + "补扫"));

                scannerWindow->setObjectName("ScannerWindow-Extend");
                scannerWindow->setGeometry((QApplication::desktop()->width() - 1016) / 2,
                                           (QApplication::desktop()->height() - 600) / 2,
                                           1016, 600);
                // scannerWindow->setWindowFlags(Qt::Dialog);
                scannerWindow->exec();

                if(!scannerWindow->IsScanned()) return;

                // supplement if the scene is registrated.
                if(project_controller_->IsSceneBuilt(scene_id_)) {
                    //rebuild scene map
                    pdw = new ProgressDialogWidget(this, "建图进度", "正在建图...");
                    project_controller_->startSupplement(scene_id_);
                    pdw->exec();

                    showScenePcl(scene_id_);
                    UpdatePage();
                }
            } else QMessageBox::information(this, "点云补扫", "场景尚未扫描！");
        } else QMessageBox::information(this, "点云补扫", "请选择需要补扫的点云场景！");
    }

    // Build(Registrate)
    void ProjectView::registrateBtnClickEvent() {
        ROS_INFO("ProjectView -> registrateBtnClickEvent()");

        if(scene_id_ == "") {
            QMessageBox::information(this, "建图", "请选择需要建图的场景！");
            return;
        }

        if(scene_id_.find("-") != string::npos) {
            QMessageBox::information(this, "提示信息！", "合并场景无需建图！");
            return;
        } else {
            if(!project_controller_->IsSceneScanned(scene_id_)) {
                QMessageBox::information(this, "提示信息！", "当前场景未扫描！");
                return;
            } else {
                if(!project_controller_->IsReadyToRegistrate(scene_id_)) {
                    QMessageBox::information(this, "提示信息！", "当前所需建图场景与已建场景不相邻！");
                    return;
                } else {
                    string note = "确定开始场景" + scene_id_ + "建图吗？";
                    QMessageBox::StandardButton result = QMessageBox::information(NULL, "建图(拼接)",
                                                                                  QString::fromStdString(note),
                                                                                  QMessageBox::Yes | QMessageBox::No,
                                                                                  QMessageBox::Yes);
                    if(result == QMessageBox::Yes) startRegistrate(scene_id_);
                }
            }
        }
    }

    // Merge(Colorize)
    void ProjectView::mergeBtnClickEvent() {
        ROS_INFO("ProjectView -> mergeBtnClickEvent()");

        string l_scene_id = split(ui_->scene_pcl_title_left->text().toStdString(), ":")[1];
        string r_scene_id = split(ui_->scene_pcl_title_right->text().toStdString(), ":")[1];

        if(l_scene_id.empty() && r_scene_id.empty()) {
            QMessageBox::information(this, "点云拼接", "请选择需要拼接的点云场景！");
            return;
        }

        bool isLeftReady  = false; bool isRightReady = false;

        if(!l_scene_id.empty()) {
            if(l_scene_id.find("-") == string::npos) {
                // Is Scanned or Registrated(build)?
                if(!project_controller_->IsSceneScanned(l_scene_id) || !project_controller_->IsSceneRegistrated(l_scene_id)) {
                    QMessageBox::information(this, "点云拼接", "当前场景未扫描或未建图！");
                    return;
                }
            }
            isLeftReady = true;
        }
        if(!r_scene_id.empty()) {
            if(r_scene_id.find("-") == string::npos) {
                if(!project_controller_->IsSceneScanned(r_scene_id) || !project_controller_->IsSceneRegistrated(r_scene_id)) {
                    QMessageBox::information(this, "点云拼接", "当前场景未扫描或未建图！");
                    return;
                }
            }
            isRightReady = true;
        }

        if(isLeftReady && isRightReady) {
            if(project_controller_->checkConnexity(l_scene_id, r_scene_id, scene_rows_, scene_cols_)) {
                string note = "确定开始合并场景" + l_scene_id + "和" + r_scene_id +"吗？";
                QMessageBox::StandardButton result = QMessageBox::information(NULL, "地图合并",
                                                                              QString::fromStdString(note),
                                                                              QMessageBox::Yes | QMessageBox::No,
                                                                              QMessageBox::Yes);
                if(result == QMessageBox::Yes) startMerge(l_scene_id, r_scene_id);
                //if(result == QMessageBox::Yes) startMerge(scene_id_);
            } else {
                QMessageBox::information(this, "点云拼接", "场景不相邻，请重新选择！");
                return;
            }
        }
        // Left or Right is empty
        else {
            if(isLeftReady) {
                if(l_scene_id.find("-") == string::npos) {
                    //QMessageBox::information(this, "点云拼接", "场景暂不支持自拼接！");
                    //return;
                    string note = "确定开始自合并场景" + l_scene_id + "吗？";
                    QMessageBox::StandardButton result = QMessageBox::information(NULL, "地图合并",
                                                                                  QString::fromStdString(note),
                                                                                  QMessageBox::Yes | QMessageBox::No,
                                                                                  QMessageBox::Yes);
                    if(result == QMessageBox::Yes) startSelfMerge(l_scene_id);
                } else {
                    QMessageBox::information(this, "点云拼接", "场景已拼接！");
                    return;
                }
            }
            if(isRightReady) {
                if(r_scene_id.find("-") == string::npos) {
                    //QMessageBox::information(this, "点云拼接", "场景暂不支持自拼接！");
                    //return;
                    string note = "确定开始自合并场景" + r_scene_id + "吗？";
                    QMessageBox::StandardButton result = QMessageBox::information(NULL, "地图合并",
                                                                                  QString::fromStdString(note),
                                                                                  QMessageBox::Yes | QMessageBox::No,
                                                                                  QMessageBox::Yes);
                    if(result == QMessageBox::Yes) startSelfMerge(r_scene_id);
                } else {
                    QMessageBox::information(this, "点云拼接", "场景已拼接！");
                    return;
                }
            }
        }

        /*
        if(scene_id_ == "") {
            QMessageBox::information(this, "提示信息", "请选择需要合并的点云场景！");
            return;
        }

        if(!project_controller_->IsSceneRegistrated(scene_id_)) {
            QMessageBox::information(this, "提示信息！", "当前场景未拼接！");
            return;
        }

        string note = "确定开始合并场景" + scene_id_ + "吗？";
        QMessageBox::StandardButton result = QMessageBox::information(NULL, "地图合并",
                                                                      QString::fromStdString(note),
                                                                      QMessageBox::Yes | QMessageBox::No,
                                                                      QMessageBox::Yes);
        if(result ==QMessageBox::Yes) startMerge(scene_id_);
        */
    }

    // 格式转换
    void ProjectView::convertBtnClickEvent() {
        ROS_INFO("ProjectView -> convertBtnClickEvent()");

        convertSelectView = new ConvertSelectView(project_path_, this);
        convertSelectView->setWindowModality(Qt::ApplicationModal);
        convertSelectView->exec();
    }

    // 全站预览
    void ProjectView::previewBtnClickEvent() {
        ROS_INFO("ProjectView -> previewBtnClickEvent()");

        if(!project_controller_->IsStationMapReady()) {
            QMessageBox::information(this, "提示信息！", "当前场景未扫描,请扫描！");
            return;
        }

        PCLViewer *previewWindow = new PCLViewer(this);
        previewWindow->setWindowTitle("电站预览");

        previewWindow->show();
        previewWindow->displayPcl(project_controller_->GetStationMapFilePath()) ;

        return;
    }

    // 建图
    int ProjectView::COUNT_NUM = 3;
    // 开始建图
    void ProjectView::startRegistrate(const std::string& scene_id) {
        ROS_INFO("ProjectView -> startRegistrate(%s)", scene_id.c_str());

        if(!project_controller_->IsReadyForRegistrate(scene_id)) {
            QMessageBox::information(this, "提示信息!", "场景数据错误(ply/pbstream文件不存在)！");
            return;
        }

        pdw = new ProgressDialogWidget(this, "建图/拼接进度", "正在建图...");
        project_controller_->startBuild(scene_id_);

        if(pdw->isCanceled()) { /* TODO.*/ }

        pdw->exec();
        delete pdw;

        ROS_INFO("registrate scene files!");

        pdw = new ProgressDialogWidget(this, "Registrate", "正在registrating，请稍候...");
        // 进度显示
        if(project_controller_->startRegistrate(scene_id_)) pdw->exec();
        delete pdw;

        showPcl(scene_id);

        CleanPage();
        UpdatePage();

        return;
    }

    // Start Merge
    //void ProjectView::startMerge(std::string scene_id) {
    void ProjectView::startMerge(std::string l_scene_id, std::string r_scene_id) {
        ROS_INFO("ProjectView -> startMerge(%s, %s)", l_scene_id.c_str(), r_scene_id.c_str());

        ROS_INFO("Merge Scene Files!");

        pdw = new ProgressDialogWidget(this, "场景合并", "正在合并，请稍候...");
        if(project_controller_->startMerge(l_scene_id, r_scene_id)) pdw->exec();
        delete pdw;

        PCLViewer *previewWindow = new PCLViewer(this);
        previewWindow->setObjectName("PreviewWindow");
        previewWindow->installEventFilter(this);
        previewWindow->setWindowTitle("电站预览");
        previewWindow->showMaximized();
        //previewWindow->show();
        previewWindow->displayPcl(project_controller_->GetStationMapFilePath()) ;

        ROS_INFO("Merge & Preview Finished");
        UpdatePage();

        return;

        /*
        if(!project_controller_->IsSceneScanned(scene_id)) {
            QMessageBox::information(this, "Error!", QString::fromStdString(std::string("Scene ") +
                                                                            scene_id + " is not scanned, scan the scene first!"));
            return;
        }
        if(!project_controller_->IsReadyToMerge(scene_id)) {
            // first build map of
            QMessageBox message_box;
            message_box.setText("请完成拼接流程!");
            message_box.exec();
            return;
        }

        ROS_INFO("merge scene files!");

        pdw = new ProgressDialogWidget(this, "合并进度", "正在合并，请稍候...");
        if(project_controller_->startMerge(scene_id_)) pdw->exec();

        delete pdw;

        PCLViewer *previewWindow = new PCLViewer(this);
        previewWindow->setWindowTitle("电站预览");

        previewWindow->show();
        previewWindow->displayPcl(project_controller_->GetStationMapFilePath()) ;

        return;
        */
    }
    void ProjectView::startSelfMerge(std::string scene_id) {
        ROS_INFO("ProjectView -> startSelfMerge(%s)", scene_id.c_str());

        ROS_INFO("Merge Scene Files!");

        pdw = new ProgressDialogWidget(this, "场景自合并", "正在合并，请稍候...");
        if(project_controller_->startSelfMerge(scene_id)) pdw->exec();
        delete pdw;

        PCLViewer *previewWindow = new PCLViewer(this);
        previewWindow->setWindowTitle("电站预览");

        previewWindow->show();
        previewWindow->displayPcl(project_controller_->GetStationMapFilePath()) ;

        return;
    }

    // 关闭窗口
    void ProjectView::closeEvent(QCloseEvent *event) {
        ROS_INFO("ProjectView -> closeEvent()");
        QWidget::closeEvent(event);
    }

    void ProjectView::UpdateMappingHistory() {
        ROS_INFO("ProjectView -> UpdateMappingHistory()");

        scanned_scene_lst     = project_controller_->GetScannedScenes(scene_cols_ * scene_rows_);
        registrated_scene_lst = project_controller_->GetMergedScenes(scene_cols_ * scene_rows_);
    }

    void ProjectView::controllerProgressCallback(ProjectController::ProcessStatus status, float percent) {
        pdw->setProgress(percent);
        switch(status) {
            case ProjectController::ProcessStatus::kBuildMap :
            {
                pdw->setNote("正在建图...");
                break;
            }
            case ProjectController::ProcessStatus::kWriteMap :
            {
                pdw->setNote("正在生成地图...");
                break;
            }
            case ProjectController::ProcessStatus::kMergeMap :
            {
                pdw->setNote("正在合并地图...");
                break;
            }
            case ProjectController::ProcessStatus::kColorizeMap :
            {
                pdw->setNote("正在着色...");
                break;
            }
            case ProjectController::ProcessStatus::kPostProcess :
            {
                pdw->setNote("正在后处理...");
                break;
            }
            case ProjectController::ProcessStatus::kRegistrate :
            {
                pdw->setNote("正在拼接...");
                break;
            }
            case ProjectController::ProcessStatus::kComplete :
            {
                pdw->setNote("处理OK!...");
                pdw->close();
                break;
            }
            default : break;
        }
    }

    // Event Filter
    bool ProjectView::eventFilter(QObject *target, QEvent *e) {
        // Left Click
        if(e->type() == QEvent::MouseButtonPress) {
            QMouseEvent *event = static_cast<QMouseEvent*>(e);

            if(event->button() == Qt::LeftButton) {
                // Show Current Focus Scene
                if(target->objectName() == "scene_pcl_title_left") {
                    isLeftFocused = true;
                    ui_->scene_pcl_title_left->setStyleSheet("background-color: rgb(244, 150, 111);");
                    ui_->scene_pcl_title_right->setStyleSheet("background-color: #00ffffff;");
                } else if(target->objectName() == "scene_pcl_title_right") {
                    isLeftFocused = false;
                    ui_->scene_pcl_title_left->setStyleSheet("background-color: #00ffffff;");
                    ui_->scene_pcl_title_right->setStyleSheet("background-color: rgb(244, 150, 111);");
                }
            }
        }

        return false;
    }
}
