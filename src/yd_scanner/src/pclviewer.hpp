#pragma once

#include <iostream>

// Qt
#include <QMainWindow>

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "progress_dialog_widget.hpp"

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

namespace Ui { class PCLViewer; }

class PCLViewer : public QMainWindow {
    Q_OBJECT
        public:
            explicit PCLViewer(QWidget *parent = 0);
            void displayPcl(std::string pcl_filepath);

            ~PCLViewer ();

        public Q_SLOTS:
            void pSliderValueChanged(int value);
            void cCombocurrentTextChanged(QString value);

        protected:
            pcl::visualization::PCLVisualizer::Ptr viewer;

        private:
            void loadPcl(std::string pcl_filepath);

            /** @brief The point cloud displayed */
            Ui::PCLViewer *ui_;
            PointCloudT::Ptr cloud_;

            /** @brief Holds the color mode for @ref colorCloudDistances */
            yd_scanner_ui::ProgressDialogWidget *pdw;
};
