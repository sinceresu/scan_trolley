#pragma once
#include <string>
#include <deque>

namespace yd_scanner_ui {
    typedef struct SceneLayoutItem {
        std::string scene_id;
        int row;
        int col;
    } SceneLayoutItem;

    using SceneLayout = std::deque<SceneLayoutItem>;
}
