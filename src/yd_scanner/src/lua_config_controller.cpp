#include "lua_config_controller.hpp"

#include "fstream"
#include "stdio.h"
#include "utils.h"

#define FILE_MAX_LINE_NUM 5000
#define DISABLE_STRING "!@#$%^&*()_+"

int COUNT, TABLE_COUNT, TABLE_FLAG;
bool is_init = false;

string SRC, DES;

using namespace std;
using namespace yd_scanner_ui;

// 重置
void LuaConfigController::reset() {
    COUNT = 0; TABLE_COUNT = 0; TABLE_FLAG = 0;

    is_init = false;

    SRC = "";
    DES = "";
}

// Constructor & Destructor
LuaConfigController::LuaConfigController() {
    printf("LuaConfigController -> constructor\n");
    // to do sth.
}
LuaConfigController::~LuaConfigController() {
    printf("LuaConfigController -> destructor\n");
    // reset
    reset();
}

/// 读取Lua初始参数(TODO: 待重构)
string setPrecision(double d) {
    string finalStr;
    vector<string> vec = split(to_string(d), ".");

    if(atof(vec[1].c_str()) == 0) {
        finalStr = vec[0] + ".0";
    } else {
        string str;
        str.assign(vec[1].rbegin(), vec[1].rend());

        const char *ch = str.data();
        int len = str.length();

        for(int i = 0; i < str.length(); i++) {
            if(ch[i] == '0') { len--; }
            else { break; }
        }

        finalStr = vec[0] + "." + vec[1].substr(0, len);
    }

    return finalStr;
}
// map_build
vector<string> LuaConfigController::readMapBuild(string config_dir_path, string file_name) {
    printf("LuaConfigController -> readMapBuild()\n");
    vector<string> vs;

    auto file_resolver            = absl::make_unique<cartographer::common::ConfigurationFileResolver>(vector<string>{ config_dir_path });
    string content                = file_resolver->GetFileContentOrDie(file_name);
    auto lua_parameter_dictionary = cartographer::common::LuaParameterDictionary::NonReferenceCounted(content, move(file_resolver));

    // pcl_filter: filter_pcl
    auto pcl_filter = lua_parameter_dictionary->GetDictionary("pcl_filter");
    // build: do_landmark
    auto build = lua_parameter_dictionary->GetDictionary("build");

    vs.push_back(to_string(pcl_filter->GetBool("filter_pcl")));
    vs.push_back(to_string(build->GetBool("do_landmark")));

    return vs;
}
// build_writer
vector<string> LuaConfigController::readBuildWriter(string config_dir_path, string file_name) {
    printf("LuaConfigController -> readBuildWriter()\n");
    vector<string> vs;

    auto file_resolver            = absl::make_unique<cartographer::common::ConfigurationFileResolver>(vector<string>{ config_dir_path });
    string content                = file_resolver->GetFileContentOrDie(file_name);
    auto lua_parameter_dictionary = cartographer::common::LuaParameterDictionary::NonReferenceCounted(content, move(file_resolver));

    auto pipeline = lua_parameter_dictionary->GetDictionary("pipeline");
    vector<unique_ptr<cartographer::common::LuaParameterDictionary>> values = pipeline->GetArrayValuesAsDictionaries();

    for(int i = 0; i < values.size(); i++) {
        if(strcmp("min_max_range_filter", values[i]->GetString("action").c_str()) == 0) {
            build_minMaxRangeFilterTableDepth = values[i]->GetKeys().size() - 1;

            vs.push_back(setPrecision(values[i]->GetDouble("min_range")));
            vs.push_back(setPrecision(values[i]->GetDouble("max_range")));
        }
        if(strcmp("horizontal_range_filter", values[i]->GetString("action").c_str()) == 0) {
            build_horizontalRangeFilterTableDepth = values[i]->GetKeys().size() - 1;
            vs.push_back(setPrecision(values[i]->GetDouble("max_range")));
        }
        if(strcmp("vertical_range_filter", values[i]->GetString("action").c_str()) == 0) {
            vs.push_back(setPrecision(values[i]->GetDouble("min_z")));
            vs.push_back(setPrecision(values[i]->GetDouble("max_z")));
        }
    }

    return vs;
}
// map_registrate
vector<string> LuaConfigController::readMapRegistrate(string config_dir_path, string file_name) {
    printf("LuaConfigController -> readMapRegistrate()\n");
    vector<string> vs;

    auto file_resolver            = absl::make_unique<cartographer::common::ConfigurationFileResolver>(vector<string>{ config_dir_path });
    string content                = file_resolver->GetFileContentOrDie(file_name);
    auto lua_parameter_dictionary = cartographer::common::LuaParameterDictionary::NonReferenceCounted(content, move(file_resolver));

    // registrate: leaf_size, icp_iterations
    auto registrate = lua_parameter_dictionary->GetDictionary("registrate");
    vs.push_back(setPrecision(registrate->GetDouble("leaf_size")));
    vs.push_back(to_string(registrate->GetInt("icp_iterations")));

    return vs;
}
// merge_writer
vector<string> LuaConfigController::readMergeWriter(string config_dir_path, string file_name) {
    printf("LuaConfigController -> readMergeWriter()\n");
    vector<string> vs;

    auto file_resolver            = absl::make_unique<cartographer::common::ConfigurationFileResolver>(vector<string>{ config_dir_path });
    string content                = file_resolver->GetFileContentOrDie(file_name);
    auto lua_parameter_dictionary = cartographer::common::LuaParameterDictionary::NonReferenceCounted(content, move(file_resolver));

    auto pipeline = lua_parameter_dictionary->GetDictionary("pipeline");
    vector<unique_ptr<cartographer::common::LuaParameterDictionary>> values = pipeline->GetArrayValuesAsDictionaries();

    for(int i = 0 ; i < values.size(); i++) {
        if(strcmp("min_max_range_filter", values[i]->GetString("action").c_str()) == 0) {
            merge_minMaxRangeFilterTableDepth = values[i]->GetKeys().size() - 1;

            vs.push_back(setPrecision(values[i]->GetDouble("min_range")));
            vs.push_back(setPrecision(values[i]->GetDouble("max_range")));
        }
        if(strcmp("horizontal_range_filter", values[i]->GetString("action").c_str()) == 0) {
            merge_horizontalRangeFilterTableDepth = values[i]->GetKeys().size() - 1;
            vs.push_back(setPrecision(values[i]->GetDouble("max_range")));
        }
        if(strcmp("fixed_ratio_sampler", values[i]->GetString("action").c_str()) == 0) {
            vs.push_back(setPrecision(values[i]->GetDouble("sampling_ratio")));
        }
        if(strcmp("vertical_range_filter", values[i]->GetString("action").c_str()) == 0) {
            vs.push_back(setPrecision(values[i]->GetDouble("min_z")));
            vs.push_back(setPrecision(values[i]->GetDouble("max_z")));
        }
    }

    return vs;
}
// map_colorize
vector<string> LuaConfigController::readMapColorize(string config_dir_path, string file_name) {
    printf("LuaConfigController -> readMapColorize()\n");
    vector<string> vs;

    auto file_resolver            = absl::make_unique<cartographer::common::ConfigurationFileResolver>(vector<string>{ config_dir_path });
    string content                = file_resolver->GetFileContentOrDie(file_name);
    auto lua_parameter_dictionary = cartographer::common::LuaParameterDictionary::NonReferenceCounted(content, move(file_resolver));

    // pcl_filter: filter_pcl
    auto pcl_filter = lua_parameter_dictionary->GetDictionary("pcl_filter");
    vs.push_back(to_string(pcl_filter->GetBool("filter_pcl")));

    // colorize: skip_seconds, occlude_distance, plane_distances
    auto colorize = lua_parameter_dictionary->GetDictionary("colorize");
    vs.push_back(setPrecision(colorize->GetDouble("skip_seconds")));
    //vs.push_back(setPrecision(colorize->GetDouble("occlude_distance")));

    auto plane_distances  = colorize->GetDictionary("plane_distances");
    vector<double> values = plane_distances->GetArrayValuesAsDoubles();

    vs.push_back(setPrecision(values[0]));
    vs.push_back(setPrecision(values[1]));

    return vs;
}
// colorize_writer
vector<string> LuaConfigController::readColorizeWriter(string config_dir_path, string file_name) {
    printf("LuaConfigController -> readColorizeWriter()\n");
    vector<string> vs;

    auto file_resolver            = absl::make_unique<cartographer::common::ConfigurationFileResolver>(vector<string>{ config_dir_path });
    string content                = file_resolver->GetFileContentOrDie(file_name);
    auto lua_parameter_dictionary = cartographer::common::LuaParameterDictionary::NonReferenceCounted(content, move(file_resolver));

    auto pipeline = lua_parameter_dictionary->GetDictionary("pipeline");
    vector<unique_ptr<cartographer::common::LuaParameterDictionary>> values = pipeline->GetArrayValuesAsDictionaries();

    for(int i = 0 ; i < values.size(); i++) {
        if(strcmp("min_max_range_filter", values[i]->GetString("action").c_str()) == 0) {
            colorize_minMaxRangeFilterTableDepth = values[i]->GetKeys().size() - 1;

            vs.push_back(setPrecision(values[i]->GetDouble("min_range")));
            vs.push_back(setPrecision(values[i]->GetDouble("max_range")));
        }
        if(strcmp("horizontal_range_filter", values[i]->GetString("action").c_str()) == 0) {
            colorize_horizontalRangeFilterTableDepth = values[i]->GetKeys().size() - 1;
            vs.push_back(setPrecision(values[i]->GetDouble("max_range")));
        }
        if(strcmp("voxel_filter_and_remove_moving_objects", values[i]->GetString("action").c_str()) == 0) {
            vs.push_back(to_string(values[i]->GetInt("miss_per_hit_limit")));
        }
        if(strcmp("vertical_range_filter", values[i]->GetString("action").c_str()) == 0) {
            vs.push_back(setPrecision(values[i]->GetDouble("min_z")));
            vs.push_back(setPrecision(values[i]->GetDouble("max_z")));
        }
    }

    return vs;
}

/// 保存Lua修改参数
// Update Src&Des(TODO: 待重构)
void updateSrcDes(vector<string> v, int idx) {
    if(idx < v.size()) {
        string tmp = v[idx];

        vector<string> v1 = split(tmp, "|");
        SRC = v1[0];
        DES = v1[1];

        //printf("SRC: [%s], Des: [%s]\n", SRC.c_str(), DES.c_str());

        if(strcmp(SRC.c_str(), "action") == 0) {
            if(!is_init) {
                is_init = true;

                TABLE_COUNT = atoi(split(DES, "*")[1].c_str());

                idx++;
                COUNT = idx;
                TABLE_FLAG++;

                updateSrcDes(v, idx);
            } else {
                if(TABLE_FLAG >= TABLE_COUNT) {
                    TABLE_FLAG = 0;

                    TABLE_COUNT = atoi(split(DES, "*")[1].c_str());

                    idx++;
                    COUNT = idx;
                    TABLE_FLAG++;

                    updateSrcDes(v, idx);
                }
            }
        }
    } else {
        SRC = DISABLE_STRING;
        DES = DISABLE_STRING;
    }
}
// Save To File
bool LuaConfigController::saveToFile(string p, vector<string> v) {
    printf("LuaConfigController -> saveToFile()\n");

    ifstream in_f;
    ofstream out_f;
    string lines[FILE_MAX_LINE_NUM];

    in_f.open(p);
    if(!in_f.is_open()) {
        printf("%s open failed!\n", p.c_str());
        return false;
    }

    int count = 0, pos, len;
    updateSrcDes(v, COUNT);

    for(string s; getline(in_f, s); count++) {
        if(strcmp(SRC.c_str(), "action") == 0) {
            COUNT++;
            TABLE_FLAG++;
            updateSrcDes(v, COUNT);
        } else {
            len = SRC.length();
            pos = s.find(SRC);

            if(pos >= 0) {
                s.replace(pos, len, DES);
                COUNT++;
                TABLE_FLAG++;
                updateSrcDes(v, COUNT);
            }
        }
        lines[count] = s;
    }

    in_f.clear();
    in_f.close();

    out_f.open(p);

    for(int j = 0; j <= count; j++) {
        if(j == count) { out_f << lines[j]; }
        else { out_f << lines[j] << endl; }
    }

    out_f.clear();
    out_f.close();

    reset();

    return true;
}
