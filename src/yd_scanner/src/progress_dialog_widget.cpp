#include "progress_dialog_widget.hpp"
#include "ui_progress_view.h"

#include "utils.h"

#include <ostream>

using namespace Qt;
using namespace std;
using namespace yd_scanner_ui;


ProgressDialogWidget::ProgressDialogWidget(QWidget *parent, QString title, QString fNote, bool can_cancel) : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint)
, ui_ (new Ui::ProgressView)
, progressValue(0)
, isForceClose(false)
, is_canceled_(false)
{
    // setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    ui_->setupUi (this);

    ui_->l->setText(fNote);
    ui_->qpb->setValue(0);

    ui_->cancel_btn->setEnabled(can_cancel);
    connect(ui_->cancel_btn, SIGNAL(clicked()), SLOT(onCancelClicked()));

    int nWidth = 200;
    int nHeight = 100;
    if (parent != NULL)
        setGeometry(parent->x() + parent->width()/2 - nWidth/2,
            parent->y() + parent->height()/2 - nHeight/2,
            nWidth, nHeight);
    else
        resize(nWidth, nHeight);

    setWindowTitle(title);

    // timer = new QTimer(this);
    // connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    // timer->start(500);
}
ProgressDialogWidget::~ProgressDialogWidget() {}

// 设置提示文字信息
void ProgressDialogWidget::setNote(QString str) {
    //ROS_INFO("ProgressDialogWidget -> setNote()");
    ui_->l->setText(str);
}
// 设置进度
void ProgressDialogWidget::setProgress(int v) {
    //ROS_INFO("ProgressDialogWidget -> setProgress()");
    progressValue = v;
    ui_->qpb->setValue(progressValue);
}
// 进度完成，自动close
// void ProgressDialogWidget::update() {
//     if(progressValue == 100) {
//         timer->stop();
//         this->close();
//     }
// }

// 关闭窗口
void ProgressDialogWidget::closeEvent(QCloseEvent *e) {
    ROS_INFO("ProgressDialogWidget -> close()");

    if(isForceClose) { e->accept(); }
    else {
        if(progressValue < 100) {
            e->ignore();
            QMessageBox::information(this, "系统提示", "任务处理中！");
        } else {
            e->accept();
            //sleep(1);
            //progressValue = 0;
        }
    }
}

void ProgressDialogWidget::onCancelClicked() {
    is_canceled_ = true;
    close();
}
