#ifndef SCENES_EDIT_VIEW_HPP
#define SCENES_EDIT_VIEW_HPP

#include "progress_dialog_widget.hpp"

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

// Qt5
#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>

#include <vector>
#include <string>

#include "string.h"

using namespace Qt;
using namespace std;

namespace Ui { class ScenesEditView; }
namespace yd_scanner_ui {
    class ScenesEditController;

    class ScenesEditView : public QDialog {
        Q_OBJECT
            public:
                ScenesEditView(int *rs, int *cs, string *sceneIdIndexMapInfo, QWidget *parent = 0);
                ScenesEditView(int rs, int cs, vector<string> idVec, QWidget *parent = 0);
                ~ScenesEditView();

            private:
                /// Variables
                Ui::ScenesEditView *ui_;
                QList<QPushButton *> scenesButtonLst;
                ProgressDialogWidget *pdw;

                int *sceneROWs, *sceneCOLs;
                string *sceneIdIndexMapInfoStr;
                int Rs, Cs;

                QList<int> checkedScenes;
                map<map<int, int>, int> sceneIdCoordinateMap;
                map<int, int> sceneIdIndexMap;

                /// Fun
                void dialogInit();
                void uiInit();

                void sceneIdCoordinateInit(int Rs, int Cs);

                void saveProgressCallback(float percent);
                void doSave();

                /// Event
                // 关闭窗口
                void closeEvent(QCloseEvent *event);

                // ScenesEditController
                boost::shared_ptr<ScenesEditController> scenes_edit_controller;
                bool isSaved = false;
                boost::thread save_thread;

            private Q_SLOTS:
                void setLevels();
                void reset();
                void completed();

                void sceneChecked();
    };
}
#endif
