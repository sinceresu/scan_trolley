#ifndef DEVICE_MONITOR_VIEW_HPP
#define DEVICE_MONITOR_VIEW_HPP

//Qt5
#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QtOpenGL/QtOpenGL>

#include <vector>
#include <string>

using namespace Qt;
using namespace std;

namespace Ui { class DeviceMonitorView; }

namespace yd_scanner_ui {
    class DeviceMonitorView : public QDialog {
        Q_OBJECT
            public:
                // Constructor & Destructor
                DeviceMonitorView(QWidget *parent = 0);
                ~DeviceMonitorView();

            private:
                /// Variables
                Ui::DeviceMonitorView *ui_;

                double MEM_TOTAL; double MEM_USE;
                double DISK_USE;  double DISK_FREE;
                double CPU_RATE;

                QProcess *p;
                int run_time;

                // Devices Status Value Label
                QLabel *l1_v, *l2_v, *l3_v, *l4_v, *l5_v, *l6_v,  // VLP
                       *l7_v, *l8_v, *l9_v,                       // IMU
                       *l10_v, *l11_v, *l12_v,                    // GPS

                       *lfl1_v, *lfl2_v, *lfl3_v,                 // LeftFront Camera
                       *rfl1_v, *rfl2_v, *rfl3_v;                 // RightFront Camera

                /// Fun
                void getComputerResourcesInfo();
                void devicesStatusDialogInit();

                /// Event
                void closeEvent(QCloseEvent *event);

            private Q_SLOTS:
                void devicesStatusUiUpdate(QString qstr);

                void  outputInfo();
                void  errorInfo();
    };
}
#endif
