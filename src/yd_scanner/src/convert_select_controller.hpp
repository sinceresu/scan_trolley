#ifndef CONVERT_SELECT_CONTROLLER_HPP
#define CONVERT_SELECT_CONTROLLER_HPP

// TODO: test E57
/* PROBLEM:
In file included from /usr/include/vtk-6.3/vtkAtomicTypes.h:18:0,
                 from /usr/include/vtk-6.3/vtkObjectBase.h:46,
                 from /usr/include/vtk-6.3/vtkSmartPointerBase.h:27,
                 from /usr/include/vtk-6.3/vtkSmartPointer.h:23,
                 from /usr/include/pcl-1.8/pcl/visualization/point_cloud_geometry_handlers.h:48,
                 from /usr/include/pcl-1.8/pcl/visualization/point_cloud_handlers.h:41,
                 from /usr/include/pcl-1.8/pcl/visualization/common/actor_map.h:40,
                 from /usr/include/pcl-1.8/pcl/visualization/pcl_visualizer.h:48,
                 from yd_scanner_autogen/UVLADIE3JM/../../../../src/yd_scanner/src/pclviewer.hpp:11,
                 from yd_scanner_autogen/UVLADIE3JM/moc_pclviewer.cpp:9,
                 from yd_scanner_autogen/mocs_compilation.cpp:9:
/usr/include/vtk-6.3/vtkAtomic.h:245:15: error: reference to ‘detail’ is ambiguous
       typedef detail::AtomicOps<sizeof(T)> Impl;

   MODIFY:
   -> /usr/include/vtk-6.3/vtkAtomic.h
   1. -> typedef detail::AtomicOps<sizeof(T)> Impl;
      to
      typedef ::detail::AtomicOps<sizeof(T)> Impl;

   2. -> typedef detail::AtomicOps<sizeof(T*)> Impl;
      to
      typedef ::detail::AtomicOps<sizeof(T*)> Impl;

   3. -> typedef detail::AtomicOps<sizeof(void*)> Impl;
      to
      typedef ::detail::AtomicOps<sizeof(void*)> Impl;
*/
//#include "e57.h"

#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>
#include <QProcess>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <iostream>
#include <sstream>

#include <vector>
#include <string>

#include "string.h"

using namespace Qt;
using namespace std;

namespace yd_scanner_ui {
    class ConvertSelectController : QObject {
        Q_OBJECT
            public:
                ConvertSelectController();
                ~ConvertSelectController();

                /// Variables
                //E57 *e57;
                QProcess *process;

                // 转换进度回调
                using ConvertProgressCallback = std::function<void(float percentage)>;
                void setConvertProgressCallback(ConvertSelectController::ConvertProgressCallback call_back) {
                    convert_progress_call_back = call_back;
                };

                /// Fun
                // 获取 ply-filename-list
                QStringList getPlys(string file_type_path);
                // 转换
                bool convert(string file_path, string type, string pointSize);

            private:
                bool isConvertCompleted;
                ConvertProgressCallback convert_progress_call_back = nullptr;

                void convertToPCD(string file_path, string new_file_path);
                //void convertToE57(string file_path, string new_file_path, float scale_factor);

            private Q_SLOTS:
                void convertProcessOutputInfo();
                void convertProcessErrorInfo();
                void convertProcessFinished();
    };
}
#endif
