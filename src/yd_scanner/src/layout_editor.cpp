﻿#include "layout_editor.hpp"
#include "ui_layout_editor.h"


#include <algorithm>
#include <fstream>
#include <iostream>
#include <thread>

#include <stdlib.h>

#include <QtCore/QtCore>
#include <QtGui/QtGui>

#include <QtWidgets/QGroupBox>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>

#include "time.h"
// #include "utils.h"


// PCL
#include<pcl/io/io.h>
#include<pcl/io/ply_io.h>
#include<pcl/point_types.h>
#include<pcl/common/common.h>

#include "project_controller.hpp"

#include "glog/logging.h"

using namespace Qt;

using namespace boost::filesystem;
namespace {


}
namespace yd_scanner_ui {

LayoutEditor::LayoutEditor(int rows, int cols, const SceneLayout& scene_layout) : QDialog() ,
  ui_ (new Ui::LayoutEditor),
  rows_(rows),
  cols_(cols),
  layout_(scene_layout),
  selected_row_(-1),
  selected_col_(-1)
{
   ui_->setupUi (this);

    initSceneLayout();
}
LayoutEditor::~LayoutEditor() {}

SceneLayout LayoutEditor::getLayout(){
    return layout_;
}


void LayoutEditor::clearSceneLayout() {
    LOG(INFO) << "update sence display";
    QLayoutItem *s_child;
    while((s_child = ui_->gridLayout_scenes->takeAt(0)) != 0) {
         ui_->gridLayout_scenes->removeWidget(s_child->widget());
        s_child->widget()->setParent(0);
        delete s_child;
    }
}


void LayoutEditor::initSceneLayout() {
    std::vector<std::vector<std::string>> scene_blocks(rows_, std::vector<std::string>(cols_, ""));

    for(const auto scene : layout_) {
        scene_blocks[scene.row][scene.col] = scene.scene_id;
    }
    for(int i = 0; i < scene_blocks.size(); ++i) {
        for(int j = 0; j < scene_blocks[i].size(); ++j) {
            QToolButton *button = new QToolButton();

            button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
            button->setStyleSheet("background-color: white;");
            button->setIconSize(QSize(70, 70));
            // button->setIcon(QIcon(icon));
            button->setText(QString::fromStdString(scene_blocks[i][j]));
            button->setFixedSize(120, 120);
            connect(button, SIGNAL(clicked()), SLOT(sceneSelected()));
            ui_->gridLayout_scenes->addWidget(button, i, j);
        }

        connect(ui_->pushButton_delete, SIGNAL(clicked()), SLOT(deleteSelectedScene()));
        connect(ui_->pushButton_close, SIGNAL(clicked()), SLOT(close()));

    }

}

void LayoutEditor::deleteSelectedScene() {
    auto iter = layout_.begin();
    for(; iter != layout_.end(); iter++) {
        if (iter->row == selected_row_ && iter->col == selected_col_) {
            break;
        }
    }
    if (iter == layout_.end()) {
        return;
    }

    auto next_iter = iter + 1;
    for (; next_iter != layout_.end(); iter++, next_iter++) {
        iter->col = next_iter->col; 
        iter->row = next_iter->row; 
    }
    layout_.pop_back();

    clearSceneLayout();
    initSceneLayout();
}

void LayoutEditor::sceneSelected() {
    QToolButton *scene_label = (QToolButton*)sender();
    int idx = 0;
    for (; idx <  ui_->gridLayout_scenes->count(); idx++) {
        if (ui_->gridLayout_scenes->itemAt(idx)->widget() == scene_label)
            break;
    }
    int ros_span, col_span;
    ui_->gridLayout_scenes->getItemPosition(idx, &selected_row_, &selected_col_, &ros_span, &col_span);

    for (int idx = 0; idx <  ui_->gridLayout_scenes->count(); idx++) {
        ui_->gridLayout_scenes->itemAt(idx)->widget()->setStyleSheet("background-color: white;");
    }

    scene_label->setStyleSheet("background-color: rgb(244, 150, 111);");

}

void LayoutEditor::deleteScene(int row, int col) {

}

void LayoutEditor::addScene(int row, int col) {


}


}