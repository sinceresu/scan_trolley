#include "utils.h"

#include <stdio.h>
#include <memory.h>

#include <iostream>
#include <fstream>
#include <ctime>

#include <boost/filesystem.hpp>

using namespace std;

// 按格式获取当前系统时间戳string
string get_system_cur_time() {
    time_t raw_time;
    struct tm * time_info;
    time(&raw_time);
    time_info = localtime(&raw_time);

    char buffer[50];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d-%H:%M:%S", time_info);

    string time_str(buffer);
    memset(buffer, 0, sizeof(buffer));

    return time_str;
}

// 字符串分割函数
vector<string> split(string str, string pattern) {
    string::size_type pos;
    vector<string> result;

    // 拓展字符串，方便操作
    str += pattern;
    int size = str.size();

    for (int i = 0; i < size; i++) {
        pos = str.find(pattern, i);
        if (pos < size) {
            string s = str.substr(i, pos - i);
            result.push_back(s);
            i = pos + pattern.size() - 1;
        }
    }

    return result;
}

// 以指定字符串new_str替换原字符串string中的字符串old_str
string replace(string &str, const string &old_value, const string &new_value) {
    for(string::size_type pos(0); pos != string::npos; pos += new_value.length()) {
        if((pos = str.find(old_value, pos)) != string::npos) {
            str.replace(pos, old_value.length(), new_value);
        } else { break; }
    }

    return str;
}

// base64解、编码
static const string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";

static inline bool is_base64(unsigned char c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
}
// 编码
string base64_encode(unsigned char const *bytes_to_encode, unsigned int in_len) {
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];

    string ret;

    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for (i = 0; (i < 4); i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }

    if (i) {
        for (j = i; j < 3; j++)
            char_array_3[j] = '\0';

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

        for (j = 0; (j < i + 1); j++)
            ret += base64_chars[char_array_4[j]];

        while ((i++ < 3))
            ret += '=';
    }

    return ret;
}
// 解码
string base64_decode(string const &encoded_string) {
    int i   = 0;
    int j   = 0;
    int in_ = 0;

    size_t in_len = encoded_string.size();
    unsigned char char_array_4[4], char_array_3[3];

    string ret;
    while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
        char_array_4[i++] = encoded_string[in_];
        in_++;

        if (i == 4) {
            for (i = 0; i < 4; i++)
                char_array_4[i] = base64_chars.find(char_array_4[i]) & 0xff;

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (i = 0; (i < 3); i++)
                ret += char_array_3[i];
            i = 0;
        }
    }

    if (i) {
        for (j = 0; j < i; j++)
            char_array_4[j] = base64_chars.find(char_array_4[j]) & 0xff;

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

        for (j = 0; (j < i - 1); j++)
            ret += char_array_3[j];
    }

    return ret;
}

// 解析json
Json::Value readJsonInfo(string path) {
    ifstream fs(path.c_str(), ios::binary);
    Json::Reader reader;
    Json::Value root;

    if(!fs.is_open()) {
        cout << "json file open error!" << endl;
    } else {
        reader.parse(fs, root);
    }

    fs.close();
    return root;
}

// 根据路径获取文件名
void split_and_replace_path(string &path, const string &src, const string &dst) {
    string::size_type pos = 0;
    string::size_type src_len = src.size();
    string::size_type dst_len = dst.size();

    while((pos = path.find(src, pos)) != string::npos) {
        path.replace(pos, src_len, dst);
        pos += dst_len;
    }
}
string getFileNameFromPath(string path) {
    if(path.empty()) { return ""; }

    split_and_replace_path(path, "/", "\\");
    string::size_type pos = path.find_last_of('\\') + 1;

    return path.substr(pos, path.length() - pos);
}

// Vector元素 去重
vector<string> vector_unique_element(vector<string> v) {
    vector<string>::iterator it;
    sort(v.begin(), v.end());

    it = unique(v.begin(), v.end());
    if(it != v.end()) { v.erase(it, v.end()); }

    return v;
}

// 拼接log消息字符串
string getLogInfo(sensor_msgs::PointCloud2 vlpMsg1,
                  sensor_msgs::PointCloud2 vlpMsg2,

                  sensor_msgs::Image flCamMsg,
                  sensor_msgs::Image frCamMsg,
                  sensor_msgs::Image blCamMsg,
                  sensor_msgs::Image brCamMsg,
                  //sensor_msgs::CompressedImage lCamMsg,
                  //sensor_msgs::CompressedImage rCamMsg,

                  sensor_msgs::Imu imuMsg,
                  sensor_msgs::NavSatFix gpsMsg) {

    string logStr     = "";
    string l_fields_s = "";
    string s_fields_s = "";

    for(int i = 0; i < vlpMsg1.fields.size(); ++i) {
        l_fields_s += "{\n"
                      "INT8 = " + to_string(vlpMsg1.fields[i].INT8) + "\n"
                      "UINT8 = " + to_string(vlpMsg1.fields[i].UINT8) + "\n"
                      "INT = " + to_string(vlpMsg1.fields[i].INT16) + "\n"
                      "UINT = " + to_string(vlpMsg1.fields[i].UINT16) + "\n"
                      "INT32 = " + to_string(vlpMsg1.fields[i].INT32) + "\n"
                      "UINT32 = " + to_string(vlpMsg1.fields[i].UINT32) + "\n"
                      "FLOAT32 = " + to_string(vlpMsg1.fields[i].FLOAT32) + "\n"
                      "FLOAT64 = " + to_string(vlpMsg1.fields[i].FLOAT64) + "\n"
                      "name = " + vlpMsg1.fields[i].name + "\n"
                      "offset = " + to_string(vlpMsg1.fields[i].offset) + "\n"
                      "datatype = " + to_string(vlpMsg1.fields[i].datatype) + "\n"
                      "count = " + to_string(vlpMsg1.fields[i].count) + "\n"
                      "}\n";
    }
    for(int i = 0; i < vlpMsg2.fields.size(); ++i) {
        s_fields_s += "{\n"
                      "INT8 = " + to_string(vlpMsg2.fields[i].INT8) + "\n"
                      "UINT8 = " + to_string(vlpMsg2.fields[i].UINT8) + "\n"
                      "INT = " + to_string(vlpMsg2.fields[i].INT16) + "\n"
                      "UINT = " + to_string(vlpMsg2.fields[i].UINT16) + "\n"
                      "INT32 = " + to_string(vlpMsg2.fields[i].INT32) + "\n"
                      "UINT32 = " + to_string(vlpMsg2.fields[i].UINT32) + "\n"
                      "FLOAT32 = " + to_string(vlpMsg2.fields[i].FLOAT32) + "\n"
                      "FLOAT64 = " + to_string(vlpMsg2.fields[i].FLOAT64) + "\n"
                      "name = " + vlpMsg2.fields[i].name + "\n"
                      "offset = " + to_string(vlpMsg2.fields[i].offset) + "\n"
                      "datatype = " + to_string(vlpMsg2.fields[i].datatype) + "\n"
                      "count = " + to_string(vlpMsg2.fields[i].count) + "\n"
                      "}\n";
    }

    logStr = "Localization VLP16 >>> \n"
             "header: \n "
             "seq = " + to_string(vlpMsg1.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(vlpMsg1.header.stamp.toBoost()) + "\n"
             "width = " + to_string(vlpMsg1.width) + "\n"
             "height = " + to_string(vlpMsg1.height) + "\n"
             "fields: \n" + l_fields_s +
             "is_bigendian = " + to_string(vlpMsg1.is_bigendian) + "\n"
             "point_step = " + to_string(vlpMsg1.point_step) + "\n"
             "row_step = " + to_string(vlpMsg1.row_step) + "\n"
             "data(size) = " + to_string(vlpMsg1.data.size()) + "\n"
             "is_dense = " + to_string(vlpMsg1.is_dense) + "\n"

             "Scan VLP16 >>> \n"
             "header: \n"
             "seq = " + to_string(vlpMsg2.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(vlpMsg2.header.stamp.toBoost()) + "\n"
             "width = " + to_string(vlpMsg2.width) + "\n"
             "height = " + to_string(vlpMsg2.height) + "\n"
             "fields: \n" + s_fields_s +
             "is_bigendian = " + to_string(vlpMsg2.is_bigendian) + "\n"
             "point_step = " + to_string(vlpMsg2.point_step) + "\n"
             "row_step = " + to_string(vlpMsg2.row_step) + "\n"
             "data(size) = " + to_string(vlpMsg2.data.size()) + "\n"
             "is_dense = " + to_string(vlpMsg2.is_dense) + "\n"

             "Camera Left Front >>> \n"
             "header: \n"
             "seq = " + to_string(flCamMsg.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(flCamMsg.header.stamp.toBoost()) + "\n"
             "width = " + to_string(flCamMsg.width) + "\n"
             "height = " + to_string(flCamMsg.height) + "\n"
             "encoding = " + flCamMsg.encoding + "\n"
             "is_bigendian = " + to_string(flCamMsg.is_bigendian) + "\n"
             "step = " + to_string(flCamMsg.step) + "\n"
             "data(size) = " + to_string(flCamMsg.data.size()) + "\n"

             "Camera Right Front >>> \n"
             "header: \n"
             "seq = " + to_string(frCamMsg.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(frCamMsg.header.stamp.toBoost()) + "\n"
             "width = " + to_string(frCamMsg.width) + "\n"
             "height = " + to_string(frCamMsg.height) + "\n"
             "encoding = " + frCamMsg.encoding + "\n"
             "is_bigendian = " + to_string(frCamMsg.is_bigendian) + "\n"
             "step = " + to_string(frCamMsg.step) + "\n"
             "data(size) = " + to_string(frCamMsg.data.size()) + "\n"

             "Camera Left Behind >>> \n"
             "header: \n"
             "seq = " + to_string(blCamMsg.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(blCamMsg.header.stamp.toBoost()) + "\n"
             "width = " + to_string(blCamMsg.width) + "\n"
             "height = " + to_string(blCamMsg.height) + "\n"
             "encoding = " + blCamMsg.encoding + "\n"
             "is_bigendian = " + to_string(blCamMsg.is_bigendian) + "\n"
             "step = " + to_string(blCamMsg.step) + "\n"
             "data(size) = " + to_string(blCamMsg.data.size()) + "\n"

             "Camera Right Behind >>> \n"
             "header: \n"
             "seq = " + to_string(brCamMsg.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(brCamMsg.header.stamp.toBoost()) + "\n"
             "width = " + to_string(brCamMsg.width) + "\n"
             "height = " + to_string(brCamMsg.height) + "\n"
             "encoding = " + brCamMsg.encoding + "\n"
             "is_bigendian = " + to_string(brCamMsg.is_bigendian) + "\n"
             "step = " + to_string(brCamMsg.step) + "\n"
             "data(size) = " + to_string(brCamMsg.data.size()) + "\n"

             "IMU >>> \n"
             "header: \n"
             "seq = " + to_string(imuMsg.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(imuMsg.header.stamp.toBoost()) + "\n"
             "orientation: \n"
             "x = " + to_string(imuMsg.orientation.x) + "\n"
             "y = " + to_string(imuMsg.orientation.y) + "\n"
             "z = " + to_string(imuMsg.orientation.z) + "\n"
             "w = " + to_string(imuMsg.orientation.w) + "\n"
             "orientation_covariance: " + to_string(imuMsg.orientation_covariance[0]) + " " +
                                          to_string(imuMsg.orientation_covariance[1]) + " " +
                                          to_string(imuMsg.orientation_covariance[2]) + " " +
                                          to_string(imuMsg.orientation_covariance[3]) + " " +
                                          to_string(imuMsg.orientation_covariance[4]) + " " +
                                          to_string(imuMsg.orientation_covariance[5]) + " " +
                                          to_string(imuMsg.orientation_covariance[6]) + " " +
                                          to_string(imuMsg.orientation_covariance[7]) + " " +
                                          to_string(imuMsg.orientation_covariance[8]) + "\n"
             "angular_velocity: \n"
             "x = " + to_string(imuMsg.angular_velocity.x) + "\n"
             "y = " + to_string(imuMsg.angular_velocity.y) + "\n"
             "z = " + to_string(imuMsg.angular_velocity.z) + "\n"
             "angular_velocity_covariance: " + to_string(imuMsg.angular_velocity_covariance[0]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[1]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[2]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[3]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[4]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[5]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[6]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[7]) + " " +
                                               to_string(imuMsg.angular_velocity_covariance[8]) + "\n"
             "linear_acceleration: \n"
             "x = " + to_string(imuMsg.linear_acceleration.x) + "\n"
             "y = " + to_string(imuMsg.linear_acceleration.y) + "\n"
             "z = " + to_string(imuMsg.linear_acceleration.z) + "\n"
             "linear_acceleration_covariance: " + to_string(imuMsg.linear_acceleration_covariance[0]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[1]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[2]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[3]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[4]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[5]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[6]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[7]) + " " +
                                                  to_string(imuMsg.linear_acceleration_covariance[8]) + "\n"
             "GPS >>> \n"
             "header: \n"
             "seq = " + to_string(gpsMsg.header.seq) + "\n"
             "stamp = " + boost::posix_time::to_iso_extended_string(gpsMsg.header.stamp.toBoost()) + "\n"
             "COVARIANCE_TYPE_UNKNOWN = " + to_string(gpsMsg.COVARIANCE_TYPE_UNKNOWN) + "\n"
             "COVARIANCE_TYPE_APPROXIMATED = " + to_string(gpsMsg.COVARIANCE_TYPE_APPROXIMATED) + "\n"
             "COVARIANCE_TYPE_DIAGONAL_KNOWN = " + to_string(gpsMsg.COVARIANCE_TYPE_DIAGONAL_KNOWN) + "\n"
             "COVARIANCE_TYPE_KNOWN = " + to_string(gpsMsg.COVARIANCE_TYPE_KNOWN) + "\n"

             "status: \n"
             "STATUS_NO_FIX = " + to_string(gpsMsg.status.STATUS_NO_FIX) + "\n"
             "STATUS_FIX = " + to_string(gpsMsg.status.STATUS_NO_FIX) + "\n"
             "STATUS_SBAS_FIX = " + to_string(gpsMsg.status.STATUS_SBAS_FIX) + "\n"
             "STATUS_GBAS_FIX = " + to_string(gpsMsg.status.STATUS_GBAS_FIX) + "\n"
             "SERVICE_GPS = " + to_string(gpsMsg.status.SERVICE_GPS) + "\n"
             "SERVICE_GLONASS = " + to_string(gpsMsg.status.SERVICE_GLONASS) + "\n"
             "SERVICE_COMPASS = " + to_string(gpsMsg.status.SERVICE_COMPASS) + "\n"
             "SERVICE_GALILEO = " + to_string(gpsMsg.status.SERVICE_GALILEO) + "\n"
             "status = " + to_string(gpsMsg.status.status) + "\n"
             "service = " + to_string(gpsMsg.status.service) + "\n"

             "latitude = " + to_string(gpsMsg.latitude) + "\n"
             "longitude = " + to_string(gpsMsg.longitude) + "\n"
             "altitude = " + to_string(gpsMsg.altitude) + "\n"
             "position_covariance: " + to_string(gpsMsg.position_covariance[0]) + " " +
                                       to_string(gpsMsg.position_covariance[1]) + " " +
                                       to_string(gpsMsg.position_covariance[2]) + " " +
                                       to_string(gpsMsg.position_covariance[3]) + " " +
                                       to_string(gpsMsg.position_covariance[4]) + " " +
                                       to_string(gpsMsg.position_covariance[5]) + " " +
                                       to_string(gpsMsg.position_covariance[6]) + " " +
                                       to_string(gpsMsg.position_covariance[7]) + " " +
                                       to_string(gpsMsg.position_covariance[8]) + "\n"
             "position_covariance_type: " + to_string(gpsMsg.position_covariance_type);

    return logStr;
}
bool copy_dir(
    boost::filesystem::path const & source,
    boost::filesystem::path const & destination
)
{
    namespace fs = boost::filesystem;
    try
    {
        // Check whether the function call is valid
        if(
            !fs::exists(source) ||
            !fs::is_directory(source)
        )
        {
            std::cerr << "Source directory " << source.string()
                << " does not exist or is not a directory." << '\n'
            ;
            return false;
        }
        if(fs::exists(destination))
        {
            std::cerr << "Destination directory " << destination.string()
                << " already exists." << '\n'
            ;
            return false;
        }
        // Create the destination directory
        if(!fs::create_directory(destination))
        {
            std::cerr << "Unable to create destination directory"
                << destination.string() << '\n'
            ;
            return false;
        }
    }
    catch(fs::filesystem_error const & e)
    {
        std::cerr << e.what() << '\n';
        return false;
    }
    // Iterate through the source directory
    for(
        fs::directory_iterator file(source);
        file != fs::directory_iterator(); ++file
    )
    {
        try
        {
            fs::path current(file->path());
            if(fs::is_directory(current))
            {
                // Found directory: Recursion
                if(
                    !copy_dir(
                        current,
                        destination / current.filename()
                    )
                )
                {
                    return false;
                }
            }
            else
            {
                // Found file: Copy
                fs::copy_file(
                    current,
                    destination / current.filename()
                );
            }
        }
        catch(fs::filesystem_error const & e)
        {
            std:: cerr << e.what() << '\n';
        }
    }
    return true;
}

bool copyDir(
    const std::string& src_dir,
    const std::string & dst_dir
)
{
    boost::filesystem::path source(src_dir);
    boost::filesystem::path destination(dst_dir);
    return copy_dir(source, destination);
}
