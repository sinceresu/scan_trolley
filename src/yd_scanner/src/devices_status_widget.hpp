#ifndef DEVICES_STATUS_WIDGET_HPP
#define DEVICES_STATUS_WIDGET_HPP

#ifndef Q_MOC_RUN
    #include "ros/ros.h"
    // ROS MESSAGE
    #include <sensor_msgs/PointCloud2.h>
    #include <sensor_msgs/Image.h>
    #include <sensor_msgs/Imu.h>
    #include <sensor_msgs/NavSatFix.h>
#endif

//Qt5
#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>

#ifndef WITHOUT_ROS
#include "qnode.hpp"
#endif

using namespace Qt;
using namespace std;

namespace yd_scanner_ui {
    class DevicesStatusWidget : public QWidget {
        Q_OBJECT
            public:
                DevicesStatusWidget();
                ~DevicesStatusWidget();

                // Variables
                static double MEM_TOTAL; static double MEM_USE;
                static double DISK_USE;  static double DISK_FREE;
                static double CPU_RATE;

                QProcess *p;
                static int run_time;

                QNode *qnode;

                // UI
                QLabel *l1_v, *l2_v, *l3_v, *l1__v, *l2__v, *l3__v,
                       *l10_v, *l11_v, *l12_v, *l13_v, *l14_v, *l15_v,
                       *l16_v, *l17_v, *l18_v, *l19_v, *l20_v, *l21_v;

                // Fun
                void getComputerResourcesInfo();

                // Event
                void closeEvent(QCloseEvent *event);

            public Q_SLOTS:
                // 设备状态更新SLOT
                //void devicesStatusUiUpdate(QString qstr);
                void closeDevicesStatusDialog();

                void  outputInfo();
                void  errorInfo();

            Q_SIGNALS:

            protected:
    };
}
#endif
