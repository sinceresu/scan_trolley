#ifndef SCENES_EDIT_CONTROLLER_HPP
#define SCENES_EDIT_CONTROLLER_HPP

#include "check_connexity.hpp"

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

#include <QtGui/QtGui>
#include <QtCore/QtCore>
#include <QtOpenGL/QtOpenGL>

#include <algorithm>

#include <map>
#include <set>
#include <vector>
#include <string>

#include <iostream>

#include "string.h"

using namespace Qt;
using namespace std;

namespace yd_scanner_ui {
    class ScenesEditController {
        public:
            ScenesEditController();
            ~ScenesEditController();

            /// Variables
            using SaveProgressCallback = std::function<void(float percentage)>;
            void setSaveProgressCallback(ScenesEditController::SaveProgressCallback call_back) {
                save_progress_call_back = call_back;
            };

            /// Fun
            bool checkConnexity(map<int, int> sceneIdIndexMap, int Rs, int Cs);

        private:
            /// Variables
            boost::shared_ptr<CheckConnexity> check_connexity_ptr;
            SaveProgressCallback save_progress_call_back = nullptr;

            // Funs
    };
}
#endif
