#include "Eigen/Eigen"

//Qt5
#include <QtGui/QtGui>
#include <QtCore/QFile>
#include <QtWidgets/QApplication>
#include <QtCore/QTextCodec>

#include <iostream>
#include <fstream>
#include <string>
#include <thread>

#include <boost/filesystem.hpp>
#include <ros/package.h>

#include <math.h>
#include <urdf/model.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "fcntl.h"
#include "sys/stat.h"
#include "sys/wait.h"
#include "sys/types.h"
#include "sys/statfs.h"

#include "utils.h"
#include "main_view.hpp"
#include "qnode.hpp"

DEFINE_string(workspace_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_string(topics, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_string(luas, "", "Lua files need to config from launch '-luas'");

using namespace std;
using namespace yd_scanner_ui;
using namespace boost::filesystem;

bool initialize_workspace (string workspace_path) {
    if(!exists(workspace_path)) {
       if(!create_directory(workspace_path))
        return false;
    }

    std::string config_dir = workspace_path + "/" + "configs/";
    if(!exists(config_dir)) {
        create_directory(workspace_path + "/" + "configs");
        string proj_path     = QDir::currentPath().toStdString();
        string  package_path = ros::package::getPath(boost::filesystem::path(proj_path).stem().string());

        if(!copyDir(package_path + "/" + "configuration_files", config_dir + "configuration_files"))
            return false;
        if(!copyDir(package_path + "/urdf", config_dir + "urdf"))
            return false;
        if(!copyDir(package_path + "/styles", config_dir + "styles"))
            return false;
    }
    if(!exists(workspace_path + "/" + "projects")) {
        create_directory(workspace_path + "/" + "projects");
    }

    return true;
}

int main(int argc, char **argv) {
    ROS_INFO("yd_scanner -> main()");

    google::InitGoogleLogging(argv[0]);
    google::ParseCommandLineFlags(&argc, &argv, true);

    QApplication app(argc, argv);

    // 设置codec
    QTextCodec *codec = QTextCodec::codecForName("utf8");
    QTextCodec::setCodecForLocale(codec);

    // Workspace Path
    string work_space_path = FLAGS_workspace_directory + "/";
    ROS_INFO("Workspace Path: %s", work_space_path.c_str());

    if(!initialize_workspace(work_space_path)) return -1;

    QString qss;
    string filePath = work_space_path + "configs/styles/style.qss";
    ROS_INFO("style file path: %s", filePath.c_str());

    // TODO: test 直接读取资源文件目录 有问题
    //QFile styleFile(":/style/style.qss");
    QFile styleFile(QString::fromStdString(filePath));

    if(styleFile.open(QFile::ReadOnly)) {
        if(styleFile.isOpen()) {
            ROS_INFO("style.qss open success!");

            qss = QLatin1String(styleFile.readAll());
            qApp->setStyleSheet(qss);
            styleFile.close();
        } else { ROS_INFO("style.qss open failed2 (file borken)!"); }
    } else { ROS_INFO("style.qss open failed1 (no such file)!"); }

    // MainWindow
    // MainWindow w(argc, argv, work_space_path);
    MainView w(argc, argv, work_space_path);
    w.setGeometry(0, 0, 1050, 600);
    w.setWindowFlags(Qt::Dialog);
    w.move((QApplication::desktop()->width() - w.width()) / 2,
           (QApplication::desktop()->height() - w.height()) / 2);
    w.show();
    //w.showFullScreen();  // 丢失 窗口最大化/最小化/关闭按钮

    return app.exec();
}
