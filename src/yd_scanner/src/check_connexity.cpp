#include "check_connexity.hpp"

#include "stdio.h"
#include "utils.h"

using namespace std;
using namespace yd_scanner_ui;

/// Constructor & Destructor
// Constructor
CheckConnexity::CheckConnexity(map<int, int> sceneIdIndexMap, int rs, int cs) {
    printf("CheckConnexity -> constructor\n");

    this->sceneIdIndexMap = sceneIdIndexMap;
    this->Rs = rs;
    this->Cs = cs;

    // Initialize SceneIdCoordinateMap
    int val = 1;
    for(int i = 1; i <= rs; i++) {
        for(int j = 1; j <= cs; j++) {
            map<int, int> tmp_m;

            tmp_m[i] = j;
            sceneIdCoordinateMap[tmp_m] = val;
            val++;
        }
    }

    // Initialize scene_id_lst
    for(auto it = sceneIdIndexMap.begin(); it != sceneIdIndexMap.end(); it++) {
        scene_id_lst.push_back(it->second);
    }
}
// Destructor
CheckConnexity::~CheckConnexity() {
    printf("CheckConnexity -> destructor\n");
    // clear
    clear();
}

/// Funs
void CheckConnexity::clear() {
    printf("CheckConnexity -> clear()\n");

    sceneIdIndexMap.clear();
    sceneIdCoordinateMap.clear();
    idSet.clear();

    vector<int>().swap(scene_id_lst);
    COUNT = 0;
}
// Get Scene-Id Coordinate
vector<int> CheckConnexity::getSceneIdCoordinate(int scene_id) {
    vector<int> coors;

    for(auto coor_map_it = sceneIdCoordinateMap.begin();
        coor_map_it != sceneIdCoordinateMap.end(); coor_map_it++) {

        map<int, int> m = coor_map_it->first;
        int value       = coor_map_it->second;

        if(scene_id == value) {
            for(auto it = m.begin(); it != m.end(); it++) {
                coors.push_back(it->first);
                coors.push_back(it->second);
            }
        }
    }

    return coors;
}
// Get Value by Scene-Id Coordinate
int CheckConnexity::getValueBySceneIdCoordinate(int row, int col) {
    for(auto coor_map_it = sceneIdCoordinateMap.begin();
        coor_map_it != sceneIdCoordinateMap.end(); coor_map_it++) {

        map<int, int> m = coor_map_it->first;
        for(auto it = m.begin(); it != m.end(); it++) {
            if(it->first == row && it->second == col) {
                return sceneIdCoordinateMap[m];
            }
        }
    }

    return -1;
}
/************************TOP-RIGHT-DOWN-LEFT************************/
int CheckConnexity::getTop(int row, int col) {
    int top_row, top_col;

    top_row = row - 1;
    if(top_row <= 0) top_row = 0;

    top_col = col;

    if(top_row == 0 || top_col == 0) return 0;
    else return getValueBySceneIdCoordinate(top_row, top_col);
}
int CheckConnexity::getRight(int row, int col, int Cs) {
    int right_row, right_col;

    right_row = row;

    right_col = col + 1;
    if(right_col > Cs) right_col = 0;

    if(right_row == 0 || right_col == 0) return 0;
    else return getValueBySceneIdCoordinate(right_row, right_col);
}
int CheckConnexity::getDown(int row, int col, int Rs) {
    int down_row, down_col;

    down_row = row + 1;
    if(down_row > Rs) down_row = 0;

    down_col = col;

    if(down_row == 0 || down_col == 0) return 0;
    else return getValueBySceneIdCoordinate(down_row, down_col);
}
int CheckConnexity::getLeft(int row, int col) {
    int left_row, left_col;

    left_row = row;

    left_col = col - 1;
    if(left_col <= 0) left_col = 0;

    if(left_row == 0 || left_col == 0) return 0;
    else return getValueBySceneIdCoordinate(left_row, left_col);
}
/************************TOP-RIGHT-DOWN-LEFT************************/

// Is Connexity-Num in id_lst
bool CheckConnexity::is_in_id_lst(int num) {
    auto it = find(scene_id_lst.begin(), scene_id_lst.end(), num);
    if(it != scene_id_lst.end()) return true;
    return false;
}
// Is Connexity-Num in id_set
bool CheckConnexity::is_in_id_set(int num) {
    auto it = idSet.find(num);
    if(it != idSet.end()) return true;
    return false;
}

// Search
void CheckConnexity::search(int scene_id) {
    COUNT++;

    vector<int> coors = getSceneIdCoordinate(scene_id);
    cout << scene_id << "->" << "(" << coors[0] << ", " << coors[1] << ")" << endl;

    // top
    int topConnexityNum = getTop(coors[0], coors[1]);
    //cout << "top:" << topConnexityNum << endl;
    while(topConnexityNum != 0) {
        if(is_in_id_lst(topConnexityNum)) {
            if(!is_in_id_set(topConnexityNum)) {
                idSet.insert(topConnexityNum);
                search(topConnexityNum);
            }
        }
        break;
    }
    // right
    int rightConnexityNum = getRight(coors[0], coors[1], Cs);
    //cout << "right:" << rightConnexityNum << endl;
    while(rightConnexityNum != 0) {
        if(is_in_id_lst(rightConnexityNum)) {
            if(!is_in_id_set(rightConnexityNum)) {
                idSet.insert(rightConnexityNum);
                search(rightConnexityNum);
            }
        }
        break;
    }
    // down
    int downConnexityNum = getDown(coors[0], coors[1], Rs);
    //cout << "down:" << downConnexityNum << endl;
    while(downConnexityNum != 0) {
        if(is_in_id_lst(downConnexityNum)) {
            if(!is_in_id_set(downConnexityNum)) {
                idSet.insert(downConnexityNum);
                search(downConnexityNum);
            }
        }
        break;
    }
    // left
    int leftConnexityNum = getLeft(coors[0], coors[1]);
    //cout << "left:" << leftConnexityNum << endl;
    while(leftConnexityNum != 0) {
        if(is_in_id_lst(leftConnexityNum)) {
            if(!is_in_id_set(leftConnexityNum)) {
                idSet.insert(leftConnexityNum);
                search(leftConnexityNum);
            }
        }
        break;
    }

    return;
}
// Check
bool CheckConnexity::check() {
    printf("CheckConnexity -> check()\n");

    for(int i = 0; i < scene_id_lst.size(); i++) {
        idSet.insert(scene_id_lst[i]);
        search(scene_id_lst[i]);

        if(idSet.size() == scene_id_lst.size()) return true;
        else { if(idSet.size() != COUNT + 1) return false; }
    }

    if(idSet.size() == scene_id_lst.size()) {
        int count = 0;
        for(auto it = idSet.begin(); it != idSet.end(); it++) {
            if(*it != scene_id_lst[count]) return false;
            count++;
        }
        return true;
    }

    return false;
}
// Get Adjacent Connexity Num
vector<int> CheckConnexity::getAdjacentConnexityNums(int scene_id) {
    printf("CheckConnexity -> getAdjacentConnexityNums()\n");

    vector<int> adjacentNumsVec;

    vector<int> coors = getSceneIdCoordinate(scene_id);
    cout << scene_id << "->" << "(" << coors[0] << ", " << coors[1] << ")" << endl;

    int topConnexityNum   = getTop(coors[0], coors[1]);
    int rightConnexityNum = getRight(coors[0], coors[1], Cs);
    int downConnexityNum  = getDown(coors[0], coors[1], Rs);
    int leftConnexityNum  = getLeft(coors[0], coors[1]);

    if(topConnexityNum != 0) {
        if(is_in_id_lst(topConnexityNum)) adjacentNumsVec.push_back(topConnexityNum);
    }
    if(rightConnexityNum != 0) {
        if(is_in_id_lst(rightConnexityNum)) adjacentNumsVec.push_back(rightConnexityNum);
    }
    if(downConnexityNum != 0) {
        if(is_in_id_lst(downConnexityNum)) adjacentNumsVec.push_back(downConnexityNum);
    }
    if(leftConnexityNum != 0) {
        if(is_in_id_lst(leftConnexityNum)) adjacentNumsVec.push_back(leftConnexityNum);
    }

    qSort(adjacentNumsVec.begin(), adjacentNumsVec.end());

    return adjacentNumsVec;
}
