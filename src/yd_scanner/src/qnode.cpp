#include "qnode.hpp"
#include "device_error.hpp"

#include "boost/bind.hpp"

#include <ros/ros.h>
#include <ros/network.h>

#include <string>
#include <sstream>
#include <std_msgs/String.h>

#include "time.h"
#include "utils.h"
#include "gflags/gflags.h"

//Qt5
#include <QtCore/QProcess>
#include <QtWidgets/QApplication>

// PORT
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/types.h>

DECLARE_string(topics);

using namespace std;

namespace yd_scanner_ui {
    QNode::QNode(int argc, char** argv ) : init_argc(argc), init_argv(argv) { }
    QNode::~QNode() {
        if(ros::isStarted()) {
            ros::shutdown();
            ros::waitForShutdown();
        }
        wait();
    }

    DeviceError *deviceError;

    bool QNode::init() {
        ROS_INFO("QNode -> init()");
        ros::init(init_argc, init_argv, "yd_scanner");

        if(!ros::master::check()) return false;

        deviceError = new DeviceError();

        ros::start();
        ros::NodeHandle n;

        launchInit();  // 读取相关配置
        initMessages(n);

        start();

        return true;
    }

    bool QNode::init(const std::string &master_url, const std::string &host_url) {
        ROS_INFO("QNode -> init(params)");

        std::map<std::string,std::string> remappings;
        remappings["__master"]   = master_url;
        remappings["__hostname"] = host_url;

        ros::init(remappings, "yd_scanner");

        if(!ros::master::check()) return false;

        deviceError = new DeviceError();

        ros::start();
        ros::NodeHandle n;

        launchInit();  // 读取相关配置
        initMessages(n);

        start();

        return true;
    }

    // 告警标识
    bool QNode::hasAlarm = false;
    string QNode::alarmInfo = "";

    // 激光
    bool QNode::localization_vlp16_ip_pingable = false;
    bool QNode::scan_vlp16_ip_pingable         = false;

    bool QNode::localization_vlp16_have_messages   = false;
    bool QNode::localization_vlp16_messages_normal = false;

    bool QNode::scan_vlp16_have_messages   = false;
    bool QNode::scan_vlp16_messages_normal = false;
    /// GPS & IMU
    // GPS
    bool QNode::gps_port_opened     = false;
    bool QNode::gps_have_messages   = false;
    bool QNode::gps_messages_normal = false;
    // IMU
    bool QNode::imu_port_opened     = false;
    bool QNode::imu_have_messages   = false;
    bool QNode::imu_messages_normal = false;
    // Cameras
    bool QNode::left_camera_port_opened     = false;
    bool QNode::left_camera_have_messages   = false;
    bool QNode::left_camera_messages_normal = false;

    bool QNode::right_camera_port_opened     = false;
    bool QNode::right_camera_have_messages   = false;
    bool QNode::right_camera_messages_normal = false;

    // launch初始化
    void QNode::launchInit() {
        ROS_INFO("QNode -> launchInit()");
        // TODO
    }
    // 消息回调初始化
    void QNode::initMessages(ros::NodeHandle& n) {
        ROS_INFO("QNode -> initMessages()");

        vector<string> v = split(FLAGS_topics, ",");

        /// 消息订阅
        // VLP
        _velodynePoints[0] = n.subscribe<sensor_msgs::PointCloud2>(v[0], 1, boost::bind(&QNode::ns1VelodyneCallback, this, _1));
        _velodynePoints[1] = n.subscribe<sensor_msgs::PointCloud2>(v[1], 1, boost::bind(&QNode::ns2VelodyneCallback, this, _1));
        // Cam
        _camCompressedImage[0] = n.subscribe<sensor_msgs::CompressedImage>(v[2], 1, boost::bind(&QNode::leftCameraCallback, this, _1));
        _camCompressedImage[1] = n.subscribe<sensor_msgs::CompressedImage>(v[3], 1, boost::bind(&QNode::rightCameraCallback, this, _1));
        // IMU & GPS
        _gps = n.subscribe<sensor_msgs::NavSatFix>(v[4], 1, boost::bind(&QNode::gpsDataCallback, this, _1));
        _imu = n.subscribe<sensor_msgs::Imu>(v[5], 1, boost::bind(&QNode::imuDataCallback, this, _1));
    }

    // VLP16激光 pingable 监测
    bool vlp16Pingable(const std::string ipStr) {
        QString cmdStr = QString("ping -s 1 -c 1 %1").arg(QString::fromStdString(ipStr));

        QProcess cmd;
        cmd.start(cmdStr);
        cmd.waitForFinished(-1);

        bool status = false;
        if(cmd.readAll().indexOf("TTL") != -1) { status = true; }

        return status;
    }
    // IMU & GPS & Cam port_opened 监测
    bool isPortOpened(std::string portName) {
        int port_open_result = open(portName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
        return  port_open_result < 0 ? false : true;
    }

    /// Reset & Check Devices Status Flag
    // Reset
    void QNode::resetDevicesStatusFlag() {
        /// 2s内收不到 设备回调消息，则认为消息通信出现异常
        // 激光
        QNode::localization_vlp16_have_messages = false;
        QNode::scan_vlp16_have_messages         = false;
        // GPS & IMU
        QNode::gps_have_messages = false;
        QNode::imu_have_messages = false;
        // Cameras
        QNode::left_camera_have_messages  = false;
        QNode::right_camera_have_messages = false;

        /*
        // 每2秒对激光ip进行pingable 监测
        QNode::localization_vlp16_ip_pingable = vlp16Pingable("192.168.1.201");
        QNode::scan_vlp16_ip_pingable         = vlp16Pingable("192.168.1.202");

        // 每2秒对IMU、GPS Cam port 进行 is_opened 监测
        QNode::imu_port_opened = isPortOpened("/dev/ttyUSB0");
        QNode::gps_port_opened = isPortOpened("/dev/ttyUSB1");

        QNode::left_camera_port_opened  = isPortOpened("/dev/ttyUSB2");
        QNode::right_camera_port_opened = isPortOpened("/dev/ttyUSB3");
        */
    }
    // Check
    void QNode::checkDevicesStatusFlag() {
        /*
        if(!QNode::localization_vlp16_ip_pingable) deviceError->parseErrorCode(10);

        if(!QNode::scan_vlp16_ip_pingable)         deviceError->parseErrorCode(11);

        if(!QNode::imu_port_opened);               deviceError->parseErrorCode(20);
        if(!QNode::gps_port_opened);               deviceError->parseErrorCode(21);
        if(!QNode::left_camera_port_opened);       deviceError->parseErrorCode(22);
        if(!QNode::right_camera_port_opened);      deviceError->parseErrorCode(23);

        if(!QNode::localization_vlp16_have_messages); deviceError->parseErrorCode(30);
        if(!QNode::scan_vlp16_have_messages);         deviceError->parseErrorCode(31);
        if(!QNode::left_camera_have_messages);        deviceError->parseErrorCode(32);
        if(!QNode::right_camera_have_messages);       deviceError->parseErrorCode(33);

        if(!QNode::localization_vlp16_messages_normal); deviceError->parseErrorCode(40);
        if(!QNode::scan_vlp16_messages_normal);         deviceError->parseErrorCode(41);
        if(!QNode::gps_messages_normal);                deviceError->parseErrorCode(42);
        if(!QNode::imu_messages_normal);                deviceError->parseErrorCode(43);
        if(!QNode::left_camera_messages_normal);        deviceError->parseErrorCode(44);
        if(!QNode::right_camera_messages_normal);       deviceError->parseErrorCode(45);
        */
    }

    // Update Devices Status Data
    string QNode::updateDevicesStatueData() {
        string data = QNode::localization_vlp16_ip_pingable ? "正常" : "断开";
        data += QNode::localization_vlp16_have_messages ? " 正常" : " 断开";
        data += QNode::localization_vlp16_messages_normal ? " 正常" : " 异常";

        data += QNode::scan_vlp16_ip_pingable ? " 正常" : " 断开";
        data += QNode::scan_vlp16_have_messages ? " 正常" : " 断开";
        data += QNode::scan_vlp16_messages_normal ? " 正常" : " 异常";

        // IMU & GPS
        data += QNode::imu_port_opened ? " 正常" : " 断开";
        data += QNode::imu_have_messages ? " 正常" : " 断开";
        data += QNode::imu_messages_normal ? " 正常" : " 异常";

        data += QNode::gps_port_opened ? " 正常" : " 断开";
        data += QNode::gps_have_messages ? " 正常" : " 断开";
        data += QNode::gps_messages_normal ? " 正常" : " 异常";

        // CAMERAS
        data += QNode::left_camera_port_opened ? " 正常" : " 断开";
        data += QNode::left_camera_have_messages ? " 正常" : " 断开";
        data += QNode::left_camera_messages_normal ? " 正常" : " 异常";

        data += QNode::right_camera_port_opened ? " 正常" : " 断开";
        data += QNode::right_camera_have_messages ? " 正常" : " 断开";
        data += QNode::right_camera_messages_normal ? " 正常" : " 异常";

        return data;
    }

    void QNode::run() {
        ros::Rate loop_rate(ros::Duration(2));

        while(ros::ok()) {
            resetDevicesStatusFlag();
            checkDevicesStatusFlag();

            // 发送
            Q_EMIT uiUpdate(QString::fromStdString(updateDevicesStatueData()));

            ros::spinOnce();
            loop_rate.sleep();
        }

        std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
        Q_EMIT rosShutdown();

        cout << "ROSNODE LIST:" << endl;
        system("rosnode list");

        //system("rosnode kill -a");
        system("rosnode kill cam_capture_node");
    }
}
