#include "convert_select_controller.hpp"

#include <dirent.h>
#include <sys/stat.h>

#include "fstream"
#include "stdio.h"
#include "utils.h"

using namespace std;
using namespace yd_scanner_ui;

ConvertSelectController::ConvertSelectController() : isConvertCompleted(true) {
    printf("ConvertSelectController -> constructor\n");

    //e57 = new E57();
}
ConvertSelectController::~ConvertSelectController() {
    printf("ConvertSelectController -> destructor\n");
    // to do sth.
}

// 获取 ply-filename-list
QStringList ConvertSelectController::getPlys(string file_type_path) {
    printf("ConvertSelectController -> getPlys()\n");

    DIR *dir;
    struct dirent *drt;

    QStringList tmpPlys;

    dir = opendir(file_type_path.c_str());
    while((drt = readdir(dir)) != NULL) {
        if(!strcmp(drt->d_name, ".") || !strcmp(drt->d_name, "..")) { continue; }
        else if(drt->d_type == 8) {
            int len = strlen(drt->d_name);

            if((drt->d_name[len - 3] == 'p') && (drt->d_name[len - 2] == 'l') && (drt->d_name[len - 1] == 'y')) {
                tmpPlys << QString::fromStdString(drt->d_name);
            }
        }
    }
    closedir(dir);

    return tmpPlys;
}

/// 转换
// ply->las
void ConvertSelectController::convertProcessOutputInfo() {
    printf("ConvertSelectController -> convertProcessOutputInfo()\n");
    if(convert_progress_call_back) { convert_progress_call_back(60.0); }

    QString info = process->readAllStandardOutput().data();
    printf("%s\n", info.toStdString().c_str());
}
// TODO: test 有问题(待重写)
void ConvertSelectController::convertProcessErrorInfo() {
    QString info = process->readAllStandardError().data();

    if(info.toStdString() != "") {
        printf("ConvertSelectController -> convertProcessErrorInfo()\n");
        if(convert_progress_call_back) { convert_progress_call_back(60.0); }

        printf("%s\n", info.toStdString().c_str());

        //isConvertCompleted = false;
        QMessageBox::information(0, "格式转换问题", info);
    }
}
void ConvertSelectController::convertProcessFinished() {
    printf("ConvertSelectController -> convertProcessFinished()\n");

    if(convert_progress_call_back) { convert_progress_call_back(100.0); }

    process->close();
    process->deleteLater();
    process = nullptr;
}
// ply->pcd
void ConvertSelectController::convertToPCD(string file_path, string new_file_path) {
    printf("ConvertSelectController -> convertToPCD()\n");

    try {
        pcl::PCLPointCloud2 pcd2;
        pcl::PLYReader plyReader;
        plyReader.read(file_path, pcd2);

        if(convert_progress_call_back) { convert_progress_call_back(30.0); }

        // 默认转化为“XYZRGBA”格式
        pcl::PointCloud<pcl::PointXYZRGBA> pcd;
        pcl::fromPCLPointCloud2(pcd2, pcd);

        if(convert_progress_call_back) { convert_progress_call_back(60.0); }

        pcl::PCDWriter pcdWriter;
        pcdWriter.writeASCII(new_file_path, pcd);

        if(convert_progress_call_back) { convert_progress_call_back(100.0); }
    } catch(...) {
        isConvertCompleted = false;
        QMessageBox::information(0, "转换异常", "ply2pcd转换失败");
    }
}
/* ply->e57
void ConvertSelectController::convertToE57(string file_path, string new_file_path, float scale_factor) {
    printf("ConvertSelectController -> convertToE57()\n");

    string pcd_file_path = new_file_path;
    pcd_file_path = replace(pcd_file_path, "e57", "pcd");

    try {
        /// ply->pcd
        // Read ply
        pcl::PCLPointCloud2 pcd2;
        pcl::PLYReader plyReader;

        if(plyReader.read(file_path, pcd2) != -1) {
            printf("Read ply success!\n");
            if(convert_progress_call_back) { convert_progress_call_back(30.0); }

            // Save to pcd
            pcl::PointCloud<pcl::PointXYZI> pcd;
            pcl::fromPCLPointCloud2(pcd2, pcd);

            pcl::PCDWriter pcdWriter;
            if(pcdWriter.writeASCII(pcd_file_path, pcd) != -1) {
                printf("Save pcd success!\n");
                if(convert_progress_call_back) { convert_progress_call_back(40.0); }

                // Load pcd
                PtrXYZ pcd(new pcl::PointCloud<P_XYZ>);
                if(pcl::io::loadPCDFile(pcd_file_path, *pcd) != -1) {
                    printf("Load pcd success!\n");
                    if(convert_progress_call_back) { convert_progress_call_back(70.0); }

                    /// pcd->e57
                    if(e57->saveE57File(new_file_path, pcd, scale_factor) == 1) {
                        printf("pcd2e57成功\n");
                        if(convert_progress_call_back) { convert_progress_call_back(100.0); }
                    } else {
                        isConvertCompleted = false;
                        printf("pcd2e57失败\n");
                    }
                } else {
                    isConvertCompleted = false;
                    printf("Load pcd失败\n");
                }
            } else {
                isConvertCompleted = false;
                printf("Save pcd失败\n");
            }
        } else {
            isConvertCompleted = false;
            printf("Read ply失败\n");
        }
    } catch(...) {
        isConvertCompleted = false;
        QMessageBox::information(0, "转换异常", "ply2e57转换失败");
    }
}
*/

/// Convert
// 获取转换后的文件路径
string getNewFilePath(string file_path, string type) {
    printf("ConvertSelectController -> getNewFilePath()\n");

    vector<string> tmp_vec = split(file_path, "/");
    string new_file_name   = split(tmp_vec[tmp_vec.size() - 1], ".")[0] + "." + type;

    string new_file_path = "";
    for(int i = 0; i < tmp_vec.size() - 1; i++) {
        new_file_path += tmp_vec[i] + "/";
    }
    new_file_path = new_file_path + new_file_name;

    return new_file_path;
}
bool ConvertSelectController::convert(string file_path, string type, string pointSize) {
    printf("ConvertSelectController -> convert [\n        file_path: %s,\n        type: %s,\n        pointSize: %s ]\n", file_path.c_str(), type.c_str(), pointSize.c_str());

    // las2las64
    if(strcmp(type.c_str(), "las") == 0) {
        // Process 命令行转换
        process = new QProcess();
        connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(convertProcessOutputInfo()));
        connect(process, SIGNAL(readyReadStandardError()), this, SLOT(convertProcessErrorInfo()));
        connect(process, SIGNAL(finished(int)), this, SLOT(convertProcessFinished()));

        string cmd = "las2las64 -i " + file_path + " -o " + getNewFilePath(file_path, type) + " -set_point_size " + pointSize;
        printf("cmd: %s\n", cmd.c_str());

        if(convert_progress_call_back) { convert_progress_call_back(10.0); }

        process->start(QString::fromStdString(cmd));
        if(convert_progress_call_back) { convert_progress_call_back(30.0); }

        process->waitForFinished();
    }
    // PCL(ply->pcd)
    else if(strcmp(type.c_str(), "pcd") == 0) { convertToPCD(file_path, getNewFilePath(file_path, type)); }
    // E57(ply->e57)
    else if(strcmp(type.c_str(), "e57") == 0) {
        //convertToE57(file_path, getNewFilePath(file_path, type), atof("0")/*atof(pointSize.c_str())*/);
    }

    return isConvertCompleted;
}
