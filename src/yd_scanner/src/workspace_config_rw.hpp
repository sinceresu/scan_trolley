#pragma once
#include <string>
#include <vector>


namespace yd_scanner_ui {
      class WorkspaceConfigRW {

      public:
        struct ProjectItem {
          std::string name;
          std::string parent_directory;
        };
        struct Configuration {
          std::vector<ProjectItem> projects;
        };

          // Constructor & Destructor
          WorkspaceConfigRW(std::string wp_dir);
          ~WorkspaceConfigRW(){};      

          bool ReadConfiguration(Configuration& config);
          bool WriteConfiguration(const Configuration& config);
      private:
        std::string wp_dir_;

      };
  
}