#include "toast_tool.hpp"

#include <QPropertyAnimation>
#include <QScreen>
#include <QGuiApplication>
#include <QPainter>
#include <QTimer>

Toast::Toast(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);
		// 无边框，无任务栏，背景透明
    setWindowFlags(Qt::FramelessWindowHint|Qt::Tool);
    setAttribute(Qt::WA_TranslucentBackground);
}
Toast::~Toast() {}

void Toast::setText(const QString& text) { ui.label->setText(text); }

void Toast::showAnimation(int timeout /*= 2000*/) {
    // 开始动画
    QPropertyAnimation *animation = new QPropertyAnimation(this, "windowOpacity");
    animation->setDuration(1000);
    animation->setStartValue(0);
    animation->setEndValue(1);
    animation->start();
    show();

    QTimer::singleShot(timeout, [&] {
        // 结束动画
        QPropertyAnimation *animation = new QPropertyAnimation(this, "windowOpacity");
        animation->setDuration(1000);
        animation->setStartValue(1);
        animation->setEndValue(0);
        animation->start();
        connect(animation, &QPropertyAnimation::finished, [&] {
            close();
            deleteLater(); // 关闭后析构
        });
    });
}

void Toast::showTip(const QString& text, QWidget* parent /*= nullptr*/) {
    Toast* toast = new Toast(parent);
    toast->setWindowFlags(toast->windowFlags() | Qt::WindowStaysOnTopHint); // 置顶
    toast->setText(text);
    toast->adjustSize();

    // 屏幕高度70%位置
    QScreen* pScreen = QGuiApplication::primaryScreen();
    toast->move((pScreen->size().width() - toast->width()) / 2, pScreen->size().height() * 7 / 10);
    toast->showAnimation();
}

void Toast::paintEvent(QPaintEvent *event) {
    QPainter paint(this);
    paint.begin(this);

    auto kBackgroundColor = QColor(255, 255, 255);
    kBackgroundColor.setAlpha(0.0 * 255);

    paint.setRenderHint(QPainter::Antialiasing, true);
    paint.setPen(Qt::NoPen);
    paint.setBrush(QBrush(kBackgroundColor, Qt::SolidPattern));

    paint.drawRect(0, 0, width(), height());
    paint.end();
}
