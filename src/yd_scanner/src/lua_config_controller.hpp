#ifndef LUA_CONFIG_CONTROLLER_HPP
#define LUA_CONFIG_CONTROLLER_HPP

extern "C" {
    #include <lua5.2/lua.h>
    #include <lua5.2/lualib.h>
    #include <lua5.2/lauxlib.h>
}

// TODO: test LuaDictionary
#include "absl/memory/memory.h"
#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"

#include <QtCore/QtCore>

#include <iomanip>
#include <iostream>
#include <sstream>

#include <map>
#include <vector>
#include <string>

#include "string.h"

using namespace Qt;
using namespace std;

namespace yd_scanner_ui {
    class LuaConfigController {
        public:
            LuaConfigController();
            ~LuaConfigController();

            /// Variables
            int build_minMaxRangeFilterTableDepth, build_horizontalRangeFilterTableDepth;
            int merge_minMaxRangeFilterTableDepth, merge_horizontalRangeFilterTableDepth;
            int colorize_minMaxRangeFilterTableDepth, colorize_horizontalRangeFilterTableDepth;

            /// Fun
            // 读取Lua
            vector<string> readMapBuild(string config_dir_path, string file_name);
            vector<string> readBuildWriter(string config_dir_path, string file_name);

            vector<string> readMapRegistrate(string config_dir_path, string file_name);
            vector<string> readMergeWriter(string config_dir_path, string file_name);

            vector<string> readMapColorize(string config_dir_path, string file_name);
            vector<string> readColorizeWriter(string config_dir_path, string file_name);

            // 保存Lua
            bool saveToFile(string path, vector<string> v);

            // 重置reset
            void reset();
    };
}
#endif
