#ifndef LAYOUT_EDITOR_HPP
#define LAYOUT_EDITOR_HPP

#ifndef Q_MOC_RUN
    // LIBMAP_SCANNER
    #include "map_common/global.h"
    #include "map_common/err_code.h"

    #include "map_build/libmap_build.h"
    #include "map_build/map_build_interface.h"
    #include "map_registrate/libmap_registrate.h"
    #include "map_registrate/map_registrate_interface.h"
    #include "map_colorize/libmap_colorize.h"
    #include "map_colorize/map_colorize_interface.h"
#endif

//Qt5
#include <QDialog>
#include <QtGui/QList>
#include <QtCore/QMetaType>
#include <QToolButton>

#include <vector>

#include "common.hpp"

using namespace Qt;
using namespace std;

namespace Ui { class LayoutEditor; }
namespace yd_scanner_ui {
    class LayoutEditor : public QDialog {
        Q_OBJECT
            public:
                // Constructor & Destructor
                LayoutEditor(int rows, int cols, const SceneLayout& scene_layout);
                ~LayoutEditor();

                SceneLayout getLayout();

            private Q_SLOTS:
                void sceneSelected();
                void deleteSelectedScene();

            private:
                void initSceneLayout();
                void clearSceneLayout();

                void deleteScene(int row, int col);
                void addScene(int row, int col);

                Ui::LayoutEditor *ui_;
                int rows_;
                int cols_;
                int selected_row_;
                int selected_col_;

                SceneLayout layout_;
                std::string scene_id_;
    };
}
#endif
