#include "notification_monitor.hpp"

using namespace std;
using namespace yd_scanner_ui;

// Constructor & Destructor
NotificationMonitor::NotificationMonitor() {
    ROS_INFO("NotificationMonitor -> Constructor()");
}
NotificationMonitor::~NotificationMonitor() {
    ROS_INFO("NotificationMonitor -> Destructor()");
}

// Notify
void NotificationMonitor::notify() {
    ROS_INFO("NotificationMonitor -> notify()");

    while(!isWindowClosed) {
        if(QNode::hasAlarm) {
            this->hasAlarm = true;
            break;
        }
        sleep(1);
    }

    Q_EMIT finished();
}

// WindowClosedStatus
void NotificationMonitor::setWindowClosedStatus() {
    ROS_INFO("NotificationMonitor -> setWindowClosedStatus()");
    this->isWindowClosed = true;
}
