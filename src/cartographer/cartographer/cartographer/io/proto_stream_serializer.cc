/*
 * Copyright 2018 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cartographer/io/proto_stream_serializer.h"

#include "cartographer/io/internal/mapping_state_serialization.h"
#include "cartographer/io/proto_stream.h"
#include "glog/logging.h"
#include "cartographer/mapping/proto/serialization.pb.h"

namespace cartographer {
namespace io {
using mapping::proto::SerializedData;
namespace
{
  mapping::proto::SerializationHeader CreateHeader() {
  mapping::proto::SerializationHeader header;
  header.set_format_version(kMappingStateSerializationFormatVersion);
  return header;
}
}
void SerializePoseGraphToFile(
    const std::string& file_name, const mapping::proto::PoseGraph& pose_graph, const mapping::proto::AllTrajectoryBuilderOptions& all_trajectory_builder_options_) {
  ProtoStreamWriter writer(file_name);
  writer.WriteProto(cartographer::io::CreateHeader());
  SerializedData proto;
  *proto.mutable_pose_graph() = pose_graph;
  writer.WriteProto(proto);
  SerializedData proto1;
  *proto1.mutable_all_trajectory_builder_options() =all_trajectory_builder_options_;
  writer.WriteProto(proto1);
}



}  // namespace io
}  // namespace cartographer
