/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cartographer/io/min_max_intensity_filtering_points_processor.h"

#include "absl/memory/memory.h"
#include "cartographer/common/lua_parameter_dictionary.h"
#include "cartographer/io/points_batch.h"

namespace cartographer {
namespace io {

std::unique_ptr<MinMaxIntensityFilteringPointsProcessor>
MinMaxIntensityFilteringPointsProcessor::FromDictionary(
    common::LuaParameterDictionary* const dictionary,
    PointsProcessor* const next) {
  const float min_intensity = dictionary->GetDouble("min_intensity");
  const float max_intensity = dictionary->GetDouble("max_intensity");
  return absl::make_unique<MinMaxIntensityFilteringPointsProcessor>(
      min_intensity, max_intensity, next);
}

MinMaxIntensityFilteringPointsProcessor::MinMaxIntensityFilteringPointsProcessor(
    const double min_intensity, const double max_intensity, PointsProcessor* next)
    : min_intensity_(min_intensity),
      max_intensity_(max_intensity),
      next_(next) {}

void MinMaxIntensityFilteringPointsProcessor::Process(
  std::unique_ptr<PointsBatch> batch) {

  absl::flat_hash_set<int> to_remove;
  if (!batch->intensities.empty()) {
    absl::flat_hash_set<int> to_remove;
    for (size_t i = 0; i < batch->points.size(); ++i) {
      const float intensity = batch->intensities[i];
      if (intensity < min_intensity_  || 
          intensity > max_intensity_) {
        to_remove.insert(i);
      }
    }
    RemovePoints(to_remove, batch.get());
  }
  next_->Process(std::move(batch));
}

PointsProcessor::FlushResult MinMaxIntensityFilteringPointsProcessor::Flush() {
  return next_->Flush();
}

}  // namespace io
}  // namespace cartographer
