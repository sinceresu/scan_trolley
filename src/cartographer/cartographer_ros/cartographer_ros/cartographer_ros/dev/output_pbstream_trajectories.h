
#ifndef OUTPUT_PBSTREAM_TRAJECTORIES_H
#define OUTPUT_PBSTREAM_TRAJECTORIES_H
#include <string>

namespace cartographer_ros{
void output_pbstream_trajectories(const std::string& pbstream_filename,
                                  const std::string& output_filename,
                                  const std::string& parent_frame_id
                                  ) ;
}  // namespace cartographer_ros
#endif  // OUTPUT_PBSTREAM_TRAJECTORIES_H   
