#ifndef _MAP_COLORIZE_MAP_COLORIZE_INTERFACE_H
#define _MAP_COLORIZE_MAP_COLORIZE_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <stdint.h>

#include <opencv2/opencv.hpp>


#include "map_common/global.h"

namespace yida_mapping{
namespace map_colorize{

      
class MapColorizeInterface
{
  
public:

  using ProgressCallback =
      std::function<void(float percentage)>;
      
  typedef struct ColorizeParam {
    std::string urdf_filename;  //urdf文件路径
    std::string configuration_directory;    //配置文件目录
    std::string colorize_configuration_basename;    //建图配置文件名
    std::string write_configuration_basename;    //地图生成配置文件名
  }ColorizeParam;

  

  virtual ~MapColorizeInterface(){};

  virtual  int SetProgressCallback(ProgressCallback callback) = 0 ;

    /******************************************************************************
  * \fn MapColorizeInterface.SetParam
  * Create Time: 2020/06/02
  * Description: -
  *    设置着色参数
  *
  * \param param 输入参数
  * 		各项着色参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const ColorizeParam & param) = 0 ;

  /******************************************************************************
  * \fn MapColorizeInterface.ColorizeMap
  * Create Time: 2020/06/02
  * Description: -
  *   开始进行着色建图
  *
  * \param map_file_path 输入参数
   *
  * \param save_directory 输入参数
  * 	 着色结果保存目录
  * 
  * \return
  * 		错误码
  *
  * \note 着色输出的map名与输入map名一致。
  *******************************************************************************/
  virtual  int ColorizeMap(const std::string & state_filename, const std::string & map_bag_filenames, const std::string & map_pcd_filename, const std::string & save_directory, const std::string & map_name) = 0 ;
  
  virtual  int ColorizeMaps(const std::vector<std::string> & state_filenames, const std::vector<std::string> & map_bag_filenames, const std::string & map_pcd_filename, const std::string & save_directory,  const std::string& map_name) = 0 ;
  virtual  int Stop() = 0 ;

};
} // namespace map_colorize
}// namespace yida_mapping

#endif  
