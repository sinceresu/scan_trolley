#ifndef _MAP_COLORIZE_LIBMAP_COLORIZE_H
#define _MAP_COLORIZE_LIBMAP_COLORIZE_H


#if (defined _WIN32 || defined WINCE || defined __CYGWIN__) && defined  LIBmap_colorize_EXPORTS
#define LIBmap_colorize_API __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define LIBmap_colorize_API __attribute__ ((visibility ("default")))
#else
#define LIBmap_colorize_API
#endif

#include <memory>
#include <stdint.h>
#include <memory>
namespace yida_mapping{
namespace map_colorize {

class MapColorizeInterface;

enum MapColorizeType {
  CARTOGRAPHER,
 TYPE_NUMBER,
};

  /******************************************************************************
  * \fn CreateMapColorize
  * Create Time: 2020/06/02
  * Description: -
  *   创建一个新的扫描组件实例
  *
  * \param type 输入参数
  * 		扫描组件实例类型
  *
  * \return
  * 		扫描组件实例
  *
  * \note 
  *******************************************************************************/
std::shared_ptr<MapColorizeInterface> CreateMapColorizer(MapColorizeType type = CARTOGRAPHER);

} // namespace map_colorize
}// namespace yida_mapping

#endif  
