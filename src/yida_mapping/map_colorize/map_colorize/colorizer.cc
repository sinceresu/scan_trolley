#include "colorizer.h"

#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/surface/mls.h>
#include <pcl/filters/frustum_culling.h>

#include "cartographer_ros/time_conversion.h"

#include "map_common/err_code.h"
#include "afrustum_culling.h"

using namespace std;

namespace yida_mapping{

namespace map_colorize{
namespace {
constexpr float kHorizontalFOV = 100.0f;
constexpr float kVerticalFOV = 120.0f;
constexpr float kNearPlaneDistance = 0.7f;
constexpr float kFarPlaneDistance = 30.0f;
constexpr int kMaxImagesInDistanceMap = 50;
constexpr float kOccludeDistanceTolerence = 0.2f;

cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt)
{
    cv::Mat patch;
    cv::getRectSubPix(img, cv::Size(1,1), pt, patch);
    return patch.at<cv::Vec3b>(0,0);
}
Eigen::Matrix4f pose_to_transform(const ::cartographer::transform::Rigid3f & pose) {
  auto orientation = pose.rotation();
  Eigen::Quaternionf quaternion = Eigen::Quaternionf(orientation.w(), orientation.x(), orientation.y(), orientation.z()); // w x y z
  auto position = pose.translation();

  Eigen::Matrix4f tf_matrix = Eigen::Matrix4f::Identity();
  tf_matrix.block<3, 3>(0, 0) = quaternion.toRotationMatrix();
  tf_matrix.block<3, 1>(0, 3) << position.x(), position.y(), position.z(); 

  return  tf_matrix;  
}
}

Colorizer::Colorizer() :
        horizontal_fov_(kHorizontalFOV),
        vertical_fov_(kVerticalFOV),
        near_plane_distance_(kNearPlaneDistance),
        far_plane_distance_(kFarPlaneDistance)
{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
  num_threads_ = omp_get_max_threads();

}

Colorizer::~Colorizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}


int Colorizer::SetParamters( float horizontal_left_fov, 
                          float horizontal_right_fov,
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance,
                          float occlude_distance) {

    horizontal_left_fov_ = horizontal_left_fov >= 90.0 ? 89.0 : horizontal_left_fov;
    horizontal_right_fov_ = horizontal_right_fov >= 90.0 ? 89.0 : horizontal_right_fov;
    vertical_fov_ = vertical_fov;
    near_plane_distance_ = near_plane_distance;
    far_plane_distance_ = far_plane_distance;
    occlude_distance_ = occlude_distance;

    return ERRCODE_OK;
}
 int Colorizer::LoadCalibrationFile(const std::string& calibration_filepath) {
	cv::FileStorage fs;

    fs.open(calibration_filepath, cv::FileStorage::READ);
    if(!fs.isOpened()){
        LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
        return ERRCODE_FAILED;
    }
    
    fs["CameraMat"] >> intrinsic_mat_;
    fs["DistCoeff"] >> distortion_coeffs_;
    fs["ImageSize"] >> image_size_;

    cv::Mat cam_extrinsic_mat;

    fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
    fs.release();   
    cv::cv2eigen(cam_extrinsic_mat, camara_to_ref_pose_);

// Note: This assumes a coordinate system where X is forward, Y is up, and Z is right. To convert from the traditional camera coordinate system (X right, Y down, Z forward), one can use:
     Eigen::Matrix4f cam2robot;
    cam2robot << 0, 0, 1, 0,
             0,-1, 0, 0,
             1, 0, 0, 0,
             0, 0, 0, 1;
    camara_to_ref_for_culling_ = camara_to_ref_pose_ * cam2robot;

    ref_to_camera_pose_ = camara_to_ref_pose_.inverse();

    return ERRCODE_OK;
 }

 std::vector<int> Colorizer::CullingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud, const Eigen::Matrix4f& camara_pose) {

  pcl::AFrustumCulling<pcl::PointXYZRGB> fc_;
  fc_.setInputCloud (point_cloud);
  fc_.setVerticalFOV (vertical_fov_);
  fc_.setHorizontalFOV (horizontal_left_fov_, horizontal_right_fov_);
  fc_.setNearPlaneDistance (near_plane_distance_);
  fc_.setFarPlaneDistance (far_plane_distance_);
  fc_.setCameraPose (camara_pose);

  std::vector<int> filter_indices;
  fc_.filter (filter_indices);

  return filter_indices;
}


pcl::PointCloud<pcl::PointXYZ>::Ptr Colorizer::CullingOccludePointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camara_pose) {

  pcl::AFrustumCulling<pcl::PointXYZ> fc_;
  fc_.setInputCloud (point_cloud);
  fc_.setVerticalFOV (vertical_fov_);
  fc_.setHorizontalFOV (horizontal_left_fov_, horizontal_right_fov_);
  fc_.setFarPlaneDistance (far_plane_distance_);
  fc_.setCameraPose (camara_pose);

  std::vector<int> filter_indices;
  fc_.filter (filter_indices); 

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  inliers->indices = move(filter_indices);
  pcl::ExtractIndices<pcl::PointXYZ> extract;
  extract.setInputCloud (point_cloud);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*cloud_filtered);
  return cloud_filtered;

}

pcl::PointCloud<pcl::PointXYZ>::Ptr Colorizer::UpsamplingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud) {
  pcl::PointCloud<pcl::PointXYZ>::Ptr filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> filter;
  filter.setInputCloud(point_cloud);
  //建立搜索对象
  pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree;
  filter.setSearchMethod(kdtree);
  //设置搜索邻域的半径为3cm
  filter.setSearchRadius(0.03);
  // Upsampling 采样的方法有 DISTINCT_CLOUD, RANDOM_UNIFORM_DENSITY
  filter.setUpsamplingMethod(pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ>::SAMPLE_LOCAL_PLANE);
  // 采样的半径是
  filter.setUpsamplingRadius(0.03);
  // 采样步数的大小
  filter.setUpsamplingStepSize(0.02);

  filter.process(*filteredCloud);
  return filteredCloud;
}
// static bool upsampling = false;

void Colorizer::GenerateDistanceImage(pcl::PointCloud<pcl::PointXYZ>::ConstPtr occlude_pcl, const cv::Mat& image, const Eigen::Matrix4f& ref_to_world, cv::Mat& distance_image) {
    distance_image = cv::Mat(image.rows, image.cols, CV_32F, numeric_limits<float>::max());
    if (occlude_pcl->empty()) {
      return;
    }

    Eigen::Matrix4f world_to_camera_transform = ref_to_camera_pose_ * ref_to_world.inverse();

    cv::Mat world_to_camera;
    // rotatiion matrix convert world coordinate of a point  to camera coordinate 
    cv::eigen2cv(world_to_camera_transform, world_to_camera);

    Eigen::Matrix4f camera_to_world_for_culling = ref_to_world * camara_to_ref_for_culling_;
    auto culled_pcl = CullingOccludePointCloud(occlude_pcl, camera_to_world_for_culling);
    // if (upsampling) {
    //   auto upsamping_pcl = UpsamplingPointCloud(culled_pcl);
    //   culled_pcl = upsamping_pcl;
    // }
    std::vector<cv::Point3f> points_to_colorize;

    points_to_colorize.reserve(culled_pcl->size());
    for (size_t i = 0; i < culled_pcl->size(); ++i) {
        const auto& point = culled_pcl->at(i);
        points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));
    }


    cv::Mat rotation_vec;
    cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
    rotation_vec_ = rotation_vec.t();
    transition_vec_ = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
    std::vector<cv::Point2f> image_points;
    world_to_camera.convertTo(world_to_camera, CV_64F);

    cv::fisheye::projectPoints(points_to_colorize, image_points, rotation_vec_, transition_vec_, intrinsic_mat_, distortion_coeffs_);
    std::vector<cv::Point3f> points_to_camera;
    if (!points_to_colorize.empty())
      cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);

    for (size_t i = 0; i < image_points.size(); ++i)
    {
        float y = image_points[i].y, x = image_points[i].x;
        if (y >= 0 && y < image_size_.height && x >= 0 && x < image_size_.width) {
            int col = round(x), row = round(y);
            float distance = cv::norm(points_to_camera[i]);
            if (distance< distance_image.at<float>(row, col))
              distance_image.at<float>(row, col) = distance;
        }

    }

    int erode_pixels  = 5;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                       cv::Size( 2*erode_pixels + 1, 2*erode_pixels+1 ),
                       cv::Point( erode_pixels, erode_pixels ) );
    cv::Mat eroded = cv::Mat();
    cv::erode(distance_image, eroded, element);
    eroded.copyTo(distance_image);
  // int dilation_size  = 2;
  // element = cv::getStructuringElement( cv::MORPH_RECT,
  //                      cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
  //                      cv::Point( dilation_size, dilation_size ) );

  //   cv::dilate(eroded, distance_image, element);

}
void Colorizer::UpdataDistanceImages(pcl::PointCloud<pcl::PointXYZ>::ConstPtr occlude_pcl,  const std::vector<std::shared_ptr<TimedImage>>&  timed_images) {
  std::set<uint32_t>  input_image_ids;
  for (const auto& timed_image : timed_images) {
    // if (timed_image->id != 3) continue;

    input_image_ids.insert(timed_image->id);
// generate distance image for new input image.
    if (distance_images.count(timed_image->id) == 0) {
      Eigen::Matrix4f ref_to_world = pose_to_transform(timed_image->pose);
      cv::Mat distance_image;

      // upsampling = timed_image->id == 17 ? true : false;

      GenerateDistanceImage(occlude_pcl, timed_image->image, ref_to_world, distance_image);
      distance_images[timed_image->id] = move(distance_image);
      
      // std::stringstream png_file;
      // png_file << "cam_" << timed_image->id << ".png";
      // imwrite(png_file.str(), timed_image->image);
    }
  }
  // remove outdated distance images
  // if (distance_images.size() > kMaxImagesInDistanceMap) {
  //   for(auto iter = distance_images.begin(); iter != distance_images.end(); ) {
  //     if (input_image_ids.count(iter->first) == 0) {
  //         iter = distance_images.erase(iter);
  //         if (distance_images.size() <=kMaxImagesInDistanceMap )
  //           break;
  //     } else {
  //         ++iter;
  //     }
  //   }
  // }

}

int Colorizer::ColorizeImage(pcl::PointCloud<pcl::PointXYZ>::ConstPtr map_pcl, const TimedImage& timed_image, pcl::PointCloud<pcl::PointXYZRGB>::Ptr colored_pcl ) {
  const cv::Mat& image = timed_image.image;
  Eigen::Matrix4f ref_to_world = pose_to_transform(timed_image.pose);

  Eigen::Matrix4f world_to_camera_transform = ref_to_camera_pose_ * ref_to_world.inverse();

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);

  Eigen::Matrix4f camera_to_world_for_culling = ref_to_world * camara_to_ref_for_culling_;
  auto culled_pcl = CullingOccludePointCloud(map_pcl, camera_to_world_for_culling);

  std::vector<cv::Point3f> points_to_colorize;

  points_to_colorize.reserve(culled_pcl->size());
  for (size_t i = 0; i < culled_pcl->size(); ++i) {
      const auto& point = culled_pcl->at(i);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));
  }

  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  rotation_vec_ = rotation_vec.t();
  transition_vec_ = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
  std::vector<cv::Point2f> image_points;
  cv::fisheye::projectPoints(points_to_colorize, image_points, rotation_vec_, transition_vec_, intrinsic_mat_, distortion_coeffs_);
  for (size_t i = 0; i < image_points.size(); ++i)
  {
      float y = image_points[i].y, x = image_points[i].x;
      if (y >= 0 && y < image_size_.height && x >= 0 && x < image_size_.width) {
          const auto &point  = points_to_colorize[i];
          cv::Vec3b color = getColorSubpix(image, cv::Point2f(x, y));
          pcl::PointXYZRGB colored_point(color[2], color[1], color[0]);
          colored_point.x = point.x;  
          colored_point.y = point.y;  
          colored_point.z = point.z;
          colored_pcl->push_back(colored_point);
          
      }
  }

  return ERRCODE_OK;

}




int Colorizer::Colorize(pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud, const std::vector<std::shared_ptr<TimedImage>>& timed_images, pcl::PointCloud<pcl::PointXYZ>::ConstPtr occlude_pcl, std::vector<float >& min_distances) {
  bool do_occlude =  !occlude_pcl->empty() ;
  if (do_occlude) {
    // LOG(INFO)   << "UpdataDistanceImages: " ;
    UpdataDistanceImages(occlude_pcl, timed_images);
  }
  for (const auto & timed_image: timed_images) {
    const cv::Mat& image = timed_image->image;
    // note , incomplete image only in office, should removed.
    // if (timed_image->id != 3) continue;
    uint32_t image_id = timed_image->id;
    auto distance_image = distance_images[image_id];

    Eigen::Matrix4f ref_to_world = pose_to_transform(timed_image->pose);

    Eigen::Matrix4f world_to_camera_transform = ref_to_camera_pose_ * ref_to_world.inverse();

    cv::Mat world_to_camera;
    // rotatiion matrix convert world coordinate of a point  to camera coordinate 
    cv::eigen2cv(world_to_camera_transform, world_to_camera);
    // LOG(INFO) <<  "target_to_camera" ;
    // LOG(INFO) <<  target_to_camera ;

    Eigen::Matrix4f camera_to_world_for_culling = ref_to_world * camara_to_ref_for_culling_;
    std::vector<int> filtered_indices = CullingPointCloud(point_cloud, camera_to_world_for_culling);

    std::vector<cv::Point3f> points_to_colorize;
    points_to_colorize.reserve(filtered_indices.size());
    for (size_t i = 0; i < filtered_indices.size(); ++i) {
        auto point = point_cloud->at(filtered_indices[i]);
        points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));

    }
      
    cv::Mat rotation_vec;
    cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
    rotation_vec_ = rotation_vec.t();
    transition_vec_ = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
    std::vector<cv::Point2f> image_points;

    cv::fisheye::projectPoints(points_to_colorize, image_points, rotation_vec_, transition_vec_, intrinsic_mat_, distortion_coeffs_);
    std::vector<cv::Point3f> points_to_camera;
    if (!points_to_colorize.empty())
        cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);

  #pragma omp parallel for num_threads(num_threads_) schedule(guided, 8)
    for (size_t i = 0; i < image_points.size(); ++i)
    {
      float y = image_points[i].y, x = image_points[i].x;
      if (y >= 0 && y < image_size_.height && x >= 0 && x < image_size_.width) {
          auto &point  = point_cloud->at(filtered_indices[i]);
          float distance = cv::norm(points_to_camera[i]);
          //discard if occluded
          int col = round(x), row = round(y);
          
          if (do_occlude && distance > distance_image.at<float>(row, col) + occlude_distance_)
              continue;
              //filter points not in final pcl (filtered by occupancy rate)
          if (do_occlude && distance < distance_image.at<float>(row, col) - 0.01)
              continue;
          // float up_distance = distance_image.at<float>(max(0, row - 1), col) ;
          // float left_distance = distance_image.at<float>(row, max(0, col - 1)) ;
          // float down_distance = distance_image.at<float>(min( image_size_.height - 1, row + 1), col) ;
          // float right_distance = distance_image.at<float>(row, min( image_size_.width - 1, col + 1)) ;
          // if (distance > max(up_distance, max(left_distance, max(down_distance, right_distance))) + kOccludeDistanceTolerence)
          //     continue;      
          // float distance_2d = cv::norm(cv::Point2f(points_to_camera[i].x, points_to_camera[i].y) );  
          // if (distance_2d >5.0f)
          //     continue;
          //only use the nearest image .
          if (distance > min_distances[filtered_indices[i]])
              continue;

//           0-1.56668
// 0-1.56668
// -0.380136
//[17:49:53] [Picked]	- P#2896760 (-12.496859;-1.778022;-1.386778)
// [18:15:35] [Picked]	- Color: (56;82;97;255)
    static Eigen::Vector3f point1 =  {-12.496859,-1.778022,-1.386778};
    static Eigen::Vector3f point2 = {1.574694,0.253129,-0.056384};

        if ((Eigen::Vector3f(point.x, point.y, point.z) - point1).norm() < 0.0001f || 
          (Eigen::Vector3f(point.x, point.y, point.z) - point2).norm() < 0.0001f) {
            if ((Eigen::Vector3f(point.x, point.y, point.z) - point1).norm() < 0.0001f) {
              // LOG(INFO) << "point 1 camera time: " <<  std::chrono::time_point_cast<std::chrono::milliseconds>(timed_image->stamp).time_since_epoch().count();
              
              LOG(INFO) << "point 1 camera time: " <<  std::endl << cartographer_ros::ToRos(timed_image->stamp);
              cv::imwrite("error1.png", image);
            }else {
              // LOG(INFO) << "point 2 camera time: " <<  std::chrono::time_point_cast<std::chrono::milliseconds>(timed_image->stamp).time_since_epoch().count();
              LOG(INFO) << "point 2 camera time: " <<  cartographer_ros::ToRos(timed_image->stamp);
              cv::imwrite("error2.png", image);
            }
            LOG(INFO)  << "image_id: " << image_id << ", " << ", sensor_to_map " << std::endl << timed_image->pose;
            LOG(INFO)   << "point: " << point;
            // LOG(INFO)   << "filtered_indices[i]: " << filtered_indices[i] ;           
            LOG(INFO) << "distance: " << distance;
            LOG(INFO) <<  "pcl_distance: " << min_distances[filtered_indices[i]];
            if (!distance_image.empty())
              LOG(INFO)<< ", "  << "distance map: " << distance_image.at<float>(row, col);

            LOG(INFO) << "x, y: " << x << ", "  << y;
            cv::Vec3b color = image.at<cv::Vec3b>(row, col);
            LOG(INFO) << "r, g, b: " << (int)color[2] << ", "  << (int)color[1]  << ", "  << (int)color[0];
            // for (int current_col = col - 16; current_col < col + 16; current_col++) {
            //   for (int current_row = row - 16; current_row < row + 16; current_row++) {
            //     std::cout << "[" << current_row << ", "  << current_col << "]:  " << distance_image.at<float>(current_row, current_col) << std::endl;
            //   }               
            // }
        }
        min_distances[filtered_indices[i]] = distance;
        cv::Vec3b color = getColorSubpix(image, cv::Point2f(x, y));
        point.b = color[0];  
        point.g = color[1];  
        point.r = color[2];        
      }

    }
  }
  return ERRCODE_OK;

}


} // namespace map_colorize
}// namespace yida_mapping
