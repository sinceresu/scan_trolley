#include "carto_map_colorizer.h"

#include <algorithm>
#include <fstream>
#include <iostream>

#include "absl/memory/memory.h"
#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/dev/output_pbstream_trajectories.h"

#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>

#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include "tf2_eigen/tf2_eigen.h"
#include "tf2_msgs/TFMessage.h"
#include "tf2_ros/buffer.h"
#include "urdf/model.h"


#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>

#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include "pcl_conversions/pcl_conversions.h"

#include  "colorizer.h"
#include "common.h"
#include "image_filter.h"
#include "image_buffer.h"

#include "map_common/err_code.h"
#include "map_common/image_process.h"

// #define OUTPUT_PCL_FILES
// #define OUTPUT_IMAGE_FILES

using namespace std;

namespace yida_mapping{
using namespace cartographer_ros;

namespace map_colorize{

namespace {

namespace carto = ::cartographer;

::cartographer::io::FileWriterFactory CreateFileWriterFactory(
    const std::string& file_path) {
  const auto file_writer_factory = [file_path](const std::string& filename) {
    return absl::make_unique<carto::io::StreamFileWriter>(file_path + filename);
  };
  return file_writer_factory;
}

std::unique_ptr<carto::io::PointsProcessorPipelineBuilder>
CreatePipelineBuilder(
    const std::vector<carto::mapping::proto::Trajectory>& trajectories,
    const std::string file_prefix) {
  const auto file_writer_factory =
      CreateFileWriterFactory(file_prefix);
  auto builder = absl::make_unique<carto::io::PointsProcessorPipelineBuilder>();
  carto::io::RegisterBuiltInPointsProcessors(trajectories, file_writer_factory,
                                             builder.get());
  builder->Register(RosMapWritingPointsProcessor::kConfigurationFileActionName,
                    [file_writer_factory](
                        carto::common::LuaParameterDictionary* const dictionary,
                        carto::io::PointsProcessor* const next)
                        -> std::unique_ptr<carto::io::PointsProcessor> {
                      return RosMapWritingPointsProcessor::FromDictionary(
                          file_writer_factory, dictionary, next);
                    });
  return builder;
}

std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
    const std::string& configuration_directory,
    const std::string& configuration_basename) {
  auto file_resolver =
      absl::make_unique<carto::common::ConfigurationFileResolver>(
          std::vector<std::string>{configuration_directory});

  const std::string code =
      file_resolver->GetFileContentOrDie(configuration_basename);
  auto lua_parameter_dictionary =
      absl::make_unique<carto::common::LuaParameterDictionary>(
          code, std::move(file_resolver));
  return lua_parameter_dictionary;
}

//  carto::transform::Rigid3d lidarv_to_lidarh(carto::transform::Rigid3d::Vector(-0.00108534, 0.0200793, -0.00103185),
//  carto::transform::Rigid3d::Quaternion(0.999999, 0.000391255, 0.000131068, 0.00121841));

// static bool point_cloud_found = false;
template <typename T>
std::unique_ptr<carto::io::PointsBatch> HandlePCLMessage(
    const T& message, const std::string& tracking_frame,
    const tf2_ros::Buffer& tf_buffer,
    const carto::transform::TransformInterpolationBuffer&
        transform_interpolation_buffer) {
  const carto::common::Time start_time = FromRos(message.header.stamp);

  auto points_batch = absl::make_unique<carto::io::PointsBatch>();
  points_batch->start_time = start_time;
  points_batch->frame_id = message.header.frame_id;
  // carto::transform::Rigid3d lidar_calibrate;
  // if (points_batch->frame_id == "velodyne_v") {
  //   lidar_calibrate = lidarv_to_lidarh;
  // }

  carto::sensor::PointCloudWithIntensities point_cloud;
  carto::common::Time point_cloud_time;
  std::tie(point_cloud, point_cloud_time) =
      ToPointCloudWithIntensities(message);
  CHECK_EQ(point_cloud.intensities.size(), point_cloud.points.size());
  #ifdef OUTPUT_PCL_FILES
    if (message.header.frame_id == "velodyne_h") {
      pcl::PointCloud<pcl::PointXYZ> pcl_point_cloud;
      pcl::fromROSMsg(message, pcl_point_cloud);
      std::stringstream pcd_filepath ;
      pcd_filepath <<  "/home/sujin/output/tmp/" <<  message.header.stamp << ".ply";

      pcl::io::savePLYFile<pcl::PointXYZ>(pcd_filepath.str(),pcl_point_cloud);
    }
  #endif
  for (size_t i = 0; i < point_cloud.points.size(); ++i) {
    const carto::common::Time time =
        point_cloud_time +
        carto::common::FromSeconds(point_cloud.points[i].time);
    if (!transform_interpolation_buffer.Has(time)) {
      continue;
    }
     const carto::transform::Rigid3d tracking_to_map =
        transform_interpolation_buffer.Lookup(time);
    const carto::transform::Rigid3d sensor_to_tracking =
        ToRigid3d(tf_buffer.lookupTransform(
          tracking_frame, message.header.frame_id, ToRos(time)));
    const carto::transform::Rigid3f sensor_to_map =
       (tracking_to_map * sensor_to_tracking).cast<float>();
        // (lidar_calibrate *tracking_to_map * sensor_to_tracking).cast<float>();
    carto::sensor::RangefinderPoint range_point = carto::sensor::ToRangefinderPoint(point_cloud.points[i]);
    points_batch->points.push_back(
        sensor_to_map *
        range_point);

//[[15:06:08] [Picked]	- P#3689052 (-11.029497;-1.365904;-1.377263)
//[13:00:06] [Picked]	- P#32468325 (1.574694;0.253129;-0.056384)
// [16:45:30] [Picked]	- Color: (68;72;71;255)
// carto::sensor::RangefinderPoint world_range_point =  sensor_to_map *
//     range_point;
// static Eigen::Vector3f point1 =  {-11.029497,-1.365904,-1.377263};
// static Eigen::Vector3f point2 = {1.574694,0.253129,-0.056384};
// if ((world_range_point.position - point1).norm() < 0.0001 || (world_range_point.position - point2).norm() < 0.0001) {
//     if (((world_range_point.position - point1).norm() < 0.0001)) {
//       // LOG(INFO) << "pointcloud 1 lidar time " <<  std::chrono::time_point_cast<std::chrono::milliseconds>(start_time).time_since_epoch().count();
//       LOG(INFO) << "pointcloud 1 lidar time " <<  message.header.stamp;
//     }

//     else
//       LOG(INFO) << "pointcloud 2 lidar time " <<  message.header.stamp;
//     //LOG(INFO) << "point cloud time " <<  message.header.stamp;
//   // LOG(INFO) << "point time " <<  time;

//   LOG(INFO) << "frame_id " << points_batch->frame_id << std::endl << "original  " << std::endl  << range_point.position;
//   LOG(INFO) << "sensor_to_map " << std::endl << sensor_to_map;

//   pcl::PointCloud<pcl::PointXYZ> pcl_point_cloud;
//   pcl::fromROSMsg(message, pcl_point_cloud);
//   pcl::io::savePLYFile<pcl::PointXYZ>("original.ply", pcl_point_cloud);
// }



    points_batch->intensities.push_back(point_cloud.intensities[i]);
    // We use the last transform for the origin, which is approximately correct.
    points_batch->origin = sensor_to_map * Eigen::Vector3f::Zero();
  }
  // if (points_batch->frame_id == "velodyne_h") {
  //   static double nearest_time = 0.2;
  //   double now = message.header.stamp.toSec();
  //   double target = 1608705941.426749200;
  //   if (now < target && abs(now - target) < nearest_time) {
  //       nearest_time = abs(now - target);
  //       LOG(INFO) << "raw pcl  time" << message.header.stamp;
  //         pcl::PointCloud<pcl::PointXYZ> pcl_point_cloud;
  //         pcl::fromROSMsg(message, pcl_point_cloud);
  //         pcl::io::savePLYFile<pcl::PointXYZ>(points_batch->frame_id + "_raw.ply", pcl_point_cloud);
  //         pcl::PointCloud<pcl::PointXYZ> world_point_cloud;
  //         for (const auto& point : points_batch->points) {
  //           auto position = point.position;
  //           pcl::PointXYZ world_point(position[0], position[1], position[2]);
  //           world_point_cloud.push_back(world_point);
  //         }
  //         pcl::io::savePLYFile<pcl::PointXYZ>(points_batch->frame_id + "_world.ply", world_point_cloud);

  //   }
  // }

  if (points_batch->points.empty()) {
    return nullptr;
  }
  return points_batch;
}

TimedImage HandleImageMessage(
    sensor_msgs::CompressedImage::ConstPtr img_msg,
    const std::string tracking_frame,
    const std::string ref_lidar,
    const tf2_ros::Buffer& tf_buffer,
    const carto::transform::TransformInterpolationBuffer&
        transform_interpolation_buffer,
        RotateImageMode rotate_image_mode) {


  // cv::Mat(message.height, message.width, CV_8UC3, message.data.data()).copyTo(latest_img[0]);
  // shift 0.5s backward to get more stable time
   //carto::common::Time time =  FromRos(img_msg->header.stamp) - carto::common::FromSeconds(0.5);
  carto::common::Time time =  FromRos(img_msg->header.stamp);
  if (!transform_interpolation_buffer.Has(time)) {
      return TimedImage();
  }
  const carto::transform::Rigid3d tracking_to_map =
    transform_interpolation_buffer.Lookup(time);
    const carto::transform::Rigid3d pre_tracking_to_map =
    transform_interpolation_buffer.Lookup(time );
    auto distance = (tracking_to_map.translation() - pre_tracking_to_map.translation()).norm();
    if (distance > 0.03) {
      return TimedImage();
  }

  auto sensor_to_tracking = ToRigid3d(tf_buffer.lookupTransform(
                tracking_frame, ref_lidar, ToRos(time)));
  const carto::transform::Rigid3f sensor_to_map =
        (tracking_to_map * sensor_to_tracking).cast<float>();

  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, "bgr8");
  cv::Mat  rotated_image;

  if (rotate_image_mode == RotateImageMode::kRotate270Degree) {
    map_common::RotateImage270Degree(cv_ptr->image, rotated_image);
  } else if (rotate_image_mode == RotateImageMode::kFlipUpDown)  {
    map_common::FlipImageUpDown(cv_ptr->image, rotated_image);
  } else {
    rotated_image = std::move(cv_ptr->image);
  }
  TimedImage timed_image;
  timed_image.id = 0, timed_image.image = std::move(rotated_image),  timed_image.stamp = time, timed_image.pose = sensor_to_map;

  return timed_image;

}



std::map<std::string, std::shared_ptr<ImageFilter>> BuildImageFilters( const std::vector<std::string>& image_frame_ids) {
  std::map<std::string, std::shared_ptr<ImageFilter>> image_filters;
  for (size_t  i = 0;  i < image_frame_ids.size(); i++) {
    auto  image_filter = std::make_shared<ImageFilter>();
    image_filters[image_frame_ids[i]] = image_filter;
  }
  return image_filters;

}

std::map<std::string, std::shared_ptr<ImageBuffer>> BuildImageBuffers( const std::vector<std::string>& image_frame_ids,
                      int buffered_frames) {
  std::map<std::string, std::shared_ptr<ImageBuffer>> image_buffers;
  for (size_t  i = 0;  i < image_frame_ids.size(); i++) {
    auto  image_buffer = std::make_shared<ImageBuffer>();
    ImageBufferParam param;
    param.buffered_frames = buffered_frames;
    image_buffer->SetParam(param);
    image_buffers[image_frame_ids[i]] = image_buffer;
  }
  return image_buffers;
}


// void FilterPCLWithRor(pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr input_pcl, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr output_pcl, double search_radius, int min_neighbors_in_radius) {
//   pcl::RadiusOutlierRemoval <pcl::PointXYZRGBA> ror_filter;
//   ror_filter.setInputCloud (input_pcl);
//   ror_filter.setRadiusSearch (search_radius);
//   ror_filter.setMinNeighborsInRadius (min_neighbors_in_radius);
//   ror_filter.filter (*output_pcl);
// }

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
void FilterPCLWithRor(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl, double search_radius, int min_neighbors_in_radius) {
  pcl::RadiusOutlierRemoval <PointT> ror_filter;
  ror_filter.setInputCloud (input_pcl);
  ror_filter.setRadiusSearch (search_radius);
  ror_filter.setMinNeighborsInRadius (min_neighbors_in_radius);
  ror_filter.filter (*output_pcl);
}

void FilterPCLWithSor(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl, int mean_k, double std_mul) {
  pcl::StatisticalOutlierRemoval <PointT> sor_filter;
  sor_filter.setInputCloud (input_pcl);
  sor_filter.setMeanK (mean_k);
  sor_filter.setStddevMulThresh (std_mul);
  sor_filter.filter (*output_pcl);
}

void FilterPCLFile(const std::string &pcl_file_name, int filter_mean_k,  double filter_std_mul, double filter_search_radius,  int filter_min_neighbors_in_radius) {

  PointCloud::Ptr input_pcl = PointCloud::Ptr(new PointCloud() );
  PointCloud::Ptr filtered_map_pcl = PointCloud::Ptr(new PointCloud() );
  // LOG(INFO) << "Filtering   with ror ... " ;

  // pcl::io::loadPLYFile<PointT>(pcl_file_name, *input_pcl);
  // FilterPCLWithRor(input_pcl, filtered_map_pcl, filter_mean_k, filter_std_mul);
  // pcl::io::savePLYFileBinary<PointT>(pcl_file_name, *filtered_map_pcl);

  LOG(INFO) << "Filtering   with sor ... " ;
  pcl::io::loadPLYFile<pcl::PointXYZRGB>(pcl_file_name, *input_pcl);
  FilterPCLWithSor(input_pcl, filtered_map_pcl, filter_search_radius, filter_min_neighbors_in_radius);
  pcl::io::savePLYFileBinary<PointT>(pcl_file_name, *filtered_map_pcl);

  LOG(INFO) << "Filter OK. " ;

  }
}
CartoMapColorizer::CartoMapColorizer()
: stop_flag_(false),
progress_callback_(nullptr)
{

}

int CartoMapColorizer::SetProgressCallback(ProgressCallback callback) {
    progress_callback_ = callback;
    return ERRCODE_OK;
}

int CartoMapColorizer::SetParam(const ColorizeParam & param) {
    if (param.configuration_directory.empty())
        return ERRCODE_INVALIDARG;
    if (param.colorize_configuration_basename.empty())
        return ERRCODE_INVALIDARG;
    if (param.write_configuration_basename.empty())
        return ERRCODE_INVALIDARG;
    if (param.urdf_filename.empty())
        return ERRCODE_INVALIDARG;
    parameter_ = param;
    return ERRCODE_OK;
}

int CartoMapColorizer::ColorizeMap(const std::string & state_filename, const std::string & map_bag_filename, const std::string & map_pcd_filename, const std::string & save_directory, const std::string & map_name){
  std::vector<std::string> map_bag_filenames = {map_bag_filename};
  std::vector<std::string> state_filenames = {state_filename};
  return ColorizeMaps(state_filenames, map_bag_filenames, map_pcd_filename, save_directory, map_name);
}

int CartoMapColorizer::ColorizeMaps(const std::vector<std::string> & state_filenames, const std::vector<std::string> & map_bag_filenames, const std::string & map_pcd_filename, const std::string & save_directory,  const std::string& map_name) {

  LoadOptions(parameter_.configuration_directory, parameter_.colorize_configuration_basename);

  colorizers = BuildColorizers(parameter_.configuration_directory);

  Process(state_filenames,
                    map_bag_filenames,
                   map_pcd_filename,
                   save_directory,
                    map_name);

  LOG(INFO) << "Finished map colorize!";

  return ERRCODE_OK;
}
int CartoMapColorizer::Stop() {
   stop_flag_ = true;
  return ERRCODE_OK;
}
void CartoMapColorizer::LoadOptions( const std::string& configuration_directory,
    const std::string& configuration_basename){

  const auto lua_parameter_dictionary =
      LoadLuaDictionary(configuration_directory, configuration_basename);

  const auto filter_options = lua_parameter_dictionary->GetDictionary("pcl_filter");
  filter_pcl = filter_options->GetBool("filter_pcl");
  filter_search_radius = filter_options->GetDouble("search_radius");
  filter_min_neighbors_in_radius = filter_options->GetInt("min_neighbors_in_radius");
  filter_mean_k = filter_options->GetInt("mean_k");
  filter_std_mul = filter_options->GetDouble("std_mul");

  const auto colorize_options = lua_parameter_dictionary->GetDictionary("colorize");
  skip_seconds = colorize_options->GetDouble("skip_seconds");
  rotate_image_mode = static_cast<RotateImageMode>(colorize_options->GetInt("rotate_image_mode"));
  ref_frame_id =  colorize_options->GetString("ref_frame_id");
  image_frame_ids = colorize_options->GetDictionary("image_frame_ids")->GetArrayValuesAsStrings();
  calibration_basenames = colorize_options->GetDictionary("calibration_basenames")->GetArrayValuesAsStrings();

  horizontal_left_fovs = colorize_options->GetDictionary("horizontal_left_fovs")->GetArrayValuesAsDoubles();
  horizontal_right_fovs = colorize_options->GetDictionary("horizontal_right_fovs")->GetArrayValuesAsDoubles();
  vertical_fov = colorize_options->GetDouble("vertical_fov");
  colorize_plane_distances = colorize_options->GetDictionary("plane_distances")->GetArrayValuesAsDoubles();
  occlude_distance = colorize_options->GetDouble("occlude_distance");
  buffered_frames =  colorize_options->GetInt("buffered_frames");

}

std::map<std::string, std::shared_ptr<Colorizer>> CartoMapColorizer::BuildColorizers( const std::string& configuration_directory) {


  std::map<std::string, std::shared_ptr<Colorizer>> colorizers;
  for (size_t i = 0; i <  image_frame_ids.size(); i++) {
    auto  colorizer = std::make_shared<Colorizer>();
    colorizer->LoadCalibrationFile(configuration_directory + "/" + calibration_basenames[i]);
    colorizer->SetParamters( horizontal_left_fovs[i],
                                                      horizontal_right_fovs[i],
                                                      vertical_fov,
                                                      colorize_plane_distances[0],
                                                      colorize_plane_distances[1],
                                                      occlude_distance);
    colorizers[image_frame_ids[i]] = colorizer;
  }
  return colorizers;
}
constexpr int pointsprocess_times = 3;
constexpr int  frames_per_progresscallback= 1000;
constexpr float  kColorizeRatio = 0.8;

int CartoMapColorizer::Process(const std::vector<std::string> & state_filenames,
                          const std::vector<std::string>& bag_filenames,
                          const std::string& pcd_filename,
                          const std::string & save_directory,  const std::string& map_name) {

  std::vector<::cartographer::mapping::proto::Trajectory> all_trajectories;
    for (std::string pose_graph_filename : state_filenames) {
      ::cartographer::mapping::proto::PoseGraph pose_graph(
                carto::io::DeserializePoseGraphFromFile(pose_graph_filename));
      // This vector must outlive the pipeline.
      all_trajectories .insert(all_trajectories.end(),
            pose_graph.trajectory().begin(), pose_graph.trajectory().end());
    }

  CHECK_GE(all_trajectories.size(), bag_filenames.size())
      << "Pose graphs contains " << all_trajectories.size()
      << " trajectories while " << bag_filenames.size()
      << " bags were provided. This tool requires one bag for each "
          "trajectory in the same order as the correponding trajectories in the "
          "pose graph proto.";


  tf2_ros::Buffer tf_buffer;
  if (!parameter_.urdf_filename.empty()) {
    ReadStaticTransformsFromUrdf(parameter_.urdf_filename, &tf_buffer);
  }
      const std::string output_file_prefix = save_directory + "/" + map_name ;
  std::unique_ptr<::cartographer::io::PointsProcessorPipelineBuilder>
      point_pipeline_builder_ =
    CreatePipelineBuilder(all_trajectories, output_file_prefix);

   const auto lua_parameter_dictionary =
      LoadLuaDictionary(parameter_.configuration_directory, parameter_.write_configuration_basename);
  std::vector<std::unique_ptr<carto::io::PointsProcessor>> pipeline =
    point_pipeline_builder_->CreatePipeline(
        lua_parameter_dictionary->GetDictionary("pipeline").get());
  const std::string tracking_frame =
      lua_parameter_dictionary->GetString("tracking_frame");


  pcl::PointCloud<pcl::PointXYZ>::Ptr map_pcl(new pcl::PointCloud<pcl::PointXYZ>() );
  if (!pcd_filename.empty())
    pcl::io::loadPLYFile<pcl::PointXYZ>(pcd_filename, *map_pcl);
  #ifdef OUTPUT_IMAGE_FILES

  std::ofstream out_path;
  out_path.open(save_directory + "/" + map_name + "_images.txt");
  LOG(INFO) << "write  Image pose  to : "  << save_directory + "/" + map_name + "_images.txt";
#endif

  std::map<std::string, std::shared_ptr<ImageFilter>> image_buffers = BuildImageFilters(image_frame_ids);
   //std::map<std::string, std::shared_ptr<ImageBuffer>> image_buffers = BuildImageBuffers(image_frame_ids, buffered_frames);

  int pointsprocess_cycles = 0;
  float progress_step =  1.0f / bag_filenames.size() / pointsprocess_times;
  float current_progress = 0.0f;
  boost::filesystem::path colored_pcl_dir( "./colored_pcl");
  boost::filesystem::create_directory(colored_pcl_dir);

  stop_flag_ = false;

  float colorize_ratio = filter_pcl  ? kColorizeRatio : 1.0f;
  do {
    uint32_t image_id = 1;
    uint32_t pcl_frame_id = 0;

    for (size_t trajectory_id = 0; trajectory_id < bag_filenames.size();
        ++trajectory_id) {
      const carto::mapping::proto::Trajectory& trajectory_proto =
          all_trajectories[trajectory_id];
      const std::string& bag_filename = bag_filenames[trajectory_id];
      LOG(INFO) << "Colorizing map for  " << bag_filename << "...";
      if (trajectory_proto.node_size() == 0) {
        continue;
      }
      const carto::transform::TransformInterpolationBuffer
          transform_interpolation_buffer(trajectory_proto);
      rosbag::Bag bag;
      bag.open(bag_filename, rosbag::bagmode::Read);
      rosbag::View view(bag);
      const ::ros::Time begin_time = view.getBeginTime();
      const double duration_in_seconds =
          (view.getEndTime() - begin_time).toSec();

      // We need to keep 'tf_buffer' small because it becomes very inefficient
      // otherwise. We make sure that tf_messages are published before any data
      // messages, so that tf lookups always work.
      std::deque<rosbag::MessageInstance> messages;
      // We publish tf messages one second earlier than other messages. Under
      // the assumption of higher frequency tf this should ensure that tf can
      // always interpolate.

      const ::ros::Time kSkip = begin_time + ::ros::Duration(skip_seconds);

      for (const rosbag::MessageInstance& message : view) {
          if (stop_flag_)
            break;
          if (message.getTime() < kSkip) {
            // LOG(INFO) <<  "SKIP " << message.getTime();
            continue;
          }
          // process images;
          if (pointsprocess_cycles == 0 ) {
            if(message.isType<sensor_msgs::CompressedImage>()) {
              sensor_msgs::CompressedImage::Ptr image_message =message.instantiate<sensor_msgs::CompressedImage>();
              if (image_buffers.count( image_message->header.frame_id) > 0) {
                TimedImage timed_image = HandleImageMessage(image_message,  tracking_frame, ref_frame_id,  tf_buffer, transform_interpolation_buffer, rotate_image_mode);
                if (!timed_image.image.empty()) {

                  LOG(INFO) << "image id :" << image_id << std::endl;
                  auto image_filter = image_buffers[ image_message->header.frame_id];
                  timed_image.id = image_id;
                  image_filter->HandleImage(std::move(timed_image));

  #ifdef OUTPUT_IMAGE_FILES

                std::stringstream png_file;
                png_file << "/home/sujin/output/tmp/" << image_message->header.stamp  << "_"<< image_message->header.frame_id << ".png";
                imwrite(png_file.str(), timed_image.image);

                Eigen::Matrix4f Twv = Eigen::Matrix4f::Identity();
                Eigen::Quaternionf quaternion = timed_image.pose.cast<float>().rotation(); // w x y z

                Twv.block<3, 3>(0, 0) = quaternion.toRotationMatrix();
                Twv.block<3, 1>(0, 3) << timed_image.pose.cast<float>().translation();

                out_path << image_message->header.stamp  << " "<< image_message->header.frame_id  << " "  << image_message->header.stamp << " "  <<  image_message->header.frame_id << " "<< Twv(0,0) << " " <<  Twv(0,1) << " " << Twv(0,2) << " " << Twv(0,3)
                                                              << " " <<  Twv(1,0) << " " <<  Twv(1,1) << " " << Twv(1,2) << " " << Twv(1,3)
                                                              << " " << Twv(2,0) << " " <<  Twv(2,1) << " " << Twv(2,2) << " " << Twv(2,3)
                                                              << " " << Twv(3,0) << " " <<  Twv(3,1) << " " << Twv(3,2) << " " << Twv(3,3)
                                                              << std::endl;

  #endif


                  image_id++;
                }
              }
            }
            continue;
                // cv::Mat(message.height, message.width, CV_8UC3, message.data.data()).copyTo(latest_img[0]);
          }

          std::unique_ptr<carto::io::PointsBatch> points_batch;
          if (message.isType<sensor_msgs::PointCloud2>()) {
            sensor_msgs::PointCloud2::Ptr cloud_msg = message.instantiate<sensor_msgs::PointCloud2>();

            points_batch = HandlePCLMessage(
                *cloud_msg,
                tracking_frame, tf_buffer, transform_interpolation_buffer);
            if (points_batch != nullptr) {
              points_batch->trajectory_id = trajectory_id;

              pcl::PointCloud<pcl::PointXYZRGB>::Ptr  colored_point_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>() );
              //colorize
              if (pointsprocess_cycles == 1) {
                for (const auto& point :  points_batch->points) {
                  pcl::PointXYZRGB color_point(0u, 0u, 0u);
                  color_point.x = point.position[0];
                  color_point.y = point.position[1];
                  color_point.z = point.position[2];
                  colored_point_cloud->push_back(pcl::PointXYZRGB(color_point));
                }

                std::vector<float > min_distances(colored_point_cloud->size(),std::numeric_limits<float>::max());
                // pcl::io::savePLYFile<pcl::PointXYZRGB>(std::string("/home/sujin/output/") + "original.ply", *colored_point_cloud);
                for (auto image_frame_id :  image_frame_ids) {
                  TimedImage timed_image;
                  //auto timed_images= image_filters[ image_frame_id]->GetNearestImages(FromRos(cloud_msg->header.stamp));
                  auto timed_images= image_buffers[ image_frame_id]->GetNearestImages(points_batch->origin, 10);
                  if (!timed_images.empty())
                    colorizers[image_frame_id]->Colorize(colored_point_cloud,  timed_images, map_pcl, min_distances);
                }
                  // static double diff_time = 0.2;
                  // double now = cloud_msg->header.stamp.toSec();
                  // double target = 1608088327.453029600;
                  // if (now < target && abs(now - target) < diff_time) {
                  //     LOG(INFO) << "colorized pcl  " <<  points_batch->frame_id << ", time" <<now;
                  //     pcl::io::savePLYFile<pcl::PointXYZRGB>(points_batch->frame_id + "_colored_static.ply", *colored_point_cloud);
                  // }
                  // if (point_cloud_found) {
                  //     pcl::io::savePLYFile<pcl::PointXYZRGB>(points_batch->frame_id + "_colored_error.ply", *colored_point_cloud);
                  //     point_cloud_found = false;
                  // }
                std::stringstream frame_file_name;
                frame_file_name << pcl_frame_id << ".pcd";
                pcl::io::savePCDFile<pcl::PointXYZRGB>(colored_pcl_dir.string() + "/" +  frame_file_name.str(), *colored_point_cloud, true);
              } else{
                std::stringstream frame_file_name;
                frame_file_name << pcl_frame_id << ".pcd";
                pcl::io::loadPCDFile<pcl::PointXYZRGB>(colored_pcl_dir.string() + "/" +  frame_file_name.str(), *colored_point_cloud);
              }
              absl::flat_hash_set<int> to_remove;
              for (size_t i = 0;  i < colored_point_cloud->size(); i++ ) {
                const auto & raw_point = colored_point_cloud->at(i);
                if (raw_point.r == 0 && raw_point.g == 0 && raw_point.b == 0  ) {
                  to_remove.insert(i);
                }
                cartographer::io::Uint8Color color_8{{raw_point.r, raw_point.g, raw_point.b}};

                points_batch->colors.push_back(cartographer::io::ToFloatColor(color_8));
              }
              cartographer::io::RemovePoints(to_remove, points_batch.get());
              // pcl::io::savePLYFile<pcl::PointXYZRGB>(std::string("/home/sujin/output/") + points_batch->frame_id +"_original_colored.ply", *colored_point_cloud);
              pipeline.back()->Process(std::move(points_batch));
            }

            LOG_EVERY_N(INFO, 2000)
                << "Processed " << (message.getTime() - begin_time).toSec()
                << " of " << duration_in_seconds << " bag time seconds...";
            if ((++pcl_frame_id % frames_per_progresscallback) == 0) {
                if (progress_callback_ != nullptr) {
                    float progress =  progress_step *  (message.getTime() - begin_time).toSec() /   duration_in_seconds + current_progress ;
                    progress_callback_(colorize_ratio * progress* 100.0f) ;
                    LOG(INFO) << "colorize progress " <<  progress * 100.0f;

                }
            }

          }


      }
      bag.close();
      if (pointsprocess_cycles != 0)
        current_progress += progress_step;
    }

    pointsprocess_cycles++;

    if (stop_flag_)
      break;

  } while (pointsprocess_cycles == 1 ||  pipeline.back()->Flush() ==
          carto::io::PointsProcessor::FlushResult::kRestartStream);

boost::filesystem::remove_all(colored_pcl_dir);

if (progress_callback_ != nullptr) {
    progress_callback_(colorize_ratio * 100.f);
}
  // image_filters.clear();
  image_buffers.clear();
  colorizers.clear();

  LOG(INFO) << "Finished map writing...";

  if (!stop_flag_ && filter_pcl) {
    LOG(INFO) << "Filtering ...";
    FilterPCLFile(output_file_prefix + ".ply", filter_mean_k, filter_std_mul, filter_search_radius, filter_min_neighbors_in_radius);
  }

if (progress_callback_ != nullptr) {
    progress_callback_(100.f);
}

  LOG(INFO) << "Finished Colorization. " ;
  return ERRCODE_OK;
}

} // namespace map_colorize
}// namespace yida_mapping
