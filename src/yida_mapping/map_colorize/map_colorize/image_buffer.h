#ifndef _MAP_COLORIZE_IMAGE_BUFFER_H
#define _MAP_COLORIZE_IMAGE_BUFFER_H

#include <vector>
#include <deque>
#include <memory>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "sensor_msgs/Imu.h"


#include "opencv2/opencv.hpp"
#include "common.h"

#include "map_common/global.h"

namespace yida_mapping{

namespace map_colorize{

typedef struct ImageBufferParam {
  int buffered_frames = 15;
}ImageBufferParam;

typedef struct BufferedImage {
    uint32_t id;
    std::string image_filepath;
    ::cartographer::common::Time stamp;
    ::cartographer::transform::Rigid3f pose;
  bool operator == (const uint32_t & i) {
    return id == i;
  }
}BufferedImage;

class ImageBuffer 
{
public:
  ImageBuffer();
  ~ImageBuffer(){};

  virtual  int SetParam(const ImageBufferParam & param);
  virtual  int HandleImage(TimedImage image);

  std::vector<std::shared_ptr<TimedImage>> GetNearestImages(const Eigen::Vector3f& position, int nearest_frames);

private:

  ImageBufferParam parameter_;

  std::deque<std::shared_ptr<BufferedImage>> image_queue_;
  std::map<uint32_t, std::shared_ptr<TimedImage>> buffered_images_;
  std::string temp_dir;
};
} // namespace map_colorize 
}// namespace yida_mapping

#endif 