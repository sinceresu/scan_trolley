#ifndef _MAP_COLORIZE_TIMED_IMAGE_H
#define _MAP_COLORIZE_TIMED_IMAGE_H

#include "cartographer/common/time.h"
#include "cartographer/transform/rigid_transform.h"


#include "opencv2/opencv.hpp"



namespace yida_mapping{

namespace map_colorize{


typedef struct TimedImage {
    uint32_t id;
    cv::Mat image;
    ::cartographer::common::Time stamp;
    ::cartographer::transform::Rigid3f pose;
    bool operator ==(uint32_t other_id) {
        return id == other_id;
    }
    TimedImage() = default;

    TimedImage(const TimedImage &) = delete;
    TimedImage &operator=(const TimedImage &) = delete;

    TimedImage(TimedImage && other) {
        id = other.id;
        stamp = other.stamp;
        pose = other.pose;
        image = std::move(other.image);
    }

}TimedImage;

typedef struct ImuData {
    uint64_t time_ms;
    Eigen::Vector3d linear_acceleration;
    Eigen::Vector3d angular_velocity;
}ImuData;
} // namespace map_colorize
}// namespace yida_mapping

#endif  