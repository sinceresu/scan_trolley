#include "image_filter.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>
#include <limits>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>
#include "glog/logging.h"

#include "cartographer/transform/transform.h"


#include "map_common/err_code.h"
#include "stop_detector.h"


using namespace std;


namespace yida_mapping{
  
namespace map_colorize{
namespace {
// static constexpr  size_t kMaxImageQueueSize = 10u;


}

ImageFilter::ImageFilter() 
{
} 


 int ImageFilter::HandleImage(TimedImage timed_image) {
      auto image_ptr = std::make_shared<TimedImage>();
      image_ptr->id = timed_image.id;
      image_ptr->stamp = timed_image.stamp;
      image_ptr->pose = timed_image.pose;
      image_ptr->image = std::move(timed_image.image);
      image_queue_.push_back(image_ptr);
      return ERRCODE_OK;
}



std::vector<std::shared_ptr<TimedImage>> ImageFilter:: GetNearestImages(const Eigen::Vector3f& position, size_t nearest_frames) {

  size_t frames = min(nearest_frames, image_queue_.size());
  std::partial_sort(image_queue_.begin(),image_queue_.begin() + frames, image_queue_.end(), [&position](const std::shared_ptr<TimedImage>& left,  const std::shared_ptr<TimedImage>& right) {
            return  (left->pose.translation() - position).norm() <  (right->pose.translation() - position).norm();
      });
  return std::vector<std::shared_ptr<TimedImage>>(image_queue_.begin(), image_queue_.begin() + frames);
}

// std::vector<std::shared_ptr<TimedImage>>  ImageFilter:: GetNearestImages(const ::cartographer::common::Time& time) {
//     auto nearest_image = std::min_element(image_queue_.begin(), image_queue_.end(), [&time](const std::shared_ptr<TimedImage>& left,  const std::shared_ptr<TimedImage>& right) {
//         return abs((left->stamp - time).count()) <  abs((right->stamp - time).count());
//     });
// if (nearest_image == image_queue_.end() ) {
//     return std::vector<std::shared_ptr<TimedImage>>();
// }
// std::vector<std::shared_ptr<TimedImage>>  nerarest_images;
// nerarest_images.push_back(*nearest_image);
// if (nearest_image !=image_queue_.begin()  ) {
//   nerarest_images.push_back(*(nearest_image - 1));
// }
// if (nearest_image + 1 !=image_queue_.end()  ) {
//   nerarest_images.push_back(*(nearest_image + 1));
// }
// return nerarest_images;
// }

// std::vector<std::shared_ptr<TimedImage>>  ImageFilter::GetAllImages() {
//   std::vector<std::shared_ptr<TimedImage>>  nerarest_images(image_queue_.begin(), image_queue_.end());
//   return nerarest_images;
// }


}
}