#include "image_buffer.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>
#include <limits>
#include <boost/filesystem.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>
#include "glog/logging.h"

#include "cartographer/transform/transform.h"


#include "map_common/err_code.h"
#include "stop_detector.h"


using namespace std;


namespace yida_mapping{
  
namespace map_colorize{
namespace {

}

ImageBuffer::ImageBuffer() :
  temp_dir(boost::filesystem::temp_directory_path().string()  +  "/")
{
} 

int ImageBuffer::SetParam(const ImageBufferParam & param) {
  parameter_ = param;

  return ERRCODE_OK;
}


int ImageBuffer::HandleImage(TimedImage image) {
  auto image_ptr = std::make_shared<BufferedImage>();
  image_ptr->id = image.id;
  image_ptr->stamp = image.stamp;
  image_ptr->pose = image.pose;
  std::stringstream  image_filepath;
  image_filepath <<  temp_dir <<  image.id << ".png";
  image_ptr->image_filepath = image_filepath.str();
  cv::imwrite(image_filepath.str(), image.image);

  image_queue_.push_back(image_ptr);

  return ERRCODE_OK;

}


std::vector<std::shared_ptr<TimedImage>> ImageBuffer:: GetNearestImages(const Eigen::Vector3f& position, int nearest_frames) {
     std::partial_sort(image_queue_.begin(),image_queue_.begin() + nearest_frames, image_queue_.end(), [&position](const std::shared_ptr<BufferedImage>& left,  const std::shared_ptr<BufferedImage>& right) {
            return  (left->pose.translation() - position).norm() <  (right->pose.translation() - position).norm();
      });

    std::set<uint32_t> nearest_ids;
    for (int i = 0; i < nearest_frames; i++  ) {
        auto & input_image = image_queue_[i];
        if (buffered_images_.count(input_image-> id) == 0) {
          std::shared_ptr<TimedImage> new_image = std::make_shared<TimedImage>();
          new_image->id = input_image->id;
          new_image->stamp = input_image->stamp;
          new_image->pose = input_image->pose;
          new_image->image = cv::imread(input_image->image_filepath);
          buffered_images_[input_image-> id ] = new_image;
        }
        nearest_ids.insert(input_image-> id);
    }
    while (buffered_images_.size() > parameter_. buffered_frames) {
      for (const auto & buffered_image :  buffered_images_) {
        if (nearest_ids.count(buffered_image.first) == 0) {
          buffered_images_.erase(buffered_image.first);
          break;
        }
      }
    }
    std::vector<std::shared_ptr<TimedImage>> result_images;
    for (int i = 0; i < nearest_frames; i++  ) {
       result_images.push_back(buffered_images_[image_queue_[i]->id]);
    }

    return result_images;
  }

}
}