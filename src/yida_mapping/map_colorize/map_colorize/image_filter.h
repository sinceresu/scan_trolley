#ifndef _MAP_COLORIZE_IMAGE_FILTER_H
#define _MAP_COLORIZE_IMAGE_FILTER_H

#include <vector>
#include <deque>
#include <memory>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "sensor_msgs/Imu.h"


#include "opencv2/opencv.hpp"
#include "common.h"

#include "map_common/global.h"

namespace cartographer {
namespace mapping {
  class MotionFilter;
}
}

namespace yida_mapping{

namespace map_colorize{
class StopDetector;

class ImageFilter 
{
public:
  ImageFilter();
  ~ImageFilter(){};

  virtual  int HandleImage(TimedImage image);
  // virtual  std::vector<std::shared_ptr<TimedImage>> GetNearestImage(const Eigen::Vector3d& position);
  // std::vector<std::shared_ptr<TimedImage>> GetNearestImages(const ::cartographer::common::Time& time);
  std::vector<std::shared_ptr<TimedImage>> GetNearestImages(const Eigen::Vector3f& position, size_t nearest_frames);
  std::vector<std::shared_ptr<TimedImage>> GetAllImages();


private:

  std::deque<std::shared_ptr<TimedImage>> image_queue_;


};
} // namespace map_colorize 
}// namespace yida_mapping

#endif 