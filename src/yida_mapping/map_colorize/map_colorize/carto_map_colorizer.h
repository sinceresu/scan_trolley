#ifndef _MAP_COLORIZE_MAP_COLORIZE_H
#define _MAP_COLORIZE_MAP_COLORIZE_H

#include <vector>
#include <memory>
#include <thread>
#include "ros/spinner.h"
#include "cartographer/io/image.h"
#include <pcl/point_types.h> 
#include <pcl/point_cloud.h>

#include "include/map_colorize_interface.h"


namespace yida_mapping{
namespace map_colorizemap_colorize{
  class Node;
}
namespace map_colorize{
class Colorizer; 

class CartoMapColorizer : public MapColorizeInterface
{
public:
  CartoMapColorizer();
  ~CartoMapColorizer(){};
 virtual  int SetProgressCallback(ProgressCallback callback) override ;
  virtual  int SetParam(const ColorizeParam & param) override ;
  virtual  int ColorizeMap(const std::string & state_filename, const std::string & map_bag_filename, const std::string & map_pcd_filename,  const std::string & save_directory, const std::string & map_name) override ;
  virtual  int  ColorizeMaps(const std::vector<std::string> & state_filenames, const std::vector<std::string> & map_bag_filenames, const std::string & map_pcd_filename, const std::string & save_directory,  const std::string& map_name) override ;
  virtual  int Stop() override ;

private:
  void LoadOptions( const std::string& configuration_directory,
    const std::string& configuration_basename);
std::map<std::string, std::shared_ptr<Colorizer>> BuildColorizers( const std::string& configuration_directory);
  int Process(const std::vector<std::string> & state_filenames,
                          const std::vector<std::string>& bag_filenames,
                          const std::string& pcd_filename,
                          const std::string & save_directory,  
                          const std::string& map_name
            );

  ColorizeParam parameter_;


  bool filter_pcl ;
  double filter_search_radius;
  int filter_min_neighbors_in_radius;
  int filter_mean_k;
  double filter_std_mul;

  double skip_seconds;
  std::string ref_frame_id;
  std::vector<std::string> image_frame_ids;    
  std::vector<std::string> calibration_basenames;    
  float horizontal_fov;
  std::vector<double> horizontal_left_fovs;
  std::vector<double> horizontal_right_fovs;
  float vertical_fov ;
  float occlude_distance;
  std::vector<double> colorize_plane_distances ;
  RotateImageMode rotate_image_mode;

  int nearest_frames ;
  int buffered_frames;
  std::map<std::string, std::shared_ptr<Colorizer>>  colorizers ;


  bool stop_flag_;
  // ::cartographer::mapping::proto::PoseGraph pose_graph_;
  ProgressCallback progress_callback_;

};
} // namespace map_colorizef
}// namespace yida_mapping

#endif  