#include "include/libmap_colorize.h"
#include "carto_map_colorizer.h"

namespace yida_mapping{
namespace map_colorize{

std::shared_ptr<MapColorizeInterface> CreateMapColorizer(MapColorizeType type)
{
    switch (type)
    {
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_colorize();
    case CARTOGRAPHER:
        return std::shared_ptr<MapColorizeInterface>(new CartoMapColorizer());
    default:
        return nullptr;
    }
}

} // namespace map_colorize
}// namespace yida_mapping
