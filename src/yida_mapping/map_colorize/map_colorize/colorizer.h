#ifndef _MAP_COLORIZE_COLORIZER_H
#define _MAP_COLORIZE_COLORIZER_H
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "common.h"

namespace yida_mapping{

namespace map_colorize{
  
class Colorizer 
{
public:
  explicit Colorizer() ;

  virtual ~Colorizer();

  Colorizer(const Colorizer&) = delete;
  Colorizer& operator=(const Colorizer&) = delete;

  int LoadCalibrationFile(const std::string& calibration_filepath) ;
  int SetParamters( float horizontal_left_fov, 
                          float horizontal_right_fov,
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance,
                          float occlude_distance = 0.2) ;

  int Colorize(pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud, const std::vector<std::shared_ptr<TimedImage>>& timed_images, pcl::PointCloud<pcl::PointXYZ>::ConstPtr occlude_pcl, std::vector<float >& min_distances ) ;
  int ColorizeImage(pcl::PointCloud<pcl::PointXYZ>::ConstPtr map_pcl, const TimedImage& timed_image, pcl::PointCloud<pcl::PointXYZRGB>::Ptr colored_pcl ) ;
private:
  std::vector<int> CullingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud, const Eigen::Matrix4f& parent_link_pose) ;
  pcl::PointCloud<pcl::PointXYZ>::Ptr CullingOccludePointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& parent_link_pose) ;
  pcl::PointCloud<pcl::PointXYZ>::Ptr UpsamplingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud) ;

  void GenerateDistanceImage(pcl::PointCloud<pcl::PointXYZ>::ConstPtr occlude_pcl, const cv::Mat& image, const Eigen::Matrix4f& ref_to_world, cv::Mat& distance_image);
  void UpdataDistanceImages(pcl::PointCloud<pcl::PointXYZ>::ConstPtr occlude_pcl,  const std::vector<std::shared_ptr<TimedImage>>&  timed_images);


  // void InitializeOcclusionFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  Eigen::Matrix4f camara_to_ref_pose_;
  Eigen::Matrix4f camara_to_ref_for_culling_;
  Eigen::Matrix4f ref_to_camera_pose_;

  cv::Mat rotation_vec_;
  cv::Mat transition_vec_;

  float horizontal_fov_;
  float horizontal_left_fov_;
  float horizontal_right_fov_;

  float vertical_fov_;
  float near_plane_distance_; 
  float far_plane_distance_;
  float occlude_distance_;

  // pcl::VoxelGridOcclusionEstimation<pcl::PointXYZRGB> occlusion_filter_;
  std::set<uint32_t> image_id_set;

  std::map<uint32_t, cv::Mat> distance_images;
  int num_threads_;
 };

} // namespace map_colorize
}// namespace yida_mapping
#endif  
