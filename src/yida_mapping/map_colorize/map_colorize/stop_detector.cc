#include "stop_detector.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"

#include "map_common/err_code.h"

using namespace std;

namespace yida_mapping{

namespace map_colorize{
static constexpr  double kMoveThreshFactor = 4.0;

StopDetector::StopDetector()  :
state_(StopDetectState::DETECTING),
buffer_filled_(false)
{
}

StopDetector::~StopDetector() {

}

void StopDetector::SetParameter(StopDetectParam param) {
    parameter_ = param;
}

int   StopDetector::Detect(const ImuData& imu_data) {
  InsertImuData(imu_data);
  switch (state_) 
    {
      case StopDetectState::DETECTING : {
        if (IsStill()) {
            // LOG(INFO) << "detected stop at " << imu_data.time_ms << " ms.";
            state_ =  StopDetectState::DETECTED;
        } 
        break;
      }

      case StopDetectState::DETECTED : 
      {
        if (IsMoving()) {
            // LOG(INFO) << "detected move at " << imu_data.time_ms << " ms.";
            state_ =  StopDetectState::DETECTING;
        }
        break;                                                                                                          
          
      }
      default : {
        LOG(FATAL) << "wrong StopDetector";
        break;
      }
    }
  return ERRCODE_OK;
}

bool StopDetector::StopDetected(){
  return state_ == StopDetectState::DETECTED ;
}



void  StopDetector::InsertImuData(const ImuData& imu_data) {
    imu_buffer_.push_back(imu_data);
    uint64_t first_time = imu_buffer_.front().time_ms;
    if (imu_data.time_ms - first_time > parameter_.min_still_interval_ms) {
        imu_buffer_.pop_front();
        buffer_filled_= true;
    }
}

double StopDetector::GetMaxAngularVelocity() {

    double max_angular_velocity = 0.;
    for (size_t i = 0; i < imu_buffer_.size() ; i ++) {
        Eigen::Vector3d& angular_velocity = imu_buffer_[i] .angular_velocity;
        max_angular_velocity = std::max(max_angular_velocity, std::max(abs(angular_velocity[0]), std::max(abs(angular_velocity[1]), abs(angular_velocity[2]))));
    }
    return max_angular_velocity;
}

bool StopDetector::IsStill()  {
    if (!buffer_filled_) 
        return false;
    double max_angular_velocity = GetMaxAngularVelocity();

    if ( max_angular_velocity < parameter_.angel_velocity_still_thresh )
      return true;

    return false;

}

bool StopDetector::IsMoving()  {
    if (!buffer_filled_) 
        return false;
    double max_angular_velocity = GetMaxAngularVelocity();

    if (max_angular_velocity > parameter_.angel_velocity_still_thresh * kMoveThreshFactor )
      return true;

    return false;

}
} // namespace map_colorize
}// namespace yida_mapping