#ifndef _MAP_COLORIZE_STOPDETECTOR_H
#define _MAP_COLORIZE_STOPDETECTOR_H
#include <vector>
#include <deque>

#include <Eigen/Core>

#include "common.h"

namespace yida_mapping{

namespace map_colorize{

typedef struct DetectResult {
    uint64_t start_time_ms;
    uint64_t stop_time_ms;
    uint64_t stillest_time_ms;
    Eigen::Vector3d stillest_linear_acceleration;
    Eigen::Vector3d stillest_angular_velocity;
}DetectResult;  

class StopDetector 
{
  enum class StopDetectState {
    DETECTING = 0, 
    DETECTED, 
  };
public:
   typedef struct StopDetectParam {
    uint64_t min_still_interval_ms = 1000;
    double angel_velocity_still_thresh = 0.02;
   }StopDetectParam;
  explicit StopDetector() ;

  virtual ~StopDetector();

  StopDetector(const StopDetector&) = delete;
  

  StopDetector& operator=(const StopDetector&) = delete;
  void SetParameter(StopDetectParam param);
 int Detect(const ImuData& imu_data) ;
 bool StopDetected();
private:
  void InsertImuData(const ImuData& imu_data);
  double GetMaxAngularVelocity ();

  bool IsStill() ;
  bool IsMoving() ;

  StopDetectState state_;

  std::deque<ImuData> imu_buffer_;
  StopDetectParam parameter_;
  bool buffer_filled_;


};

} // namespace map_colorize
}// namespace yida_mapping
#endif  
