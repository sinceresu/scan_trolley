#include "gflags/gflags.h"
#include <stdio.h>
#include <ros/ros.h>

#include "cartographer/io/image.h"
#include "cartographer_ros/ros_log_sink.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/static_transform_broadcaster.h"
#include "urdf/model.h"

#include "cartographer_ros/node.h"
#include "cartographer_ros/playable_bag.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/assets_writer.h"

#include "cartographer_ros/node_options.h"
#include "cartographer_ros/ros_log_sink.h"

#include "map_registrate/carto_map_registrater.h"
 DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(registrate_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");             

DEFINE_string(
    pcl_filenames, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(state_filenames, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");


#include <memory>
using namespace std;

namespace yida_mapping{
using namespace cartographer_ros;

namespace map_registrate{
namespace {
 std::shared_ptr<CartoMapRegistrater>  registrator;

void RegistrateMaps(int argc, char** argv) {

    ::ros::init(argc, argv, "map_registrate");
    ::ros::start();

    const std::vector<std::string> state_filenames =
      absl::StrSplit(FLAGS_state_filenames, ',', absl::SkipEmpty());
    LOG(INFO) << "FLAGS_state_filenames"   << FLAGS_state_filenames;

    const std::vector<std::string> pcl_filenames =
      absl::StrSplit(FLAGS_pcl_filenames, ',', absl::SkipEmpty());
    LOG(INFO) << "FLAGS_pcl_filenames"   << FLAGS_pcl_filenames;

    registrator = make_shared<CartoMapRegistrater>();
    
    MapRegistrateInterface::RegistrateParam param;
    param.configuration_directory = FLAGS_configuration_directory;
    param.registrate_configuration_basename = FLAGS_registrate_configuration_basename;
    registrator->SetParam(param);
    
    if (!boost::filesystem::exists(FLAGS_save_directory)) {
        boost::filesystem::create_directory(FLAGS_save_directory);
    }
    registrator->RegistrateMaps(state_filenames, pcl_filenames, FLAGS_save_directory);

}

}
}
}


int main(int argc, char** argv) {

  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  cartographer_ros::ScopedRosLogSink ros_log_sink;

  yida_mapping::map_registrate::RegistrateMaps(argc, argv);

  LOG(INFO) << "registrate ok!" ;

}
