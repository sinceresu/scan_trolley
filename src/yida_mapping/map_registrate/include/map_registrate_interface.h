#ifndef _MAP_REGISTRATE_INTERFACE_H
#define _MAP_REGISTRATE_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <stdint.h>
#include <vector>

#include "map_common/global.h"

namespace yida_mapping 
{

namespace map_registrate
{



class MapRegistrateInterface
{
public:
      
  typedef struct RegistrateParam {
    std::string configuration_directory;    //配置文件目录
    std::string registrate_configuration_basename;    //建图配置文件名
  }RegistrateParam;


  virtual ~MapRegistrateInterface(){};

    /******************************************************************************
  * \fn MapRegistrateInterface.SetOptionFiles
  * Create Time: 2020/06/02
  * Description: -
  *    设置拼图配置文件
  *
  * \param param 输入参数
  * 		urdf 文件路径
  *
  * \param config_file_name 输入参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const RegistrateParam & param) = 0 ;
  virtual  int RegistrateMaps(const std::vector<std::string>& state_filepaths, const std::vector<std::string>& pointcloud_filepaths, 
        const std::string&  output_pointcloud_filepath) = 0 ;

};
} // namespace map_registrate
}// namespace yida_mapping

#endif  
