#include "include/libmap_registrate.h"
#include "carto_map_registrater.h"

namespace yida_mapping{
namespace map_registrate{

std::shared_ptr<MapRegistrateInterface> CreateMapRegistrater(MapRegistrateType type)
{
    switch (type)
    {
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_registrate();
    case CARTOGRAPHER:
        return std::shared_ptr<MapRegistrateInterface>(new CartoMapRegistrater());
    default:
        return nullptr;
    }
}

} // namespace map_registrate
}// namespace yida_mapping
