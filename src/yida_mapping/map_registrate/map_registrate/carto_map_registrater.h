#ifndef _map_registrate_CARTO_MAP_REGISTRATE_H
#define _map_registrate_CARTO_MAP_REGISTRATE_H

#include <vector>
#include <memory>

#include "include/map_registrate_interface.h"
namespace yida_mapping{

namespace map_registrate{

class CartoMapRegistrater : public MapRegistrateInterface
{

public:
  CartoMapRegistrater();
  ~CartoMapRegistrater(){};

  virtual  int SetParam(const RegistrateParam & param) override ;
  virtual int RegistrateMaps(const std::vector<std::string>& state_filepaths, const std::vector<std::string>& pointcloud_filepaths, 
        const std::string&  output_pointcloud_filepath) override;
        
private:
  int RegistrateMap(const std::string& src_state_filepath, const std::string& src_pcl_filepath, 
           const std::string& tgt_state_filepath, const std::string& tgt_pcl_filepath, 
          const std::string&  output_state_filepath, const std::string&  output_pcl_filepath);
  void LoadOptions( const std::string& configuration_directory,
      const std::string& configuration_basename);
  RegistrateParam parameter_;
  double overlap_radius_;
  int     iterations_;
  double resolution_;
  double max_fitness_score_;
  double euclidean_fitness_epsilon_;

};
} // namespace map_registrate
}// namespace yida_mapping

#endif  