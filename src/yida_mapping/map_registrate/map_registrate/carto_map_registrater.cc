#include "carto_map_registrater.h"
#include <thread>
#include <chrono>

#include "cartographer/common/configuration_file_resolver.h"

#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer/transform/transform.h"
#include "cartographer/io/proto_stream_serializer.h"

#include "tf2_ros/transform_listener.h"
#include "tf2_ros/static_transform_broadcaster.h"
#include "urdf/model.h"

#include "cartographer_ros/node.h"
#include "cartographer_ros/playable_bag.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/assets_writer.h"

#include "cartographer_ros/node_options.h"
#include "cartographer_ros/ros_log_sink.h"
#include "cartographer_ros_msgs/StatusCode.h"
#include "cartographer_ros_msgs/StatusResponse.h"
#include <sensor_msgs/Image.h>

#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h> 
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/ndt.h>
#include <rosbag/recorder.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/extract_indices.h>

#include <Eigen/Core>
#include <Eigen/Geometry>


#include "map_common/err_code.h"
#include "cartographer_ros/dev/output_pbstream_trajectories.h"

#include "map_landmark/libmap_landmark.h"
#include "map_landmark/map_landmark_interface.h"



using namespace std;

namespace yida_mapping{
using namespace cartographer_ros;

namespace map_registrate{
namespace {
namespace carto = ::cartographer;
 
std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
  const std::string& configuration_directory,
  const std::string& configuration_basename) {
auto file_resolver =
    absl::make_unique<carto::common::ConfigurationFileResolver>(
        std::vector<std::string>{configuration_directory});

const std::string code =
    file_resolver->GetFileContentOrDie(configuration_basename);
auto lua_parameter_dictionary =
    absl::make_unique<carto::common::LuaParameterDictionary>(
        code, std::move(file_resolver));
return lua_parameter_dictionary;
}
//convenient typedefs
typedef pcl::PointXYZI PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

////////////////////////////////////////////////////////////////////////////////
/** \brief Align a pair of PointCloud datasets and return the result
  * \param cloud_src the source PointCloud
  * \param cloud_tgt the target PointCloud
  * \param final_transform the resultant transform between source and target
  */
void pairAlignICP (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, 
    Eigen::Matrix4d &final_transform, 
    int iterations, 
  double max_correspondence_distance,
    double euclidean_fitness_epsilon,
   double max_fitness_score
    )
{
  final_transform = Eigen::Matrix4d::Identity();

  pcl::IterativeClosestPoint<PointT, PointT, double> reg;
  reg.setMaxCorrespondenceDistance (max_correspondence_distance);
// Set the maximum number of iterations (criterion 1)
  reg.setMaximumIterations (iterations);
  // Set the transformation epsilon (criterion 2)
  reg.setTransformationEpsilon (1e-6);
// Set the euclidean distance difference epsilon (criterion 3)
  reg.setEuclideanFitnessEpsilon (euclidean_fitness_epsilon);
  // reg.setRANSACIterations(0);
  reg.setInputSource(cloud_src);
  reg.setInputTarget(cloud_tgt);

  PointCloud Final;
  reg.align(Final);
 float fitness_score = reg.getFitnessScore();
  if (reg.hasConverged() && fitness_score < max_fitness_score ) {
    LOG(INFO) << "succeed to registrate, fitness_score: " << fitness_score;
    final_transform = reg.getFinalTransformation ().inverse();
    auto transform_step = std::abs ((reg.getLastIncrementalTransformation ()).sum ()) ;
    PCL_INFO ("transform step  %f.\n",  transform_step);
  } else {
    LOG(WARNING) << "failed to registrate!  fitness_score: " << fitness_score;
  }


}

void pairAlignNDT (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, 
    Eigen::Matrix4d &final_transform, 
    int iterations, 
    double resolution,
    double euclidean_fitness_epsilon,
   double max_fitness_score)
{
  final_transform = Eigen::Matrix4d::Identity();
  pcl::NormalDistributionsTransform<PointT, PointT> reg;
  reg.setResolution (resolution);
// Set the maximum number of iterations (criterion 1)
  reg.setMaximumIterations (iterations);
  // Set the transformation epsilon (criterion 2)
  reg.setTransformationEpsilon (1e-6);
// Set the euclidean distance difference epsilon (criterion 3)
  reg.setStepSize (euclidean_fitness_epsilon);

  reg.setInputSource(cloud_src);
  reg.setInputTarget(cloud_tgt);

  PointCloud Final;
  Eigen::Matrix4f initial_transform = Eigen::Matrix4f::Identity();;

  reg.align(Final, initial_transform);
 float fitness_score = reg.getFitnessScore();

  if (reg.hasConverged() && fitness_score < max_fitness_score ) {
    LOG(INFO) << "succeed to registrate, fitness_score: " << fitness_score;
    final_transform = reg.getFinalTransformation ().inverse().cast<double>();
    auto transform_step = std::abs ((reg.getLastIncrementalTransformation ()).sum ()) ;
    PCL_INFO ("transform step  %f.\n",  transform_step);
  } else {
    LOG(WARNING) << "failed to registrate!  fitness_score: " << fitness_score;
  }

}

std::map<std::string,  Eigen::Isometry3d> generate_landmarks(const ::cartographer::mapping::proto::PoseGraph& pose_graph) 
{
  std::map<std::string,  Eigen::Isometry3d> landmarks;
  for (int i = 0; i < pose_graph.landmark_poses_size(); i++ ) {
    auto landmark_proto = pose_graph.landmark_poses(i);
    auto landmark_pose = landmark_proto.global_pose();
    auto orientation = landmark_pose.rotation();
    Eigen::Quaterniond quaternion = Eigen::Quaterniond(orientation.w(), orientation.x(), orientation.y(), orientation.z()); // w x y z
    auto position = landmark_pose.translation();

    Eigen::Matrix4d tf_matrix = Eigen::Matrix4d::Identity();

    tf_matrix.block<3, 3>(0, 0) = quaternion.toRotationMatrix();
    tf_matrix.block<3, 1>(0, 3) << position.x(), position.y(), position.z();     
    if ( landmarks.count(landmark_proto.landmark_id()) == 0)
      landmarks[landmark_proto.landmark_id()] = Eigen::Isometry3d(tf_matrix);
  }
  return landmarks;
}

void display_landmarks (std::map<std::string,  Eigen::Isometry3d> landmarks) {
    for (const auto &current_landmark : landmarks){
      LOG(INFO) << "landmark :" << current_landmark.first;
      LOG(INFO) << "landmark pose: "  ;
      LOG(INFO) << std::endl<< current_landmark.second.matrix(); 
    }

}

std::pair<Eigen::Isometry3d, Eigen::Isometry3d> get_paired_landmarks( const std::string& src_state_filepath, const std::string& tgt_state_filepath) {
  std::pair<Eigen::Isometry3d, Eigen::Isometry3d> paired_landmark;

  // read landmarks info from pbstream
    ::cartographer::mapping::proto::PoseGraph src_pose_graph(
            ::cartographer::io::DeserializePoseGraphFromFile(src_state_filepath));    
    std::map<std::string,  Eigen::Isometry3d> src_landmarks = generate_landmarks(src_pose_graph);
      LOG(INFO) << "landmarks :" << src_landmarks.size();
    
    display_landmarks(src_landmarks);
      ::cartographer::mapping::proto::PoseGraph tgt_pose_graph(
            ::cartographer::io::DeserializePoseGraphFromFile(tgt_state_filepath));    
    std::map<std::string,  Eigen::Isometry3d> tgt_landmarks = generate_landmarks(tgt_pose_graph);  
      // find the same  landmark  also exists in last point cloud, then transform current point cloud according the poses of the landmark .
    for (const auto &current_landmark : tgt_landmarks){
      if (src_landmarks.count(current_landmark.first) > 0) {
        LOG(INFO) << "matched landmark :" << current_landmark.first;
        LOG(INFO) << "src landmark "  ;
        LOG(INFO) << std::endl << src_landmarks[current_landmark.first].matrix() ;
        LOG(INFO) << "tgt landmark "  ;
        LOG(INFO) << std::endl<< current_landmark.second.matrix(); 
        paired_landmark = std::make_pair(src_landmarks[current_landmark.first],  current_landmark.second);
        // Eigen::Isometry3d relative_pose = src_landmarks[current_landmark.first] * current_landmark.second.inverse();
        // Eigen::Vector3d rpy  = relative_pose.linear().eulerAngles(0, 1, 2);
        // LOG(INFO) << "landmark transform euler :  "  ;
        // LOG(INFO) << std::endl<< rpy;
        // Eigen::Matrix3d yaw;
        // yaw = Eigen::AngleAxisd(0, Eigen::Vector3d::UnitX())
        //   * Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY())
        //   * Eigen::AngleAxisd(rpy[2], Eigen::Vector3d::UnitZ());
        // Eigen::Isometry3d yaw_transform = relative_pose ;
        // yaw_transform.linear() = yaw;
        // LOG(INFO) << "landmark yaw  transform matrix :  "  ;
        // LOG(INFO) << std::endl<< yaw_transform.matrix();    
        // transform_pose  = relative_pose;
        // LOG(INFO) << "landmark transform matrix:  "  ;
        // LOG(INFO) << std::endl<< transform_pose.matrix();
        break;
      }
  }
  return paired_landmark;
}

constexpr static double kDownSamplingLeafSize = 0.02;

Eigen::Isometry3d get_transforms_by_registration(const Eigen::Isometry3d& transform_pose,  PointCloud::Ptr src,  PointCloud::Ptr input,
    int iterations, 
    double overlap_radius,
    double resolution,
    double euclidean_fitness_epsilon,
    double max_fitness_score
    ) {
  Eigen::Isometry3d final_pose;
  
  PointCloud::Ptr tgt = PointCloud::Ptr(new PointCloud() );

  pcl::transformPointCloud (*input, *tgt, transform_pose.cast<float>());

LOG(INFO)   << "save to  landmark_registrated ply  file. " ;
  pcl::io::savePLYFileBinary("landmark_registrated.ply", *tgt);

  Eigen::Matrix4d pairTransform = Eigen::Matrix4d::Identity();
  // pairAlignNDT (src, tgt, pairTransform, iterations, resolution, euclidean_fitness_epsilon, max_fitness_score);
  pairAlignICP (src, tgt, pairTransform, iterations, overlap_radius * 2, euclidean_fitness_epsilon, max_fitness_score);
//update the global transform
  final_pose = Eigen::Isometry3d(pairTransform) * transform_pose;

  return move(final_pose);
}

void transform_pbstream(const std::string& state_filepath,  const Eigen::Isometry3d& final_pose, const std::string&  output_pb_file) {

    ::cartographer::mapping::proto::PoseGraph pose_graph(
            ::cartographer::io::DeserializePoseGraphFromFile(state_filepath));   
    for (  auto & trajectory : *pose_graph.mutable_trajectory()) {
      for (auto & node : *trajectory.mutable_node()) {
        auto node_pose =::cartographer:: transform::ToRigid3(node.pose());
        auto transform = ::cartographer::transform::Rigid3d(final_pose.translation(), Eigen::Quaterniond(final_pose.linear()));
        *node.mutable_pose() = ::cartographer:: transform::ToProto(transform * node_pose);
      }

    }
    for (auto & landmark_proto : *pose_graph.mutable_landmark_poses()) {
      auto landmark_pose = ::cartographer:: transform::ToRigid3(landmark_proto.global_pose());
      auto transform = ::cartographer::transform::Rigid3d(final_pose.translation(), Eigen::Quaterniond(final_pose.linear()));
      *(landmark_proto.mutable_global_pose()) = ::cartographer:: transform::ToProto(transform * landmark_pose);
      LOG(INFO) << "merged landmark :" << landmark_proto.landmark_id();
      LOG(INFO) << "merged landmark "  ;
      LOG(INFO) << std::endl << transform * landmark_pose ;
    }
    ::cartographer::mapping::proto::AllTrajectoryBuilderOptions  all_trajectory_builder_options(
    ::cartographer::io::DeserializeBuilderOptionsFromFile(state_filepath));        

    ::cartographer::io::SerializePoseGraphToFile(output_pb_file, pose_graph, all_trajectory_builder_options);    

}
PointCloud::Ptr generate_nearby_pcl (PointCloud::ConstPtr input_pcl, Eigen::Vector3d position, double radius) {
    pcl::KdTreeFLANN<PointT>::Ptr kdtreeHistoryKeyPoses;
    kdtreeHistoryKeyPoses.reset(new pcl::KdTreeFLANN<PointT>());

    kdtreeHistoryKeyPoses->setInputCloud(input_pcl);
    std::vector<int> pointSearchIndLoop;
    std::vector<float> pointSearchSqDisLoop;
    PointT point;
    point.x = position[0];
    point.y = position[1];
    point.z = position[2];

    kdtreeHistoryKeyPoses->radiusSearch(point, radius, pointSearchIndLoop, pointSearchSqDisLoop, 0);

  PointCloud::Ptr cloud_filtered (new PointCloud);

  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  inliers->indices = move(pointSearchIndLoop);
  pcl::ExtractIndices<PointT> extract;
  extract.setInputCloud (input_pcl);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*cloud_filtered);

  return cloud_filtered;
}

void merge_pbstreams(const std::vector<std::string>& state_filepaths,  const std::vector<Eigen::Isometry3d>& final_poses, const std::string&  output_pb_file) {
   ::cartographer::mapping::proto::PoseGraph base_pose_graph(
          ::cartographer::io::DeserializePoseGraphFromFile(state_filepaths[0]));
        
  ::cartographer::mapping::proto::AllTrajectoryBuilderOptions  base_all_trajectory_builder_options(
          ::cartographer::io::DeserializeBuilderOptionsFromFile(state_filepaths[0]));             
  for (size_t trajectory_id = 1; trajectory_id < state_filepaths.size(); ++trajectory_id) {
      LOG(INFO) << "merging pbstream " << state_filepaths[trajectory_id];
 
      ::cartographer::mapping::proto::PoseGraph pose_graph(
              ::cartographer::io::DeserializePoseGraphFromFile(state_filepaths[trajectory_id]));   
      auto & trajectory = *pose_graph.mutable_trajectory(0);
      for (auto & node : *trajectory.mutable_node()) {
        auto node_pose =::cartographer:: transform::ToRigid3(node.pose());
        auto transform = ::cartographer::transform::Rigid3d(final_poses[trajectory_id].translation(), Eigen::Quaterniond(final_poses[trajectory_id].linear()));
        *node.mutable_pose() = ::cartographer:: transform::ToProto(transform * node_pose);
      }
      base_pose_graph.mutable_trajectory()->MergeFrom(pose_graph.trajectory());

      for (auto & landmark_proto : *pose_graph.mutable_landmark_poses()) {
        auto landmark_pose = ::cartographer:: transform::ToRigid3(landmark_proto.global_pose());
        auto transform = ::cartographer::transform::Rigid3d(final_poses[trajectory_id].translation(), Eigen::Quaterniond(final_poses[trajectory_id].linear()));
        *(landmark_proto.mutable_global_pose()) = ::cartographer:: transform::ToProto(transform * landmark_pose);
      }
      base_pose_graph.mutable_landmark_poses()->MergeFrom(pose_graph.landmark_poses());

      ::cartographer::mapping::proto::AllTrajectoryBuilderOptions  all_trajectory_builder_options(
        ::cartographer::io::DeserializeBuilderOptionsFromFile(state_filepaths[trajectory_id]));           
      
    base_all_trajectory_builder_options.MergeFrom(all_trajectory_builder_options);
  }
  LOG(INFO) << "serialize to pbstream.." << output_pb_file;

   ::cartographer::io::SerializePoseGraphToFile(output_pb_file, base_pose_graph, base_all_trajectory_builder_options);    
}

}

CartoMapRegistrater::CartoMapRegistrater(){
}

int CartoMapRegistrater::SetParam(const RegistrateParam & param) {
  parameter_ = param;
  return ERRCODE_OK;

}

void CartoMapRegistrater::LoadOptions( const std::string& configuration_directory,
    const std::string& configuration_basename){

  const auto lua_parameter_dictionary =
      LoadLuaDictionary(configuration_directory, configuration_basename);

 const auto registrate_options = lua_parameter_dictionary->GetDictionary("registrate");
  overlap_radius_ = registrate_options->GetDouble("overlap_radius");
  iterations_ = registrate_options->GetInt("iterations");
  resolution_ = registrate_options->GetDouble("resolution");
  euclidean_fitness_epsilon_ = registrate_options->GetDouble("euclidean_fitness_epsilon");
  max_fitness_score_ = registrate_options->GetDouble("max_fitness_score");
}

int CartoMapRegistrater::RegistrateMap(const std::string& src_state_filepath, const std::string& src_pcl_filepath, 
           const std::string& tgt_state_filepath, const std::string& tgt_pcl_filepath, 
          const std::string&  output_state_filepath, const std::string&  output_pcl_filepath){

 auto paired_landmarks = get_paired_landmarks(src_state_filepath, tgt_state_filepath);
  LOG(INFO) << "landmark pose ";

  PointCloud::Ptr src = PointCloud::Ptr(new PointCloud() );
  pcl::io::loadPLYFile(src_pcl_filepath, *src);    
  LOG(INFO)   << "registrate original point cloud file  " << src_pcl_filepath;

  PointCloud::Ptr tgt = PointCloud::Ptr(new PointCloud() );
  pcl::io::loadPLYFile(tgt_pcl_filepath, *tgt);
  LOG(INFO)
      << "registrate ref point cloud file  " << tgt_pcl_filepath;
    
  PointCloud::Ptr src_nearby = generate_nearby_pcl(src, paired_landmarks.first.translation(),  overlap_radius_ );
  PointCloud::Ptr tgt_nearby  = generate_nearby_pcl(tgt, paired_landmarks.second.translation(),  overlap_radius_ );
  // pcl::io::savePLYFileBinary("src_nearby.ply", *src_nearby);
  // pcl::io::savePLYFileBinary("tgt_nearby.ply", *tgt_nearby);


  Eigen::Isometry3d transform_pose = paired_landmarks.first * paired_landmarks.second.inverse();
  LOG(INFO) << std::endl<< transform_pose.matrix();

  Eigen::Isometry3d registrated_pose = get_transforms_by_registration(transform_pose, src_nearby, tgt_nearby, 
            iterations_, overlap_radius_, resolution_, euclidean_fitness_epsilon_, max_fitness_score_);

  LOG(INFO) << "registrated pose "  ;
  LOG(INFO) << std::endl<< registrated_pose.matrix();
  PointCloud::Ptr input = PointCloud::Ptr(new PointCloud() );

  pcl::io::loadPLYFile(tgt_pcl_filepath, *input);
  PointCloud::Ptr transformed_pointcloud = PointCloud::Ptr(new PointCloud() );

  pcl::transformPointCloud (*input, *transformed_pointcloud, registrated_pose.cast<float>());

  transform_pbstream(tgt_state_filepath, registrated_pose, output_state_filepath);
  pcl::io::savePLYFileBinary(output_pcl_filepath, *transformed_pointcloud);
            
  return ERRCODE_OK;

}

int CartoMapRegistrater::RegistrateMaps(const std::vector<std::string>& state_filepaths, const std::vector<std::string>& pointcloud_filepaths, 
        const std::string&  save_directory){

  if (state_filepaths.empty() || pointcloud_filepaths.empty()) {
    return ERRCODE_INVALIDARG;
  }
  // if (state_filepaths.size() < 2) {
  //   LOG(WARNING) << "Number of state files should be larger than 2.";
  //   return ERRCODE_INVALIDARG;
  // }

  CHECK(state_filepaths.size() == pointcloud_filepaths.size());

  LoadOptions(parameter_.configuration_directory, parameter_.registrate_configuration_basename);
  LOG(INFO) << "get_paired_landmarks " ;

  std::string src_state_filepath = state_filepaths[0];
  std::string src_pcl_filepath = pointcloud_filepaths[0];

  for (size_t trajectory_id = 1; trajectory_id < state_filepaths.size(); trajectory_id++) {
    LOG(INFO) << "registrating file  " << pointcloud_filepaths[trajectory_id] ;

    std::string tgt_state_filepath = state_filepaths[trajectory_id];
    std::string tgt_pcl_filepath = pointcloud_filepaths[trajectory_id];  

    boost::filesystem::path p(pointcloud_filepaths[trajectory_id]) ;
    boost::filesystem::path file_stem = p.stem();
    std::string output_prefix =  (boost::filesystem::path(save_directory) /  file_stem ).string();
    std::string output_state_filepath = output_prefix + ".pbstream";
    std::string output_pcl_filepath = output_prefix + ".ply";
    RegistrateMap(src_state_filepath, src_pcl_filepath, tgt_state_filepath, tgt_pcl_filepath, output_state_filepath, output_pcl_filepath);

    src_state_filepath = output_state_filepath;
    src_pcl_filepath = output_pcl_filepath;
  }

  LOG(INFO) << "registration ok.";
  return ERRCODE_OK;

}

}
}