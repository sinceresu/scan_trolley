#!/usr/bin/env python
import sys
import roslib;
import rospy
import rosbag
from rospy import rostime
import argparse
import os
from shutil import copyfile


def parse_args():
    parser = argparse.ArgumentParser(
        prog = 'sortbag.py',
        description='sort bagfiles.')
    parser.add_argument('-o', type=str, help='name of the output file', 
        default = None, metavar = "output_file")
    parser.add_argument('-t', type=str, help='topics which should be merged to the main bag', 
        default = None, metavar = "topics")
    parser.add_argument('-f', type=str, help='file with rosbag txt', 
        default = None, metavar = "topics")
    parser.add_argument('-i', help='reindex bagfile', 
        default = False, action="store_true")
    args = parser.parse_args()
    return args


offset_sec = rospy.Time.from_seconds(10)
def get_next(bag_iter,
        main_start_time = None, start_time = None, 
        topics = None):
    try:
        result = bag_iter.next()
        if topics != None:
            while not result[0] in topics:
                result = bag_iter.next()
        result[1].header.stamp = result[1].header.stamp - start_time + main_start_time
        return (result[0], result[1], 
            result[2] - start_time + main_start_time)
    except StopIteration:
        return None
def sort_bag(main_bagfile, bagfile, output_file, topics = None, 
        reindex = True):
    #get min and max time in bagfile
    main_limits = get_limits(main_bagfile)
    limits = get_limits(bagfile)
    #check output file
    main_limits[1] +=offset_sec
    # main_header_limits[1].secs +=offset_sec
    #output some information
    print "sort bag %s in %s"%(bagfile, main_bagfile)
    print "topics filter: ", topics
    print "writing to %s"%output_file
    #merge bagfile
    bag = rosbag.Bag(bagfile).__iter__()
    outbag = rosbag.Bag(output_file, 'w')
    next = get_next(bag, main_limits[1], limits[0], topics)
    try:
        while next != None:
                outbag.write(next[0], next[1], next[2])
                next = get_next(bag, main_limits[1], limits[0], topics)

    finally:
        print "finished"
        outbag.close()

def get_limits(bagfile):
    print "Determine start and end index of %s..."%bagfile
    bag = rosbag.Bag(bagfile)
    start_time =   rospy.Time.from_seconds(bag.get_start_time())
    end_time =   rospy.Time.from_seconds(bag.get_end_time())
    return (start_time, end_time)
 


if __name__ == "__main__":
    args = parse_args()
    print args
    if args.t != None:
        args.t = args.t.split(',')
    all_bag = []
    
    txt_file = args.f.strip()
    with open(txt_file) as tmp:
      for line in tmp:
        word = line.split("\n")
        print "reading...", word[0]
        all_bag.append(word[0])
    if len(all_bag) <= 2:
      sorted_bag = os.path.join(args.o, os.path.basename(all_bag[1]))
      print "num of rosbag: ", len(all_bag)
      sort_bag(all_bag[0], all_bag[1], sorted_bag, topics = args.t)
    else:
      print "num of rosbag: ", len(all_bag)
      sorted_bag = os.path.join(args.o, os.path.basename(all_bag[1]))
      sort_bag(all_bag[0], all_bag[1], sorted_bag, topics = args.t,)
      for i in range(2,len(all_bag)):
        print i
        source = sorted_bag
        sorted_bag = os.path.join(args.o, os.path.basename(all_bag[i]))

        sort_bag(source, all_bag[i], sorted_bag, topics = args.t)

    print "fininshed sorting file.!"
        
