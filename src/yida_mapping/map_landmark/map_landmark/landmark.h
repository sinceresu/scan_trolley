#ifndef _MAP_LANDMARK_LANDMARK_H
#define _MAP_LANDMARK_LANDMARK_H

#include <vector>
#include <string>
#include <memory>
#include <stdint.h>

#include <Eigen/Core>
#include <Eigen/Geometry>



namespace yida_mapping{
namespace map_landmark{

  typedef struct LandMarkEntry{
    int tag_id;   
    Eigen::Isometry3d pose;    //
  }LandMarkEntry;

  typedef std::vector<LandMarkEntry>    LandMarkList;

} // namespace map_landmark
}// namespace yida_mapping

#endif  
