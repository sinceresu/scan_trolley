#include "tag_detector.h"

#include "tagStandard52h13.h"
#include "tagStandard41h12.h"
#include "tag36h11.h"
#include "tag25h9.h"
#include "tag16h5.h"
#include "tagCustom48h12.h"
#include "tagCircle21h7.h"
#include "tagCircle49h12.h"
#include "common/homography.h"

#include "apriltag_pose.h"


using namespace std;

namespace yida_mapping{

namespace map_landmark{

TagDetector::TagDetector(std::string tag_family, double tag_size) :
    family_(tag_family),
    tag_size_(tag_size),
    threads_(4),
    decimate_(1.0),
    blur_(0.0),
    refine_edges_(1),
    debug_(0)
{
    remove_duplicates_ = true;
  // Define the tag family whose tags should be searched for in the camera
  // images
  if (family_ == "tagStandard52h13")
  {
    tf_ = tagStandard52h13_create();
  }
  else if (family_ == "tagStandard41h12")
  {
    tf_ = tagStandard41h12_create();
  }
  else if (family_ == "tag36h11")
  {
    tf_ = tag36h11_create();
  }
  else if (family_ == "tag25h9")
  {
    tf_ = tag25h9_create();
  }
  else if (family_ == "tag16h5")
  {
    tf_ = tag16h5_create();
  }
  else if (family_ == "tagCustom48h12")
  {
    tf_ = tagCustom48h12_create();
  }
  else if (family_ == "tagCircle21h7")
  {
    tf_ = tagCircle21h7_create();
  }
  else if (family_ == "tagCircle49h12")
  {
    tf_ = tagCircle49h12_create();
  }
  else
  {
    ROS_WARN("Invalid tag family specified! Aborting");
    exit(1);
  }

  // Create the AprilTag 2 detector
  td_ = apriltag_detector_create();
  apriltag_detector_add_family(td_, tf_);
  td_->quad_decimate = (float)decimate_;
  td_->quad_sigma = (float)blur_;
  td_->nthreads = threads_;
  td_->debug = debug_;
  td_->refine_edges = refine_edges_;
  

  detections_ = NULL;

}

// destructor
TagDetector::~TagDetector() {
  // free memory associated with tag detector
  apriltag_detector_destroy(td_);

  // Free memory associated with the array of tag detections
  if (detections_)
    apriltag_detections_destroy(detections_);

  // free memory associated with tag family
  if (family_ == "tagStandard52h13")
  {
    tagStandard52h13_destroy(tf_);
  }
  else if (family_ == "tagStandard41h12")
  {
    tagStandard41h12_destroy(tf_);
  }
  else if (family_ == "tag36h11")
  {
    tag36h11_destroy(tf_);
  }
  else if (family_ == "tag25h9")
  {
    tag25h9_destroy(tf_);
  }
  else if (family_ == "tag16h5")
  {
    tag16h5_destroy(tf_);
  }
  else if (family_ == "tagCustom48h12")
  {
    tagCustom48h12_destroy(tf_);
  }
  else if (family_ == "tagCircle21h7")
  {
    tagCircle21h7_destroy(tf_);
  }
  else if (family_ == "tagCircle49h12")
  {
    tagCircle49h12_destroy(tf_);
  }
}

LandMarkList TagDetector::DetectTags (
    const cv::Mat& image,
    double fx,
    double fy,
    double cx,
    double cy) {
  // Convert image to AprilTag code's format
  cv::Mat gray_image;
  if (image.channels() == 1)
  {
    gray_image = image;
  }
  else
  {
    cv::cvtColor(image, gray_image, cv::COLOR_BGR2GRAY);
  }
  image_u8_t apriltag_image = { .width = gray_image.cols,
                                  .height = gray_image.rows,
                                  .stride = gray_image.cols,
                                  .buf = gray_image.data
  };


  // Run AprilTag 2 algorithm on the image
  if (detections_)
  {
    apriltag_detections_destroy(detections_);
    detections_ = NULL;
  }
  detections_ = apriltag_detector_detect(td_, &apriltag_image);

  // If remove_dulpicates_ is set to true, then duplicate tags are not allowed.
  // Thus any duplicate tag IDs visible in the scene must include at least 1
  // erroneous detection. Remove any tags with duplicate IDs to ensure removal
  // of these erroneous detections
  if (remove_duplicates_)
  {
    removeDuplicates();
  }

  return getPoses(detections_, fx, fy, cx, cy);
}

int TagDetector::idComparison (const void* first, const void* second)
{
  int id1 = ((apriltag_detection_t*) first)->id;
  int id2 = ((apriltag_detection_t*) second)->id;
  return (id1 < id2) ? -1 : ((id1 == id2) ? 0 : 1);
}

void TagDetector::removeDuplicates ()
{
  zarray_sort(detections_, &idComparison);
  int count = 0;
  bool duplicate_detected = false;
  while (true)
  {
    if (count > zarray_size(detections_)-1)
    {
      // The entire detection set was parsed
      return;
    }
    apriltag_detection_t *detection;
    zarray_get(detections_, count, &detection);
    int id_current = detection->id;
    // Default id_next value of -1 ensures that if the last detection
    // is a duplicated tag ID, it will get removed
    int id_next = -1;
    if (count < zarray_size(detections_)-1)
    {
      zarray_get(detections_, count+1, &detection);
      id_next = detection->id;
    }
    if (id_current == id_next || (id_current != id_next && duplicate_detected))
    {
      duplicate_detected = true;
      // Remove the current tag detection from detections array
      int shuffle = 0;
      zarray_remove_index(detections_, count, shuffle);
      if (id_current != id_next)
      {
        ROS_WARN_STREAM("Pruning tag ID " << id_current << " because it "
                        "appears more than once in the image.");
        duplicate_detected = false; // Reset
      }
      continue;
    }
    else
    {
      count++;
    }
  }
}

LandMarkList TagDetector::getPoses(zarray_t *detections,
    double fx,
    double fy,
    double cx,
    double cy) {
  apriltag_detection_info_t info;
  info.tagsize = tag_size_; //打印的qrcode尺寸大小(m)
  info.fx = fx;
  info.fy = fy;
  info.cx = cx;
  info.cy = cy;
  LandMarkList tag_detection_array;
  for (int i = 0; i < zarray_size(detections); i++)
    {
      apriltag_detection_t *det;
      zarray_get(detections, i, &det);
      info.det = det; //将det赋给info用于计算
      apriltag_pose_t pose;
      double err = estimate_tag_pose(&info, &pose);
      const auto &R = pose.R->data;
      const auto &t = pose.t->data;

      Eigen::Matrix3d wRo;
      wRo << R[0], R[1], R[2], R[3], R[4], R[5], R[6], R[7], R[8];

      Eigen::Vector3d wT;
      wT << t[0], t[1], t[2];
      
      Eigen::Isometry3d tag_pose =  makeTagPose(wRo, wT);

      // Add the detection to the back of the tag detection array
      LandMarkEntry tag_detection;
      tag_detection.tag_id = det->id;
      tag_detection.pose = tag_pose;
      tag_detection_array.push_back(tag_detection);
    }

    return tag_detection_array;
}

Eigen::Isometry3d TagDetector::makeTagPose(
     const Eigen::Matrix3d & rotation, const Eigen::Vector3d & translation)
{

  Eigen::Isometry3d pose;
  pose.translation() = translation;
  pose.linear() = rotation;

  return pose;
}


} // namespace map_landmark
}// namespace yida_mapping
