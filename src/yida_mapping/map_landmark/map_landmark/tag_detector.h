#ifndef _MAP_LANDMARK_TAG_DETECTOR_H
#define _MAP_LANDMARK_TAG_DETECTOR_H

#include <vector>
#include <memory>
#include <thread>
#include "ros/spinner.h"
#include <eigen3/Eigen/Geometry>
#include <opencv2/opencv.hpp>

#include <apriltag.h>

#include "landmark.h"

namespace yida_mapping{

namespace map_landmark{

class TagDetector
{
 private:
  // Detections sorting
  static int idComparison(const void* first, const void* second);

  // Remove detections of tags with the same ID
  void removeDuplicates();

  // AprilTag 2 code's attributes
  std::string family_;
  double tag_size_;
  int threads_;
  double decimate_;
  double blur_;
  int refine_edges_;
  int debug_;

  // AprilTag 2 objects
  apriltag_family_t *tf_;
  apriltag_detector_t *td_;
  zarray_t *detections_;

  // Other members
  bool remove_duplicates_;

 public:

  TagDetector(std::string tag_family, double tag_size);
  ~TagDetector();

  void SetParams();

  // Detect tags in an image
  LandMarkList DetectTags(
    const cv::Mat& image,
    double fx,
    double fy,
    double cx,
    double cy);

private:


  LandMarkList getPoses(zarray_t *detections,
    double fx,
    double fy,
    double cx,
    double cy);

  Eigen::Isometry3d makeTagPose(
      const Eigen::Matrix3d & rotation, const Eigen::Vector3d & translation);
};

} // namespace map_landmark
}// namespace yida_mapping

#endif  
