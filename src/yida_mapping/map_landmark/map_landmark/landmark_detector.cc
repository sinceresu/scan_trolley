#include "landmark_detector.h"

#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>
#include "glog/logging.h"

#include "map_common/err_code.h"
#include "tag_detector.h"

using namespace std;

namespace yida_mapping {
    namespace map_landmark {
        namespace {
            bool compare_by_instance (const LandMarkEntry &lh, const LandMarkEntry &rh) {
                return lh.pose.translation().norm() < rh.pose.translation().norm();
            }
        }

        constexpr double kUndistortionBalance = 0.8;

        LandmarkDetector::LandmarkDetector() { }

        int LandmarkDetector::SetParam(const LandMarkDetectParam & param) {
            if(param.configuration_directory.empty()) return ERRCODE_INVALIDARG;
            if(param.calibration_basename.empty())    return ERRCODE_INVALIDARG;

            parameter_ = param;
            int result = ERRCODE_OK;

            result = LoadCalibrationFile(param.configuration_directory + "/" + param.calibration_basename);

            if(ERRCODE_OK !=result) return result;

            cv::fisheye::initUndistortRectifyMap(intrinsic_mat_, distortion_coeffs_, cv::Mat(),
                                                 intrinsic_mat_, image_size_, CV_32FC1, map_x_, map_y_);

            //CalculateUndistortionParam();

            tag_detector_ = std::make_shared<TagDetector>(param.tag_family, param.tag_size);

            return ERRCODE_OK;
        }

        int LandmarkDetector::Detect(const cv::Mat& image, LandMarkEntry & detected_landmark) {
            CHECK(!intrinsic_mat_.empty() && !distortion_coeffs_.empty()) << "set parameters first!";

            //cv::fisheye::undistortImage(image, undistorted_img, intrinsic_mat_, distortion_coeffs_, new_camera_instrinsics_);
            cv::Mat undistorted_img;
            cv::remap(image, undistorted_img, map_x_, map_y_, cv::INTER_LINEAR);
            //cv::fisheye::undistortImage(image, undistorted_img, intrinsic_mat_, distortion_coeffs_, intrinsic_mat_);
            //cv::imwrite("undistorted.png", undistorted_img);
            //undistorted_img = cv::imread("undistorted.png" );

            fx_ = intrinsic_mat_.at<float>(0, 0);
            fy_ = intrinsic_mat_.at<float>(1, 1);
            cx_ = intrinsic_mat_.at<float>(0, 2);
            cy_ = intrinsic_mat_.at<float>(1, 2);

            LandMarkList detected_landmarks = tag_detector_->DetectTags(undistorted_img, fx_, fy_, cx_, cy_);
            if(detected_landmarks.empty()) return ERRCODE_FAILED;

            // only retain nearest landmark
            std::sort(detected_landmarks.begin(), detected_landmarks.end(), compare_by_instance);
            detected_landmark = detected_landmarks[0];
            const auto& position = detected_landmark.pose.translation();

            if(position.norm() > parameter_.max_distance) return ERRCODE_FAILED;

            auto fov = atan2f(sqrt(position[0] *  position[0]  + position[1]*  position[1]), position[2]);
            if(fov < 0 || fov * 180 / M_PI >  parameter_.max_fov) return ERRCODE_FAILED;

            detected_landmark.pose = GetRelativePose(detected_landmark.pose);

            return ERRCODE_OK;
        }

        int LandmarkDetector::LoadCalibrationFile(const std::string& calibration_filepath) {
            cv::FileStorage fs;

            fs.open(calibration_filepath, cv::FileStorage::READ);
            if(!fs.isOpened()) {
                LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
                return ERRCODE_FAILED;
            }

            fs["CameraMat"] >> intrinsic_mat_;
            fs["DistCoeff"] >> distortion_coeffs_;
            fs["ImageSize"] >> image_size_;

            cv::Mat cam_extrinsic_mat;

            fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
            fs.release();

            Eigen::Matrix4d cam_to_ref_pose;

            cv::cv2eigen(cam_extrinsic_mat, cam_to_ref_pose);
            cam_to_ref_transform_ =  Eigen::Isometry3d (cam_to_ref_pose);

            return ERRCODE_OK;
        }

        void LandmarkDetector::CalculateUndistortionParam() {
            cv::Mat R = cv::Mat::eye(3, 3, CV_64F);

            new_camera_instrinsics_ = cv::Mat::zeros(3, 3, CV_64F);
            cv::Size  new_img_size  = cv::Size (image_size_.width, image_size_.height);
            cv::fisheye::estimateNewCameraMatrixForUndistortRectify(intrinsic_mat_, distortion_coeffs_, image_size_, R, new_camera_instrinsics_, kUndistortionBalance, new_img_size);
            //fx_ = new_camera_instrinsics_.at<double>(0, 0);
            //fy_ = new_camera_instrinsics_.at<double>(1, 1);
            //cx_ = new_camera_instrinsics_.at<double>(0, 2);
            //cy_ = new_camera_instrinsics_.at<double>(1, 2);
        }

        Eigen::Isometry3d LandmarkDetector::GetRelativePose(const Eigen::Isometry3d& landmark_pose) const {
            return cam_to_ref_transform_ * landmark_pose;
        }
    }
}
