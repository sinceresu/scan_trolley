#ifndef _MAP_LANDMARK_LANDMARK_DETECTOR_H
#define _MAP_LANDMARK_LANDMARK_DETECTOR_H

#include <vector>
#include <memory>
#include <thread>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "opencv2/opencv.hpp"

#include "map_common/global.h"
#include "landmark.h"


namespace yida_mapping{

namespace map_landmark{
  class TagDetector;

  typedef struct LandMarkDetectParam {
    LandMarkDetectParam() : 
    tag_family("tag36h11"),
    max_distance(4.0),
    max_fov(90)
    {

    }
    std::string configuration_directory;    //配置文件目录
    std::string calibration_basename;    //相机内外参文件
    std::string tag_family;    
    double tag_size;    
    double max_distance;    
    double max_fov;    


  }LandMarkDetectParam;

class LandmarkDetector 
{
public:
  LandmarkDetector();
  ~LandmarkDetector(){};

  virtual  int SetParam(const LandMarkDetectParam & param);
  virtual  int Detect(const cv::Mat& image, LandMarkEntry & detected_landmark);
private:
  int LoadCalibrationFile(const std::string& calibration_filepath);
  void CalculateUndistortionParam();
  // expressed in the camera frame.
  Eigen::Isometry3d GetRelativePose(const Eigen::Isometry3d& pose) const;
  std::shared_ptr<TagDetector> tag_detector_;
LandMarkDetectParam parameter_;
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  cv::Mat new_camera_instrinsics_;
  double fx_, fy_, cx_, cy_;
  Eigen::Isometry3d cam_to_ref_transform_;
  cv::Mat map_x_, map_y_;


};
} // namespace map_landmark
}// namespace yida_mapping

#endif  
