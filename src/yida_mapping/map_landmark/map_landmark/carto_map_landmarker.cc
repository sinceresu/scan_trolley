#include "carto_map_landmarker.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>
#include <functional>


#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>
#include "glog/logging.h"

#include "map_common/err_code.h"
#include "tag_detector.h"
#include "landmark_detector.h"




using namespace std;

namespace yida_mapping{
  
namespace map_landmark{
namespace {
}

// class LandmarkState : public enable_shared_from_this<LandmarkState>
// {

// public:
// enum class Event {
//     INPUT_IMU = 0, 
//     INPUT_IMAGE, 
// };
// using TansitionCondition = std::function<bool(Event event,  std::string senor_id, void* data)>;

// typedef struct StateTransition {
//       Event event;
//       TansitionCondition condition;
//       std::string nextstate_;
//   }StateTransition;

//   LandmarkState(CartoMapLandmarker* state_context): context_(state_context) 
//   {

//   }

//   std::shared_ptr<LandmarkState> HandleEvent(Event inp,  std::string senor_id, void* data){
//       for (const StateTransition & transition : GetTransitions() ) {
//           if (transition.event == inp && (transition.condition == nullptr ||  transition.condition(inp, senor_id, data))) {
//               OnExit(senor_id, data);
//               if (!transition.nextstate_.empty()) {
//                   auto newstate = CreateInstance(transition.nextstate_, context_);
//                   newstate->OnEntry(senor_id, data);

//                   return newstate;
//               }

//           }
//       }

//       return shared_from_this();
//     }


//     static std::shared_ptr<LandmarkState> CreateInstance(std::string pad_cls, CartoMapLandmarker* state_context) ;
//     virtual const std::vector<LandmarkState::StateTransition>& GetTransitions() = 0;
//     virtual void OnEntry(std::string senor_id, void* data) {};
//     virtual void OnExit(std::string senor_id, void* data) {};

// protected:
//     CartoMapLandmarker * context_;
//     ros::Time start_timestamp_;
//     std::string name_;

        
//     bool IsStill()  {
//         return context_->IsStill();
//     }

// };

// class Idle: public LandmarkState
// {
// public:
//     Idle(CartoMapLandmarker* state_context): LandmarkState(state_context) {
//         name_ = "Idle";
//     };
 
//     static std::vector<StateTransition> transitions; 


//     virtual const std::vector<LandmarkState::StateTransition>& GetTransitions() override {
//         return Idle::transitions;
//     }


// };

// class DectectingLandmark: public LandmarkState
// {
// public:
//     DectectingLandmark(CartoMapLandmarker* state_context): LandmarkState(state_context) {
//         name_ = "DectectingLandmark";
//     };
//     virtual void OnEntry(std::string senor_id, void* data) {

//     }
//     static std::vector<StateTransition> transitions; 

    
//     virtual const std::vector<LandmarkState::StateTransition>& GetTransitions() override {
//         return DectectingLandmark::transitions;
//     }

// };


// std::vector<LandmarkState::StateTransition> Idle::transitions = {{Event::INPUT_IMU, nullptr, "Idle"}};
// std::vector<LandmarkState::StateTransition> DectectingLandmark::transitions = {{Event::INPUT_IMU, nullptr, "Idle"}};

// std::shared_ptr<LandmarkState> LandmarkState:: CreateInstance(std::string pad_cls, CartoMapLandmarker* state_context)  {

//     if (pad_cls == "Idle")
//         return std::make_shared<Idle>(state_context);
//     if (pad_cls == "DectectingLandmark")
//         return std::make_shared<DectectingLandmark>(state_context);

//     return std::make_shared<Idle>(state_context);
                
// }


CartoMapLandmarker::CartoMapLandmarker()
 //: state_(LandmarkState::IDLE)
{

} 

int CartoMapLandmarker::SetParam(const LandmarkParam & param) {
    if (param.configuration_directory.empty()) 
        return ERRCODE_INVALIDARG;
    if (param.calibration_basename.empty()) 
        return ERRCODE_INVALIDARG;       
    parameter_ =    param;                                                                                                         
    int result  = ERRCODE_OK;
    
    detector_ = std::make_shared<LandmarkDetector>();
    LandMarkDetectParam detect_param ;
    detect_param.configuration_directory = param.configuration_directory;
    detect_param.calibration_basename = param.calibration_basename;
    detect_param.tag_family = param.tag_family;
    detect_param.tag_size = param.tag_size;
    detect_param.max_distance = param.max_distance;
    detect_param.max_fov = param.max_fov;
    detector_->SetParam(detect_param);


    return result;
}

// int CartoMapLandmarker::HandleImuMessage(const sensor_msgs::Imu::ConstPtr msg) {
//     if (!parameter_.stop_detection) {
//         return ERRCODE_OK;
//     }
//     UpdateImuQueue(msg);
//     switch (state_) 
//     {
//         case LandmarkState::IDLE : {
//             if (IsStill()) {
//                 LOG(INFO) << "still, begin landmarking.";
//                 state_ = LandmarkState::DETECTING;
//                 // landmarks_.clear();
//                 start_time_ms_ = FromRos(msg->header.stamp);
//             }
//             break;
//         }

//         case LandmarkState::DETECTED : 
//         case LandmarkState::DETECTING : 
//         {
//             auto current_time = FromRos(msg->header.stamp);

//             if (current_time - start_time_ms_ > kMaxDetectingDurationMs ||  IsMoving()) {
//                 LOG(INFO) << "timeout or moving, finish landmark detection";
//                 imu_queue.clear();
//                 queue_filled_  = false;
//                 state_ =  LandmarkState::IDLE;
//             }
            
//             break;
//         }
//         default : {
//             LOG(FATAL) << "wrong LandmarkState";
//             break;
//         }


//     }
    
//     return ERRCODE_OK;

// }

 int CartoMapLandmarker::HandleImage(const cv::Mat& image, bool& landmark_ready, cartographer_ros_msgs::LandmarkList & landmarks) {
    landmark_ready = false;
    LandMarkEntry detected_landmark;
    if (ERRCODE_OK  == detector_->Detect(image, detected_landmark) ) {
        LOG(INFO) << "detected landmark " <<  detected_landmark.tag_id << "!";

        // cv::imwrite(frame_id + ".png", rotated_image);

        cartographer_ros_msgs::LandmarkEntry landmark_entry;
        std::stringstream tag_id;
        tag_id << detected_landmark.tag_id;
        landmark_entry.id = tag_id.str();
        decltype(landmark_entry.tracking_from_landmark_transform.position) position;
        const Eigen::Vector3d& tag_translation = detected_landmark.pose.translation();
        position.x = tag_translation[0];
        position.y = tag_translation[1];
        position.z = tag_translation[2];

        decltype(landmark_entry.tracking_from_landmark_transform.orientation) orientation;
        const Eigen::Quaterniond tag_orientation(detected_landmark.pose.linear());
        orientation.x = tag_orientation.x();
        orientation.y = tag_orientation.y();
        orientation.z = tag_orientation.z();
        orientation.w = tag_orientation.w();

        landmark_entry.tracking_from_landmark_transform.position = position;
        landmark_entry.tracking_from_landmark_transform.orientation = orientation;

        landmark_entry.translation_weight = parameter_.translation_weight;
        landmark_entry.rotation_weight = parameter_.rotation_weight;

        landmarks.landmarks.push_back(landmark_entry);

        landmark_ready = true;
        // state_ = LandmarkState::DETECTED;

    }
    return ERRCODE_OK;

}

//  int CartoMapLandmarker::Reset() {
//     //  state_ = LandmarkState::IDLE;
//      imu_queue.clear();
//     return ERRCODE_OK;
//  }

// void  CartoMapLandmarker::UpdateImuQueue(const sensor_msgs::Imu::ConstPtr imu_data) {
//     auto current_time = FromRos(imu_data->header.stamp);
//     auto raw_linear_acceleration = imu_data->linear_acceleration;
//     Eigen::Vector3d  linear_accelerations = Eigen::Vector3d (raw_linear_acceleration.x, raw_linear_acceleration.y, raw_linear_acceleration.z);
//     auto raw_angular_velocity = imu_data->angular_velocity;
//     Eigen::Vector3d  angular_velocity = Eigen::Vector3d (raw_angular_velocity.x, raw_angular_velocity.y, raw_angular_velocity.z);
//     ImuData imu  = {current_time, linear_accelerations, angular_velocity};
//     imu_queue.push_back(imu);

//     uint64_t first_time = imu_queue.front().time_ms;
//     if (current_time - first_time > kSearchDurationMs + 100) {
//         imu_queue.pop_front();
//         queue_filled_= true;
//     }
// }
// double CartoMapLandmarker::GetMaxLinearAccelerationDiff() {
//     double max_linear_acceleration_diff = 0.;
//     for (size_t i = 0; i < imu_queue.size() - 1; i ++) {
//         Eigen::Vector3d diff  =  imu_queue[i].linear_acceleration - imu_queue[i + 1].linear_acceleration;
//         max_linear_acceleration_diff =  std::max(max_linear_acceleration_diff, std::max(abs(diff[0]), std::max(abs(diff[1]), abs(diff[2]))));
//     }
//     return max_linear_acceleration_diff;
// }
// double CartoMapLandmarker::GetMaxAngularVelocity() {

//     double max_angular_velocity = 0.;
//     for (size_t i = 0; i < imu_queue.size() ; i ++) {
//         Eigen::Vector3d& angular_velocity = imu_queue[i] .angular_velocity;
//         max_angular_velocity = std::max(max_angular_velocity, std::max(abs(angular_velocity[0]), std::max(abs(angular_velocity[1]), abs(angular_velocity[2]))));
//     }
//     return max_angular_velocity;
// }
// bool CartoMapLandmarker::IsStill()  {
//     if (!queue_filled_) 
//         return false;
//     double max_linear_acceleration_diff = GetMaxLinearAccelerationDiff();
//     double max_angular_velocity = GetMaxAngularVelocity();

//     if (max_linear_acceleration_diff < kLinearAccelerationStillThresh && max_angular_velocity < kAngelVelocityStillThresh )
//       return true;

//     return false;

// }
// bool CartoMapLandmarker::IsMoving()  {
//     if (!queue_filled_) 
//         return false;
//     double max_linear_acceleration_diff = GetMaxLinearAccelerationDiff();
//     double max_angular_velocity = GetMaxAngularVelocity();

//     if (max_linear_acceleration_diff > kLinearAccelerationMovingThresh ||  max_angular_velocity > kAngelVelocityMovingThresh )
//       return true;

//     return false;

// }
// void CartoMapLandmarker::ProcessResults(){
//     if (landmarks_.empty())
//         return;
//     cartographer_ros_msgs::LandmarkList landmark_results;

//     for (const auto &landmark_item : landmarks_) {
//         cartographer_ros_msgs::LandmarkEntry landmark_entry;
//         LandMarkEntry landmark = landmark_item.second;
//         std::stringstream tag_id;
//         tag_id << landmark.tag_id;
//         landmark_entry.id = tag_id.str();
//         landmark_entry.tracking_from_landmark_transform.position = decltype(landmark_entry.tracking_from_landmark_transform.position)(
//                         landmark.pose.position.x, landmark.pose.position.y, landmark.pose.position.z);
//         landmark_results.push_back


//     }
// }
}
}
