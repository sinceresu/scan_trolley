#include "include/libmap_landmark.h"
#include "carto_map_landmarker.h"

namespace yida_mapping{
namespace map_landmark{

std::shared_ptr<MapLandmarkInterface> CreateMapLandMarker(MapLandMarkType type)
{
    switch (type)
    {
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_LandMarker();
    case CARTOGRAPHER:
        return std::shared_ptr<MapLandmarkInterface>(new CartoMapLandmarker());
    default:
        return nullptr;
    }
}

} // namespace map_LandMarker
}// namespace yida_mapping
