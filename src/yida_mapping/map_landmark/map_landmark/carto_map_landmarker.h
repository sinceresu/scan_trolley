#ifndef _MAP_LANDMARK_CARTO_MAP_LANDMARKER_H
#define _MAP_LANDMARK_CARTO_MAP_LANDMARKER_H

#include <vector>
#include <memory>
#include <thread>
#include <deque>
#include <stdint.h>
 
#include <Eigen/Core>
#include <Eigen/Geometry>


#include "include/map_landmark_interface.h"
#include "landmark.h"


namespace yida_mapping{

namespace map_landmark{
  class LandmarkState;
  class LandmarkDetector;
struct ImuData {
    uint64_t time_ms;
    Eigen::Vector3d linear_acceleration;
    Eigen::Vector3d angular_velocity;
};

class CartoMapLandmarker : public MapLandmarkInterface
{
  // enum class LandmarkState {
  //   IDLE = 0, 
  //   DETECTING, 
  //   DETECTED, 
  //   WAITING_TIMEOUT, 

  // };
  const uint64_t kSearchDurationMs = 2000;
  const uint64_t kMaxDetectingDurationMs = 200;
  static constexpr double kLinearAccelerationStillThresh = 0.1;
  static constexpr double kAngelVelocityStillThresh = 0.1;
  static constexpr double kLinearAccelerationMovingThresh = kLinearAccelerationStillThresh * 2;
  static constexpr double kAngelVelocityMovingThresh = kAngelVelocityStillThresh * 2;

public:
  CartoMapLandmarker();
  ~CartoMapLandmarker(){};

  virtual  int SetParam(const LandmarkParam & param) override ;
  // virtual  int HandleImuMessage(const sensor_msgs::Imu::ConstPtr msg) override;
  virtual int HandleImage(const cv::Mat& image, bool& landmark_ready, cartographer_ros_msgs::LandmarkList & landmarks) override;
  // virtual  int Reset() override;
private:
  void StartDetecting() ;
  void StopDetecting() ;

  inline uint64_t FromRos(const ::ros::Time& time) {
      return time.sec * 1000L + (time.nsec + 5E5) / 1E6;
  }
  // void UpdateImuQueue(const sensor_msgs::Imu::ConstPtr imu_data) ;
  // double GetMaxLinearAccelerationDiff();
  // double GetMaxAngularVelocity ();

  // bool IsStill() ;
  // bool IsMoving() ;

  int HandleImage(const cv::Mat& image, bool& landmark_ready, cartographer_ros_msgs::LandmarkList::_landmarks_type & landmarks);


  LandmarkParam parameter_;
  LandmarkCallback callback_;
  std::shared_ptr<LandmarkDetector> detector_;
  std::deque<ImuData> imu_queue;
  bool queue_filled_ = false;
  bool do_detecting = false;

  // LandmarkState state_;
  uint64_t find_time_ms_;
  uint64_t start_time_ms_;
  uint64_t end_time_ms_;


};
} // namespace map_landmark
}// namespace yida_mapping

#endif  
