# cmake needs this line
cmake_minimum_required(VERSION 2.8.3)

# project(map_landmark_test)

# include_directories(
#   ../
#   ./
#   ${catkin_INCLUDE_DIRS}
# )

# set(srcs
#       map_merger_test.cc
# )

# add_executable(${PROJECT_NAME} 
#   ${srcs}
# )
google_binary(map_landmark_test
  SRCS
  map_landmark_test.cc
)

target_link_libraries(map_landmark_test PUBLIC
  glog
  gflags
  apriltag
  ${OpenCV_LIBS}
  )

install(TARGETS map_landmark_test
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)



