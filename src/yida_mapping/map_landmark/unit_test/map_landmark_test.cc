#include "gflags/gflags.h"
#include "glog/logging.h"

#include <stdio.h>
#include <memory>
#include "opencv2/opencv.hpp"


#include "map_landmark/landmark_detector.h"


DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(
    calibration_basename, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");              
DEFINE_string(
    tag_family, "tag36h11",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");
DEFINE_double(
    tag_size, 0.2,
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");
DEFINE_string(
    picture_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

using namespace std;
namespace yida_mapping{
namespace map_landmark{
namespace {
 std::shared_ptr<LandmarkDetector>  landmark_detector;

int TestLandMark(int argc, char** argv) {

    yida_mapping::map_landmark::LandMarkDetectParam param;
    param.configuration_directory = FLAGS_configuration_directory;
    param.calibration_basename = FLAGS_calibration_basename;
    param.tag_family = FLAGS_tag_family;
    param.tag_size =  FLAGS_tag_size;


    landmark_detector = make_shared<LandmarkDetector>();
    landmark_detector->SetParam(param);

    cv::Mat frame = cv::imread(FLAGS_picture_filepath);
    if (frame.empty())
        return -1;
    LandMarkEntry detected_landmark;
    
    int result = landmark_detector->Detect(frame, detected_landmark);

    const std::chrono::time_point<std::chrono::steady_clock> start_time =
        std::chrono::steady_clock::now();
    result = landmark_detector->Detect(frame, detected_landmark);
    const std::chrono::time_point<std::chrono::steady_clock> end_time =
        std::chrono::steady_clock::now();
    const double wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time)
            .count();
            
    LOG(INFO) << "Elapsed wall clock time: " << wall_clock_seconds << " s";


    CHECK_EQ(result, 0);
    LOG(INFO) << "landmark position:"  ;
    LOG(INFO) << std::endl << detected_landmark.pose.translation()<< std::endl ;

    LOG(INFO) << "landmark orientation:"  ;
    LOG(INFO) << std::endl << detected_landmark.pose.linear();
    // CHECK_NEAR(detected_landmark.pose.position.x, -0.08, 0.1);
    // CHECK_NEAR(detected_landmark.pose.position.y, 0.025, 0.1);
    // CHECK_NEAR(detected_landmark.pose.position.z, 0.87, 0.1);


    getchar();
    return 0;

}

}
}
}


int main(int argc, char** argv) {
google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  yida_mapping::map_landmark::TestLandMark(argc, argv);

}
