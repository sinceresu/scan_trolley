#ifndef _MAP_LANDMARK_LIBMAP_LANDMARK_H
#define _MAP_LANDMARK_LIBMAP_LANDMARK_H


#if (defined _WIN32 || defined WINCE || defined __CYGWIN__) && defined  LIBmap_landmark_EXPORTS
#define LIBmap_landmark_API __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define LIBmap_landmark_API __attribute__ ((visibility ("default")))
#else
#define LIBmap_landmark_API
#endif

#include <memory>
#include <stdint.h>
namespace yida_mapping{
namespace map_landmark {

class MapLandmarkInterface;

enum MapLandMarkType {
  CARTOGRAPHER,
 TYPE_NUMBER,
};

  /******************************************************************************
  * \fn CreateMapLandMark
  * Create Time: 2020/06/02
  * Description: -
  *   创建一个新的扫描组件实例
  *
  * \param type 输入参数
  * 		扫描组件实例类型
  *
  * \return
  * 		扫描组件实例
  *
  * \note 
  *******************************************************************************/
std::shared_ptr<MapLandmarkInterface> CreateMapLandMarker(MapLandMarkType type = CARTOGRAPHER);

} // namespace map_landmark
}// namespace yida_mapping

#endif  
