#ifndef _MAP_LANDMARK_MAP_LANDMARK_INTERFACE_H
#define _MAP_LANDMARK_MAP_LANDMARK_INTERFACE_H

#include <string>
#include <memory>
#include <stdint.h>

#include "sensor_msgs/Imu.h"
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include "cartographer_ros_msgs/LandmarkList.h"

#include "opencv2/opencv.hpp"

#include "map_common/global.h"

namespace yida_mapping{
namespace map_landmark{

      
class MapLandmarkInterface
{
  
public:

  using LandmarkCallback =
      std::function<void(cartographer_ros_msgs::LandmarkList)>;

  typedef struct LandmarkParam {
    LandmarkParam() : 
    translation_weight(500000.),
    rotation_weight(500000),
    tag_family("tag36h11"),
    max_distance(2.0),
    max_fov(60)
    {

    }
    std::string configuration_directory;    //配置文件目录
    std::string calibration_basename;    //相机内外参文件
    std::string image_frame_id;    //
    double translation_weight;
    double rotation_weight;
   std::string tag_family;    
    double tag_size;    
    double max_distance;    
    double max_fov;    


  }LandmarkParam;

 
  virtual ~MapLandmarkInterface(){};

    /******************************************************************************
  * \fn MapLandmarkInterface.SetParam
  * Create Time: 2020/06/02
  * Description: -
  *    设置着色参数
  *
  * \param param 输入参数
  * 		各项Landmark参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const LandmarkParam & param) = 0 ;
  // virtual  int HandleImuMessage(const sensor_msgs::Imu::ConstPtr msg) = 0;
  virtual  int HandleImage(const cv::Mat& image, bool& landmark_ready, cartographer_ros_msgs::LandmarkList & landmarks) = 0;
  // virtual  int Reset() = 0;
};
} // namespace map_landmark
}// namespace yida_mapping

#endif  
