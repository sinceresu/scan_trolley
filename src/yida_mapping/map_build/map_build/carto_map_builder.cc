#include "carto_map_builder.h"

#include <thread>
#include <chrono>

#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer/transform/transform.h"
#include "cartographer/mapping/map_builder.h"
#include "cartographer_ros/node.h"
#include "cartographer_ros/playable_bag.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/assets_writer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/node_options.h"
#include "cartographer_ros/ros_log_sink.h"
#include "cartographer_ros_msgs/StatusCode.h"
#include "cartographer_ros_msgs/StatusResponse.h"
#include "cartographer_ros/dev/output_pbstream_trajectories.h"
#include "cartographer/io/proto_stream_serializer.h"

#include "tf2_ros/transform_listener.h"
#include "tf2_ros/static_transform_broadcaster.h"
#include "urdf/model.h"

#include <cv_bridge/cv_bridge.h>
#include <rosbag/recorder.h>
#include <sensor_msgs/Image.h>

#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "map_common/err_code.h"
#include "map_common/image_process.h"

#include "map_landmark/libmap_landmark.h"
#include "map_landmark/map_landmark_interface.h"

#include "maps_writer.h"

using namespace std;

namespace carto = ::cartographer;
namespace yida_mapping {
    using namespace cartographer_ros;

    namespace map_build {
        namespace {
            typedef pcl::PointXYZ PointT;
            typedef pcl::PointCloud<PointT> PointCloud;
            typedef pcl::PointNormal PointNormalT;

            void FilterPCLWithRor(PointCloud::ConstPtr input_pcl,
                                  PointCloud::Ptr output_pcl,
                                  double search_radius, int min_neighbors_in_radius) {
                pcl::RadiusOutlierRemoval <PointT> ror_filter;

                ror_filter.setInputCloud(input_pcl);
                ror_filter.setRadiusSearch(search_radius);
                ror_filter.setMinNeighborsInRadius(min_neighbors_in_radius);
                ror_filter.filter(*output_pcl);
            }

            void FilterPCLWithSor(PointCloud::ConstPtr input_pcl,
                                  PointCloud::Ptr output_pcl,
                                  int mean_k,
                                  double std_mul) {
                pcl::StatisticalOutlierRemoval <PointT> sor_filter;

                sor_filter.setInputCloud(input_pcl);
                sor_filter.setMeanK(mean_k);
                sor_filter.setStddevMulThresh(std_mul);
                sor_filter.filter(*output_pcl);
            }

            void FilterPCLFile(const std::string &pcl_file_name,
                               int filter_mean_k,
                               double filter_std_mul,
                               double filter_search_radius,
                               int filter_min_neighbors_in_radius) {

                PointCloud::Ptr input_pcl        = PointCloud::Ptr(new PointCloud());
                PointCloud::Ptr filtered_map_pcl = PointCloud::Ptr(new PointCloud());

                pcl::io::loadPLYFile<PointT>(pcl_file_name, *input_pcl);
                LOG(INFO) << "Filtering   with sor ... " ;
                FilterPCLWithSor(input_pcl, filtered_map_pcl, filter_search_radius, filter_min_neighbors_in_radius);
                pcl::io::savePLYFileBinary<PointT>(pcl_file_name, *filtered_map_pcl);

                LOG(INFO) << "Filter OK. " ;
            }

            void DownSamplePCLFile(const std::string &pcl_file_name, double resolution) {
                pcl::VoxelGrid<PointT> grid;
                grid.setLeafSize(resolution, resolution, resolution);

                PointCloud::Ptr cloud_input  = PointCloud::Ptr(new PointCloud());
                PointCloud::Ptr cloud_output = PointCloud::Ptr(new PointCloud());

                pcl::io::loadPLYFile(pcl_file_name, *cloud_input);

                grid.setInputCloud(cloud_input);
                grid.filter(*cloud_output);

                pcl::io::savePLYFileBinary<PointT>(pcl_file_name, *cloud_output);
            }

            std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
                const std::string& configuration_directory,
                const std::string& configuration_basename) {

                auto file_resolver = absl::make_unique<carto::common::ConfigurationFileResolver>(
                    std::vector<std::string>{ configuration_directory });

                const std::string code        = file_resolver->GetFileContentOrDie(configuration_basename);
                auto lua_parameter_dictionary = absl::make_unique<carto::common::LuaParameterDictionary>(
                    code, std::move(file_resolver));

                return lua_parameter_dictionary;
            }

            Eigen::Isometry3d GetLinkTransform(const tf2_ros::Buffer& tf_buffer,
                                               const std::string& base_link,
                                               const std::string & ref_link) {
                ::ros::Time ros_time;
                const carto::transform::Rigid3d sensor_to_tracking = ToRigid3d(
                    tf_buffer.lookupTransform(base_link, ref_link, ros_time));

                Eigen::Isometry3d tf_sensor_to_tracking;
                tf_sensor_to_tracking.linear()      = sensor_to_tracking.rotation().matrix();
                tf_sensor_to_tracking.translation() = sensor_to_tracking.translation();

                return tf_sensor_to_tracking;
            }

            typedef struct TimedLandmark_s {
                std::string id;
                ::ros::Time stamp;
                Eigen::Isometry3d pose;
            } TimedLandmark;

            std::vector<TimedLandmark> get_landmarks(const std::string & bag_filename,
                                                     const tf2_ros::Buffer& tf_buffer,
                                                     const std::string& base_link,
                                                     const std::string & ref_link,
                                                     std::map<std::string,
                                                              std::shared_ptr<::yida_mapping::map_landmark::MapLandmarkInterface>> landmarkers) {

                Eigen::Isometry3d ref_to_track = GetLinkTransform(tf_buffer, base_link, ref_link);
                std::vector<TimedLandmark> landmarks;
                rosbag::Bag bag;
                bag.open(bag_filename, rosbag::bagmode::Read);
                rosbag::View view(bag);
                const ::ros::Time begin_time = view.getBeginTime();

                for(const rosbag::MessageInstance& message : view) {
                    if(message.isType<sensor_msgs::CompressedImage>()) {
                        sensor_msgs::CompressedImage::Ptr image_msg = message.instantiate<sensor_msgs::CompressedImage>();

                        if(landmarkers.count(image_msg->header.frame_id) != 0) {
                            cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*image_msg, "mono8");
                            cv::Mat  rotated_image;
                            rotated_image = cv_ptr->image.clone();

                            cartographer_ros_msgs::LandmarkList::Ptr landmark_list(new cartographer_ros_msgs::LandmarkList());
                            bool landmark_detected = false;
                            landmarkers[image_msg->header.frame_id]->HandleImage(rotated_image, landmark_detected, *landmark_list);

                            if(landmark_detected) {
                                const auto& landmark = landmark_list->landmarks[0];
                                Eigen::Isometry3d landmark_transform;
                                auto position = landmark.tracking_from_landmark_transform.position;
                                landmark_transform.translation() = Eigen::Vector3d(position.x, position.y, position.z);
                                const auto& orientation = landmark.tracking_from_landmark_transform.orientation;
                                const Eigen::Quaterniond landmark_orientation(orientation.w, orientation.x, orientation.y, orientation.z);
                                landmark_transform.linear() = landmark_orientation.matrix();
                                TimedLandmark timed_landmark = {landmark.id, image_msg->header.stamp, ref_to_track * landmark_transform};
                                landmarks.emplace_back(timed_landmark);
                            }
                        }
                    }
                }

                return landmarks;
            }
        }

        const ::ros::Duration kDelay = ::ros::Duration(1.0);

        CartoMapBuilder::CartoMapBuilder() : started_(false),
                                             stop_flag_(false),
                                             progress_callback_(nullptr) { }

        int CartoMapBuilder::SetProgressCallback(ProgressCallback callback) {
            progress_callback_ = callback;
            return ERRCODE_OK;
        }
        int CartoMapBuilder::SetParam(const BuildParam & param) {
            parameter_ = param;
            return ERRCODE_OK;
        }

        void CartoMapBuilder::LoadBuildOptions(const std::string& configuration_directory,
                                               const std::string& configuration_basename) {
            const auto lua_parameter_dictionary = LoadLuaDictionary(configuration_directory, configuration_basename);

            const auto filter_options = lua_parameter_dictionary->GetDictionary("pcl_filter");
            filter_pcl                     = filter_options->GetBool("filter_pcl");
            filter_mean_k                  = filter_options->GetInt("mean_k");
            filter_std_mul                 = filter_options->GetDouble("std_mul");
            filter_search_radius           = filter_options->GetDouble("search_radius");
            filter_min_neighbors_in_radius = filter_options->GetInt("min_neighbors_in_radius");

            const auto build_options = lua_parameter_dictionary->GetDictionary("build");
            skip_seconds                = build_options->GetDouble("skip_seconds");
            do_landmark                 = build_options->GetBool("do_landmark");
            //stop_detection              = build_options->GetBool("stop_detection");
            rotate_image_mode           = static_cast<RotateImageMode>(build_options->GetInt("rotate_image_mode"));
            landmark_tag_family         = build_options->GetString("tag_family");
            landmark_translation_weight = build_options->GetDouble("translation_weight");
            landmark_rotation_weight    = build_options->GetDouble("rotation_weight");
            landmark_tag_size           = build_options->GetDouble("tag_size");
            landmark_max_distance       = build_options->GetDouble("max_distance");
            landmark_max_fov            = build_options->GetDouble("max_fov");
            landmark_ref_frame_id       = build_options->GetString("ref_frame_id");
            image_frame_ids             = build_options->GetDictionary("image_frame_ids")->GetArrayValuesAsStrings();
            calibration_basenames       = build_options->GetDictionary("calibration_basenames")->GetArrayValuesAsStrings();
            build_resolution            = build_options->GetDouble("build_resolution");
            merge_resolution            = build_options->GetDouble("merge_resolution");
        }

        constexpr int pointsprocess_times         = 3;
        constexpr int frames_per_progresscallback = 1000;
        constexpr float kMapRatio         = 0.3f;
        constexpr float kWriteRatio       = 0.6f;
        constexpr float kPostProcessRatio = 0.1f;

        static float map_ratio;
        static float write_ratio;

        int CartoMapBuilder::BuildMaps(const std::string & state_filepath, const std::vector<std::string>& bag_filenames,
                                       const std::string & save_directory, const std::string& map_name) {
            CHECK(!parameter_.configuration_directory.empty())
                << "-configuration_directory is missing.";
            CHECK(!parameter_.build_configuration_basename.empty())
                << "-merge_configuration_basename is missing.";
            CHECK(!parameter_.map_configuration_basename.empty())
                << "-configuration_basename is missing.";
            CHECK(!parameter_.write_configuration_basename.empty())
                << "-write_configuration_basename is missing.";
            CHECK(!parameter_.urdf_filename.empty())
                << "-urdf_filename is missing.";
            CHECK(!(bag_filenames.empty()))
                << "-bag_filenames is unspecified.";

            LoadBuildOptions(parameter_.configuration_directory, parameter_.build_configuration_basename);

            {
                cartographer_ros::NodeOptions node_options;
                const std::vector<std::string> configuration_basenames ={parameter_.map_configuration_basename};
                std::vector<TrajectoryOptions> bag_trajectory_options(1);
                std::tie(node_options, bag_trajectory_options.at(0)) = LoadOptions(parameter_.configuration_directory,
                                                                                   configuration_basenames.at(0));

                for(size_t bag_index = 1; bag_index < bag_filenames.size(); ++bag_index) {
                    TrajectoryOptions current_trajectory_options;

                    if(bag_index < configuration_basenames.size()) {
                        std::tie(std::ignore, current_trajectory_options) = LoadOptions(
                        parameter_.configuration_directory, parameter_.map_configuration_basename);
                    } else {
                        current_trajectory_options = bag_trajectory_options.at(0);
                    }

                    bag_trajectory_options.push_back(current_trajectory_options);
                }
                if(bag_filenames.size() > 0) {
                    CHECK_EQ(bag_trajectory_options.size(), bag_filenames.size());
                }

                // Since we preload the transform buffer, we should never have to wait for a
                // transform. When we finish processing the bag, we will simply drop any
                // remaining sensor data that cannot be transformed due to missing transforms.
                node_options.lookup_transform_timeout_sec = 0.;

                auto map_builder = std::unique_ptr<::cartographer::mapping::MapBuilder>(new ::cartographer::mapping::MapBuilder(
                      node_options.map_builder_options));

                const std::chrono::time_point<std::chrono::steady_clock> start_time = std::chrono::steady_clock::now();

                tf2_ros::Buffer tf_buffer;
                write_ratio = 0.4;
                ::cartographer_ros::ReadStaticTransformsFromUrdf(parameter_.urdf_filename, &tf_buffer);

                tf_buffer.setUsingDedicatedThread(true);

                cartographer_ros::Node node(node_options, std::move(map_builder), &tf_buffer, false, true);
                size_t mapped_trajectories = 0;
                auto  landmarkers = GenerateLandmarks();
                std::vector<std::vector<TimedLandmark>> src_landmarks;

                if(!state_filepath.empty()) {
                    node.LoadState(state_filepath, false);
                    ::cartographer::mapping::proto::PoseGraph pose_graph(
                        carto::io::DeserializePoseGraphFromFile(state_filepath));

                    mapped_trajectories = pose_graph.trajectory().size();
                    for(size_t i = 0; i < mapped_trajectories; i++) {
                        src_landmarks.push_back(get_landmarks(bag_filenames.at(i), tf_buffer, bag_trajectory_options.at(i).tracking_frame, landmark_ref_frame_id, landmarkers));
                    }

                    LOG(INFO) << "src_landmarks size: " << src_landmarks.size();
                }

                std::vector<std::set<cartographer::mapping::TrajectoryBuilderInterface::SensorId>> bag_expected_sensor_ids;
                if(configuration_basenames.size() == 1) {
                    const auto current_bag_expected_sensor_ids = node.ComputeDefaultSensorIdsForMultipleBags({bag_trajectory_options.front()});
                    bag_expected_sensor_ids = {bag_filenames.size(), current_bag_expected_sensor_ids.front()};
                } else {
                    bag_expected_sensor_ids = node.ComputeDefaultSensorIdsForMultipleBags(bag_trajectory_options);
                }
                CHECK_EQ(bag_expected_sensor_ids.size(), bag_filenames.size());

                std::map<std::pair<int /* bag_index */, std::string>,
                  cartographer::mapping::TrajectoryBuilderInterface::SensorId> bag_topic_to_sensor_id;

                PlayableBagMultiplexer playable_bag_multiplexer;
                double duration_in_seconds = 0;
                std::vector<double> durations(bag_filenames.size());
                std::vector<ros::Time> begin_times(bag_filenames.size());

                for(size_t current_bag_index = mapped_trajectories; current_bag_index < bag_filenames.size(); ++current_bag_index) {
                    const std::string& bag_filename = bag_filenames.at(current_bag_index);
                    LOG(INFO) << "bag file: " << bag_filename;

                    if(!::ros::ok()) { return ERRCODE_OK; }

                    for(const auto& expected_sensor_id : bag_expected_sensor_ids.at(current_bag_index)) {
                        const auto bag_resolved_topic = std::make_pair(static_cast<int>(current_bag_index),
                                                                       node.node_handle()->resolveName(expected_sensor_id.id));

                        if(bag_topic_to_sensor_id.count(bag_resolved_topic) != 0) {
                            LOG(ERROR) << "Sensor " << expected_sensor_id.id << " of bag "
                                       << current_bag_index << " resolves to topic "
                                       << bag_resolved_topic.second << " which is already used by "
                                       << " sensor "
                                       << bag_topic_to_sensor_id.at(bag_resolved_topic).id;
                        }
                        bag_topic_to_sensor_id[bag_resolved_topic] = expected_sensor_id;
                    }

                    auto playbag = PlayableBag(bag_filename, current_bag_index, ros::TIME_MIN, ros::TIME_MAX, kDelay,
                        // PlayableBag::FilteringEarlyMessageHandler is used to get an early
                        // peek at the tf messages in the bag and insert them into 'tf_buffer'.
                        // When a message is retrieved by GetNextMessage() further below,
                        // we will have already inserted further 'kDelay' seconds worth of
                        // transforms into 'tf_buffer' via this lambda.
                        nullptr
                    );
                    duration_in_seconds +=playbag.duration_in_seconds();
                    durations[current_bag_index] = playbag.duration_in_seconds();
                    begin_times[current_bag_index] =std::get<0>(playbag.GetBeginEndTime());
                    playable_bag_multiplexer.AddPlayableBag(std::move(playbag));
                }

                std::set<std::string> bag_topics;
                std::stringstream bag_topics_string;

                for(const auto& topic : playable_bag_multiplexer.topics()) {
                    std::string resolved_topic = node.node_handle()->resolveName(topic, false);
                    bag_topics.insert(resolved_topic);
                    bag_topics_string << resolved_topic << ",";
                }

                bool print_topics = false;
                for(const auto& entry : bag_topic_to_sensor_id) {
                    const std::string& resolved_topic = entry.first.second;
                    if(bag_topics.count(resolved_topic) == 0) {
                        LOG(WARNING) << "Expected resolved topic \"" << resolved_topic
                                     << "\" not found in bag file(s).";
                        print_topics = true;
                    }
                }
                if(print_topics) {
                    LOG(WARNING) << "Available topics in bag file(s) are "
                                 << bag_topics_string.str();
                }

                std::unordered_map<int, int> bag_index_to_trajectory_id;
                const ros::Time begin_time =
                    // If no bags were loaded, we cannot peek the time of first message.
                    playable_bag_multiplexer.IsMessageAvailable() ? playable_bag_multiplexer.PeekMessageTime() : ros::Time();

                stop_flag_     = false;
                async_spinner_ = make_shared<ros::AsyncSpinner>(1);
                async_spinner_->start();

                uint32_t msg_counter     = 0;
                double processed_seconds = 0;
                map_ratio   = kMapRatio;
                write_ratio = kWriteRatio;

                if(!filter_pcl) {
                    map_ratio   = kMapRatio / (1 - kPostProcessRatio);
                    write_ratio = kWriteRatio / (1 - kPostProcessRatio);
                }

                while(playable_bag_multiplexer.IsMessageAvailable()) {
                    if(!::ros::ok()) { return ERRCODE_OK; }

                    cartographer_ros_msgs::BagfileProgress progress;
                    const auto next_msg_tuple = playable_bag_multiplexer.GetNextMessage();
                    const rosbag::MessageInstance& msg = std::get<0>(next_msg_tuple);
                    const int bag_index = std::get<1>(next_msg_tuple);
                    const bool is_last_message_in_bag = std::get<2>(next_msg_tuple);

                    // if (msg.getTime() < (begin_time + ros::Duration(skip_seconds))) {
                    //   continue;
                    // }

                    int trajectory_id;
                    // Lazily add trajectories only when the first message arrives in order
                    // to avoid blocking the sensor queue.
                    if(bag_index_to_trajectory_id.count(bag_index) == 0) {
                        auto trajectory_options = bag_trajectory_options.at(bag_index);

                        trajectory_id = node.AddOfflineTrajectory(bag_expected_sensor_ids.at(bag_index), trajectory_options);
                        CHECK(bag_index_to_trajectory_id .emplace(std::piecewise_construct, std::forward_as_tuple(bag_index),
                            std::forward_as_tuple(trajectory_id)) .second);

                        LOG(INFO) << "Assigned trajectory " << trajectory_id << " to bag "
                                  << bag_filenames.at(bag_index);
                    } else { trajectory_id = bag_index_to_trajectory_id.at(bag_index); }

                    const auto bag_topic = std::make_pair(bag_index, node.node_handle()->resolveName(msg.getTopic(), false/*resolve*/));
                    auto it = bag_topic_to_sensor_id.find(bag_topic);

                    if(it != bag_topic_to_sensor_id.end()) {
                        const std::string& sensor_id = it->second.id;
                        if(msg.isType<sensor_msgs::LaserScan>()) {
                            node.HandleLaserScanMessage(trajectory_id, sensor_id, msg.instantiate<sensor_msgs::LaserScan>());
                        }
                        if(msg.isType<sensor_msgs::MultiEchoLaserScan>()) {
                            node.HandleMultiEchoLaserScanMessage(trajectory_id, sensor_id,
                                                                 msg.instantiate<sensor_msgs::MultiEchoLaserScan>());
                        }
                        if(msg.isType<sensor_msgs::PointCloud2>()) {
                            node.HandlePointCloud2Message(trajectory_id, sensor_id,
                                                          msg.instantiate<sensor_msgs::PointCloud2>());
                        }
                        if(msg.isType<sensor_msgs::Imu>()) {
                            auto imu_msg = msg.instantiate<sensor_msgs::Imu>();
                            node.HandleImuMessage(trajectory_id, sensor_id, imu_msg);
                        }
                        if(msg.isType<nav_msgs::Odometry>()) {
                            node.HandleOdometryMessage(trajectory_id, sensor_id, msg.instantiate<nav_msgs::Odometry>());
                        }
                        if(msg.isType<sensor_msgs::NavSatFix>()) {
                            node.HandleNavSatFixMessage(trajectory_id, sensor_id, msg.instantiate<sensor_msgs::NavSatFix>());
                        }
                        if(msg.isType<cartographer_ros_msgs::LandmarkList>()) {
                            node.HandleLandmarkMessage(trajectory_id, sensor_id, msg.instantiate<cartographer_ros_msgs::LandmarkList>());
                        }
                    } else if(do_landmark) {
                        if(msg.isType<sensor_msgs::CompressedImage>()) {
                            sensor_msgs::CompressedImage::ConstPtr image_msg = msg.instantiate<sensor_msgs::CompressedImage>();
                            // LOG(INFO) << "Find Image : " << image_msg->header.stamp;
                            if(landmarkers.count(image_msg->header.frame_id) != 0) {
                                cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*image_msg, "mono8");
                                cv::Mat rotated_image;

                                if(rotate_image_mode == RotateImageMode::kRotate270Degree) {
                                    map_common::RotateImage270Degree(cv_ptr->image, rotated_image);
                                } else if(rotate_image_mode == RotateImageMode::kFlipUpDown) {
                                    map_common::FlipImageUpDown(cv_ptr->image, rotated_image);
                                } else { rotated_image = cv_ptr->image.clone(); }

                                std::stringstream png_file;
                                png_file << "/home/sujin/output/tmp/" << image_msg->header.stamp << "_" << image_msg->header.frame_id << ".png";
                                LOG(INFO) << "write Image to : " << png_file.str();
                                imwrite(png_file.str(), rotated_image);

                                cartographer_ros_msgs::LandmarkList::Ptr landmark_list(new cartographer_ros_msgs::LandmarkList());
                                bool landmark_detected = false;
                                landmarkers[image_msg->header.frame_id]->HandleImage(rotated_image, landmark_detected, *landmark_list);

                                if(landmark_detected) {
                                    LOG(INFO) << "landmark image id :" << image_msg->header.frame_id;
                                    LOG(INFO) << "landmark id :" << landmark_list->landmarks[0].id;

                                    landmark_list->header = image_msg->header;
                                    landmark_list->header.frame_id = landmark_ref_frame_id;
                                    node.HandleLandmarkMessage(trajectory_id, "landmark", landmark_list);
                                }
                            }
                        }
                    }

                    if((++msg_counter % frames_per_progresscallback) == 0) {
                        if(progress_callback_ != nullptr) {
                            float progress = map_ratio * (
                                (msg.getTime() - begin_times[trajectory_id]).toSec() +
                                    processed_seconds) / duration_in_seconds;
                            progress_callback_(ProcessType::kBuildMap, progress* 100.0f) ;
                        }
                    }

                    if(is_last_message_in_bag) {
                        node.FinishTrajectory(trajectory_id);
                        processed_seconds += durations[trajectory_id];
                    }
                } // while

                if(::ros::ok()) { node.RunFinalOptimization(); }
                async_spinner_->stop();

                const std::chrono::time_point<std::chrono::steady_clock> end_time =
                    std::chrono::steady_clock::now();
                const double wall_clock_seconds = std::chrono::duration_cast<
                    std::chrono::duration<double>>(end_time - start_time).count();

                LOG(INFO) << "Elapsed wall clock time: " << wall_clock_seconds << " s";

                // Serialize unless we have neither a bagfile nor an explicit state filename.
                if(::ros::ok() && !save_directory.empty() && !map_name.empty()) {
                    const std::string output_file_prefix       = save_directory + "/" + map_name;
                    const std::string save_pose_graph_filename = save_directory + "/" + map_name +".pbstream";

                    LOG(INFO) << "Writing state to '" << save_pose_graph_filename << "'...";

                    if(!node.SerializeState(save_pose_graph_filename, true /* include_unfinished_submaps */)) return ERRCODE_FAILED;

                    LOG(INFO) << "Generating trajectory file...";
                    const std::string save_trajectories_filename = output_file_prefix +".txt";
                    ::cartographer_ros::output_pbstream_trajectories(save_pose_graph_filename,
                                                                     save_trajectories_filename, "map");

                    if(progress_callback_ != nullptr) { progress_callback_(ProcessType::kWriteMap, (map_ratio)* 100.0f); }
                    LOG(INFO) << "Generating map file...";
                    GenerateMaps(bag_filenames, save_pose_graph_filename, output_file_prefix);

                    if(progress_callback_ != nullptr) {
                        progress_callback_(ProcessType::kPostProcess, (map_ratio + write_ratio)* 100.0f);
                    }
                    if(boost::filesystem::exists(output_file_prefix +".ply")) {
                        DownSamplePCLFile(output_file_prefix +".ply", build_resolution);
                        if(filter_pcl) {
                            FilterPCLFile(output_file_prefix +".ply", filter_mean_k, filter_std_mul,
                                          filter_search_radius, filter_min_neighbors_in_radius);
                        }
                    }
                }
            }

            LOG(INFO) << "build ok!" ;
            return ERRCODE_OK;
        }

        int CartoMapBuilder::Stop() {
            stop_flag_ = true;
            if(map_writer_) { map_writer_->Stop(); }
            return ERRCODE_OK;
        }

        int CartoMapBuilder::GetBuildedMaps(std::vector<std::string> &state_filepaths, std::vector<std::string> &pcl_filepaths) {
            state_filepaths = trajectory_pose_graph_files_;
            pcl_filepaths = trajectory_pcl_files_;
            return ERRCODE_OK;
        }

        void CartoMapBuilder::GenerateMaps(const std::vector<std::string>& bag_filepaths,
                                           const std::string& pose_graph_filename, const std::string & output_file_prefix) {
            const std::vector<std::string> & state_filepaths = {pose_graph_filename};
            map_writer_ = std::make_shared<MapsWriter>(state_filepaths, bag_filepaths, output_file_prefix);

            map_writer_->SetProgressCallback(std::bind(&CartoMapBuilder::BuildWriteCallBack, this, std::placeholders::_1));
            map_writer_->Run(parameter_.configuration_directory, parameter_.write_configuration_basename, parameter_.urdf_filename, skip_seconds);
        }

        void CartoMapBuilder::BuildWriteCallBack(float percent) {
            if(progress_callback_ != nullptr) {
                LOG(INFO) << "BuildWriteCallBack percent " << percent;
                progress_callback_(ProcessType::kWriteMap, map_ratio * 100 + percent * write_ratio);
            }
        }

        constexpr float kMergeRatio = 0.9f;
        static float merge_ratio;

        void CartoMapBuilder::MergeWriteCallBack(float percent) {
            if(progress_callback_ != nullptr) {
                LOG(INFO) << "MergeWriteCallBack percent " << percent;
                progress_callback_(ProcessType::kMergeMap, merge_ratio * percent);
            }
        }

        int CartoMapBuilder::MergeMaps(const std::vector<std::string>& state_filepaths,
                                       const std::vector<std::string>& bag_filepaths,
                                       const std::string & output_file_prefix) {
            CHECK(!parameter_.configuration_directory.empty())
                << "-configuration_directory is missing.";
            CHECK(!parameter_.write_configuration_basename.empty())
                << "-write_configuration_basename is missing.";
            CHECK(!parameter_.build_configuration_basename.empty())
                << "-build_configuration_basename is missing.";
            CHECK(!parameter_.urdf_filename.empty())
                << "-urdf_filename is missing.";

            CHECK_LE(state_filepaths.size(), bag_filepaths.size());

            LoadBuildOptions(parameter_.configuration_directory, parameter_.build_configuration_basename);
            merge_ratio = kMergeRatio;
            // if(!filter_pcl) { merge_ratio = 0.9f; }
            map_writer_ = std::make_shared<MapsWriter>(state_filepaths, bag_filepaths, output_file_prefix);

            map_writer_->SetProgressCallback(std::bind(&CartoMapBuilder::MergeWriteCallBack, this, std::placeholders::_1));
            map_writer_->Run(parameter_.configuration_directory, parameter_.write_configuration_basename, parameter_.urdf_filename);

            if(progress_callback_ != nullptr) progress_callback_(ProcessType::kPostProcess, 100.f * merge_ratio);

            DownSamplePCLFile(output_file_prefix +".ply", merge_resolution);
            // if(filter_pcl) {
            //   FilterPCLFile(output_file_prefix +".ply", filter_mean_k, filter_std_mul, filter_search_radius, filter_min_neighbors_in_radius);
            // }

            if(progress_callback_ != nullptr) progress_callback_(ProcessType::kPostProcess, 100.f);

            return ERRCODE_OK;
        }

        std::map<std::string, std::shared_ptr<map_landmark::MapLandmarkInterface>> CartoMapBuilder::GenerateLandmarks() {
            std::map<std::string, std::shared_ptr<map_landmark::MapLandmarkInterface>> landmarkers;

            if(do_landmark) {
                for(size_t i  = 0; i  < image_frame_ids.size(); i++) {
                    std::shared_ptr<::yida_mapping::map_landmark::MapLandmarkInterface> landmarker =
                        ::yida_mapping::map_landmark::CreateMapLandMarker();
                    ::yida_mapping::map_landmark::MapLandmarkInterface::LandmarkParam landmark_param;

                    //landmark_param.stop_detection          = stop_detection;
                    landmark_param.tag_family              =  landmark_tag_family;
                    landmark_param.translation_weight      = landmark_translation_weight;
                    landmark_param.rotation_weight         = landmark_rotation_weight;
                    landmark_param.tag_size                = landmark_tag_size;
                    landmark_param.configuration_directory = parameter_.configuration_directory;
                    landmark_param.calibration_basename    = calibration_basenames[i];
                    landmark_param.image_frame_id          = image_frame_ids[i];
                    landmark_param.max_distance            = landmark_max_distance;
                    landmark_param.max_fov                 = landmark_max_fov;

                    landmarker->SetParam(landmark_param);
                    landmarkers[image_frame_ids[i]] = landmarker;
                }
            }

            return landmarkers;
        }
    } // namespace map_build
} // namespace yida_mapping1
