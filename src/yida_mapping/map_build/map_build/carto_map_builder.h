#ifndef _MAP_BUILD_CARTO_MAP_BUILD_H
#define _MAP_BUILD_CARTO_MAP_BUILD_H

#include <vector>
#include <memory>
#include <thread>

#include <jsoncpp/json/json.h>

#include "ros/spinner.h"
#include "cartographer/io/image.h"

#include "include/map_build_interface.h"

namespace yida_mapping {
    namespace map_landmark { class MapLandmarkInterface; }
    namespace map_build {
        class MapsWriter;
        class CartoMapBuilder : public MapBuildInterface {

        public:
            CartoMapBuilder();
            ~CartoMapBuilder() {};

            virtual int SetProgressCallback(ProgressCallback callback) override;
            virtual int SetParam(const BuildParam & param) override;
            virtual int BuildMaps(const std::string & state_filepath, const std::vector<std::string>& bag_filepaths,
                                  const std::string & save_directory, const std::string& map_name) override;
            virtual int GetBuildedMaps(std::vector<std::string> &state_filepaths, std::vector<std::string> &pcl_filepaths) override;
            virtual int MergeMaps(const std::vector<std::string> &state_filepaths,
                                  const std::vector<std::string> &bag_filepaths,
                                  const std::string &output_file_prefix ) override;
            virtual int Stop() override;

        private:
            void LoadBuildOptions(const std::string& configuration_directory, const std::string& configuration_basename);

            int BuildTrajectoryMap(const std::string& bag_filepath, const std::string &save_directory,
                                   const std::string &map_name);
            void GenerateMaps(const std::vector<std::string> &bag_filenames, const std::string& pose_graph_filename,
                              const std::string &output_file_prefix);
            void BuildWriteCallBack(float percent);
            void MergeWriteCallBack(float percent);
            std::map<std::string, std::shared_ptr<map_landmark::MapLandmarkInterface>> GenerateLandmarks();

            BuildParam parameter_;

            bool started_;
            std::vector<std::string> trajectory_bag_filenames_;
            bool stop_flag_;
            std::shared_ptr<ros::AsyncSpinner> async_spinner_;

            bool filter_pcl;       //if do point cloud  outlier filtering
            int filter_mean_k;     // min neibors in radius when filtering point cloud
            double filter_std_mul; // search ridius in meter when filtering point cloud
            double filter_search_radius;
            int filter_min_neighbors_in_radius;

            double skip_seconds;

            bool do_landmark;
            RotateImageMode rotate_image_mode;
            std::string landmark_tag_family;

            double landmark_translation_weight;
            double landmark_rotation_weight;
            double landmark_tag_size;
            double landmark_max_distance;
            double landmark_max_fov;

            double build_resolution;
            double merge_resolution;

            std::string landmark_ref_frame_id;
            std::vector<std::string> image_frame_ids;
            std::vector<std::string> calibration_basenames;
            std::vector<std::string> trajectory_pose_graph_files_;
            std::vector<std::string> trajectory_pcl_files_;

            std::shared_ptr<MapsWriter> map_writer_;
            ProgressCallback progress_callback_;
        };
    } // namespace map_build
} // namespace yida_mapping

#endif
