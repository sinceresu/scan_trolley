#include "include/libmap_build.h"
#include "carto_map_builder.h"

namespace yida_mapping{
namespace map_build{

std::shared_ptr<MapBuildInterface> CreateMapBuilder(MapBuilderType type)
{
    switch (type)
    {
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_builder();
    case CARTOGRAPHER:
        return std::shared_ptr<MapBuildInterface>(new CartoMapBuilder());
    default:
        return nullptr;
    }
}

} // namespace map_build
}// namespace yida_mapping
