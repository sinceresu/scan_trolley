/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "maps_writer.h"

#include <algorithm>
#include <fstream>
#include <iostream>

#include "absl/memory/memory.h"
#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include "tf2_eigen/tf2_eigen.h"
#include "tf2_msgs/TFMessage.h"
#include "tf2_ros/buffer.h"
#include "urdf/model.h"

#include <pcl/common/transforms.h>
#include "pcl_conversions/pcl_conversions.h"

using namespace cartographer_ros;
namespace yida_mapping {
namespace map_build{

namespace {
Eigen::Matrix4f pose_to_transform(const ::cartographer::transform::Rigid3f & pose) {
  auto orientation = pose.rotation();
  Eigen::Quaternionf quaternion = Eigen::Quaternionf(orientation.w(), orientation.x(), orientation.y(), orientation.z()); // w x y z
  auto position = pose.translation();

  Eigen::Matrix4f tf_matrix = Eigen::Matrix4f::Identity();
  tf_matrix.block<3, 3>(0, 0) = quaternion.toRotationMatrix();
  tf_matrix.block<3, 1>(0, 3) << position.x(), position.y(), position.z(); 

  return  tf_matrix;  
}
constexpr char kTfStaticTopic[] = "/tf_static";
namespace carto = ::cartographer;
// static std::string to_string(double d)
// {
//     std::ostringstream oss;
//     oss.precision(std::numeric_limits<double>::digits10);
//     oss << std::fixed << d;
//     std::string str = oss.str();

//     // Remove padding
//     // This must be done in two steps because of numbers like 700.00
//     std::size_t pos1 = str.find_last_not_of("0");
//     if(pos1 != std::string::npos)
//         str.erase(pos1+1);

//     std::size_t pos2 = str.find_last_not_of(".");
//     if(pos2 != std::string::npos)
//         str.erase(pos2+1);

//     return str;
// }
std::unique_ptr<carto::io::PointsProcessorPipelineBuilder>
CreatePipelineBuilder(
    const std::vector<carto::mapping::proto::Trajectory>& trajectories,
    const std::string file_prefix) {
  const auto file_writer_factory =
      MapsWriter::CreateFileWriterFactory(file_prefix);
  auto builder = absl::make_unique<carto::io::PointsProcessorPipelineBuilder>();
  carto::io::RegisterBuiltInPointsProcessors(trajectories, file_writer_factory,
                                             builder.get());
  builder->Register(RosMapWritingPointsProcessor::kConfigurationFileActionName,
                    [file_writer_factory](
                        carto::common::LuaParameterDictionary* const dictionary,
                        carto::io::PointsProcessor* const next)
                        -> std::unique_ptr<carto::io::PointsProcessor> {
                      return RosMapWritingPointsProcessor::FromDictionary(
                          file_writer_factory, dictionary, next);
                    });
  return builder;
}

std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
    const std::string& configuration_directory,
    const std::string& configuration_basename) {
  auto file_resolver =
      absl::make_unique<carto::common::ConfigurationFileResolver>(
          std::vector<std::string>{configuration_directory});

  const std::string code =
      file_resolver->GetFileContentOrDie(configuration_basename);
  auto lua_parameter_dictionary =
      absl::make_unique<carto::common::LuaParameterDictionary>(
          code, std::move(file_resolver));
  return lua_parameter_dictionary;
}

 carto::transform::Rigid3d lidarv_to_lidarh(carto::transform::Rigid3d::Vector(-0.00108534, 0.0200793, -0.00103185), 
 carto::transform::Rigid3d::Quaternion(0.999999, 0.000391255, 0.000131068, 0.00121841));

template <typename T>
std::unique_ptr<carto::io::PointsBatch> HandleMessage(
    const T& message, const std::string& tracking_frame,
    const tf2_ros::Buffer& tf_buffer,
    const carto::transform::TransformInterpolationBuffer&
        transform_interpolation_buffer) {
  const carto::common::Time start_time = FromRos(message.header.stamp);

  auto points_batch = absl::make_unique<carto::io::PointsBatch>();
  points_batch->start_time = start_time;
  points_batch->frame_id = message.header.frame_id;
  // carto::transform::Rigid3d lidar_calibrate;
  // if (points_batch->frame_id == "velodyne_v") {
  //   lidar_calibrate = lidarv_to_lidarh;
  // }

  carto::sensor::PointCloudWithIntensities point_cloud;
  carto::common::Time point_cloud_time;
  std::tie(point_cloud, point_cloud_time) =
      ToPointCloudWithIntensities(message);
  CHECK_EQ(point_cloud.intensities.size(), point_cloud.points.size());

  for (size_t i = 0; i < point_cloud.points.size(); ++i) {
    const carto::common::Time time =
        point_cloud_time +
        carto::common::FromSeconds(point_cloud.points[i].time);
    if (!transform_interpolation_buffer.Has(time)) {
      continue;
    }
    const carto::transform::Rigid3d tracking_to_map =
        transform_interpolation_buffer.Lookup(time);
    const carto::transform::Rigid3d sensor_to_tracking =
        ToRigid3d(tf_buffer.lookupTransform(
            tracking_frame, message.header.frame_id, ToRos(time)));
    const carto::transform::Rigid3f sensor_to_map =
        (tracking_to_map * sensor_to_tracking).cast<float>();
       // (lidar_calibrate * tracking_to_map * sensor_to_tracking).cast<float>();
    carto::sensor::RangefinderPoint range_point = carto::sensor::ToRangefinderPoint(point_cloud.points[i]);
// [16:45:30] [Picked]	- P#1037453 (-3.572718;0.801809;-1.431631)
    // carto::sensor::RangefinderPoint world_range_point =  sensor_to_map *
    //     range_point;
    // static Eigen::Vector3f point1 =  {-3.572718,0.801809,-1.431631};

    // if ((world_range_point.position - point1).norm() < 0.0001) {
    //     // LOG(INFO) << "pointcloud 1 lidar time " <<  std::chrono::time_point_cast<std::chrono::milliseconds>(start_time).time_since_epoch().count();
    //     LOG(INFO) << "pointcloud 1 lidar time " <<  message.header.stamp;
    // }

    points_batch->points.push_back( 
        sensor_to_map *
        range_point);
    points_batch->intensities.push_back(point_cloud.intensities[i]);
    // We use the last transform for the origin, which is approximately correct.
    points_batch->origin = sensor_to_map * Eigen::Vector3f::Zero();

  }
  if (points_batch->points.empty()) {
    return nullptr;
  }


  // if (points_batch->frame_id == "velodyne_h") {
  //   static bool first = true;
  //   std::ofstream pose_file;
  //   if (transform_interpolation_buffer.Has(point_cloud_time)) {
  //     if (first) {
  //       pose_file.open("/home/sujin/output/map_build/pose.txt");
  //       first = false;
  //     } else {
  //       pose_file.open("/home/sujin/output/map_build/pose.txt", std::ios_base::app);
  //     }

  //     const carto::transform::Rigid3d tracking_to_map =
  //       transform_interpolation_buffer.Lookup(point_cloud_time);
  //     const carto::transform::Rigid3d sensor_to_tracking =
  //     ToRigid3d(tf_buffer.lookupTransform(
  //         tracking_frame, message.header.frame_id, ToRos(point_cloud_time)));
  //         const carto::transform::Rigid3f sensor_to_map =
  //     (tracking_to_map * sensor_to_tracking).cast<float>();
  //     auto tf_matrix = pose_to_transform(sensor_to_map);
  //     pose_file <<  tf_matrix(0,0) << " " <<  tf_matrix(0,1) << " " << tf_matrix(0,2) << " " << tf_matrix(0,3) 
  //                                           << " " <<  tf_matrix(1,0) << " " <<  tf_matrix(1,1) << " " << tf_matrix(1,2) << " " << tf_matrix(1,3) 
  //                                           << " " << tf_matrix(2,0) << " " <<  tf_matrix(2,1) << " " << tf_matrix(2,2) << " " << tf_matrix(2,3) 
  //                                           << std::endl;
  //     double nearest_time = message.header.stamp.toSec() ;
  //     pcl::PointCloud<pcl::PointXYZI> pcl_point_cloud;
  //     pcl::fromROSMsg(message, pcl_point_cloud); 
  //     std::stringstream file_path; 
  //     file_path << "/home/sujin/output/map_build/"  << std::to_string(nearest_time)<<  ".bin";
  //     std::ofstream output_pcl(file_path.str(), std::ios_base::binary);
  //     output_pcl.write((char*)&pcl_point_cloud.points[0], pcl_point_cloud.points.size() * sizeof(pcl::PointXYZI) );

  //   }
  // }
  return points_batch;
}

}  // namespace

MapsWriter::MapsWriter(const std::vector<std::string>& state_filenames,
                           const std::vector<std::string>& bag_filenames,
                           const std::string& output_file_prefix)
    : bag_filenames_(bag_filenames)
    , stop_flag_(false) {
    for (std::string pose_graph_filename : state_filenames) {
      ::cartographer::mapping::proto::PoseGraph pose_graph(
                carto::io::DeserializePoseGraphFromFile(pose_graph_filename));
      // This vector must outlive the pipeline.
      all_trajectories_ .insert(all_trajectories_.end(), 
            pose_graph.trajectory().begin(), pose_graph.trajectory().end());
    }

  CHECK_EQ(all_trajectories_.size(), bag_filenames_.size())
      << "Pose graphs contains " << all_trajectories_.size()
      << " trajectories while " << bag_filenames_.size()
      << " bags were provided. This tool requires one bag for each "
         "trajectory in the same order as the correponding trajectories in the "
         "pose graph proto.";

  const std::string file_prefix = !output_file_prefix.empty()
                                      ? output_file_prefix
                                      : bag_filenames_.front() + "_";
  point_pipeline_builder_ =
      CreatePipelineBuilder(all_trajectories_, file_prefix);
}

void MapsWriter::RegisterPointsProcessor(
    const std::string& name,
    cartographer::io::PointsProcessorPipelineBuilder::FactoryFunction factory) {
  point_pipeline_builder_->Register(name, factory);
}


void  MapsWriter::SetProgressCallback(ProgressCallback callback) {
  progress_callback_ = callback;
}

constexpr int pointsprocess_times = 3;
constexpr int  frames_per_progresscallback= 1000;

void MapsWriter::Run(const std::string& configuration_directory,
                       const std::string& configuration_basename,
                       const std::string& urdf_filename,
                      double skip_seconds) {
  const auto lua_parameter_dictionary =
      LoadLuaDictionary(configuration_directory, configuration_basename);

  std::vector<std::unique_ptr<carto::io::PointsProcessor>> pipeline =
      point_pipeline_builder_->CreatePipeline(
          lua_parameter_dictionary->GetDictionary("pipeline").get());
  const std::string tracking_frame =
      lua_parameter_dictionary->GetString("tracking_frame");

  float progress_step =  1.0f / bag_filenames_.size() / pointsprocess_times;
  float current_progress = 0.0f;
  stop_flag_ = false;
  do {
    for (size_t trajectory_id = 0; trajectory_id < bag_filenames_.size();
         ++trajectory_id) {
      const carto::mapping::proto::Trajectory& trajectory_proto =
          all_trajectories_[trajectory_id];
      const std::string& bag_filename = bag_filenames_[trajectory_id];
      LOG(INFO) << "Processing " << bag_filename << "...";
      if (trajectory_proto.node_size() == 0) {
        continue;
      }
      tf2_ros::Buffer tf_buffer;
      if (!urdf_filename.empty()) {
        ReadStaticTransformsFromUrdf(urdf_filename, &tf_buffer);
      }

      const carto::transform::TransformInterpolationBuffer
          transform_interpolation_buffer(trajectory_proto);
      rosbag::Bag bag;
      bag.open(bag_filename, rosbag::bagmode::Read);
      rosbag::View view(bag);
      const ::ros::Time begin_time = view.getBeginTime();
      const double duration_in_seconds =
          (view.getEndTime() - begin_time).toSec();

      const ::ros::Time kSkip = begin_time + ::ros::Duration(skip_seconds);
      uint32_t msg_counter = 0;

      for (const rosbag::MessageInstance& message : view) {
          if (message.getTime() < kSkip) {
            // LOG(INFO) <<  "SKIP " << message.getTime();
            continue;
          }
          if (stop_flag_) 
            break;
          std::unique_ptr<carto::io::PointsBatch> points_batch;
          if (message.isType<sensor_msgs::PointCloud2>()) {
            points_batch = HandleMessage(
                *message.instantiate<sensor_msgs::PointCloud2>(),
                tracking_frame, tf_buffer, transform_interpolation_buffer);
          } 
          if (points_batch != nullptr) {
            points_batch->trajectory_id = trajectory_id;
            // if (points_batch->frame_id == "velodyne_h") 
            pipeline.back()->Process(std::move(points_batch));
          }
        LOG_EVERY_N(INFO, 10000)
            << "Processed " << (message.getTime() - begin_time).toSec()
            << " of " << duration_in_seconds << " bag time seconds...";
        if ((++msg_counter % frames_per_progresscallback) == 0) { 
          if (progress_callback_ != nullptr) {
            float progress =  progress_step * (message.getTime() - begin_time).toSec() /   duration_in_seconds + current_progress  ;
            progress_callback_(progress* 100.0f) ;
          }
        }
      }
      bag.close();
      current_progress += progress_step;
      if (stop_flag_) 
        break;
    }
    if (stop_flag_) 
      break; 
  } while (pipeline.back()->Flush() ==
           carto::io::PointsProcessor::FlushResult::kRestartStream);
  if (!stop_flag_ && progress_callback_ != nullptr) {
    progress_callback_(100.0f) ;
  }

}

void MapsWriter::Stop() {
  stop_flag_ = true;
}

::cartographer::io::FileWriterFactory MapsWriter::CreateFileWriterFactory(
    const std::string& file_path) {
  const auto file_writer_factory = [file_path](const std::string& filename) {
    return absl::make_unique<carto::io::StreamFileWriter>(file_path + filename);
  };
  return file_writer_factory;
}

}
}  // namespace yida_mapping
