-- Copyright 2016 The Cartographer Authors
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.


options = {
  pcl_filter = {
    filter_pcl = true,
    mean_k = 40,
    std_mul = 1.0,
    search_radius = 0.02,
    min_neighbors_in_radius = 5,
  },
  build = {
    skip_seconds = 5.0,
    do_landmark = true,
    rotate_image_mode = 0,
    tag_family = "tag36h11",
    max_distance = 2.0,
    max_fov = 45.0,

    translation_weight = 100000.,
    rotation_weight = 100000.,
    tag_size = 0.16,
    ref_frame_id = "velodyne_h",
    image_frame_ids = { "spinnaker_picture_left","spinnaker_picture_right"},
    calibration_basenames = { "left_fisheye_lidar_calibration.yaml",  "right_fisheye_lidar_calibration.yaml"},
    -- image_frame_ids = { "spinnaker_picture_left","spinnaker_picture_right"},
    -- calibration_basenames = { "left_fisheye_lidar_calibration.yaml","right_fisheye_lidar_calibration.yaml"},

    build_resolution = 0.1,
    merge_resolution = 0.05,

  },

}

return options
