#ifndef _LIBMAP_BUILD_H
#define _LIBMAP_BUILD_H


#if (defined _WIN32 || defined WINCE || defined __CYGWIN__) && defined  LIBmap_scanner_EXPORTS
#define LIBmap_scanner_API __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define LIBmap_scanner_API __attribute__ ((visibility ("default")))
#else
#define LIBmap_scanner_API
#endif

#include <memory>
#include <stdint.h>
#include <memory>

namespace yida_mapping{
namespace map_build 
{

class MapBuildInterface;

enum MapBuilderType {
  CARTOGRAPHER,
 TYPE_NUMBER,
};

  /******************************************************************************
  * \fn CreateMapBuilderr
  * Create Time: 2020/06/02
  * Description: -
  *   创建一个新的扫描组件实例
  *
  * \param type 输入参数
  * 		扫描组件实例类型
  *
  * \return
  * 		扫描组件实例
  *
  * \note 
  *******************************************************************************/
std::shared_ptr<MapBuildInterface> CreateMapBuilder(MapBuilderType type = CARTOGRAPHER);

} // namespace map_build
}// namespace yida_mapping

#endif  
