#include "camera_insta360.h"

#include <iostream>
#include <fstream>

#include <chrono>
#include <boost/filesystem.hpp>

#include "common.h"
#include "camera/camera.h"
#include "camera/photography_settings.h"
#include "camera/device_discovery.h"

#include "cam_capture/capture_const.h"

namespace yida_mapping {
    namespace cam_capture {
        std::shared_ptr<ins_camera::Camera> CameraInsta360::camera_;

        CameraInsta360::CameraInsta360() : initialized_(false) {
            service_servers_.push_back(node_handle_.advertiseService(
                  kCamCaptureServiceName, &CameraInsta360::HandleCamCaptureCmd, this));
        }
        CameraInsta360::~CameraInsta360() { Deinitialize(); }

        int CameraInsta360::SetParam(const CaptureParam & param) {
            parameter_ = param;
            return 0;
        }
        int CameraInsta360::Initialize() {
            std::cout << "CameraInsta360::Initialize!" << std::endl;

            if(initialized_) return 0;

            int result = open_device();
            if(0 != result) return result;

            for(size_t  i = 0; i < 2; i++) {
              image_transport::ImageTransport it(node_handle_);
              image_publishers_.push_back(it.advertise(parameter_.topic_names[i], 1));
            }

            initialized_ = true;
            return 0;
        }

        void CameraInsta360::Deinitialize() {
            if(!initialized_) return;

            // close_device();
            publisher_image_left_.shutdown();
            publisher_image_right_.shutdown();

            std::cout << "Deleting camera files" << std::endl;

            if(camera_) {
                auto camera_files =  camera_->GetCameraFilesList();
                for(const auto & camere_file : camera_files) {
                    if(!camera_->DeleteCameraFile(camere_file)) break;
                }

                std::cout << "Deletion succeed" << std::endl;
                //camera_->Close();
                std::cout << "camera Closed" << std::endl;
            }

            initialized_ = false;
        }

        int CameraInsta360::open_device() {
            if(camera_) return 0;

            std::cout << "begin open camera" << std::endl;
            ins_camera::DeviceDiscovery discovery;
            auto list = discovery.GetAvailableDevices();

            for(int i = 0;i < list.size(); ++i) {
                auto desc = list[i];

                std::cout << "serial:"      << desc.serial_number    << "\t"
                          << "camera type:" << int(desc.camera_type) << "\t"
                          << "lens type:"   << int(desc.lens_type)   << std::endl;
            }

            if(list.size() <= 0) {
                std::cout << "no device found." << std::endl;
                return -1;
            }

            camera_       = std::make_shared<ins_camera::Camera>(list[0].info);
            int try_times = 5;

            while(!camera_->Open()) {
		            //std::cout << "failed to open camera" << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(200));
                try_times--;
                if(try_times == 0) break;
	          }

            if(try_times == 0) {
                std::cout << "failed to open camera" << std::endl;
                camera_.reset();
                return -1;
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return 0;
        }

        void* RunSnapshot(void *arg) {
            std::cout << "!!!!!!!!!!!!!!!!!!!!CameraInsta360::RunSnapshot()" << std::endl;
            CameraInsta360* self = (CameraInsta360*)arg;
            self->Snapshot();
        }

        int CameraInsta360::Snapshot() {
            std::cout << "!!!!!!!!!!!!!!!!!!!!CameraInsta360::Snapshot()" << std::endl;

            double time_now = ros::Time::now().toSec();
            const auto url  = camera_->TakePhoto();

            std_msgs::Bool msg;
            msg.data = false;
            camCaptureStatePub.publish(msg);

            std::cout << "!!!!!!!!!!!!!!!!!!!!CameraInsta360::Excute Capture Command" << std::endl;

            if(!url.IsSingleOrigin() || url.Empty()) {
                std::cout << "failed to take picture" << std::endl;
                return -1;
            }

            double time_takephoto = time_now + 0.1;

            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            std::string file_to_download = url.GetSingleOrigin();
            std::cout << "Take a picture : " << file_to_download << std::endl;

            vector<std::string> download_str;
            SplitString(file_to_download, download_str, "/");

            vector<std::string> image_name;
            SplitString(download_str[3], image_name, ".");
            std::cout << "image_name: " << image_name[0] << std::endl;

            //std::string file_to_save = "/dev/shm/download.jpg";
            std::string file_to_save = "/dev/shm/" + image_name[0] + ".jpg";
            std::cout << "file_to_download: " << file_to_download << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(50));

            cout << "START DOWNLOAD IMAGE FILE" << endl;

            const auto ret = camera_->DownloadCameraFile(file_to_download, file_to_save);
            if(ret) {
                std::cout << "Download & Save " << file_to_download << " as local jpg succeed!" << std::endl;
                std::cout << "Start cv::imread jpg" << endl;

                //IplImage *pSrc = cvLoadImage(file_to_save.c_str(), CV_LOAD_IMAGE_COLOR);
                cv::Mat src_image = cv::imread(file_to_save, cv::IMREAD_COLOR);

                std::cout << "cv::imread END" << endl;

                if(src_image.empty()) {
                    std::cout << "图像文件加载失败" << std::endl;
                    //return 0;
                } else {
                    //cv::Mat frame_left  = src_image(cv::Rect(0, 0, 3040, 3040));
                    //cv::Mat frame_right = src_image(cv::Rect(3040, 0, 3040, 3040));

                    std::cout << "Start get image_frames" << endl;
                    std::vector<cv::Mat > image_frames ={src_image(cv::Rect(0, 0, 3040, 3040)),
                                                         src_image(cv::Rect(3040, 0, 3040, 3040))};
                    std::cout << "Get image_frames END" << endl;

                    for(size_t i = 0; i < image_frames.size(); i++) {
                        sensor_msgs::ImagePtr img_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image_frames[i]).toImageMsg();
                        img_msg->header.stamp = ros::Time().fromSec(time_takephoto);
                        img_msg->header.frame_id = parameter_.frame_ids[i];
                        image_publishers_[i].publish(img_msg);
                    }

                    /*
                    sensor_msgs::ImagePtr msg_right = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame_right).toImageMsg();
                    msg_right->header.stamp    = ros::Time().fromSec(time_takephoto);
                    msg_right->header.frame_id = parameter_.frame_ids[1];
                    publisher_image_right_.publish(msg_right);

                    sensor_msgs::ImagePtr msg_left = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame_left).toImageMsg();
                    msg_left->header.stamp    = ros::Time().fromSec(time_takephoto);
                    msg_left->header.frame_id = parameter_.frame_ids[0];
                    publisher_image_left_.publish(msg_left);
                    */
                }

                std::cout << "Will remove local jpg" << endl;
                boost::filesystem::remove(boost::filesystem::path(file_to_save));
                std::cout << "Remove local jpg END" << endl;
            } else {
                std::cout << "Download " << file_to_download << " failed!" << std::endl;
            }

            //if(camera_->DeleteCameraFile(file_to_download)) {
            //    std::cout << "Deletion succeed" << std::endl;
            //}

            return 0;
        }

        bool CameraInsta360::HandleCamCaptureCmd(
            ::map_ros_msgs::CamCaptureCmd::Request& request,
            ::map_ros_msgs::CamCaptureCmd::Response& response) {

            std::cout << "!!!!!!!!!!!!!!!!!!!!CameraInsta360::HandleCamCaptureCmd()" << std::endl;

            switch(request.command_id) {
                case (uint32_t)CamCaptureID::OPEN_DEVICE: { return HandleOpenDevice(); }
                case (uint32_t)CamCaptureID::SNAPSHOT: { return HandleSnapshot(); }
                default: { return false; }
            }

            return true;
        }
        bool CameraInsta360::HandleOpenDevice() {
            std::cout << "!!!!!!!!!!!!!!!!!!!!CameraInsta360::HandleOpenDevice()" << std::endl;
            if(0 != Initialize()) return false;
            return true;
        }
        bool CameraInsta360::HandleSnapshot() {
            std::cout << "!!!!!!!!!!!!!!!!!!!!CameraInsta360::HandleSnapshot()" << std::endl;

            if(!initialized_) return false;

            pthread_t snapshot_thread = 0;
            pthread_create(&snapshot_thread, NULL, RunSnapshot, (void*)this);

            return true;
        }
    }
}
