#ifndef __CameraInsta360_H__
#define __CameraInsta360_H__

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <pwd.h>
#include <sys/time.h>
#include <memory>

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/Imu.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>

#include "camera/camera.h"
#include "camera/photography_settings.h"
#include "camera/device_discovery.h"

#include "map_ros_msgs/CamCaptureCmd.h"

#define __app_name__ "CameraInsta360"

#include <thread>

using namespace std;

namespace ins_camera { class Camera; }
namespace yida_mapping {
    namespace cam_capture {
        class CameraInsta360 {
            public:
                ros::Publisher camCaptureStatePub;

                struct CaptureParam {
                    CaptureParam() {
                        topic_names.push_back("/camera/image_left");
                        topic_names.push_back("/camera/image_right");
                        frame_ids.push_back("spinnaker_picture_left");
                        frame_ids.push_back("spinnaker_picture_right");
                    }

                    std::vector<std::string> topic_names; //urdf文件路径
                    std::vector<std::string> frame_ids;   //urdf文件路径
                };

            public:
                CameraInsta360();
                ~CameraInsta360();

                int SetParam(const CaptureParam & param);
                int Initialize();
                void Deinitialize();
                int Snapshot ();

            private:
                int open_device();
                bool HandleCamCaptureCmd(
                    ::map_ros_msgs::CamCaptureCmd::Request& request,
                    ::map_ros_msgs::CamCaptureCmd::Response& response);

                bool HandleOpenDevice();
                bool HandleSnapshot();

                ros::NodeHandle node_handle_;

                std::vector<::ros::ServiceServer> service_servers_;
                std::vector<image_transport::Publisher> image_publishers_;

                image_transport::Publisher publisher_image_right_;
                image_transport::Publisher publisher_image_left_;

                CaptureParam parameter_;

                static std::shared_ptr<ins_camera::Camera> camera_;
                bool initialized_;
        };
    }
}
#endif
