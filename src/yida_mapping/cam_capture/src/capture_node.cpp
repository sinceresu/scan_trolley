#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "camera_insta360.h"

DEFINE_string(left_camera_topic_name, "/camera/image_left",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_string(right_camera_topic_name, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_string(left_camera_frameid, "spinnaker_picture_left",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_string(right_camera_frameid, "spinnaker_picture_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

using namespace std;

namespace yida_mapping {
    namespace cam_capture {
        void Run(int argc, char** argv) {
            yida_mapping::cam_capture::CameraInsta360 camera_controller;
            yida_mapping::cam_capture::CameraInsta360::CaptureParam param;

            param.topic_names = {FLAGS_left_camera_topic_name, FLAGS_right_camera_topic_name};
            param.frame_ids   = {FLAGS_left_camera_frameid, FLAGS_right_camera_frameid};

            camera_controller.SetParam(param);

            ros::NodeHandle n;
            camera_controller.camCaptureStatePub = n.advertise<std_msgs::Bool>("/cam_capture_state", 1);

            ROS_INFO("camera capture node started!");

            ros::spin();
            camera_controller.Deinitialize();
        }
    }
}

int main(int argc, char** argv) {
    google::InitGoogleLogging(argv[0]);
    google::ParseCommandLineFlags(&argc, &argv, true);

    ::ros::init(argc, argv, "map_scanner");
    ::ros::start();
    yida_mapping::cam_capture::Run(argc, argv);
}
