#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "src/camera_insta360.h"



DEFINE_string(left_camera_topic_name, "/camera/image_left",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_string(right_camera_topic_name, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_string(left_camera_frameid, "spinnaker_picture_left",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_string(right_camera_frameid, "spinnaker_picture_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;

namespace yida_mapping {
namespace cam_capture{

void Test(int argc, char** argv) {
  yida_mapping::cam_capture::CameraInsta360 camera_controller;
    yida_mapping::cam_capture::CameraInsta360::CaptureParam param;
    param.topic_names = {FLAGS_left_camera_topic_name, FLAGS_right_camera_topic_name};
    param.frame_ids = {FLAGS_left_camera_frameid, FLAGS_right_camera_frameid};

  int result;
  camera_controller.SetParam(param);
  for (int i = 0; i < 2; i++) {
    result= camera_controller.Initialize();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    CHECK(result == 0) << "Failed to open inst360";
    camera_controller.Snapshot();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    camera_controller.Deinitialize();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    LOG(INFO) << "circle:" << i;
 

  }
  result= camera_controller.Initialize();
  CHECK(result == 0) << "Failed to open inst360";
  for (int i = 0; i < 10; i++) {
    camera_controller.Snapshot();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  }
  camera_controller.Deinitialize();


}

}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  ::ros::init(argc, argv, "map_scanner");
  ::ros::start();
  yida_mapping::cam_capture::Test(argc, argv);

}
