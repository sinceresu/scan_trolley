#pragma once

namespace yida_mapping {
    namespace cam_capture {
        constexpr char kCamCaptureServiceName[] = "cam_capture";

        enum class CamCaptureID {
            OPEN_DEVICE = 0,
            SNAPSHOT    = 1
        };
    }
}
