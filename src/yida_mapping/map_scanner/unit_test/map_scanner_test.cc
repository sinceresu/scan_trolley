#include "gflags/gflags.h"
#include <stdio.h>
#include <ros/ros.h>

#include "cartographer/io/image.h"
#include "cartographer_ros/ros_log_sink.h"

#include "map_scanner/carto_map_scanner.h"

DEFINE_bool(collect_metrics, false,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");
DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");

DEFINE_string(load_state_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_bool(load_frozen_state, true,
            "Load the saved state as frozen (non-optimized) trajectories.");
DEFINE_bool(
    start_trajectory_with_default_topics, true,
    "Enable to immediately start the first trajectory with default topics.");
DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");
DEFINE_string(
    scan_name, "test2",
    "If non-empty, serialize state and write it to disk before shutting down.");

void SaveBmpFile(uint32_t *img, int w, int h, const char *filename)  
{      
	FILE *f;
	int filesize = 54 + (3*w+3)/4*4*h;  //w is your image width, h is image height, both int

	unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
	unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
	unsigned char bmppad[3] = {0,0,0};

	bmpfileheader[ 2] = (unsigned char)(filesize    );
	bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
	bmpfileheader[ 4] = (unsigned char)(filesize>>16);
	bmpfileheader[ 5] = (unsigned char)(filesize>>24);

	bmpinfoheader[ 4] = (unsigned char)(       w    );
	bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
	bmpinfoheader[ 6] = (unsigned char)(       w>>16);
	bmpinfoheader[ 7] = (unsigned char)(       w>>24);
	bmpinfoheader[ 8] = (unsigned char)(       h    );
	bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
	bmpinfoheader[10] = (unsigned char)(       h>>16);
	bmpinfoheader[11] = (unsigned char)(       h>>24);

	f = fopen(filename,"wb");
	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);
	uint32_t * line = img + w * (h - 1 );
	for(int i=0; i<h; i++)
	{
		for (int j = 0; j < w ; j+= 1) {
            uint32_t pixel_value = line[j];
			uint8_t  rgb[] = { (unsigned char)(pixel_value>>16),  (unsigned char)(pixel_value>>8),   (unsigned char)(pixel_value)};
			fwrite(rgb,3,1,f);
		}
		line -= w;
		fwrite(bmppad,1,(4-(w*3)%4)%4,f);
	}
	fclose(f);
	
}
std::string save_map_image_path;
void OnUpdateOccupancyImage(::yida_mapping::Image occupancy_image){
    SaveBmpFile(occupancy_image.pixels.data(),  occupancy_image.width, occupancy_image.height, save_map_image_path.c_str());
}

#include <memory>
using namespace std;
namespace yida_mapping{
namespace map_scanner{
namespace {
 std::shared_ptr<CartoMapScanner>  scanner;

void StartScan(int argc, char** argv) {
    ::ros::init(argc, argv, "map_scanner");
    ::ros::start();
    MapScannerInterface::ScanParam param;
    param.urdf_filename = FLAGS_urdf_filename;
    param.configuration_directory = FLAGS_configuration_directory;
    param.configuration_basename = FLAGS_configuration_basename;
    param.collect_metrics = FLAGS_collect_metrics;

    scanner = make_shared<CartoMapScanner>();
    scanner->SetParam(param);
    scanner->SubscribeOccupancyImageCallback(OnUpdateOccupancyImage);
    save_map_image_path   = std::string(FLAGS_save_directory) +  std::string("/") + std::string(FLAGS_scan_name) + std::string("_occupancy.bmp");

    scanner->StartScan(FLAGS_save_directory, FLAGS_scan_name);
}
void StopScan() { 
    scanner->StopScan();
    scanner.reset();
    
}
}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";
  CHECK(!FLAGS_configuration_basename.empty())
      << "-configuration_basename is missing.";

  cartographer_ros::ScopedRosLogSink ros_log_sink;

  yida_mapping::map_scanner::StartScan(argc, argv);

  getchar();
 
  yida_mapping::map_scanner::StopScan();
}
