#include "include/libmap_scanner.h"
// #include "Houghmap_scanner.h"
// #include "Contourmap_scanner.h"
#include "carto_map_scanner.h"

namespace yida_mapping{
namespace map_scanner{

std::shared_ptr<MapScannerInterface> CreateMapScanner(MapScannerType type)
{
    switch (type)
    {
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_scanner();
    case CARTOGRAPHER:
        return std::shared_ptr<MapScannerInterface>(new CartoMapScanner());
    default:
        return nullptr;
    }
}

} // namespace map_scanner
}// namespace yida_mapping
