#include "carto_map_scanner.h"
#include <thread>
#include <chrono>

#include "absl/memory/memory.h"
#include "cartographer/mapping/map_builder.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/static_transform_broadcaster.h"
#include "urdf/model.h"

#include "cartographer_ros/node.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/assets_writer.h"

#include "cartographer_ros/node_options.h"
#include "cartographer_ros/ros_log_sink.h"
#include "cartographer_ros_msgs/StatusCode.h"
#include "cartographer_ros_msgs/StatusResponse.h"

#include "absl/strings/str_split.h"
#include "map_common/err_code.h"


using namespace std;
namespace yida_mapping{
namespace map_scanner{
constexpr double kTfBufferCacheTimeInSeconds = 10.;
tf2_ros::Buffer tf_buffer{::ros::Duration(kTfBufferCacheTimeInSeconds)};
CartoMapScanner::CartoMapScanner() 
: work_directory_("./"),
started_(false)
{

} 

int CartoMapScanner::SetParam(const ScanParam & param) {
    if (param.configuration_directory.empty()) 
        return ERRCODE_INVALIDARG;
    if (param.configuration_basename.empty()) 
        return ERRCODE_INVALIDARG;       
    parameter_ = param;

    return ERRCODE_OK;
}


int CartoMapScanner::SubscribeOccupancyImageCallback(OccupancyImageCallback callback)  {
  observer_ = callback;
  return ERRCODE_OK;
}
 
int CartoMapScanner::StartScan(const std::string & work_directory, const std::string & scan_name) {
  work_directory_ = work_directory;
  scan_name_ = scan_name;
  if (!started_) {
    DoWork();
  }

  return ERRCODE_OK;
}


int CartoMapScanner::StopScan() {
  observer_ = nullptr;
  async_spinner_->stop();

  // scan_node_->FinishAllTrajectories();
  //  scan_node_->RunFinalOptimization();

  // SaveState();

  started_ = false;
  return ERRCODE_OK;
}

int CartoMapScanner::BuildMap(const std::string& bag_filepath, const std::string& pose_graph_filepath, const std::string & output_file_prefix)  {
  const std::vector<std::string> bag_filepaths = {bag_filepath};
  ::cartographer_ros::AssetsWriter asset_writer(
      pose_graph_filepath,
      bag_filepaths, 
      output_file_prefix
      );

  asset_writer.Run(parameter_.configuration_directory, parameter_.write_configuration_basename,
                   parameter_.urdf_filename, false);
  return ERRCODE_OK;

}

void CartoMapScanner:: OnUpdateOccupancyImage(std::unique_ptr<cartographer::io::Image> occupancy_image) {
  if (observer_) {
    Image image ;
    image.width = occupancy_image->width();
    image.height = occupancy_image->height();
    const int num_pixels = image.width * image.height;
    image.pixels.reserve(num_pixels);

    auto surface = occupancy_image->GetCairoSurface();

    const uint32_t* pixel_data =
      reinterpret_cast<uint32_t*>(cairo_image_surface_get_data(surface.get()));

    for (int i = 0; i < num_pixels; ++i) {
      image.pixels.push_back(pixel_data[i]);
    }
    observer_(move(image));
  }
}

void CartoMapScanner::DoWork() {
  std::vector<geometry_msgs::TransformStamped> urdf_transforms;
  const auto current_urdf_transforms =
      ReadStaticTransformsFromUrdf(parameter_.urdf_filename, &tf_buffer);
  urdf_transforms.insert(urdf_transforms.end(),
                          current_urdf_transforms.begin(),
                          current_urdf_transforms.end());

  tf_buffer.setUsingDedicatedThread(true);
  ::tf2_ros::StaticTransformBroadcaster static_tf_broadcaster;
  if (urdf_transforms.size() > 0) {
    static_tf_broadcaster.sendTransform(urdf_transforms);
  }
  // tf2_ros::TransformListener tf(tf_buffer);
  NodeOptions node_options;
  TrajectoryOptions trajectory_options;
  std::tie(node_options, trajectory_options) =
      LoadOptions(parameter_.configuration_directory, parameter_.configuration_basename);

  auto map_builder = absl::make_unique<cartographer::mapping::MapBuilder>(
    node_options.map_builder_options);

  scan_node_ = std::make_shared<Node>(node_options, std::move(map_builder), &tf_buffer, parameter_.collect_metrics);
  scan_node_->SubscribeOccupancyImage(std::bind(&CartoMapScanner::OnUpdateOccupancyImage, this, std::placeholders::_1));
  scan_node_->StartTrajectoryWithDefaultTopics(trajectory_options);

  async_spinner_ = make_shared<ros::AsyncSpinner>(1); 
  async_spinner_->start();

  started_ = true;

}


int CartoMapScanner::SaveState()   {
  const std::string save_state_name = work_directory_ + "/" + scan_name_ +".pbstream" ;
  scan_node_->SerializeState(save_state_name,
                      true /* include_unfinished_submaps */);

  return ERRCODE_OK;
}
} // namespace map_scanner
}// namespace yida_mapping
