#ifndef _CARTO_MAP_SCANNER_H
#define _CARTO_MAP_SCANNER_H

#include <vector>
#include <memory>
#include <thread>
#include "ros/spinner.h"
#include "cartographer/io/image.h"

#include "../include/map_scanner_interface.h"

namespace cartographer_ros{
class Node;
class NodeObserverInterface;
}

using namespace cartographer_ros;

namespace yida_mapping{
namespace map_scanner{

class CartoMapScanner : public MapScannerInterface
{
public:
  CartoMapScanner();
  ~CartoMapScanner(){};

  virtual  int SetParam(const ScanParam & param) override ;
  virtual  int SubscribeOccupancyImageCallback(OccupancyImageCallback callback)  override ;
  virtual  int StartScan(const std::string & work_directory, const std::string & scan_name) override ;
  virtual  int StopScan() override ;
  virtual  int BuildMap(const std::string& bag_filepath, const std::string& pose_graph_filepath, const std::string & output_file_prefix)  override;
private:
  void DoWork();

  int SaveState();

  void OnUpdateOccupancyImage(std::unique_ptr<cartographer::io::Image>);

  ScanParam parameter_;
  std::string work_directory_;
  OccupancyImageCallback observer_;
  bool started_;
  std::shared_ptr<Node>  scan_node_;
  std::string scan_name_;
  int current_trajectory_id_ = -1;
  std::shared_ptr<ros::AsyncSpinner> async_spinner_;

};
} // namespace map_scanner
}// namespace yida_mapping

#endif  