#ifndef _MAP_SCANNER_INTERFACE_H
#define _MAP_SCANNER_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <functional>

#include <stdint.h>


#include "map_common/global.h"

namespace yida_mapping{
namespace map_scanner{

      
class MapScannerInterface
{

  
public:

  using OccupancyImageCallback =
      std::function<void(::yida_mapping::Image)>;

  typedef struct ScanParam {
    std::string urdf_filename;  //urdf文件路径
    std::string configuration_directory;    //配置文件目录
    std::string configuration_basename;    //配置文件名
    std::string write_configuration_basename;    //配置文件名
    bool collect_metrics;       //收集度量数据
    std::string remappings;   

  }ScanParam;


  virtual ~MapScannerInterface(){};

    /******************************************************************************
  * \fn MapScannerInterface.SetParam
  * Create Time: 2020/06/02
  * Description: -
  *    设置扫描参数
  *
  * \param param 输入参数
  * 		各项扫描参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const ScanParam & param) = 0 ;

  /******************************************************************************
  * \fn MapScannerInterface.SubscribeOccupancyImage
  * Create Time: 2020/06/02
  * Description: -
  *    设置扫描观察者，用于在扫描过程中返回扫描状态。
  *
  * \param callback 输入参数
  * 		观察者
  * 
  * \return
  * 		当前工作目录
  *
  * \note 
  *******************************************************************************/
  virtual int SubscribeOccupancyImageCallback(OccupancyImageCallback callback) = 0 ;  

    /******************************************************************************
  * \fn MapScannerInterface.StartScan
  * Create Time: 2020/06/02
  * Description: -
  *    创建并启动新的扫描过程。
  * \param work_directory 输入参数
  * 		工作目录路径
  *
  * \param scan_name 输入参数
  * 		轨迹名称
  *
  * \return
  * 		错误码
  *
  * \note 扫描模块会在工作目录下建立与轨迹同名的子目录，将扫描结果保存到该子目录下
  *******************************************************************************/
  virtual  int StartScan(const std::string & work_directory, const std::string & scan_name) = 0 ;

      /******************************************************************************
  * \fn MapScannerInterface.StopScan
  * Create Time: 2020/06/02
  * Description: -
  *    结束当前扫描过程
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int StopScan() = 0 ;

  virtual  int BuildMap(const std::string& bag_filepath, const std::string& pose_graph_filepath, const std::string & output_file_prefix) = 0 ;

};
} // namespace map_scanner
}// namespace yida_mapping

#endif  
