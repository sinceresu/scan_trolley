#include "glog/logging.h"
#include "gflags/gflags.h"
#include <stdio.h>
#include <boost/filesystem.hpp>

#include <ros/ros.h>

#include "absl/strings/str_split.h"

#include "cartographer_ros/node_options.h"
#include "cartographer_ros/ros_log_sink.h"

#include "map_calibrate/carto_map_calibrater.h"

 DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(write_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");
DEFINE_string(calibrate_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");  
DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");   

DEFINE_string(state_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


DEFINE_string(
    bag_filenames, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");


DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");


#include <memory>
using namespace std;

namespace yida_mapping{
using namespace cartographer_ros;

namespace map_calibrate{
namespace {
 std::shared_ptr<CartoMapCalibrater>  calibrater;

void CalibrateMaps(int argc, char** argv) {

    ::ros::init(argc, argv, "map_calibrate");
    ::ros::start();

    const std::vector<std::string> bag_filenames =
      absl::StrSplit(FLAGS_bag_filenames, ',', absl::SkipEmpty());
    LOG(INFO) << "FLAGS_bag_filenames"   << FLAGS_bag_filenames;

    if (!boost::filesystem::exists(FLAGS_save_directory)) {
          boost::filesystem::create_directories(FLAGS_save_directory);
    }
    calibrater = make_shared<CartoMapCalibrater>();

    MapCalibrateInterface::CalibrateParam param;
    param.urdf_filename = FLAGS_urdf_filename;
    param.configuration_directory = FLAGS_configuration_directory;
    param.write_configuration_basename = FLAGS_write_configuration_basename;
    param.calibrate_configuration_basename = FLAGS_calibrate_configuration_basename;
    calibrater->SetParam(param);
    calibrater->Calibrate(FLAGS_state_filename, bag_filenames, FLAGS_save_directory);
    LOG(INFO) << "calibrate ok!" ;

}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  cartographer_ros::ScopedRosLogSink ros_log_sink;

  yida_mapping::map_calibrate::CalibrateMaps(argc, argv);

  getchar();
  return 1;
}
