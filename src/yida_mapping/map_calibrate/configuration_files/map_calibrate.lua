-- Copyright 2016 The Cartographer Authors
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.


options = {
  pcl_filter = {
    filter_pcl = true,
    mean_k = 40,
    std_mul = 0.8,
    search_radius = 0.1,
    min_neighbors_in_radius = 20,
  },
  registration = {
    points_frame_ids = { "velodyne_h","velodyne_v"},
    sampling_ratio = 1.0,
    icp_iterations = 50000,
    max_correspondence_distance = 0.1,
    euclidean_fitness_epsilon = 0.0001
  }
  
}

return options
