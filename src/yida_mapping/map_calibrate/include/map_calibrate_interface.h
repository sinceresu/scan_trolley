#ifndef _MAP_CALIBRATE_INTERFACE_H
#define _MAP_CALIBRATE_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <stdint.h>
#include <vector>

#include "map_common/global.h"

namespace yida_mapping 
{

namespace map_calibrate
{

class MapCalibrateInterface
{
public:
      
  typedef struct CalibrateParam {
    std::string urdf_filename;  //urdf文件路径
    std::string configuration_directory;    //配置文件目录
    std::string calibrate_configuration_basename;    //配置文件目录
    std::string write_configuration_basename;    //地图生成配置文件名
  }CalibrateParam;


  virtual ~MapCalibrateInterface(){};

    /******************************************************************************
  * \fn MapCalibrateInterface.SetOptionFiles
  * Create Time: 2020/06/02
  * Description: -
  *    设置拼图配置文件
  *
  * \param param 输入参数
  * 		urdf 文件路径
  *
  * \param config_file_name 输入参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const CalibrateParam & param) = 0 ;

  virtual  int Calibrate(const std::string& state_filepath, const std::vector<std::string>& bag_filepaths, const std::string& save_directory) = 0 ;
  virtual int CalibratePcls(const std::string& state_filepath, const std::vector<std::string>& pcl_filepaths, const std::string& save_directory) =  0;


};
} // namespace map_calibrate
}// namespace yida_mapping

#endif  
