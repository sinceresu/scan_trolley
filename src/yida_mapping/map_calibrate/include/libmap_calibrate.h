#ifndef _LIBMAP_CALIBRATE_H
#define _LIBMAP_CALIBRATE_H


#if (defined _WIN32 || defined WINCE || defined __CYGWIN__) && defined  LIBmap_scanner_EXPORTS
#define LIBmap_scanner_API __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define LIBmap_scanner_API __attribute__ ((visibility ("default")))
#else
#define LIBmap_scanner_API
#endif

#include <memory>
#include <stdint.h>

namespace yida_mapping{
namespace map_calibrate 
{

class MapCalibrateInterface;

enum MapCalibrateType {
  CARTOGRAPHER,
 TYPE_NUMBER,
};

  /******************************************************************************
  * \fn CreateMapCalibrater
  * Create Time: 2020/06/02
  * Description: -
  *   创建一个新的扫描组件实例
  *
  * \param type 输入参数
  * 		扫描组件实例类型
  *
  * \return
  * 		扫描组件实例
  *
  * \note 
  *******************************************************************************/
std::shared_ptr<MapCalibrateInterface> CreateMapCalibrater(MapCalibrateType type = CARTOGRAPHER);

} // namespace map_calibrate
}// namespace yida_mapping

#endif  
