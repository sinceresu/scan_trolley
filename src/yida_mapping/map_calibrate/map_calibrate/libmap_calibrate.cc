#include "include/libmap_calibrate.h"
#include "carto_map_calibrater.h"

namespace yida_mapping{
namespace map_calibrate{

std::shared_ptr<MapCalibrateInterface> CreateMapCalibrater(MapCalibrateType type)
{
    switch (type)
    {
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_calibrate();
    case CARTOGRAPHER:
        return std::shared_ptr<MapCalibrateInterface>(new CartoMapCalibrater());
    default:
        return nullptr;
    }
}

} // namespace map_calibrate
}// namespace yida_mapping
