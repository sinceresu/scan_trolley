#include "carto_map_calibrater.h"

#include <thread>
#include <chrono>

#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer/transform/transform.h"
#include "cartographer/io/proto_stream_serializer.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"

#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/dev/output_pbstream_trajectories.h"

#include "rosbag/bag.h"
#include "rosbag/view.h"

#include "tf2_ros/buffer.h"
#include "urdf/model.h"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Matrix3x3.h"

#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <opencv2/core/core.hpp>

#include "map_common/err_code.h"

#include "include/libmap_calibrate.h"
#include "include/map_calibrate_interface.h"
#include "ceres_pose.h"
#include "rotation_delta_cost_functor_3d.h"
#include "translation_delta_cost_functor_3d.h"

using namespace std;

namespace yida_mapping {
    using namespace cartographer_ros;

    namespace map_calibrate {
        namespace {
            namespace carto = ::cartographer;

            typedef pcl::PointXYZ PointT;
            typedef pcl::PointCloud<PointT> PointCloud;
            typedef pcl::PointNormal PointNormalT;
            typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

            void FilterPCLWithRor(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl,
                                  double search_radius, int min_neighbors_in_radius) {
                pcl::RadiusOutlierRemoval<PointT> ror_filter;
                ror_filter.setInputCloud(input_pcl);
                ror_filter.setRadiusSearch(search_radius);
                ror_filter.setMinNeighborsInRadius(min_neighbors_in_radius);
                ror_filter.filter(*output_pcl);
            }
            void FilterPCLWithSor(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl,
                                  int mean_k, double std_mul) {
                pcl::StatisticalOutlierRemoval<PointT> sor_filter;
                sor_filter.setInputCloud(input_pcl);
                sor_filter.setMeanK(mean_k);
                sor_filter.setStddevMulThresh(std_mul);
                sor_filter.filter(*output_pcl);
            }
            void FilterPCLFile(const std::string &pcd_file_name, int filter_mean_k,
                               double filter_std_mul, double filter_search_radius, int filter_min_neighbors_in_radius) {
                PointCloud::Ptr input_pcl = PointCloud::Ptr(new PointCloud());
                pcl::io::loadPLYFile<PointT>(pcd_file_name, *input_pcl);
                LOG(INFO) << "Filtering   with ror ... ";

                PointCloud::Ptr filtered_map_pcl = PointCloud::Ptr(new PointCloud());
                FilterPCLWithRor(input_pcl, filtered_map_pcl, filter_mean_k, filter_std_mul);

                LOG(INFO) << "Filtering   with sor ... ";
                input_pcl->clear();
                FilterPCLWithSor(filtered_map_pcl, input_pcl, filter_search_radius, filter_min_neighbors_in_radius);
                pcl::io::savePCDFileBinary<PointT>(pcd_file_name, *input_pcl);

                LOG(INFO) << "Filter OK. ";
            }

            constexpr static double kDownSamplingLeafSize = 0.08;

            void pairAlign(const PointCloud::Ptr cloud_src,
                           const PointCloud::Ptr cloud_tgt,
                           Eigen::Isometry3d &aligned_transform,
                           int iterations,
                           double max_correspondence_distance,
                           double euclidean_fitness_epsilon) {

                PointCloud::Ptr src(new PointCloud());
                PointCloud::Ptr tgt(new PointCloud());

                src = cloud_src;
                tgt = cloud_tgt;

                aligned_transform = Eigen::Isometry3d().Identity();

                pcl::IterativeClosestPoint<PointT, PointT, double> reg;
                reg.setMaxCorrespondenceDistance(max_correspondence_distance);
                // Set the maximum number of iterations (criterion 1)
                reg.setMaximumIterations(iterations);
                // Set the transformation epsilon (criterion 2)
                reg.setTransformationEpsilon(1e-6);
                // Set the euclidean distance difference epsilon (criterion 3)
                reg.setEuclideanFitnessEpsilon(euclidean_fitness_epsilon);
                reg.setInputSource(src);
                reg.setInputTarget(tgt);

                PointCloud Final;
                reg.align(Final);

                if(reg.hasConverged()) {
                    LOG(INFO) << "succeed to registrate.";

                    auto transform_mat = reg.getFinalTransformation().inverse();

                    aligned_transform.linear()      = transform_mat.block<3, 3>(0, 0);
                    aligned_transform.translation() = transform_mat.block<3, 1>(0, 3);

                    auto transform_step = std::abs((
                          reg.getLastIncrementalTransformation()).sum());

                    PCL_INFO ("transform step  %f.\n",  transform_step);
                }
            }

            ::cartographer::io::FileWriterFactory CreateFileWriterFactory(
                const std::string& file_path) {
                const auto file_writer_factory = [file_path](const std::string& filename) {
                    return absl::make_unique<carto::io::StreamFileWriter>(file_path + filename);
                };
                return file_writer_factory;
            }

            std::unique_ptr<carto::io::PointsProcessorPipelineBuilder> CreatePipelineBuilder(
                const std::vector<carto::mapping::proto::Trajectory>& trajectories,
                const std::string file_prefix) {

                const auto file_writer_factory = CreateFileWriterFactory(file_prefix);
                auto builder = absl::make_unique<carto::io::PointsProcessorPipelineBuilder>();

                carto::io::RegisterBuiltInPointsProcessors(trajectories,
                                                           file_writer_factory,
                                                           builder.get());
                builder->Register(RosMapWritingPointsProcessor::kConfigurationFileActionName,
                                  [file_writer_factory](
                                      carto::common::LuaParameterDictionary* const dictionary,
                                      carto::io::PointsProcessor* const next) ->
                                          std::unique_ptr<carto::io::PointsProcessor> {
                                              return RosMapWritingPointsProcessor::FromDictionary(
                                                  file_writer_factory, dictionary, next);
                                  });
                return builder;
            }

            std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(const std::string& configuration_directory,
                                                                                     const std::string& configuration_basename) {
                auto file_resolver = absl::make_unique<carto::common::ConfigurationFileResolver>(
                    std::vector<std::string>{configuration_directory});

                const std::string code = file_resolver->GetFileContentOrDie(configuration_basename);
                auto lua_parameter_dictionary = absl::make_unique<carto::common::LuaParameterDictionary>(code, std::move(file_resolver));

                return lua_parameter_dictionary;
            }

            Eigen::Isometry3d GetLinkTransform(const std::string& urdf_filename,
                                               const std::string& base_link,
                                               const std::string & ref_link) {
                tf2_ros::Buffer tf_buffer;
                if(!urdf_filename.empty()) ReadStaticTransformsFromUrdf(urdf_filename, &tf_buffer);

                ::ros::Time ros_time;
                const carto::transform::Rigid3d sensor_to_tracking = ToRigid3d(tf_buffer.lookupTransform(base_link,
                                                                                                         ref_link,
                                                                                                         ros_time));

                Eigen::Isometry3d tf_sensor_to_tracking;
                tf_sensor_to_tracking.linear()      = sensor_to_tracking.rotation().matrix();
                tf_sensor_to_tracking.translation() = sensor_to_tracking.translation();

                return tf_sensor_to_tracking;
            }

            std::tuple<cv::Vec3d, cv::Vec3d> GetPose(const Eigen::Isometry3d& transform_matrix) {
                Eigen::Quaterniond rotation(transform_matrix.linear());
                Eigen::Vector3d position = transform_matrix.translation();

                tf2::Quaternion q(rotation.x(), rotation.y(), rotation.z(), rotation.w());
                tf2::Matrix3x3 m(q);
                cv::Vec3d rpy;
                m.getRPY(rpy[0], rpy[1], rpy[2]);

                return std::make_tuple(cv::Vec3d(position[0], position[1], position[2]), rpy);
            }

            geometry_msgs::TransformStamped ToTransformStamped(int64_t timestamp_uts,
                                                               const std::string& parent_frame_id,
                                                               const std::string& child_frame_id,
                                                               const cartographer::transform::proto::Rigid3d& parent_T_child) {
                static int64_t seq = 0;

                geometry_msgs::TransformStamped transform_stamped;
                transform_stamped.header.seq      = ++seq;
                transform_stamped.header.frame_id = parent_frame_id;
                transform_stamped.header.stamp    = cartographer_ros::ToRos(::cartographer::common::FromUniversal(timestamp_uts));
                transform_stamped.child_frame_id  = child_frame_id;
                transform_stamped.transform       = cartographer_ros::ToGeometryMsgTransform(
                    ::cartographer::transform::ToRigid3(parent_T_child));

                return transform_stamped;
            }

            void Optimize(const std::string& pbstream_filename, const Eigen::Isometry3d& lidar_h_to_base,
                          const Eigen::Isometry3d& lidar_v_to_lidar_h, const Eigen::Isometry3d& cloud_v_to_cloud_h,
                          Eigen::Isometry3d& calibrate_transform) {

                static double translation_weight = 5;
                static double rotation_weight    = 10;

                ::cartographer::io::ProtoStreamReader reader(pbstream_filename);
                ::cartographer::io::ProtoStreamDeserializer deserializer(&reader);

                const auto& pose_graph = deserializer.pose_graph();
                Eigen::Isometry3d Twv  = Eigen::Isometry3d::Identity();

                ceres::Problem problem;
                CeresPose ceres_pose(transform::Rigid3d(), nullptr /* translation_parameterization */,
                      std::unique_ptr<ceres::LocalParameterization>(
                      absl::make_unique<ceres::QuaternionParameterization>()), &problem);

                for(const auto trajectory : pose_graph.trajectory()) {
                    //const auto child_frame_id = absl::StrCat("trajectory_", trajectory.trajectory_id());
                    std::stringstream child_frame_id;
                    child_frame_id << "trajectory_" << trajectory.trajectory_id();

                    LOG(INFO)
                        << "Writing tf and geometry_msgs/TransformStamped for trajectory id "
                        << trajectory.trajectory_id() << " with " << trajectory.node_size()
                        << " nodes.";

                    for(const auto& node : trajectory.node()) {
                        ::cartographer::transform::Rigid3d transform = ::cartographer::transform::ToRigid3(node.pose());
                        //std::cout << "time is: " << transform_stamped.header.stamp << std::endl;

                        Eigen::Quaterniond quaternion = transform.rotation();

                        Twv.linear()      = quaternion.matrix();
                        Twv.translation() = transform.translation();

                        Eigen::Isometry3d target_transform = cloud_v_to_cloud_h.inverse() * lidar_h_to_base.inverse() *
                                                             Twv.inverse() * cloud_v_to_cloud_h * Twv * lidar_h_to_base *
                                                             cloud_v_to_cloud_h;

                        Eigen::Vector3d target_translation = target_transform.translation();
                        problem.AddResidualBlock(TranslationDeltaCostFunctor3D::CreateAutoDiffCostFunction(
                            translation_weight, target_translation), nullptr /* loss function */, ceres_pose.translation());

                        CHECK_GT(translation_weight, 0.);
                        Eigen::Quaterniond target_rotation(target_transform.linear());
                        problem.AddResidualBlock(RotationDeltaCostFunctor3D::CreateAutoDiffCostFunction(
                            rotation_weight, target_rotation), nullptr /* loss function */, ceres_pose.rotation());
                    }

                    ceres::Solver::Summary summary;
                    ceres::Solver::Options ceres_solver_options_;
                    ceres_solver_options_.linear_solver_type     = ceres::DENSE_QR;
                    ceres_solver_options_.use_nonmonotonic_steps = true;
                    ceres_solver_options_.max_num_iterations     = 200;
                    ceres_solver_options_.num_threads            = 8;
                    ceres_solver_options_.function_tolerance     = 1e-10;
                    ceres_solver_options_.gradient_tolerance     = 1e-14;
                    ceres::Solve(ceres_solver_options_, &problem, &summary);

                    calibrate_transform.linear()      = ceres_pose.ToRigid().rotation().matrix();
                    calibrate_transform.translation() = ceres_pose.ToRigid().translation();

                    LOG(INFO) << "summary: " << summary.FullReport();
                }
            }

            template <typename T> std::unique_ptr<carto::io::PointsBatch> HandlePCLMessage(
                const T& message, const std::string& tracking_frame, const tf2_ros::Buffer& tf_buffer,
                const carto::transform::TransformInterpolationBuffer& transform_interpolation_buffer) {

                const carto::common::Time start_time = FromRos(message.header.stamp);

                auto points_batch = absl::make_unique<carto::io::PointsBatch>();
                points_batch->start_time = start_time;
                points_batch->frame_id   = message.header.frame_id;

                carto::sensor::PointCloudWithIntensities point_cloud;
                carto::common::Time point_cloud_time;
                std::tie(point_cloud, point_cloud_time) = ToPointCloudWithIntensities(message);
                CHECK_EQ(point_cloud.intensities.size(), point_cloud.points.size());

                for(size_t i = 0; i < point_cloud.points.size(); ++i) {
                    const carto::common::Time time = point_cloud_time + carto::common::FromSeconds(point_cloud.points[i].time);

                    if(!transform_interpolation_buffer.Has(time)) continue;

                    const carto::transform::Rigid3d tracking_to_map = transform_interpolation_buffer.Lookup(time);
                    const carto::transform::Rigid3d sensor1_to_tracking = ToRigid3d(tf_buffer.lookupTransform(
                        tracking_frame, message.header.frame_id, ToRos(time)));
                    const carto::transform::Rigid3f sensor_to_map = (tracking_to_map * sensor1_to_tracking).cast<float>();

                    points_batch->points.push_back(sensor_to_map * carto::sensor::ToRangefinderPoint(point_cloud.points[i]));
                    points_batch->intensities.push_back(point_cloud.intensities[i]);
                    // We use the last transform for the origin, which is approximately correct.
                    points_batch->origin = sensor_to_map * Eigen::Vector3f::Zero();
                }

                if(points_batch->points.empty()) return nullptr;

                return points_batch;
            }
        }

        CartoMapCalibrater::CartoMapCalibrater(){ }

        int CartoMapCalibrater::SetParam(const CalibrateParam & param) {
            parameter_ = param;
            return ERRCODE_OK;
        }

        void CartoMapCalibrater::LoadBuildOptions(const std::string& configuration_directory,
                                                  const std::string& configuration_basename) {

            const auto lua_parameter_dictionary = LoadLuaDictionary(configuration_directory, configuration_basename);

            const auto filter_options = lua_parameter_dictionary->GetDictionary("pcl_filter");
            filter_pcl                     = filter_options->GetBool("filter_pcl");
            filter_mean_k                  = filter_options->GetInt("mean_k");
            filter_std_mul                 = filter_options->GetDouble("std_mul");
            filter_search_radius           = filter_options->GetDouble("search_radius");
            filter_min_neighbors_in_radius = filter_options->GetInt("min_neighbors_in_radius");

            const auto registration_options = lua_parameter_dictionary->GetDictionary("registration");
            sampling_ratio              = registration_options->GetDouble("sampling_ratio");
            points_frame_ids            = registration_options->GetDictionary("points_frame_ids")->GetArrayValuesAsStrings();
            icp_iterations              = registration_options->GetInt("icp_iterations");
            max_correspondence_distance = registration_options->GetDouble("max_correspondence_distance");
            euclidean_fitness_epsilon   = registration_options->GetDouble("euclidean_fitness_epsilon");
        }

        std::vector<double> registrate_mat = { 0.999999821186,  0.000209449616, -0.000682806945, -0.005745916627,
                                              -0.000209187448,  1.000000000000,  0.000383999490,  0.000836494379,
                                               0.000682887330, -0.000383856503,  0.999999761581, -0.030569195747,
                                               0.000000000000,  0.000000000000,  0.000000000000,  1.000000000000 };

        int CartoMapCalibrater::CalibratePcls(const std::string& state_filepath,
                                              const std::vector<std::string>& pcl_filepaths,
                                              const std::string& save_directory) {

            PointCloud::Ptr pcl_h(new PointCloud());
            pcl::io::loadPCDFile<pcl::PointXYZ>(pcl_filepaths[0], *pcl_h);
            PointCloud::Ptr pcl_v(new PointCloud());
            pcl::io::loadPCDFile<pcl::PointXYZ>(pcl_filepaths[1], *pcl_v);

            Eigen::Isometry3d lidar_v_to_lidar_h = GetLinkTransform(parameter_.urdf_filename,
                                                                    points_frame_ids[0], points_frame_ids[1]);
            Eigen::Isometry3d lidar_h_to_base    = GetLinkTransform(parameter_.urdf_filename,
                                                                    "base_link", points_frame_ids[0]);

            Eigen::Map<Eigen::Matrix<double,4,4,Eigen::RowMajor> > map(&registrate_mat[0]);
            Eigen::Isometry3d calibrated_transform (map.matrix());

            Eigen::Isometry3d calibrate_v__to_h;
            Optimize(state_filepath, lidar_h_to_base, lidar_v_to_lidar_h, calibrated_transform, calibrate_v__to_h);

            Eigen::Isometry3d final_lidar_v_to_h = lidar_v_to_lidar_h * calibrate_v__to_h;

            cv::Vec3d translation, rpy;
            std::tie<cv::Vec3d, cv::Vec3d>(translation, rpy) = GetPose(final_lidar_v_to_h);
            Eigen::Quaterniond final_quaternion(final_lidar_v_to_h.linear());
            LOG(INFO) << "calibrate translation: " << std::endl << translation;
            LOG(INFO) << "calibrate rpy: " << std::endl << rpy;
            LOG(INFO) << "calibrate quaternion: wxyz:" << final_quaternion.w() << ", "
                      << final_quaternion.x() << ", " << final_quaternion.y() << ", "
                      << final_quaternion.z();

            std::string fileto = save_directory + "/" + "calibrated.yaml";
            cv::FileStorage yaml_file(fileto, cv::FileStorage::WRITE);
            yaml_file << "rpy" << rpy;
            yaml_file << "translation" << translation;

            return ERRCODE_OK;
        }

        /*
        std::vector<std::string> CartoMapCalibrater::GeneratePclFiles(const std::string& state_filepath,
                                                                      const std::vector<std::string>& bag_filepaths,
                                                                      const std::string& save_directory) {
            std::vector<std::string> generated_pcl_files;

            for(const auto & points_frame_id : points_frame_ids) {
                const std::string output_file_prefix = save_directory + "/" + points_frame_id;;
                GeneratePcd(state_filepath, bag_filepaths, points_frame_id, output_file_prefix);

                if(filter_pcl) {
                    FilterPCLFile(output_file_prefix + ".ply", filter_mean_k, filter_std_mul, filter_search_radius, filter_min_neighbors_in_radius);
                }
                generated_pcl_files.push_back(output_file_prefix + ".ply");
            }

            return generated_pcl_files;
        }
        */

        int CartoMapCalibrater::Calibrate(const std::string& state_filepath,
                                          const std::vector<std::string>& pcl_filepaths,
                                          const std::string& save_directory){
            LoadBuildOptions(parameter_.configuration_directory, parameter_.calibrate_configuration_basename);

            CalibratePcls(state_filepath, pcl_filepaths, save_directory);
            LOG(INFO) << "calibrate ok.";

            return ERRCODE_OK;
        }
    }
}
