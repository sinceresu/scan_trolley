#ifndef _MAP_CALIBRATE_CARTO_MAP_CALIBRATE_H
#define _MAP_CALIBRATE_CARTO_MAP_CALIBRATE_H

#include <vector>
#include <memory>

#include "pcl/point_types.h"
#include "include/map_calibrate_interface.h"

namespace yida_mapping {
    namespace map_calibrate {
        class CartoMapCalibrater : public MapCalibrateInterface {
            public:
                CartoMapCalibrater();
                ~CartoMapCalibrater() {};

                virtual int SetParam(const CalibrateParam & param) override;

                virtual int Calibrate(const std::string& state_filepath,
                                      const std::vector<std::string>& pcl_filepaths,
                                      const std::string& save_directory) override;
                virtual int CalibratePcls(const std::string& state_filepath,
                                          const std::vector<std::string>& pcl_filepaths,
                                          const std::string& save_directory) override;

            private:
                void LoadBuildOptions(const std::string& configuration_directory,
                                      const std::string& configuration_basename);
                void GeneratePcd(const std::string& state_filepath,
                                 const std::vector<std::string>& bag_filepaths_,
                                 const std::string & points_frame_id,
                                 const std::string & output_file_prefix);
                //std::vector<std::string> GeneratePclFiles(const std::string& state_filepath,
                //                                          const std::vector<std::string>& bag_filepaths ,
                //                                          const std::string& save_directory);

                //std::tuple<PointCloud::Ptr, PointCloud::Ptr> TransformPcls(const PointCloud::Ptr& base_pcl,
                //                                                           const PointCloud::Ptr& target_pcl);

            private:
                CalibrateParam parameter_;

                bool filter_pcl;       //if do point cloud  outlier filtering
                int filter_mean_k;     // min neibors in radius when filtering point cloud
                double filter_std_mul; // search ridius in meter when filtering point cloud
                double filter_search_radius;
                int filter_min_neighbors_in_radius;

                double sampling_ratio = 0.1;
                std::vector<std::string> points_frame_ids;
                int icp_iterations = 300;
                double max_correspondence_distance = 0.1;
                double euclidean_fitness_epsilon = 0.01;

        };
    } // namespace map_calibrate
} // namespace yida_mapping

#endif
